---
Тип: Задача
Описание: -
Исполнители: 
Проект: ВнедрениеЗнаний

Tags: Задача
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: 10 # Не трогать!
Месяц: 03 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-03-10

дата_завершения: "2023-нет-нет"

Выполнено: нет
Результат: 
---

# [[Простой паблишер-сабскрайбер ROS2(Python)]]

> [!example]- Чеклист знания
> - Получение источников и ссылок
> 	- [ ] Получение одной ссылки
> 	- [ ] Получение ссылок на разные источники
> - Конспектирование
> 	- [ ] Достаточное для использования конспектирование
> 	- [ ] Полное конспектирование
> - Создание справок
> 	- [ ] Справки практического применения
> 	- [ ] Список терминов
> 	- [ ] Определение терминов из списка терминов
> 	- [ ] Список связанных тем(если нет внутри текста)
> - Внедрение в базу знаний
> 	- [ ] Создание связей с оригинальной страницей (этой)
> 	- [ ] Разбиение на подтемы
> 	- [ ] Атомизация - (по необходимости) разбиение конспекта по подтемам на страницы
> - [ ] Внедрение в практику
> 

<mark style="background: #FF5582A6;">**Содержание потом вырезается, все содержимое конспекта удаляется и содержание вставляется по шаблону входящего конспекта.**</mark> 

# Содержание 

## Шаблон паблишера

```python
import rclpy
from rclpy.node import Node

from std_msgs.msg import String

class MinimalPublisher(Node):

    def __init__(self):
        super().__init__('minimal_publisher')
        self.publisher_ = self.create_publisher(String, 'topic', 10)
        timer_period = 0.5  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback)
        self.i = 0

    def timer_callback(self):
        msg = String()
        msg.data = 'Hello World: %d' % self.i
        self.publisher_.publish(msg)
        self.get_logger().info('Publishing: "%s"' % msg.data)
        self.i += 1

def main(args=None):
    rclpy.init(args=args)

    minimal_publisher = MinimalPublisher()

    rclpy.spin(minimal_publisher)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    minimal_publisher.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
```

### Настройка package.xml

Убедитесь, что эти поля заполнены:

```xml
<description>Examples of minimal publisher/subscriber using rclpy</description>
<maintainer email="you@email.com">Your Name</maintainer>
<license>Apache License 2.0</license>
```

Добавьте зависимости:

```xml
<exec_depend>rclpy</exec_depend>
<exec_depend>std_msgs</exec_depend>
```

### Настройка setup.py

Откройте файл setup.py. Снова сопоставьте поля maintainer, maintainer_email, description и license с вашим package.xml:

```python
maintainer='YourName',
maintainer_email='you@email.com',
description = "Примеры минимального Publisher/subscriber с использованием rclpy",
license='Apache License 2.0',
```

Добавьте следующую строку в скобках console_scripts поля entry_points:

```python
entry_points={
        'console_scripts': [
                'talker = py_pubsub.publisher_member_function:main',
        ],
},
```

Не забудьте сохранить.

### Настройка setup.cfg

Содержимое файла setup.cfg должно быть правильно заполнено автоматически, как показано ниже:

```cmake
[develop]
script-dir=$base/lib/py_pubsub
[install]
install-scripts=$base/lib/py_pubsub
```

## Шаблон сабскрайбера

```python
import rclpy
from rclpy.node import Node

from std_msgs.msg import String

class MinimalSubscriber(Node):

    def __init__(self):
        super().__init__('minimal_subscriber')
        self.subscription = self.create_subscription(
            String,
            'topic',
            self.listener_callback,
            10)
        self.subscription  # prevent unused variable warning

    def listener_callback(self, msg):
        self.get_logger().info('I heard: "%s"' % msg.data)

def main(args=None):
    rclpy.init(args=args)

    minimal_subscriber = MinimalSubscriber()

    rclpy.spin(minimal_subscriber)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    minimal_subscriber.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
```


### Настройка setup.py

Снова откройте setup.py и добавьте точку входа для узла подписчика ниже точки входа издателя. Поле entry_points теперь должно выглядеть следующим образом:

```python
entry_points={
        'console_scripts': [
                'talker = py_pubsub.publisher_member_function:main',
                'listener = py_pubsub.subscriber_member_function:main',
        ],
},
```

Не забудьте сохранить файл, и тогда ваша система pub/sub будет готова.

## Смотрите также:

> [!question]- Соурслист
> 1. https://docs.ros.org/en/foxy/Tutorials/Beginner-Client-Libraries/Writing-A-Simple-Py-Publisher-And-Subscriber.html

> [!quote]- Контекст
> ```dataview
> LIST 
> FROM !outgoing([[]])
> WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
> ```

---
#ВнедрениеЗнаний 
#ROS2 
#Python 

---

[^def]: термин
[^que]: вопрос