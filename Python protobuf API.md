---
Тип: Задача
Описание: -
Исполнители: 
Проект: ВнедрениеЗнаний

Tags: Задача
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: 10 # Не трогать!
Месяц: 03 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-03-10

дата_завершения: "2023-нет-нет"

Выполнено: нет
Результат: 
---

# [[Python protobuf API]]

> [!danger] Обязательный чеклист
> - [x] Получение одной ссылки
> - [x] Расставить теги 


> [!example]- Чеклист знания
> - Получение источников и ссылок
> 	- [ ] Получение ссылок на разные источники
> - Конспектирование
> 	- [x] Достаточное для использования конспектирование
> 	- [ ] Полное конспектирование
> - Создание справок
> 	- [ ] Справки практического применения
> 	- [ ] Список терминов
> 	- [ ] Определение терминов из списка терминов
> 	- [ ] Список связанных тем(если нет внутри текста)
> - Внедрение в базу знаний
> 	- [x] Создание связей с оригинальной страницей (этой)
> 	- [ ] Разбиение на подтемы
> 	- [ ] Атомизация - (по необходимости) разбиение конспекта по подтемам на страницы
> - [ ] Внедрение в практику
> 

<mark style="background: #FF5582A6;">**Содержание потом вырезается, все содержимое конспекта удаляется и содержание вставляется по шаблону входящего конспекта.**</mark> 

# Содержание 

```python
import addressbook_pb2 # Импорт сгенерированного сообщения
person = addressbook_pb2.Person()
person.id = 1234
person.name = "John Doe"
person.email = "jdoe@example.com"
phone = person.phones.add()
phone.number = "555-4321"
phone.type = addressbook_pb2.Person.HOME
```

Note that these assignments are not just adding arbitrary new fields to a generic Python object. If you were to try to assign a field that isn’t defined in the `.proto` file, an `AttributeError` would be raised. If you assign a field to a value of the wrong type, a `TypeError` will be raised. Also, reading the value of a field before it has been set returns the default value.

```python
person.no_such_field = 1  # raises AttributeError
person.id = "1234"        # raises TypeError
```

For more information on exactly what members the protocol compiler generates for any particular field definition, see the [Python generated code reference](https://protobuf.dev/reference/python/python-generated).

## [Standard Message Methods](https://protobuf.dev/getting-started/pythontutorial/#standard-message-methods)

-   `IsInitialized()`: checks if all the required fields have been set.
-   `__str__()`: returns a human-readable representation of the message, particularly useful for debugging. (Usually invoked as `str(message)` or `print message`.)
-   `CopyFrom(other_msg)`: overwrites the message with the given message’s values.
-   `Clear()`: clears all the elements back to the empty state.

## [Parsing and Serialisation](https://protobuf.dev/getting-started/pythontutorial/#parsing-serialization)

-   `SerializeToString()`: serializes the message and returns it as a string. Note that the bytes are binary, not text; we only use the `str` type as a convenient container.
-   `ParseFromString(data)`: parses a message from the given string.

## Смотрите также:

> [!question]- Соурслист
> 1. https://protobuf.dev/getting-started/pythontutorial/#protobuf-api

> [!quote]- Контекст
> ```dataview
> LIST 
> FROM !outgoing([[]])
> WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
> ```

---
#ВнедрениеЗнаний/НеобходимоеЗнание  
#Python 
#protobuf 

---

[^def]: термин
[^que]: вопрос