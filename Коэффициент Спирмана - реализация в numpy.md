---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 22 # Не трогать!
Месяц: 11 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-11-22

---
# [[Коэффициент Спирмана - реализация в numpy]]


**Коэффициент Спирмана (Spearman coefficient)** - непараметрическая мера корреляции рангов (статистическая зависимость между рангами двух переменных). Она показывает то, насколько хорошо зависимость между двумя переменными может быть описана [[Монотонная функция|монотонной функцией]] (независимо от того, является ли характер зависимости линейным или нет).

Коэффициент Спирмана двух переменных равен [[numpy.corrcoef - коэффициент линейной корреляции двух переменных|корреляции Пирсона]] между значениями рангов этих двух переменных.  

Ранг переменной (для переменной, представленной в виде ряда значений) - это относительная мера, показывающая, на каком месте по значению находится значение переменной. Ранг переменной - целое число от 1 до $+ \infty$.

Например, для ряда `[1, 5, 4]` ряд рангов - `[3, 1, 2]`. Т.е, чем больше значение, тем ниже значение ранга. 

> [!info]- ##### Пример получения рангов ряда значений переменной `x` в numpy
>```python
>import numpy as np
>def rank(arr: np.ndarray):
>    arr_ranks = arr.argsort()[::-1].argsort() + 1
>    return arr_ranks
>
># Генерация нескольких рандомных значений
>x = (np.random.rand(100) * 10).tolist()
># Вставка заведомо больших значений для демонстрации порядка ранжирования
>x.append(25)
>x.append(50)
>x.append(100)
>
># Перевод в numpy.ndarray с типом int
>x = np.array(x)
>x = np.array(list(map(lambda a: int(np.round(a)), x.tolist())))
>
>x_ranks = rank(x)
>
>print(f"{x}\n{x_ranks}")
>plt.plot(np.arange(len(x)), x)
>plt.scatter(x_ranks, x, c='red', s=5)
>####### 
>Output:
>[  4   2   9   1   7   6   4   4   3   9   2   6   7   7  10   9   3   9
>   1  10   0   9   3   6   4   4   1   6   4   1   7   4   9   2   4   0
>   8   3   3   2   8   3   8   5   4   8   2   6   4   6   9  10   5   5
>   1   1   8   3  10   7   8   6   4   8   2   5   1   4   7   2   2   9
>   5   8   4   4   7  10   4   9   2   6   1   8   6   7   8   8   4   7
>   3   1   0   1   4   3   2   7  10   4  25  50 100]
>[ 63  83  12 100  38  40  54  57  73  11  85  46  37  35   5  17  75  13
>  95   6 103  16  76  47  60  59  94  43  61  91  31  56  14  87  58 101
>  28  80  79  89  20  77  23  50  66  27  86  44  69  42  15   8  49  52
>  92  96  29  78   7  30  22  48  64  21  90  53  98  71  33  88  82  10
>  51  19  70  68  36   4  67  18  84  45  97  24  41  34  25  26  65  39
>  72  99 102  93  62  74  81  32   9  55   3   2   1]
>```
>![[Pasted image 20231122122514.png]]

## Пример реализации на Python

```python
import numpy as np
import matplotlib.pyplot as plt

def rank(arr: np.ndarray):
    arr_ranks = arr.argsort()[::-1].argsort() + 1
    return arr_ranks

def spearman(x, y):
    assert len(x) == len(y) and len(x.shape) == 1 and len(y.shape) == 1
    x_rank = rank(x)
    y_rank = rank(y)
    
    vars_ = np.vstack((x_rank, y_rank))
    
    cov_rx_ry = np.cov(vars_)[0, 1]
    
    mean_x = np.std(x_rank)
    mean_y = np.std(y_rank)
    
    pearson_c = cov_rx_ry / (mean_x * mean_y)
    return pearson_c

x = np.arange(-100, 100)
y = x ** 3

y = np.array(list(map(lambda a: a + np.random.randint(-80, 80) ** 3, y.tolist())))

plt.plot(x, y)

print(spearman(x, y))
plt.show()

######################
# Output:
0.8015310935537208
```
![[Pasted image 20231122140718.png]]

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Python/NumPy 

---

[^def]: термин
[^que]: вопрос