---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 19 # Не трогать!
Месяц: 07 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-07-19

---
# [[Исследование влияния засветов на цвета в пространстве HSV - OpenCV]]

[[ColorChangeVisualization.nb]]

## Зеленый цвет

### Засвет

> [!info] В засвете
> - Нижний диапазон:
> 	- Hue: 87
> 	- Saturation: 67
> 	- Value: 112
> - Верхний диапазон:
> 	- Hue: 107
> 	- Saturation: 142
> 	- Value: 166

![[Pasted image 20230719180032.png]]
![[Pasted image 20230719180045.png]]

### Без засвета


- Нижний диапазон:
	- Hue: 74
	- Saturation: 75
	- Value: 93
- Верхний диапазон:
	- Hue: 95
	- Saturation: 246
	- Value: 253


![[Pasted image 20230719180555.png]]
![[Pasted image 20230719180605.png]]

## Красный 

### С засветом
- Нижний диапазон:
	- Hue: 128
	- Saturation: 38
	- Value: 131
- Верхний диапазон:
	- Hue: 164
	- Saturation: 76
	- Value: 197

![[Pasted image 20230719181520.png]]
![[Pasted image 20230719181534.png]]

### Без засвета

- Нижний диапазон:
	- Hue: 162
	- Saturation: 89
	- Value: 52
- Верхний диапазон:
	- Hue: 174
	- Saturation: 192
	- Value: 160


![[Pasted image 20230719181221.png]]
![[Pasted image 20230719181202.png]]

## Синий

### С засветом

- Нижний диапазон:
	- Hue: 102
	- Saturation: 97
	- Value: 125
- Верхний диапазон:
	- Hue: 117
	- Saturation: 165
	- Value: 226


![[Pasted image 20230719182223.png]]
![[Pasted image 20230719182023.png]]

### Без засвета

- Нижний диапазон:
	- Hue: 105
	- Saturation: 143
	- Value: 81
- Верхний диапазон:
	- Hue: 119
	- Saturation: 203
	- Value: 146

![[Pasted image 20230719181904.png]]
![[Pasted image 20230719181815.png]]

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```


---
#КомпьютерноеЗрение/opencv
#Автор-я

---

[^def]: термин
[^que]: вопрос