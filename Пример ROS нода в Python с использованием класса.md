---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 03 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-10-03

---
# [[Пример ROS нода в Python с использованием класса]]


```python
import cv2
import rospy
from cv_bridge import CvBridge
from sensor_msgs.msg import Image
from drone_msgs.msg import GraphicalCodes, GraphicalCode
from geometry_msgs.msg import PoseStamped, Quaternion

class QRDetectionNode:
    def __init__(self):
        self.bridge = CvBridge()
        self.current_pose = None
        self.front_img_sub = rospy.Subscriber('/r200/image_raw', Image, self._front_img_cb)
        self.down_img_sub = rospy.Subscriber('/iris_rplidar/usb_cam/image_raw', Image, self._down_img_cb)
        self.pose_sub = rospy.Subscriber('/mavros/local_position/pose', PoseStamped, self._pose_cb)
        self.rate = rospy.Rate(10)


    def _front_img_cb(self, img):
        cv_image = None
        try:
            cv_image = self.bridge.imgmsg_to_cv2(img, desired_encoding="bgr8")
            self.find_qrs_in_image(cv_image)
        except Exception as e:
            print(f"Caught exception: {e}")
            print("Trying to decode image when there is none")
        print("\n\nFront image")


    def _down_img_cb(self, img):
        cv_image = None
        try:
            cv_image = self.bridge.imgmsg_to_cv2(img, desired_encoding="bgr8")
            self.find_qrs_in_image(cv_image)
        except Exception as e:
            print(f"Caught exception: {e}")
            print("Trying to decode image when there is none")
        print("\n\nBottom image")


    def _pose_cb(self, pose_in):
        self.current_pose = pose_in



    def find_qrs_in_image(self, frame):
        """
        Brief:
            Эта функция отвечает за логику определения кодов и их координат.
            Далее она заполняет необходимоее сообщение и отправляет его в топик

        Args:
            frame (np.ndarray): Картинка формата OpenCV
        """
        assert frame.shape[2] == 3
        
        frame2 = frame.copy()
        codes_msg = GraphicalCodes()
        points = []

        qr_detector = cv2.QRCodeDetector()
        retval, info, points, _ = qr_detector.detectAndDecodeMulti(frame)

        if points is not None:
            for i in range(len(points)):
                cv2.line(frame, tuple(map(lambda x: int(x), points[i][0])), tuple(map(lambda x: int(x), points[i][1])), (0, 0, 255), 5)
                cv2.line(frame, tuple(map(lambda x: int(x), points[i][1])), tuple(map(lambda x: int(x), points[i][2])), (0, 0, 255), 5)
                cv2.line(frame, tuple(map(lambda x: int(x), points[i][2])), tuple(map(lambda x: int(x), points[i][3])), (0, 0, 255), 5)
                cv2.line(frame, tuple(map(lambda x: int(x), points[i][3])), tuple(map(lambda x: int(x), points[i][0])), (0, 0, 255), 5)
            
            print(f"Current_pose.z: {self.current_pose.pose.position.z}")

            for i in range(len(info)):
                code_msg = GraphicalCode()
                code_msg.data = info[i]
                # code_msg.positio

            print(f"info: {info}")
            print(f"points.size: {len(points)}")


    def detector(self):
        while not rospy.is_shutdown():
            # print("Running...")
            # find_qrs_in_image(bridge.imgmsg_to_cv2(current_front_img, desired_encoding="bgr8"))
            # find_qrs_in_image(bridge.imgmsg_to_cv2(current_down_img, desired_encoding="bgr8"))
            if( cv2.waitKey(5) >= 0 ):
                break # stop capturing by pressing ESC 
            self.rate.sleep()








if __name__ == '__main__':
    rospy.init_node('qr_detector', anonymous=True)
    try:
        detector = QRDetectionNode()
        detector.detector()
    except rospy.ROSInterruptException:
        pass
```


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```


---
#Python #ROS 

---

[^def]: термин
[^que]: вопрос