---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 03 # Не трогать!
Месяц: 02 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-02-03

---
# [[Типы данных в Python]]

Python использует динамическую типизацию, однако с 3 питона возможна статическая типизация.

Основные четыре типа данных в питоне это `int`, `float`, `string` и `bool`.

```Python
# Data types
# Int
-5456
4546465

# Float

23345.0
-5.45

# String
'hello' # Между ' ' и " " нет разницы, но если вы хотите добавить эти символы в строку
"Hello" # то для границ строки лучше использовать те ковычки, которые в строку не войдут

# Bool
True
False 

# Complex nums
a = complex(5, 2) # 5 real, 2 imaginary
b = complex('5+2j') # WITHOUT SPACES

a == b # True


```

Так же существует тип [[None - эквивалент null в Python|none]].

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Python 

---

[^def]: термин
[^que]: вопрос