---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 22 # Не трогать!
Месяц: 11 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-11-22

---
# [[Поиск коэффициентов полинома методом наименьших квадратов - numpy.polyfit()]]

## Пример

```python
import numpy as np
import matplotlib.pyplot as plt

x = np.arange(-1000, 1000)
y = x**3 * 5 + np.tan(x) ** 2 * (-12) + np.sin(x) * 8 - 7 

plt.plot(x, y)

poly = np.polyfit(x, y, deg=3)
print(poly)
y = x ** 3 * poly[0] + x ** 2 * poly[1] + x * poly[2] + poly[3]

plt.scatter(x, y, c='red', s=3)
###############
# Output
[ 4.99999999e+00 3.12640443e-03 6.41516278e-03 -4.86195465e+03]
```

![[Pasted image 20231122152000.png]]

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Python/NumPy 

---

[^def]: термин
[^que]: вопрос