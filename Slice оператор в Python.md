---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 03 # Не трогать!
Месяц: 02 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-02-03

---
# [[Slice оператор в Python]]


```Python
x = [1, 2, 3, 4, 5, 6, 7, 8, 9]

start = 0
stop = 4
step = 2

sliced = x[start:stop:step]

``` 

In Python, `X[:10]` is a slicing operation that returns the first 10 elements of the iterable `X`.

Slicing is a way to extract a subset of elements from an iterable like a string or a list. The colon (`:`) operator is used to specify the start and end indices of the slice. When the start index is not specified, it defaults to 0, and when the end index is not specified, it defaults to the length of the iterable.

So, in the case of `X[:10]`, it means "give me the first 10 elements of X." If `X` is a list or a string, it will return a new list or string containing the first 10 elements of `X`. If `X` is a generator or another iterable that doesn't support efficient random access, it will iterate over the first 10 elements of `X`.

```python
X = [[0.0000],
    [0.0200],
    [0.0400],
    [0.0600],
    [0.0800],
    [0.1000],
    [0.1200],
    [0.1400],
    [0.1600],
    [0.1800]]

print(X[:10])

---
[[0.0], [0.02], [0.04], [0.06], [0.08], [0.1], [0.12], [0.14], [0.16], [0.18]]
```

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Python  

---

[^def]: термин
[^que]: вопрос