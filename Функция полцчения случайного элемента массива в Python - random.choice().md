---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 09 # Не трогать!
Месяц: 09 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-09-09

---
# [[Функция полцчения случайного элемента массива в Python - random.choice()]]


```python
# 2. Get random image path
random_image_path = random.choice(image_path_list)

print(f"Random image path: {random_image_path}")

# Output
Random image path: data/pizza_steak_sushi/train/steak/1647351.jpg
```


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. [[04_pytorch_custom_datasets.ipynb]]


---
#Python 

---

[^def]: термин
[^que]: вопрос