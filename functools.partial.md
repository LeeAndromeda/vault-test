---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 09 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-10-09

---
# [[functools.partial]]


```python
from functools import partial

def say_congrat(name, phrase):
    print(f"{name}, {phrase}!")
    
    
# def say_hello(name):
#     say_congrat(name, "hello")
    
# def say_hi(name):
#     say_congrat(name, "hi")


...

# # Первый вариа(нт укорачивание кода
# say_hello = lambda name: say_congrat(name, 'hello')
# say_hi = lambda name: say_congrat(name, 'hi')

# Вариант с partial
say_hello = partial(say_congrat, phrase='hello')
say_hi = partial(say_congrat, phrase='hi')

say_hello("Dmitry")
say_hello("Sergey")

say_hi('Olga')
say_hi('Oleg')

```


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Python 

---

[^def]: термин
[^que]: вопрос