---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 19 # Не трогать!
Месяц: 11 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-11-19

---
# [[Пользовательские исключения в Python]]

> [!info]+ Примечание
> 
>Когда мы разрабатываем большую программу на языке Python, хорошей практикой является размещение всех определяемых пользователем исключений, которые вызывает наша программа, в отдельном файле.
>
>Многие стандартные модули определяют свои исключения отдельно в виде exceptions.py или errors.py (обычно, но не всегда).

Объявление искоючений происходит следующим образом:

```python
# define Python user-defined exceptions
class InvalidAgeException(Exception):
    "Raised when the input value is less than 18"
    pass
```

## Кастомизация исключений

```python
class SalaryNotInRangeError(Exception):
    """Exception raised for errors in the input salary.

    Attributes:
        salary -- input salary which caused the error
        message -- explanation of the error
    """

    def __init__(self, salary, message="Salary is not in (5000, 15000) range"):
        self.salary = salary
        self.message = message
        super().__init__(self.message)


salary = int(input("Enter salary amount: "))
if not 5000 < salary < 15000:
    raise SalaryNotInRangeError(salary)
```

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://www.programiz.com/python-programming/user-defined-exception


---
#Python 

---

[^def]: термин
[^que]: вопрос