---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 19 # Не трогать!
Месяц: 07 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-07-19

---
# [[HSV диапазоны цветов]]

## Засвет

- Нижний диапазон:
	- Hue: 0
	- Saturation: 0
	- Value: 228
- Верхний диапазон:
	- Hue: 77
	- Saturation: 13
	- Value: 255


## Красный 

- Lows:
	- Hue: 161
	- Saturation: 163
	- Value: 37
- Highs:
	- Hue: 180
	- Saturation: 255
	- Value: 255

## Зеленый

- Нижний диапазон:
	- Hue: 68
	- Saturation: 150
	- Value: 0
- Верхний диапазон:
	- Hue: 90
	- Saturation: 255
	- Value: 255

- Нижний диапазон:
	- Hue: 67
	- Saturation: 69
	- Value: 0
- Верхний диапазон:
	- Hue: 99
	- Saturation: 255
	- Value: 255



## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```


---
#КомпьютерноеЗрение/opencv 
#Автор-я

---

[^def]: термин
[^que]: вопрос