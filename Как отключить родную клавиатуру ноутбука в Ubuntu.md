---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 17 # Не трогать!
Месяц: 05 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-05-17

---
# [[Как отключить родную клавиатуру ноутбука в Ubuntu]]


1.  Execute the command xinput list to list your input devices.
2.  Locate AT Translated Set 2 keyboard and take note of its id number; this will be used to disable the keyboard. ...
3.  To disable the keyboard, execute the command xinput float <id#> , where <id#> is your keyboard's id number.

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://askubuntu.com/questions/160945/is-there-a-way-to-disable-a-laptops-internal-keyboard


---
#Linux 

---

[^def]: термин
[^que]: вопрос