---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 17 # Не трогать!
Месяц: 07 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-07-17

---
# [[Итерирование через cv Mat]]



## Эффективный способ

When it comes to performance you cannot beat the classic C style `operator[]` (pointer) access. Therefore, the most efficient method we can recommend for making the assignment is:
```cpp
Mat& ScanImageAndReduceC(Mat& I, const uchar* const table)
{
 // accept only char type matrices
 CV_Assert(I.depth() == CV_8U);
 int channels = I.channels();
 int nRows = I.rows;
 int nCols = I.cols * channels;
 if (I.isContinuous())
 {
	 nCols *= nRows;
	 nRows = 1;
 }
 int i,j;
 uchar* p;
 for( i = 0; i < nRows; ++i)
 {
	 p = I.ptr<uchar>(i);
	 for ( j = 0; j < nCols; ++j)
	 {
		 p[j] = table[p[j]];
	 }
 }
 return I;
}
```

Here we basically just acquire a pointer to the start of each row and go through it until it ends. In the special case that the matrix is stored in a continuous manner we only need to request the pointer a single time and go all the way to the end. We need to look out for color images: we have three channels so we need to pass through three times more items in each row.

> [!info]
> В этом методе каждая подколонна считывается в цикле, как отдельная колонна. Это можно заметить в инициализации переменной `nCols` 


## Метод итераторов (безопасный)

In case of the efficient way making sure that you pass through the right amount of uchar fields and to skip the gaps that may occur between the rows was your responsibility. The iterator method is considered a safer way as it takes over these tasks from the user. All you need to do is to ask the begin and the end of the image matrix and then just increase the begin iterator until you reach the end. To acquire the value pointed by the iterator use the * operator (add it before it).

```cpp
Mat& ScanImageAndReduceIterator(Mat& I, const uchar* const table)
{
 // accept only char type matrices
 CV_Assert(I.depth() == CV_8U);
 const int channels = I.channels();
 
 switch(channels)
 {
	 case 1:
	 {
		 MatIterator_<uchar> it, end;
		 for( it = I.begin<uchar>(), end = I.end<uchar>(); it != end; ++it)
		 *it = table[*it];
		 break;
	 }
	 case 3:
	 {
		 MatIterator_<Vec3b> it, end;
		 for( it = I.begin<Vec3b>(), end = I.end<Vec3b>(); it != end; ++it)
		 {
			 (*it)[0] = table[(*it)[0]];
			 (*it)[1] = table[(*it)[1]];
			 (*it)[2] = table[(*it)[2]];
		 }
	 }
 }
 return I;
}
```

> [!info]
> В этом методе каждая подколонна воспринимается в цикле, как третье измерение.

## Обращение к конкретному элементу

The final method isn't recommended for scanning. It was made to acquire or modify somehow random elements in the image. Its basic usage is to specify the row and column number of the item you want to access. During our earlier scanning methods you could already notice that it is important through what type we are looking at the image. It's no different here as you need to manually specify what type to use at the automatic lookup. You can observe this in case of the grayscale images for the following source code (the usage of the + cv::Mat::at() function):

```cpp
Mat& ScanImageAndReduceRandomAccess(Mat& I, const uchar* const table)
{
 // accept only char type matrices
 CV_Assert(I.depth() == CV_8U);
 const int channels = I.channels();
 switch(channels)
 {
	 case 1:
	 {
		 for( int i = 0; i < I.rows; ++i)
		 for( int j = 0; j < I.cols; ++j )
		 I.at<uchar>(i,j) = table[I.at<uchar>(i,j)];
		 break;
	 }
	 case 3:
	 {
		 Mat_<Vec3b> _I = I;
		 for( int i = 0; i < I.rows; ++i)
			 for( int j = 0; j < I.cols; ++j )
			 {
				 _I(i,j)[0] = table[_I(i,j)[0]];
				 _I(i,j)[1] = table[_I(i,j)[1]];
				 _I(i,j)[2] = table[_I(i,j)[2]];
			 }
		 I = _I;
		 break;
	 }
 }
 return I;
}
```

The function takes your input type and coordinates and calculates the address of the queried item. Then returns a reference to that. This may be a constant when you get the value and non-constant when you set the value. As a safety step in debug mode only* there is a check performed that your input coordinates are valid and do exist. If this isn't the case you'll get a nice output message of this on the standard error output stream. Compared to the efficient way in release mode the only difference in using this is that for every element of the image you'll get a new row pointer for what we use the C operator[] to acquire the column element.

If you need to do multiple lookups using this method for an image it may be troublesome and time consuming to enter the type and the at keyword for each of the accesses. To solve this problem OpenCV has a cv::Mat_ data type. It's the same as Mat with the extra need that at definition you need to specify the data type through what to look at the data matrix, however in return you can use the operator() for fast access of items. To make things even better this is easily convertible from and to the usual cv::Mat data type. A sample usage of this you can see in case of the color images of the function above. Nevertheless, it's important to note that the same operation (with the same runtime speed) could have been done with the cv::Mat::at function. It's just a less to write for the lazy programmer trick.

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://docs.opencv.org/4.x/db/da5/tutorial_how_to_scan_images.html


---
#КомпьютерноеЗрение/opencv 
#cpp

---

[^def]: термин
[^que]: вопрос