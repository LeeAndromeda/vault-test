---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 02 # Не трогать!
Месяц: 11 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-11-02

---
# [[Шаблон программы на Pygame]]


```python
import pygame as pg

width = 1280
height = 720


pg.init()

screen = pg.display.set_mode((width, height))
pg.display.set_caption('My game')

running = True
while running:
    
    for event in pg.event.get():
        if event.type == pg.QUIT:
            running = False
    
    pg.display.update()
```

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#python/pygame 

---

[^def]: термин
[^que]: вопрос