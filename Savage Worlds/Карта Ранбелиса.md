```leaflet
id: Ranbelis
image: [[a7df2537-c121-400c-8202-c0d08ba17e22.png]]
height: 500px
lat: 50
long: 50
minZoom: 1
maxZoom: 10
defaultZoom: 5
unit: meters
scale: 1
darkMode: false
```

![[a7df2537-c121-400c-8202-c0d08ba17e22.png]]