---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 28 # Не трогать!
Месяц: 04 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-04-28

---
# [[np.item() и np.itemset()]]



> [!def]+
> `np.array.item(vec3)` - вызывается через объект изображения
> 
> **аргументы:**
> 	1. позиция цвета в пикселе : vec3 : tuple - (первые два - координаты x, y в картинке, третья координата - канал цвета)
> 
> **возвращает:** значение найденного цвета

> [!def]+
> `np.array.itemset(vec3, val)` - вызывается через объект изображения для установки значения определенного элемента этого изображения.
> 
> **аргументы:**
> 	1. позиция цвета в пикселе : vec3 : tuple - (первые два - координаты x, y в картинке, третья координата - канал цвета)
> 	2. Значение, которое функция устанавливает на выбранной позиции объекта
> 
> **возвращает:** объект

В этом примере мы меняем значение голубого канала пикселя в позиции (150, 120) с его изначального значения на 255:

```python
import cv2  
img = cv2.imread('MyPic.png')  
img.itemset((150, 120, 0), 255) # Sets the value of a pixel's blue channel  
print(img.item(150, 120, 0)) # Prints the value of a pixel's blue channel
```

> [!summary] Вывод
> > [!success] Плюсы
> > Методы `item()` и `itemset()` хороши для работы с маленькими зонами изображения
> 
> > [!fail] Минусы
> > Для объемных операций в изображении лучше использовать или методы OpenCV, или [[Слайсинг массивов NumPy]]
> 





## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. [[LOCV4 Howse & Minichino.canvas]]


---
#КомпьютерноеЗрение/opencv 

---

[^def]: термин
[^que]: вопрос