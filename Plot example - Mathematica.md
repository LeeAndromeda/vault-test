> [!bg-plate]
> 
Plot a function:
> 
In[1]:=
> 
![](https://reference.wolfram.com/language/ref/Files/Plot.en/I_1.png)
> 
`Plot[Sin[x], {x, 0, 6 Pi}]`
> 
`https://wolfram.com/xid/0obfm-fu8`
> 
Out[1]=
> 
![](https://reference.wolfram.com/language/ref/Files/Plot.en/O_1.png)
> 
Plot several functions with a legend:
> 
In[1]:=
> 
![](https://reference.wolfram.com/language/ref/Files/Plot.en/I_2.png)
> 
`Plot[{Sin[x], Sin[2 x], Sin[3 x]}, {x, 0, 2 Pi}, PlotLegends -> "Expressions"]`
> 
`https://wolfram.com/xid/0obfm-kl2`
> 
Out[1]=
> 
![](https://reference.wolfram.com/language/ref/Files/Plot.en/O_2.png)
> 
Label each curve:
> 
In[1]:=
> 
![](https://reference.wolfram.com/language/ref/Files/Plot.en/I_3.png)
> 
`Plot[{Sin[x], Cos[x]}, {x, 0, 2 Pi}, PlotLabels -> "Expressions"]`
> 
`https://wolfram.com/xid/0obfm-54akdv`
> 
Out[1]=
> 
![](https://reference.wolfram.com/language/ref/Files/Plot.en/O_3.png)
> 
Fill below a curve:
> 
In[1]:=
> 
![](https://reference.wolfram.com/language/ref/Files/Plot.en/I_4.png)
> 
`Plot[2 Sin[x] + x, {x, 0, 15}, Filling -> Bottom]`
> 
`https://wolfram.com/xid/0obfm-pxwrd7`
> 
Out[1]=
> 
![](https://reference.wolfram.com/language/ref/Files/Plot.en/O_4.png)
> 
Fill between two curves:
> 
In[2]:=
> 
![](https://reference.wolfram.com/language/ref/Files/Plot.en/I_5.png)
> 
`Plot[{Sin[x] + x/2, Sin[x] + x}, {x, 0, 10}, Filling -> {1 -> {2}}]`
> 
`https://wolfram.com/xid/0obfm-homxwa`
> 
Out[2]=
> 
![](https://reference.wolfram.com/language/ref/Files/Plot.en/O_5.png)
> 
Plot multiple filled curves, automatically using transparent colors:
> 
In[1]:=
> 
![](https://reference.wolfram.com/language/ref/Files/Plot.en/I_6.png)
> 
`Plot[Evaluate[Table[BesselJ[n, x], {n, 4}]], {x, 0, 10}, Filling -> Axis]`
> 
`https://wolfram.com/xid/0obfm-eyldpo`
> 
Out[1]=
> 
![](https://reference.wolfram.com/language/ref/Files/Plot.en/O_6.png)


#Mathematica 