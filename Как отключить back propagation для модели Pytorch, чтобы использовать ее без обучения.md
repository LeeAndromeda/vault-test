---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 20 # Не трогать!
Месяц: 06 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-06-20

---
# [[Как отключить back propagation для модели Pytorch, чтобы использовать ее без обучения]]


## Disabling Gradient Tracking[](https://pytorch.org/tutorials/beginner/basics/autogradqs_tutorial.html#disabling-gradient-tracking)

By default, all tensors with `requires_grad=True` are tracking their computational history and support gradient computation. However, there are some cases when we do not need to do that, for example, when we have trained the model and just want to apply it to some input data, i.e. we only want to do _forward_ computations through the network. We can stop tracking computations by surrounding our computation code with `torch.inference_mode()` block:

```python
z = torch.matmul(x, w)+b
print(z.requires_grad)

with torch.inference_mode():
    z = torch.matmul(x, w)+b
print(z.requires_grad)

True
False
```

Another way to achieve the same result is to use the `detach()` method on the tensor:

```python
z = torch.matmul(x, w)+b
z_det = z.detach()
print(z_det.requires_grad)

False
```

### There are reasons you might want to disable gradient tracking:

- To mark some parameters in your neural network as **frozen parameters**.
- To **speed up computations** when you are only doing forward pass, because computations on tensors that do not track gradients would be more efficient.


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#pytorch 

---

[^def]: термин
[^que]: вопрос