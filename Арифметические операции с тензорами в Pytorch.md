---

Tags: 
aliases: [pytorch dot product, матричное произведение - torch.matmul() torch.mm(), mean, sum, max, min, транспонирование матриц - torch.transpose() tensor.T]
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 13 # Не трогать!
Месяц: 06 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-06-13

---
# [[Арифметические операции с тензорами в Pytorch]]


## Basic operations
Let's start with a few of the fundamental operations, addition (+), subtraction (-), mutliplication (*).

They work just as you think they would.

```python
# Create a tensor of values and add a number to it
tensor = torch.tensor([1, 2, 3])
tensor + 10
tensor([11, 12, 13])
# Multiply it by 10
tensor * 10
tensor([10, 20, 30])
```

Notice how the tensor values above didn't end up being tensor([110, 120, 130]), this is because the values inside the tensor don't change unless they're reassigned.

> # Tensors don't change unless reassigned

tensor
tensor([1, 2, 3])

Let's subtract a number and this time we'll reassign the tensor variable.

```python
# Subtract and reassign
tensor = tensor - 10
tensor
tensor([-9, -8, -7])
# Add and reassign
tensor = tensor + 10
tensor
tensor([1, 2, 3])
```

PyTorch also has a bunch of built-in functions like torch.mul() (short for multiplication) and torch.add() to perform basic operations.

```python
# Can also use torch functions
torch.multiply(tensor, 10)
tensor([10, 20, 30])
# Original tensor is still unchanged 
tensor
tensor([1, 2, 3])
```

However, it's more common to use the operator symbols like * instead of torch.mul()

```python
# Element-wise multiplication (each element multiplies its equivalent, index 0->0, 1->1, 2->2)
print(tensor, "*", tensor)
print("Equals:", tensor * tensor)
tensor([1, 2, 3]) * tensor([1, 2, 3])
Equals: tensor([1, 4, 9])
```

## Matrix multiplication

The main two rules for matrix multiplication to remember are:

1. The inner dimensions must match:
	- (3, 2) @ (3, 2) won't work
	- (2, 3) @ (3, 2) will work
	- (3, 2) @ (2, 3) will work
1. The resulting matrix has the shape of the outer dimensions:
	- (2, 3) @ (3, 2) -> (2, 2)
	- (3, 2) @ (2, 3) -> (3, 3)
> Note: "@" in Python is the symbol for matrix multiplication.

Resource: You can see all of the rules for matrix multiplication using torch.matmul() in the PyTorch documentation.

![[IMG_20230615_013311.jpg]]

```python
# Element-wise matrix multiplication
tensor * tensor
tensor([1, 4, 9])

# Matrix multiplication
torch.matmul(tensor, tensor)
tensor(14)

# Can also use the "@" symbol for matrix multiplication, though not recommended
tensor @ tensor
tensor(14)
```

You can do matrix multiplication by hand but it's not recommended.

The in-built torch.matmul() method is faster.

You can also use torch.mm() which is a short for torch.matmul().

```python
# torch.mm is a shortcut for matmul
torch.mm(tensor_A, tensor_B.T)
tensor([[ 27.,  30.,  33.],
        [ 61.,  68.,  75.],
        [ 95., 106., 117.]])
```


> Note: A matrix multiplication like this is also referred to as the dot product of two matrices.

Neural networks are full of matrix multiplications and dot products.

The `torch.nn.Linear()` module (we'll see this in action later on), also known as a **feed-forward layer** or **fully connected layer**, implements a *[[Умножение матриц|matrix multiplication]]* between an input x and a ***weights matrix*** A. ^linearlayer

$$y=x\cdot A^T+b$$

Where:

- `x` is the input to the layer (deep learning is a stack of layers like torch.nn.Linear() and others on top of each other).
- `A` is the weights matrix created by the layer, this starts out as random numbers that get adjusted as a neural network learns to better represent patterns in the data (notice the "T", that's because the weights matrix gets transposed).
	- Note: You might also often see W or another letter like X used to showcase the weights matrix.
- `b` is the bias term used to slightly offset the weights and inputs.
> - y is the output (a manipulation of the input in the hopes to discover patterns in it).

```python
# Since the linear layer starts with a random weights matrix, let's make it reproducible (more on this later)
torch.manual_seed(42)
# This uses matrix multiplication
linear = torch.nn.Linear(in_features=2, # in_features = matches inner dimension of input 
                         out_features=6) # out_features = describes outer value 
x = tensor_A
output = linear(x)
print(f"Input shape: {x.shape}\n")
print(f"Output:\n{output}\n\nOutput shape: {output.shape}")
Input shape: torch.Size([3, 2])

Output:
tensor([[2.2368, 1.2292, 0.4714, 0.3864, 0.1309, 0.9838],
        [4.4919, 2.1970, 0.4469, 0.5285, 0.3401, 2.4777],
        [6.7469, 3.1648, 0.4224, 0.6705, 0.5493, 3.9716]],
       grad_fn=<AddmmBackward0>)

Output shape: torch.Size([3, 6])
```

### Transposing tensors

You can perform transposes in PyTorch using either:

- `torch.transpose(input, dim0, dim1)` - where `input` is the desired tensor to transpose and `dim0` and `dim1` are the dimensions to be swapped.
- `tensor.T` - where `tensor` is the desired tensor to transpose.

## [min, max, mean, sum, etc. (aggregation)](https://www.learnpytorch.io/00_pytorch_fundamentals/#finding-the-min-max-mean-sum-etc-aggregation)

I've seen like 3-4 ways of doing this in python, numpy and now torch. Dude, they are all the same, I'm tired of constantly writing down all of this.

> [!attention]
> **Note:** You may find some methods such as `torch.mean()` require tensors to be in `torch.float32` (the most common) or another specific datatype, otherwise the operation will fail.


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#pytorch 

---

[^def]: термин
[^que]: вопрос