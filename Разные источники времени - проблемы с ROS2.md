---
tags: 
aliases:
  - "terminate called after throwing an instance of 'std::runtime_error'  what():  can't subtract times with different time sources [2 != 1]"
regex-to-search: 
regex-to-be-found: NoRgxTBFKey
Тип: конспект
Описание: 
День: 30
Месяц: 11
Год: 2023
дата_создания: 2023-11-30
---
# [[Разные источники времени - проблемы с ROS2]]

Это связано с тем, что в `rclcpp` время может быть `RCL_ROS_TIME` и `RCL_SYSTEM_TIME`. Если вы выполняете арифметические операции между двумя объектами `Time` они должны быть одного типа, что обеспечивается при инициализации объекта. Например так:

```cpp
_Now(0, 0, RCL_ROS_TIME)
```

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://github.com/cra-ros-pkg/robot_localization/issues/622#issuecomment-867150029
> 2. https://answers.ros.org/question/380328/robot_localization-cant-subtract-times-with-different-time-sources/


---
#ROS2 #cpp 

---

[^def]: термин
[^que]: вопрос