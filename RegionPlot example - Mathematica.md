> [!bg-plate]
> Plot a region defined by an inequality:
> 
In[1]:=
> 
![](https://reference.wolfram.com/language/ref/Files/RegionPlot.en/I_1.png)
> 
`RegionPlot[x^2 + y^3 < 2, {x, -2, 2}, {y, -2, 2}]`
> 
`https://wolfram.com/xid/0j44868i-dqj`
> 
Out[1]=
> 
![](https://reference.wolfram.com/language/ref/Files/RegionPlot.en/O_1.png)
> 
Plot a region defined by logical combinations of inequalities:
> 
In[1]:=
> 
![](https://reference.wolfram.com/language/ref/Files/RegionPlot.en/I_2.png)
> 
`RegionPlot[x^2 + y^3 < 2 && x + y < 1, {x, -2, 2}, {y, -2, 2}]`
> 
`https://wolfram.com/xid/0j44868i-wjz`
> 
Out[1]=
> 
![](https://reference.wolfram.com/language/ref/Files/RegionPlot.en/O_2.png)
> 
Plot disconnected regions:
> 
In[1]:=
> 
![](https://reference.wolfram.com/language/ref/Files/RegionPlot.en/I_3.png)
> 
`RegionPlot[Sin[x] Sin[y] > 1/4, {x, -10, 10}, {y, -10, 10}, BoundaryStyle -> Dashed, PlotStyle -> Yellow]`
> 
`https://wolfram.com/xid/0j44868i-mr9`
> 
Out[1]=
> 
![](https://reference.wolfram.com/language/ref/Files/RegionPlot.en/O_3.png)
> 
Use legends:
> 
In[1]:=
> 
![](https://reference.wolfram.com/language/ref/Files/RegionPlot.en/I_4.png)
> 
`RegionPlot[{x^2 < y^3 + 1, y^2 < x^3 + 1}, {x, -2, 5}, {y, -2, 5}, PlotLegends -> "Expressions"]`
> 
`https://wolfram.com/xid/0j44868i-r9r3ta`
> 
Out[1]=
> 
![](https://reference.wolfram.com/language/ref/Files/RegionPlot.en/O_4.png)
> 
Style the region:
> 
In[1]:=
> 
![](https://reference.wolfram.com/language/ref/Files/RegionPlot.en/I_5.png)
> 
`RegionPlot[x^2 + y^2 < 1, {x, -1, 1}, {y, -1, 1}, Mesh -> 10, MeshShading -> {{Automatic, None}, {None, Automatic}}, ColorFunction -> "DarkRainbow"]`
> 
`https://wolfram.com/xid/0j44868i-i9fn4`
> 
Out[1]=
> 
![](https://reference.wolfram.com/language/ref/Files/RegionPlot.en/O_5.png)