---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: Запись в словаре

День: 10 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-10-10

---

# [[Незавершённый - inachevé]]

**inachevé** - `i.na.ʃə.ve` - незавершённый.

Прилагательное

- Le temple magnifique a un style typiquement grec, bien que probablement inachevé.
	- Великолепный храм имеет типично греческий стиль, хотя предположительно незавершенный
- Mais ce roman inachevé demeure son œuvre la plus accomplie.
	- Так эта поэма и осталась самым известным незавершенным произведением.


## Смотрите также:

#Французский 