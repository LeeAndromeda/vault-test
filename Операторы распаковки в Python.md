---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 03 # Не трогать!
Месяц: 02 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-02-03

---
# [[Операторы распаковки в Python]]

По сути эти опреаторы это одно и то же, только * используется для списков и туплей, а ** для словарей.

## Оператор "*"

```Python
def func(*args, **kwargs):
	pass
  
x = [1, 2, 3421, 5]
print(x)
print(*x)
# [1, 2, 3421, 5]
# 1 2 3421 5
```

```Python
def func(x, y):
print(x, y)
  
pairs = [(1,2), (2, 3)]
  
for pair in pairs:
func(*pair)
# 1 2
# 2 3
```

## Оператор "**"

```Python
def func(x, y):
print(x, y)
  
func(**{'x':1, 'y':3})
# 1 3
```

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Входящие 

---

[^def]: термин
[^que]: вопрос