---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 27 # Не трогать!
Месяц: 06 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-06-27

---
# [[torch.randn()]]


> [!def]+
> `torch.randn(*size, *, out=None, dtype=None, layout=torch.strided, device=None, requires_grad=False, pin_memory=False)` - возвращает тензор, наполненный случайными числами по закону нормального распределения со средним $o$ и вариацией $1$ (так же называемым стандартным нормальным распределением)
> 
> **аргументы:**
> 1. `generator` (torch.Generator, optional) – a pseudorandom number generator for sampling
> 2. `out` (Tensor, optional) – the output tensor.
> 3. `dtype` (torch.dtype, optional) – the desired data type of returned tensor. Default: if None, uses a global default (see torch.set_default_tensor_type()).
> 4. `layout` (torch.layout, optional) – the desired layout of returned Tensor. Default: torch.strided.
> 5. `device` (torch.device, optional) – the desired device of returned tensor. Default: if None, uses the current device for the default tensor type (see torch.set_default_tensor_type()). device will be the CPU for CPU tensor types and the current CUDA device for CUDA tensor types.
> 6. `requires_grad` (bool, optional) – If autograd should record operations on the returned tensor. Default: False.
> 7. `pin_memory` (bool, optional) – If set, returned tensor would be allocated in the pinned memory. Works only for CPU tensors. Default: False.
> 
> **возвращает:** `torch.Tensor`

## Пример

```python
>>> torch.randn(4)
tensor([-2.1436,  0.9966,  2.3426, -0.6366])
>>> torch.randn(2, 3)
tensor([[ 1.5954,  2.8929, -1.0923],
        [ 1.1719, -0.4709, -0.1996]])
```

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://pytorch.org/docs/stable/generated/torch.randn.html


---
#pytorch 

---

[^def]: термин
[^que]: вопрос