---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 29 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-10-29

---
# [[Свободное восстановление знаний - Free recall]]


**Free recall** - это время после лекции, которое вы тратите на то, чтобы вспомнить и дополнить ваш конспект всем,что вы только что слышали на лекции без вспомогательных материалов. Так же он позволяет вам выделить, какие моменты лекции вы упустили в своем понимании. 


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Саморазвитие #Знание #Конспектирование 

---

[^def]: термин
[^que]: вопрос