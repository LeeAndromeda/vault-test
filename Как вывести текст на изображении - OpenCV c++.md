---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 24 # Не трогать!
Месяц: 07 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-07-24

---
# [[Как вывести текст на изображении - OpenCV c++]]



In OpenCV, we can put some text in the image by using puttext() function. This function is defined in **<imgproc.hpp>** header. To put text in an image, we first need to declare the matrix which will load the image.

Instead of loading an image in our program, we have filled matrix with white color, and then we put the text in that matrix. We need to define the text's starting point in the matrix, the text's font, the font's color and the font's weight.

The basic syntax of this method is as follows −

## Syntax

```cpp
putText(image, "Text in Images", text_position,FONT_HERSHEY_COMPLEX, font_size,font_Color, font_weight);
```
The following program shows the way to put a text in an image in OpenCV.

## Example

```cpp
#include<iostream>
#include<opencv2/highgui/highgui.hpp>
#include<opencv2/imgproc/imgproc.hpp>
#include<string>
using namespace cv;
using namespace std;
int main() {
   Mat image=Mat(400, 400, CV_8UC3, Scalar(255, 255, 255));//Creating an empty matrix filled with white color//
   Point text_position(80, 80);//Declaring the text position//
   int font_size = 1;//Declaring the font size//
   Scalar font_Color(0, 0, 0);//Declaring the color of the font//
   int font_weight = 2;//Declaring the font weight//
   putText(image, "Text in Images", text_position,FONT_HERSHEY_COMPLEX, font_size,font_Color, font_weight);//Putting the text in the matrix//
   imshow("Image", image);//Showing the image//
   waitKey(0);//Wait for Keystroke//
   return 0;
}
```
## Output

![](https://www.tutorialspoint.com/assets/questions/media/49278/Page-36-Image-72.jpg)


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#КомпьютерноеЗрение/opencv #cpp 

---

[^def]: термин
[^que]: вопрос