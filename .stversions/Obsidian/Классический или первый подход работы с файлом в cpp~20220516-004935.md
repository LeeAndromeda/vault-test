---
annotation-target: лекции по С++_Работа с файлом_1 подход.pdf
---

>%%
>```annotation-json
>{"created":"2022-05-15T21:40:22.231Z","updated":"2022-05-15T21:40:22.231Z","document":{"title":"%D0%BB%D0%B5%D0%BA%D1%86%D0%B8%D0%B8 %D0%BF%D0%BE %D0%A1++_%D0%A0%D0%B0%D0%B1%D0%BE%D1%82%D0%B0 %D1%81 %D1%84%D0%B0%D0%B9%D0%BB%D0%BE%D0%BC_1 %D0%BF%D0%BE%D0%B4%D1%85%D0%BE%D0%B4.pdf","link":[{"href":"urn:x-pdf:8171bfc7772d53438ed6bf6391859bb2"},{"href":"vault:/%D0%BB%D0%B5%D0%BA%D1%86%D0%B8%D0%B8 %D0%BF%D0%BE %D0%A1++_%D0%A0%D0%B0%D0%B1%D0%BE%D1%82%D0%B0 %D1%81 %D1%84%D0%B0%D0%B9%D0%BB%D0%BE%D0%BC_1 %D0%BF%D0%BE%D0%B4%D1%85%D0%BE%D0%B4.pdf"}],"documentFingerprint":"8171bfc7772d53438ed6bf6391859bb2"},"uri":"vault:/%D0%BB%D0%B5%D0%BA%D1%86%D0%B8%D0%B8 %D0%BF%D0%BE %D0%A1++_%D0%A0%D0%B0%D0%B1%D0%BE%D1%82%D0%B0 %D1%81 %D1%84%D0%B0%D0%B9%D0%BB%D0%BE%D0%BC_1 %D0%BF%D0%BE%D0%B4%D1%85%D0%BE%D0%B4.pdf","target":[{"source":"vault:/%D0%BB%D0%B5%D0%BA%D1%86%D0%B8%D0%B8 %D0%BF%D0%BE %D0%A1++_%D0%A0%D0%B0%D0%B1%D0%BE%D1%82%D0%B0 %D1%81 %D1%84%D0%B0%D0%B9%D0%BB%D0%BE%D0%BC_1 %D0%BF%D0%BE%D0%B4%D1%85%D0%BE%D0%B4.pdf","selector":[{"type":"TextPositionSelector","start":5983,"end":6187},{"type":"TextQuoteSelector","exact":"ри использовании имен файлов в программах на языках C, C++ следует помнить об одном соглашении – спецификация файла задается строкой, в которой знак \"обратного слэша\" заменяется удвоенным обратным слэшем:","prefix":"ов и собственно имени файла:   П","suffix":" char namef[]=\"c:\\\\bc\\\\bin\\\\bc.e"}]}]}
>```
>%%
>*%%PREFIX%%ов и собственно имени файла:   П%%HIGHLIGHT%% ==ри использовании имен файлов в программах на языках C, C++ следует помнить об одном соглашении – спецификация файла задается строкой, в которой знак "обратного слэша" заменяется удвоенным обратным слэшем:== %%POSTFIX%%char namef[]="c:\\bc\\bin\\bc.e*
>%%LINK%%[[#^bnrbn0ds8oq|show annotation]]
>%%COMMENT%%
>
>%%TAGS%%
>
^bnrbn0ds8oq


>%%
>```annotation-json
>{"created":"2022-05-15T21:41:22.483Z","updated":"2022-05-15T21:41:22.483Z","document":{"title":"%D0%BB%D0%B5%D0%BA%D1%86%D0%B8%D0%B8 %D0%BF%D0%BE %D0%A1++_%D0%A0%D0%B0%D0%B1%D0%BE%D1%82%D0%B0 %D1%81 %D1%84%D0%B0%D0%B9%D0%BB%D0%BE%D0%BC_1 %D0%BF%D0%BE%D0%B4%D1%85%D0%BE%D0%B4.pdf","link":[{"href":"urn:x-pdf:8171bfc7772d53438ed6bf6391859bb2"},{"href":"vault:/%D0%BB%D0%B5%D0%BA%D1%86%D0%B8%D0%B8 %D0%BF%D0%BE %D0%A1++_%D0%A0%D0%B0%D0%B1%D0%BE%D1%82%D0%B0 %D1%81 %D1%84%D0%B0%D0%B9%D0%BB%D0%BE%D0%BC_1 %D0%BF%D0%BE%D0%B4%D1%85%D0%BE%D0%B4.pdf"}],"documentFingerprint":"8171bfc7772d53438ed6bf6391859bb2"},"uri":"vault:/%D0%BB%D0%B5%D0%BA%D1%86%D0%B8%D0%B8 %D0%BF%D0%BE %D0%A1++_%D0%A0%D0%B0%D0%B1%D0%BE%D1%82%D0%B0 %D1%81 %D1%84%D0%B0%D0%B9%D0%BB%D0%BE%D0%BC_1 %D0%BF%D0%BE%D0%B4%D1%85%D0%BE%D0%B4.pdf","target":[{"source":"vault:/%D0%BB%D0%B5%D0%BA%D1%86%D0%B8%D0%B8 %D0%BF%D0%BE %D0%A1++_%D0%A0%D0%B0%D0%B1%D0%BE%D1%82%D0%B0 %D1%81 %D1%84%D0%B0%D0%B9%D0%BB%D0%BE%D0%BC_1 %D0%BF%D0%BE%D0%B4%D1%85%D0%BE%D0%B4.pdf","selector":[{"type":"TextPositionSelector","start":11852,"end":11922},{"type":"TextQuoteSelector","exact":" качестве признака конца строки здесь заносятся те же самые байты 0D0A","prefix":"у с помощью оператора fprintf. В","suffix":", которые появляются на диске в "}]}]}
>```
>%%
>*%%PREFIX%%у с помощью оператора fprintf. В%%HIGHLIGHT%% ==качестве признака конца строки здесь заносятся те же самые байты 0D0A== %%POSTFIX%%, которые появляются на диске в*
>%%LINK%%[[#^qj801e8j66|show annotation]]
>%%COMMENT%%
>
>%%TAGS%%
>
^qj801e8j66


>%%
>```annotation-json
>{"created":"2022-05-15T21:41:58.952Z","text":"!","updated":"2022-05-15T21:41:58.952Z","document":{"title":"%D0%BB%D0%B5%D0%BA%D1%86%D0%B8%D0%B8 %D0%BF%D0%BE %D0%A1++_%D0%A0%D0%B0%D0%B1%D0%BE%D1%82%D0%B0 %D1%81 %D1%84%D0%B0%D0%B9%D0%BB%D0%BE%D0%BC_1 %D0%BF%D0%BE%D0%B4%D1%85%D0%BE%D0%B4.pdf","link":[{"href":"urn:x-pdf:8171bfc7772d53438ed6bf6391859bb2"},{"href":"vault:/%D0%BB%D0%B5%D0%BA%D1%86%D0%B8%D0%B8 %D0%BF%D0%BE %D0%A1++_%D0%A0%D0%B0%D0%B1%D0%BE%D1%82%D0%B0 %D1%81 %D1%84%D0%B0%D0%B9%D0%BB%D0%BE%D0%BC_1 %D0%BF%D0%BE%D0%B4%D1%85%D0%BE%D0%B4.pdf"}],"documentFingerprint":"8171bfc7772d53438ed6bf6391859bb2"},"uri":"vault:/%D0%BB%D0%B5%D0%BA%D1%86%D0%B8%D0%B8 %D0%BF%D0%BE %D0%A1++_%D0%A0%D0%B0%D0%B1%D0%BE%D1%82%D0%B0 %D1%81 %D1%84%D0%B0%D0%B9%D0%BB%D0%BE%D0%BC_1 %D0%BF%D0%BE%D0%B4%D1%85%D0%BE%D0%B4.pdf","target":[{"source":"vault:/%D0%BB%D0%B5%D0%BA%D1%86%D0%B8%D0%B8 %D0%BF%D0%BE %D0%A1++_%D0%A0%D0%B0%D0%B1%D0%BE%D1%82%D0%B0 %D1%81 %D1%84%D0%B0%D0%B9%D0%BB%D0%BE%D0%BC_1 %D0%BF%D0%BE%D0%B4%D1%85%D0%BE%D0%B4.pdf","selector":[{"type":"TextPositionSelector","start":11997,"end":12266},{"type":"TextQuoteSelector","exact":"Для инициализации текстового файла необходимо завести указатель на структуру типа FILE и открыть файл по оператору fopen в одном из нужных режимов – \"rt\" (текстовый для чтения), \"wt\" (текстовый для записи), \"at\" (текстовый для дозаписи в уже существующий набор данных):","prefix":"вывода управляющего символа \\n. ","suffix":" FILE *f1; .........   f1=fopen("}]}]}
>```
>%%
>*%%PREFIX%%вывода управляющего символа \n.%%HIGHLIGHT%% ==Для инициализации текстового файла необходимо завести указатель на структуру типа FILE и открыть файл по оператору fopen в одном из нужных режимов – "rt" (текстовый для чтения), "wt" (текстовый для записи), "at" (текстовый для дозаписи в уже существующий набор данных):== %%POSTFIX%%FILE *f1; .........   f1=fopen(*
>%%LINK%%[[#^5yuztkvwvai|show annotation]]
>%%COMMENT%%
>!
>%%TAGS%%
>
^5yuztkvwvai


>%%
>```annotation-json
>{"created":"2022-05-15T21:45:21.576Z","text":"1) FILE *f1 - создание указателя f1 типа данных FILE\n2) f1 = fopen(имя_файла, \"режим\") - значение указателя f1 приравниваетссся к адресу файла, который мы будем использовать","updated":"2022-05-15T21:45:21.576Z","document":{"title":"%D0%BB%D0%B5%D0%BA%D1%86%D0%B8%D0%B8 %D0%BF%D0%BE %D0%A1++_%D0%A0%D0%B0%D0%B1%D0%BE%D1%82%D0%B0 %D1%81 %D1%84%D0%B0%D0%B9%D0%BB%D0%BE%D0%BC_1 %D0%BF%D0%BE%D0%B4%D1%85%D0%BE%D0%B4.pdf","link":[{"href":"urn:x-pdf:8171bfc7772d53438ed6bf6391859bb2"},{"href":"vault:/%D0%BB%D0%B5%D0%BA%D1%86%D0%B8%D0%B8 %D0%BF%D0%BE %D0%A1++_%D0%A0%D0%B0%D0%B1%D0%BE%D1%82%D0%B0 %D1%81 %D1%84%D0%B0%D0%B9%D0%BB%D0%BE%D0%BC_1 %D0%BF%D0%BE%D0%B4%D1%85%D0%BE%D0%B4.pdf"}],"documentFingerprint":"8171bfc7772d53438ed6bf6391859bb2"},"uri":"vault:/%D0%BB%D0%B5%D0%BA%D1%86%D0%B8%D0%B8 %D0%BF%D0%BE %D0%A1++_%D0%A0%D0%B0%D0%B1%D0%BE%D1%82%D0%B0 %D1%81 %D1%84%D0%B0%D0%B9%D0%BB%D0%BE%D0%BC_1 %D0%BF%D0%BE%D0%B4%D1%85%D0%BE%D0%B4.pdf","target":[{"source":"vault:/%D0%BB%D0%B5%D0%BA%D1%86%D0%B8%D0%B8 %D0%BF%D0%BE %D0%A1++_%D0%A0%D0%B0%D0%B1%D0%BE%D1%82%D0%B0 %D1%81 %D1%84%D0%B0%D0%B9%D0%BB%D0%BE%D0%BC_1 %D0%BF%D0%BE%D0%B4%D1%85%D0%BE%D0%B4.pdf","selector":[{"type":"TextPositionSelector","start":12267,"end":12318},{"type":"TextQuoteSelector","exact":"FILE *f1; .........   f1=fopen(имя_файла, \"режим\");","prefix":"уже существующий набор данных): ","suffix":" Формат оператора обмена с текст"}]}]}
>```
>%%
>*%%PREFIX%%уже существующий набор данных):%%HIGHLIGHT%% ==FILE *f1; .........   f1=fopen(имя_файла, "режим");== %%POSTFIX%%Формат оператора обмена с текст*
>%%LINK%%[[#^paqrosqkep|show annotation]]
>%%COMMENT%%
>1) FILE *f1 - создание указателя f1 типа данных FILE
>2) f1 = fopen(имя_файла, "режим") - значение указателя f1 приравниваетссся к адресу файла, который мы будем использовать
>%%TAGS%%
>
^paqrosqkep


>%%
>```annotation-json
>{"created":"2022-05-15T21:47:26.279Z","updated":"2022-05-15T21:47:26.279Z","document":{"title":"%D0%BB%D0%B5%D0%BA%D1%86%D0%B8%D0%B8 %D0%BF%D0%BE %D0%A1++_%D0%A0%D0%B0%D0%B1%D0%BE%D1%82%D0%B0 %D1%81 %D1%84%D0%B0%D0%B9%D0%BB%D0%BE%D0%BC_1 %D0%BF%D0%BE%D0%B4%D1%85%D0%BE%D0%B4.pdf","link":[{"href":"urn:x-pdf:8171bfc7772d53438ed6bf6391859bb2"},{"href":"vault:/%D0%BB%D0%B5%D0%BA%D1%86%D0%B8%D0%B8 %D0%BF%D0%BE %D0%A1++_%D0%A0%D0%B0%D0%B1%D0%BE%D1%82%D0%B0 %D1%81 %D1%84%D0%B0%D0%B9%D0%BB%D0%BE%D0%BC_1 %D0%BF%D0%BE%D0%B4%D1%85%D0%BE%D0%B4.pdf"}],"documentFingerprint":"8171bfc7772d53438ed6bf6391859bb2"},"uri":"vault:/%D0%BB%D0%B5%D0%BA%D1%86%D0%B8%D0%B8 %D0%BF%D0%BE %D0%A1++_%D0%A0%D0%B0%D0%B1%D0%BE%D1%82%D0%B0 %D1%81 %D1%84%D0%B0%D0%B9%D0%BB%D0%BE%D0%BC_1 %D0%BF%D0%BE%D0%B4%D1%85%D0%BE%D0%B4.pdf","target":[{"source":"vault:/%D0%BB%D0%B5%D0%BA%D1%86%D0%B8%D0%B8 %D0%BF%D0%BE %D0%A1++_%D0%A0%D0%B0%D0%B1%D0%BE%D1%82%D0%B0 %D1%81 %D1%84%D0%B0%D0%B9%D0%BB%D0%BE%D0%BC_1 %D0%BF%D0%BE%D0%B4%D1%85%D0%BE%D0%B4.pdf","selector":[{"type":"TextPositionSelector","start":12319,"end":12610},{"type":"TextQuoteSelector","exact":"Формат оператора обмена с текстовыми файлами мало чем отличается от операторов форматного ввода ( scanf ) и вывода ( printf ). Вместо них при работе с файлами используются функции fscanf и fprintf, у которых единственным дополнительным аргументом является указатель на соответствующий файл: ","prefix":"  f1=fopen(имя_файла, \"режим\"); ","suffix":"fscanf(f1,\"список_форматов\", спи"}]}]}
>```
>%%
>*%%PREFIX%%f1=fopen(имя_файла, "режим");%%HIGHLIGHT%% ==Формат оператора обмена с текстовыми файлами мало чем отличается от операторов форматного ввода ( scanf ) и вывода ( printf ). Вместо них при работе с файлами используются функции fscanf и fprintf, у которых единственным дополнительным аргументом является указатель на соответствующий файл:== %%POSTFIX%%fscanf(f1,"список_форматов", спи*
>%%LINK%%[[#^mx2shhgagsn|show annotation]]
>%%COMMENT%%
>
>%%TAGS%%
>
^mx2shhgagsn
