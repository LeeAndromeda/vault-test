$$x^2+3x+5=0$$
$D=b^2 - 4ac = 9 - 20 = -11$
$x_1 = \frac{-3- \sqrt{-11}}{2} = \frac{-3- \sqrt{11}i}{2}$
$x_2 = \frac{-3+ \sqrt{-1}}{2} = \frac{-3 + \sqrt{11}i}{2}$


$P_g = C \sqrt{ |x-x_{goal}|^{2}}$