# Что такое потенциальное поле и как оно работает?

Потенциальное поле - это любое физическое поле, подчиняющееся уравнению Лапласа. Некоторые общие примеры потенциальных полей включают электрические, магнитные и гравитационные поля. Алгоритм потенциального поля использует искусственное потенциальное поле для управления роботом в определенном пространстве. Для простоты мы рассматриваем пространство, разделенное на сетку ячеек с препятствиями и узлом цели.
Потенциальные поля, генерируемые в двумерном пространстве

![[Pasted image 20220606113111.png]]

Алгоритм присваивает искусственное потенциальное поле каждой точке мира с помощью функций потенциального поля, которые будут описаны далее. Робот моделирует от самого высокого потенциала к самому низкому. Здесь узел цели имеет самый низкий потенциал, в то время как начальный узел будет иметь максимальный потенциал. Следовательно, мы можем сказать, что БПЛА движется от наименьшего потенциала к наибольшему.

## Генерируемые потенциальные поля:

В системе генерируются два вида искусственных потенциальных полей: Притягивающее поле и Отталкивающее поле.
Обнаружение препятствий в трехмерном пространстве

![[Pasted image 20220606113159.png]]

Целевой узел создает притягивающее поле, в то время как препятствия в системе создают отталкивающие поля.

$$F_{от} = \frac{1}{дистанция}$$

Следовательно полная сила в каждой точке равна:

$$F_п=F_{прит}+F_{от}$$


## Сила притягивания

$P_{g} = C \sqrt{ |x-x_{goal}|^{2}+ |y-y_{goal}|^2}$, где x, y - это координаты объекта, на который действует сила, xgoal, ygoal - координаты целевой точки, С - константа.

> В случае планирования для робота с началом отсчета координатной системы в точке самого робота:
> 
> $$P_{g} = C \sqrt{x_{goal}^{2}+y_{goal}^2}$$

## Сила отталкивания

$$P_{HA}= \frac{1}{\delta+ \sum_{i=1}^{s} (g_i+ |g_i|)}$$

### 1. Отталкивание границами

Где $g_i$ - линейная функция, представляющая границу выпуклой зоны, $\delta$ - постоянная с малым значением и $s$ - количество сегментов границы.

### 2. Отталкивание препятствиями

Здесь $p_{max}$ - максимальный потенциал, $(x_{0}, y_{0})$ - координаты центра препятствия и $l$ - длина стороны препятствия.

$P_{i,j}= \frac{P_{max}}{1+g}$ 

где

$$
\begin{aligned}
g(x, y)=&\left(x_{0}-l / 2-x\right)+\left|x_{0}-l / 2-x\right| \\
&+\left(x-x_{0}-l / 2+1\right)+\left|x-x_{0}-l / 2+1\right| \\
&+\left(y_{0}-l / 2-y\right)+\left|y_{0}-l / 2-y\right| \\
&+\left(y-y_{0}-l / 2+1\right)+\left|y-y_{0}-l / 2+1\right| .
\end{aligned}
$$

### Моя идея реализации этого алгоритма в качестве избегания препятствий

Создать поле точек объектом PointCloud с разрешением $r$ и шириной $w$[м].для визуализации поля. Разрешение будет определять $\Delta x = x_{2} - x_{1}$ и $\Delta y = y_{2} - y_{1}$. Соответсвенно создается массив точек $pArr[\frac{w}{r}][\frac{w}{r}]$