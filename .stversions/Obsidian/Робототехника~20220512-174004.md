# Робототехника

> # [[План обучения робототехнике]]

## Подсферы деятельности

- [[КК Программирование]]
- [[Работа с ROS]]
- [[Работа с Linux]]

## Идеи проектов

- [[АНПА-роботоносец или Подводная лодка-роботоносец]]

## Видео

- [Ultimate Roadmap to become a ROBOTICS SOFTWARE Engineer | How to become a Robotics Engineer| JLCPCB](https://youtu.be/Cbe3FflOWgU)
- [How to Start with Robotics? for Absolute Beginners || The Ultimate 3-Step Guide](https://youtu.be/J0ssFp7yN8Y)
- [How Soft Robotics is Changing Clothing | TechnoLogic](https://youtu.be/OoXCO8NB9B8)

#Робототехника 