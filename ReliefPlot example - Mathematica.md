Use elevation data to display shaded terrain:

In[1]:=

![](https://reference.wolfram.com/language/ref/Files/ReliefPlot.en/I_1.png)

`ReliefPlot[ Import["http://exampledata.wolfram.com/hailey.dem.gz", "Data"], ColorFunction -> "GreenBrownTerrain"]`

`https://wolfram.com/xid/0h2n7sw5gi-i1hw9`

Out[1]=

![](https://reference.wolfram.com/language/ref/Files/ReliefPlot.en/O_1.png)

Display density data with shading:

In[1]:=

![](https://reference.wolfram.com/language/ref/Files/ReliefPlot.en/I_2.png)

`ReliefPlot[ Table[i + Sin[i^2 + j^2], {i, -4, 4, .03}, {j, -4, 4, .03}], ColorFunction -> "SunsetColors"]`

`https://wolfram.com/xid/0h2n7sw5gi-kmaj3d`

Out[1]=

![](https://reference.wolfram.com/language/ref/Files/ReliefPlot.en/O_2.png)

Show heights with a legend:

In[1]:=

![](https://reference.wolfram.com/language/ref/Files/ReliefPlot.en/I_3.png)

`ReliefPlot[ Import["http://exampledata.wolfram.com/hailey.dem.gz", "Data"], ColorFunction -> "LightTerrain", PlotLegends -> Automatic]`

`https://wolfram.com/xid/0h2n7sw5gi-n4l08p`

Out[1]=

![](https://reference.wolfram.com/language/ref/Files/ReliefPlot.en/O_3.png)