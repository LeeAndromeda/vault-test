---
tags: 
aliases:
  - PX4
  - MAVROS
regex-to-search: 
regex-to-be-found: NoRgxTBFKey
Тип: конспект
Описание:
  - 
День: 12
Месяц: 9
Год: 2023
дата_создания: 2023-09-12
---
# [[PX4 (+ MAVROS)]]

## Теория

- [[Как заармить и запустить дрон через нод ROS]]


## Как установить сим

1. [Установка px4 autopilot](https://docs.px4.io/main/en/dev_setup/dev_env_linux_ubuntu.html)
2. [Сборка autopilot для  ROS с gazebo](https://docs.px4.io/main/en/dev_setup/building_px4.html)
3. [[Руководство пользователя виртуального тренажера для подготовки к соревнованиям Аэробот-2023]]
4. [[Гайд запуска симулятора для Аэробот]]


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#px4 #Робототехника/Квадрокоптеры #mavros

---

[^def]: термин
[^que]: вопрос