---

Tags: 
aliases: 
regex-to-search: "(.*)[Zz][Mm][Qq](.*)" # "что-то" regex
regex-to-be-found: "((.*)[Zz][Mm][Qq](.*)pub(.*)sub(.*))" # "что-то" regex

Тип: конспект
Описание: -

День: 28 # Не трогать!
Месяц: 12 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Wednesday 28th December 2022 11:29:01

---
# [[zmq шаблоны Паблишер и Сабскрайбер]]

> [! example] Список определений [^def]
>

> [! example] Список вопросов [^que]
> 

## 1. Содержание

1. [[#Паблишер|Паблишер]]
1. [[#Сабскрайбер|Сабскрайбер]]
1. [[#Связано:|Связано:]]


## Паблишер

```cpp
#include "zmq.hpp"
#include "zmq_addon.hpp"

std::string adress("tcp://192.168.88.145:8080");

void PublisherThread(zmq::context_t *ctx)
{
	// Prepare publisher
	zmq::socket_t publisher(*ctx, zmq::socket_type::pub);
	publisher.bind(adress); 
	/* Publisher and subscriber IPs and ports should be equivalent
	* however when creating a  publisher, you should bind() it to the
	* address and when creating a subscriber you should connect()
	* to the address.
	* Source: https://zguide.zeromq.org/docs/chapter1/ - Figure 4 - Publish-Subscribe
	*/

	// Give the subscribers a chance to connect, so they don't lose any messages
	std::this_thread::sleep_for(std::chrono::milliseconds(20));
	  
	while (true)
	{
		YourProtoType data_var;
		std::string data;
		GOOGLE_PROTOBUF_VERIFY_VERSION;

		data_var = something();
		  
		data_var.SerializeToString(&data);
		  
		// Message must be "rebuilt" in every loop iteration
		zmq::message_t message(data.size());
		std::memcpy(message.data(), data.data(), data.size());
		publisher.send(zmq::str_buffer("Topic"), zmq::send_flags::sndmore);
		publisher.send(message);
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	}
}
  
int main()
{
/*
* No I/O threads are involved in passing messages using the inproc transport.
* Therefore, if you are using a ØMQ context for in-process messaging only you
* can initialise the context with zero I/O threads.
*
* Source: http://api.zeromq.org/4-3:zmq-inproc
*/
zmq::context_t ctx(1);
  
auto thread1 = std::async(std::launch::async, PublisherThread, &ctx);
// Give the publisher a chance to bind, since inproc requires it
std::this_thread::sleep_for(std::chrono::milliseconds(10));
thread1.wait();
}
```


## Сабскрайбер

```cpp
#include <future>
#include <iostream>
#include <string>
#include <thread>
  
#include "zmq.hpp"
#include "zmq_addon.hpp"
  
#include "ANPAComm.pb.h"
  
std::string adress("tcp://192.168.88.145:8080");
/* Publisher and subscriber IPs and ports should be equivalent
* however when creating a publisher, you should bind() it to the
* address and when creating a subscriber you should connect()
* to the address.
*
* Source: https://zguide.zeromq.org/docs/chapter1/ - Figure 4 - Publish-Subscribe
*/
  
void SubscriberThread1(zmq::context_t *ctx)
{
	// Prepare subscriber
	zmq::socket_t subscriber(*ctx, ZMQ_SUB);
	subscriber.set(zmq::sockopt::subscribe, "ANPAShare");
	subscriber.connect(adress);
	  
	while (true)
	{
		std::vector<zmq::message_t> recv_msgs;
		zmq::recv_result_t result = zmq::recv_multipart(subscriber, std::back_inserter(recv_msgs), zmq::recv_flags::none);
		  
		ANPAComm::ANPAGroup your_var_to_catch_data;
		your_var_to_catch_data.ParseFromArray(recv_msgs[1].data(), recv_msgs[1].size());
		  
		if (your_var_to_catch_data.is_not_empty())
		{
			do_something();
		}
	}
}
  
int main()
{
	/*
	* No I/O threads are involved in passing messages using the inproc transport.
	* Therefore, if you are using a ØMQ context for in-process messaging only you
	* can initialise the context with zero I/O threads.
	*
	* Source: http://api.zeromq.org/4-3:zmq-inproc
	*/
	zmq::context_t ctx(1);
	  
	auto thread2 = std::async(std::launch::async, SubscriberThread1, &ctx);
	thread2.wait();
	return 0;
}
```

%%## Приложения%%

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Входящие 

[^def]: термин
[^que]: вопрос