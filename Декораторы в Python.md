---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 09 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-10-09

---
# [[Декораторы в Python]]


**Декораторы** позволяют добавить какое-то поведение, не меняя основного.

```python
import os
from icecream import ic
from collections import Counter
from functools import wraps


def args_counter(func):
    counter = Counter()
    @wraps(func)
    def inner(a, b):
        if a > 10 ** 6:
            raise ValueError
        counter[a] += 1
        counter[b] += 1
        ic(func)
        ic(a, b)
        result = func(a, b)
        ic(counter)
        ic(result)
        return result
    return inner


@args_counter
def calc(a, b):
    """Some calcs    """
    
    return a * b + b ** 2


@args_counter
def calc2(a, b):
    return a + b + a ** 2

# calc_decorated = args_counter(calc)(2, 5)

print(calc.__doc__)

# result_1 = calc(2, 5)
# result_1 = calc(2, 5)
# result_1 = calc(4, 5)
# result_1 = calc(2, 7)
# result_1 = calc(3, 5)
# print(result_1)
```

Как только появляется задача сделать что-то дополнительное, не меняя основного, решение - декораторы.

При использовании декоратора, возникает проблема исчезновения [[Docstring - документация кода в Python|docstring]] у функции. К сожалению, если декоратор используется, то docstring приходится терять, потому что декоратор возвращает внутренне-определенную функцию, у которой нет вашего docstring.

Решить это можно указав декторатором внутренней функцией вашего декоратора функцию [[functools - python|`functools.wraps`]]:

```python
def args_counter(func):
    counter = Counter()
    @wraps(func)
    def inner(a, b):
```


## Множественное применение декораторов

Так можно делать. Работать они будут сверху вниз, т.е. самый верхний декоратор будет принимать аргументом ниже написанный декоратор и так далее.


## Смотрите также:

- [[collections.Counter - python]]

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Входящие 

---

[^def]: термин
[^que]: вопрос