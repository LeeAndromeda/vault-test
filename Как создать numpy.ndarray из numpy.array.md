---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 11 # Не трогать!
Месяц: 03 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-03-11

---
# [[Как создать numpy.ndarray из numpy.array]]


## An `ndarray` _is_ a NumPy array.

```python
>>> x = np.array([1, 2, 3])
>>> type(x)
<type 'numpy.ndarray'>
```

The difference between `np.ndarray` and `np.array` is that the former is the actual type, while the latter is a flexible shorthand function for constructing arrays from data in other formats. The `TypeError` comes your use of `np.array` arguments to `np.ndarray`, which takes completely different arguments (see docstrings).

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://stackoverflow.com/questions/21088133/how-to-construct-a-ndarray-from-a-numpy-array-python


---
#Python/NumPy 

---

[^def]: термин
[^que]: вопрос