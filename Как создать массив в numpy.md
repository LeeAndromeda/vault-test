---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 20 # Не трогать!
Месяц: 11 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-11-20

---
# [[Как создать массив в numpy]]


-   `array(object)` - n-мерный массив из любой (возможно, вложенной) последовательности, возвращает объект типа [[ndarray NumPy - общая теория|`numpy.ndarray` ]]
-   `eye(N, M=N, k=0)` -  двумерный массив с N строками с единицами на диагонали и нулями во всех остальных позициях. Число столбцов M по умолчанию равно N, k — сдвиг диагонали (0 для основной диагонали, положительные числа для верхних диагоналей и отрицательные для нижних),  
-   `zeros(shape)` - новый массив указанной формы, заполненный нулями,
	- `zeros_like(matrix)` - новый массив, с формой, как у `matrix`, заполненный нулями
-   `ones(shape)` - новый массив указанной формы, заполненный единицами,  
-   `full(shape, fill_value)` - новый массив указанной формы, заполненный `fill_value`.
- `numpy.arrange(start, stop, step)` - создает вектор, заполненный значениями по указанному принципу
- `numpy.linspace(start, stop, num)` - создает `num` значений с равными интервалами от `start` до `stop`
- `numpy.logspace(start, stop, num, base)` - создает `num` значений возводя `base` в степени от `start` до `stop`.

[[Как создать numpy.ndarray из numpy.array]]


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Python/NumPy 

---

[^def]: термин
[^que]: вопрос