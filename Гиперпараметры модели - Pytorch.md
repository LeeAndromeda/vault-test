---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 20 # Не трогать!
Месяц: 06 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-06-20

---
# [[Гиперпараметры модели - Pytorch]]


**Hyperparameters** are adjustable parameters that let you control the model optimization process. They are called like that because they are controlled by you. Different hyperparameter values can impact model training and convergence rates ([read more](https://pytorch.org/tutorials/beginner/hyperparameter_tuning_tutorial.html) about hyperparameter tuning)

We define the following hyperparameters for training:

- **Number of Epochs** - the number times to iterate over the dataset
    
- **Batch Size** - the number of data samples propagated through the network before the parameters are updated
    
- **Learning Rate** - how much to update models parameters at each batch/epoch. Smaller values yield slow learning speed, while large values may result in unpredictable behavior during training.
    
```python
learning_rate = 1e-3
batch_size = 64
epochs = 5
```

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://pytorch.org/tutorials/beginner/basics/optimization_tutorial.html#hyperparameters


---
#pytorch 

---

[^def]: термин
[^que]: вопрос