---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 09 # Не трогать!
Месяц: 03 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-03-09

---
# [[error static assertion failed std thread arguments must be invocable after conversion to rvalues]]

Код функции:

```cpp
void SubscriberThread1(zmq::context_t *ctx, MissionGroupPub &bridge)
{
	// do something
}
```

Запуск потока:

```cpp
zmq::context_t ctx(1);
MissionGroupPub bridge;
  
std::thread thread1(SubscriberThread1, &ctx, std::ref(bridge));
thread1.join();
```

Код ошибки:

```cpp
error: static assertion failed: std::thread arguments must be invocable after conversion to rvalues
  120 |           typename decay<_Args>::type...>::value,
```

## Решение

Аргумент, который должен передаваться по ссылкее (в данном случае `bridge`)  нужно обернуть в функцию `std::ref()`:

```cpp
std::thread thread1(SubscriberThread1, &ctx, std::ref(bridge));
```

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#cpp 

---

[^def]: термин
[^que]: вопрос