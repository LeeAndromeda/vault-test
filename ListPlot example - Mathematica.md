> [!bg-plate]
> Plot a list of ![](https://reference.wolfram.com/language/ref/Files/ListPlot.en/15.png) values:
> 
> In[1]:=
> 
> ![](https://reference.wolfram.com/language/ref/Files/ListPlot.en/I_1.png)
> 
> `ListPlot[Prime[Range[25]]]`
> 
> `https://wolfram.com/xid/0wgs3aa-div`
> 
> Out[1]=
> 
> ![](https://reference.wolfram.com/language/ref/Files/ListPlot.en/O_1.png)
> 
> Plot a list of ![](https://reference.wolfram.com/language/ref/Files/ListPlot.en/16.png), ![](https://reference.wolfram.com/language/ref/Files/ListPlot.en/17.png) pairs:
> 
> In[1]:=
> 
> ![](https://reference.wolfram.com/language/ref/Files/ListPlot.en/I_2.png)
> 
> `ListPlot[Table[{Sin[n], Sin[2 n]}, {n, 50}]]`
> 
> `https://wolfram.com/xid/0wgs3aa-fpz`
> 
> Out[1]=
> 
> ![](https://reference.wolfram.com/language/ref/Files/ListPlot.en/O_2.png)
> 
> Plot several datai with a legend:
> 
> In[1]:=
> 
> ![](https://reference.wolfram.com/language/ref/Files/ListPlot.en/I_3.png)
> 
> `ListPlot[ Table[{k, PDF[BinomialDistribution[50, p], k]}, {p, {0.3, 0.5, 0.8}}, {k, 0, 50}], Filling -> Axis, PlotLegends -> {0.3, 0.5, 0.8}]`
> 
> `https://wolfram.com/xid/0wgs3aa-dipjwu`
> 
> Out[1]=
> 
> ![](https://reference.wolfram.com/language/ref/Files/ListPlot.en/O_3.png)
> 
> Label each point:
> 
> In[1]:=
> 
> ![](https://reference.wolfram.com/language/ref/Files/ListPlot.en/I_4.png)
> 
> `ListPlot[Labeled[#, #] & /@ Table[Prime[n], {n, 10}], PlotStyle -> PointSize[Medium]]`
> 
> `https://wolfram.com/xid/0wgs3aa-gmx2ul`
> 
> Out[1]=
> 
> ![](https://reference.wolfram.com/language/ref/Files/ListPlot.en/O_4.png)
> 
> Label each datai:
> 
> In[1]:=
> 
> ![](https://reference.wolfram.com/language/ref/Files/ListPlot.en/I_5.png)
> 
> `ListPlot[{Labeled[Sqrt[Range[40]], "sqrt"], Labeled[Log[Range[40, 80]], "log"]}]`
> 
> `https://wolfram.com/xid/0wgs3aa-ygy9bw`
> 
> Out[1]=
> 
> ![](https://reference.wolfram.com/language/ref/Files/ListPlot.en/O_5.png)
> 
> Plot multiple datasets in a row of panels:
> 
> In[1]:=
> 
> ![](https://reference.wolfram.com/language/ref/Files/ListPlot.en/I_6.png)
> 
> `ListPlot[{Labeled[Sqrt[Range[40]], "sqrt"], Labeled[Log[Range[40, 80]], "log"]}, PlotLayout -> "Row"]`
> 
> `https://wolfram.com/xid/0wgs3aa-0sxy9d`
> 
> Out[1]=
> 
> ![](https://reference.wolfram.com/language/ref/Files/ListPlot.en/O_6.png)
> 
> Use individual colors for each point:
> 
> Copy to clipboard.
> 
> In[1]:=
> 
> ![](https://reference.wolfram.com/language/ref/Files/ListPlot.en/I_7.png)
> 
> `https://wolfram.com/xid/0wgs3aa-7abkax`
> 
> Out[1]=
> 
![](https://reference.wolfram.com/language/ref/Files/ListPlot.en/O_7.png)