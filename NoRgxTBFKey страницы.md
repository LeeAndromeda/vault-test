---

Tags: 
aliases: 
regex-to-search: "(.*)NoRgxTBFKey(.*)" # "что-то" regex
regex-to-be-found: # "что-то" regex

Тип: конспект
Описание: -

День: 05 # Не трогать!
Месяц: 12 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Monday 5th December 2022 18:58:08

---
# [[NoRgxTBFKey страницы]]

**NoRgxTBFKey** - это ключ, который я дал всем страницам без ключа, по которому их должны контекстно искать. К сожалению, так можно найти только страницы, у которых есть поле `regex-to-be-found`.

%%## Приложения%%

## Смотрите также:

```dataview
LIST
FROM !"Obsidian/Templates"
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Dataview 