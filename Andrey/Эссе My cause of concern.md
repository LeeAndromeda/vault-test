---
Тип: ДЗ
Дисциплина: #Входящие 
Описание: -
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: 23 # Не трогать!
Месяц: 12 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Friday 23rd December 2022 14:34:30
день_завершения: 23
месяц_завершения: 12
год_завершения:  2022

# да или нет (!без ковычек!)
Выполнено: да
Результат: 

Tags: ДомашнееЗадание
---

# [[Эссе My cause of concern]]

My cause of concern is the growing confidence in the artificial intelligence's ability to drive cars autonomously.

Let me explain why. We've seen an exponencial growth of AI usage in the modern world. There are lots of AI implementations that cover many kinds of tasks from drawing beatiful pictures(DALL-E 3, Midjourney) to writing music(AIVA) and being an encyclopedia that you can talk to and ask anything(Chat-GPT). And even the harshest criticists have to admit that some of them perform extremely well. All this has led to a variety of emotions in people: from amazement to fear and most importantly - trust. We are already relying on AI assisted tools like Photoshop and Google Lens and we're not even noticing it. The other task that we are gradually starting to pass to the AI is driving cars. The problem is that the AI is not ready to safely solve this kind of task.

There were real accidents that show that. According to National Public Radio 392 car crashes in 2021 that involved automated driver-assist systems were reported. During these crashes five people were killed and six were seriosly hurt. And while not all of them happened beacuse of the AI malfunction, some did. And the biggest problem is that such systems are very hard to fix becuse of the complexity of AI development and debugging.

Another thing is that in many countries accidents are not regulated by law in any way. That's just because the technology is not mature and widespread enough. So it may lead to somebody getting into a serious trouble and there will be no help from the law for him.

Finally, I should say that the spread of the AI usage in cars and other places is inevitable. And it is not a bad thing. But the technology is not ready to get to the streets of the whole world and much more research and law regulations have to be done to prevent terrible accidents in the future. 



%%the growing problem of environmental change and the influence that burning fossil fuels has on it.

Let me explain why. I've been watching a lot of popular science videos and reading a lot of articles on this topic. Nowadays people have started to pay attention to the problem and to make some progress. But due to the political situation in the world a lot of progress was lost. Russia is one fo the biggest exporters of fossil fuels in the world. While the trade with the European countries was fine, Europeans were able to to get the amount of fossil fuels they needed to cover the shortage for a reasonable price. Nowadays, the trade with Russia is forbidden in many parts of the market and fossil fuels is one of them. %%

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
---
#ГумманитарныеНауки/Английский #frog 