---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: Лекция
Описание: -

День: 19 # Не трогать!
Месяц: 05 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-05-19

---
# [[Noun phrases]]

![[Pasted image 20230519155347.png]]

![[Pasted image 20230519155636.png]]
We also use **a** in "what a ..." closes like: 
- What a wonderful day!

![[Pasted image 20230519155840.png]]

![[Pasted image 20230519160057.png]]
![[Pasted image 20230519160512.png]]
![[Pasted image 20230519161304.png]]
![[Pasted image 20230519161733.png]]


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

> [!question]- Соурслист
> 1. 


---

#Лекция  #ГумманитарныеНауки/Английский 

---

[^def]: термин
[^que]: вопрос