# Конспект видео A Simple Guide to World Building

For a world to feel real, you should add unique calendars, a pantheon of gods, several continents full of people and cities.

If you try to make everything before the players start, you will most likely fail being overwhelmed by the difficulty of task. 

So what parts of the world should be created first?

## Lay the grounds first

1. **The Beginning** - how did the world appear in the multiverse? Your world can come from anything. Keeping it simple doesn't make it less unique.
2. **Creating Divinity** - how does divinity in your world function? Are they creators or just rulers? How involved are they in mortal affairs? Are they benevolent, malevolent or both? 
> ## Gods are personifications of ideals
3. **Creating life** - how did life begin inyour world? What race or species dominates the world now? 
> Humanoids are often created, migrated or evolved.
4. **Creating the map** - is the world similar to ours? What does the world look like and how did people affect it?
5. **Creating monsters** - what kind of wildlife does your world have? How do they interact with humanoids? How abundant are they? How intelligent are they? Are they destructive or necessary?
6. **Creating a simple story** - how the world is shaped by conflict and progress? How common is war in your world? How did each race affect your world?
> Elves barely leave a mark, dwarves mine enough to level mountains and humans build on top of everything. (Most elves build alongside nature. Dwarves dig. Plain and simple. Humans are always building)
7. **Mapping civilization** - view expansion like spilled water. Species will start in one area, build a central base and then move outward. They'll claim the resources and sometimes shift in directions that hold more. (Building will spread from the starting point on the path of least resistance and most gravity. Resources = gravity, hostiles = resistance)
8. **Involving magic** - how fantastical is your world? How common is magic? How accepted is magic? How accessible is magic?

> Quick tip for a world to feel real: stay consistent. 
> **Avoid breaking your own rules
> Write EVERYTHING down during the game**

> https://youtu.be/hVuQ8Nj7Z3I - about wars and how they go
> [[Конспект видео What is War]]

#DnD #Worldbuilding