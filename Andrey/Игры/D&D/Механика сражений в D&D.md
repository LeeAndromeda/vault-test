# Механика сражений в D&D

## Юниты

Максимальное количество юнитов с одной стороны в сражении - 15. Размеры юнитов соразмерны размерам сражения.

Юнит обладает теми же характеристиками, что и любые существа, но значения его характеристики являются средними арифметическим от значений характеристик существ, составляющих его.

Юнит так же обладает характеристикой атаки. Атака является средней арифметической от характеристик, зависящих от класса, составляющих юнит существ.

Варвар - сила;
Бард - харизма;
Жрец - мудрость;
Друид - мудрость;
Воин - сила;
Монах - ловкость;
Палладин - мудрость;
Следопыт - ловкость;
Плут - ловкость;
Чародей - харизма;
Колдун - мудрость;
Волшебник - интеллект;

## Боевая механика

Атака - это усредненная характеристика боевого взаимодействия персонажей разных классов с целью нанесения урона. При атаке юнита противника, бросается к20 и прибавляется разница **атака атакующего юнита - КД обороняющегося юнита** (пускай это будет равно X, Х ≥ 1) .

Далее: 

1. Для гуманоидов и персонажей: количество существ атакующего юнита делится на 20 и умножается на (Х + ср. уровень существ атакующего отряда/2)
2. Для монстров: количество существ атакующего юнита делится на 20 и умножается на (Х + ср. Уровень опасности),

Получается число Y. Y - число погибших существ, персонажей с уровнем 1 или монстров с уровнем опасности 1/2, во вражеском отряде. 

Т.е., если Y = 100, а отряд состоит из монстров с уровнем опасности 1, то погибает 50 монстров. Если у монстров уровень 2, то погибают 25 монстров. Если у монстров уровень 1/4, то погибают 200 монстров. 

Для персонажей, если у существ уровень 2, то погибают 50 существ. Если уровень 3, то 33 существа. Если уровень 4, то 25 существ.

> Не имеет значения, какие именно существа погибли: характеристики отряда остаются неизменными до конца боя. После конца боя смерти в отрядах по классам распределяются случайным образом.

В случае, если отряд потерял половину или больше существ, он каждый ход бросает спасбросок харизмы со сложностью 12 - мод. Харизмы отряда. В случае провала, отряд начинает бежать.

Отряд, больше чем наполовину состоящий из дальнобойных юнитов, считается дальнобойным и может атаковать на расстоянии. Дальнобойными считаются классы: следопыт, чародей, колдун, волшебник. Дальнобойный отряд может атаковать на расстоянии 12/24(с помехой).

Юниты-наездники считаются одним целым с ездовым животным и их характеристики берутся вместе средним арифметическим.

## Процесс боя

В остальном бой проходит, как обычный бой D&D. Разве что уничтоженный отряд не делает Спасброски от смерти.

