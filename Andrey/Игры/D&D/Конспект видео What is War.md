# Конспект видео What is War (Part 1)

## 1. War goals
The are a number of war goals: invasion, rebellion, succession, independence, religion, ethnicity, magic, annihilation.

## 2. Who runs the war?
Where and why are the battles waged? Who decides the meetments of war goals? Who cammands the soldiers?

> **The attacker:** has a goal sets - chooses battles, a council or ruler - commands soldiers
> **The defender:** returns fire - responss to attacks

## 3. The first move

Depends on the instigator, either fully planned like chess(provokes a devastating chain reaction) or heat of passion like a fool's errand(devastating chain reaction that devastates the attacker)

## 4. Wars are usually conventional

Conventional: what is usually done
Unconventional: the opposite of that

Unconventional methods are usually met with overwhelming opposition.

Using excessive power to make ends meet summons the excessive power police because that's not ok.

## 5. War goals (in detail)
1. Invasion
	1. Raiding
	2. Expansion
2. Civil war
	1. Rebellion
	2. War of succession
	3. War for independence
3. Religious warfare
4. Ethnic warfare
5. Magical warfare
6. War of annihilation

### 1. Invasion

**1.1. Raid**

Often waged by barbarians. Rarely larger than other wars. The goal is to get some resources and run.

**1.2. War of expansion** 

A methodical war with the goal of owning more territory. Often waged by kingdoms or nations. Battles take place at pr near the territories. The longer an invasion tales, the more time the target has to retaliate. If the invading forces overextend, the target can retaliate and crush the invader. Successful expansions struggle with loyalty and the conquered settlements can revolt.

### 2. Civil wars

**2.1. Rebellions

Armed resistance to the existing system. Rebel warfare is waged by passion and can take any form(protests, terrorism, shadow opposition-sabotage). The end goal is to reshape the system. Random rebellion battles lead to mass innocent deaths.

Rebellion outcome 1: the rebellion is destroyed, internal defences are rised, the system doesn't change or worsen.

Rebellion outcome 2: the ruler meets the rebellion's deeds and the rebellion settles.

Rebellion outcome 3(Revolution): the leader or government is destroyed and new one replaces it.

**2.2. War of succession** 

There are a number of candidates for the throne. One of them is going to become a king but others don't agree with that.

Waged to kill other candidates or strip their power to oppose you. Sometimes a regional tradition. Often with respective and expected battles.

Allied nations and factions will stand behind their candidate and offer resources to further their success.

**2.3. War for independence**

Often waged for several years. Battles are fought anywhere in the territory, usually near the borders. The military approach is direct and forceful. The rebels choose how to react. 

The victor has to outlast the opposition. 

If the separatists loose, pretty much everyone gets executed.

### 3. Religious (ideological) warfare

Any large scale disagreement can lead to armed conflict. 

Goals: to obtain land, to obtain relics, to change the religion's current state, be ause the gods command war.

### 4. Ethnic warfare

Wars, waged for the sake of cultural bias. Are either invasive or a rebellion for ethnic rights.

### 5. Magical warfare

Schools of magic disagreeing. The uprising of a new practice. Laws, preventing certain practices. Posession of magic artifacts.

Wars are wasted by independent spellcasters or conflicting collegues. Battles are much faster and more dangerous. Most battles risk unintended consequences. 

For a magic war to occure both sides should be somewhat equal in magic power.

### War of annihilation

The war with the goal of erasing the enemy.

Possible targets: race, culture, nation, kingdom, planet, ecosystem, species, universe.

Usually instigated by outsiders, alien races, mad wizards, demons and devils, dark gods.

Extraplanar tactics: emerge from an arriving point. Then spread outward.

The oppositions tactics: send powerful strike teams to stop the leader of the war. The way to stop the leader varies depending on where they are from.

The only way a war of annihilation ends is with annihilation. Monsters and dictators, who start these wars, cannot be bargained with.

# Guide to battle:
## Step 1: start with the battlefield

Base the environment on circumstance. Obstacles are more interesting than emptiness.

## Step 2: what is the battle's purpose? Who gains what and how?

## Step 3: Pull some mechanics
From existing strategy/gambling games.

Have unit tokens. Have tokens that represent sections of the army. Make the scale relative to the battle(do not create large numbers of tokens)

How big are the units and how effective sre the units?

## Step 4: Give each side a simple game plan

## Step 5: Battle lengths vary

[[Механика сражений в D&D]]

#DnD #Сторителлинг 