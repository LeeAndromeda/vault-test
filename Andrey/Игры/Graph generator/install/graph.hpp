#ifndef GRAPHCPP
#define GRAPHCPP

#include <cmath>
#include <stdlib>

class vector_2d{
    public:
    float x;
    float y;
    
    void vector_2d();
};

class Node {
    public:
    vector_2d coords;
    
    int tell_id();
    int tell_connections();
    
    void tell_position(float &x, float &y)
    {
        x = coords.x;
        y = coords.y;
    }
    
    private:
    
    int node_id;
    std::vector<int> connected_node_ids;
};

class NodeGraph {
    public:
    int tell_node_number();
    int set_node_number();
    
    int tell_max_scatter_radius();
    int set_max_scatter_radius();
    
    int tell_min_scatter_radius();
    int set_min_scatter_radius();
    
    int tell_max_node_connections();
    int set_max_node_connections();
    
    int generate_graph();
    
    private:
    int node_number;
    float max_scatter_radius;
    float min_scatter_radius;
    
    int max_node_connections;
    int min_nose_connections;
};

#endif GRAPHCPP