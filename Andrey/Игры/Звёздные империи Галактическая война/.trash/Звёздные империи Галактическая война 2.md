# Звёздные империи Галактическая война

Это пользовательское дополнение к настольной колодособирательной игре Звёздные империи.

## Нововведения
1. Звёздная карта
2. Ход игры
3. Империи и фракции империй
4. Флоты
5. Колода флота
6. Строительство баз
7. Боссы и империи ИИ
8. Сражения нескольких игроков

### 1. Звёздная карта

Действие галактической войны происходит на звёздной карте. Она содержит в себе **звезды**, **сектора** и **корридоры** между звёздами.

### 2. Ход игры

Игра проходит примерно так же, как игра в Риск:

1. Фаза Прибыли и строительства
2. Фаза Движения

В фазе **прибыли и строительства** вы сначала считаете свой доход. Он складывается из базового дохода в 3 золотых + бонусы от владения *секторами*.

В фазе **движения** вы можете двигать свои корабли на соседню звезду, если между текущей и соседней есть *корридор*.

### 3. Империи и фракции империй

Каждый игрок играет за свою империю. В начале у каждой империи есть одна звезда в секторе, **станция**[^1] на этой звезде и флот разведки(2 штурмовика,)

[^1]: Станция позволяет строить флоты и корабли на звезде, на которой она построена, а так же при сражении у этой звёзды даёт +25 к влиянию игроку, которому принадлежит. Сама сражаться не может.

#ЗИ