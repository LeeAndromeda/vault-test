> ### Intro 1-17 [17]

> ### Verse 18-25 [8]
> 
> I've been sleeping in a cardboard box
> Spending every dollar at the liquor shop
> And even though I know I haven't got a lot
> I'll try to give you love until the day you drop

> ### (intro) Проигрыш 26-32 [7]

> ### Verse 33-40 [8]
> 
> I've been training like a Pavlov dog
> Let my independence out to take a hike
> All you got to do is activate my bell
> And I'll fetch you anything you like

> ### Припев 41-48 [8]
> Going once, going twice
> Every man here has a price
> Over where? Over there, sold
> To not a single lady in here

> ### Куплет 3 49-57 [19]
> I've been in like a thousand times
> Dated every woman in the atmosphere
> I've been to every continent
> Broken all the hearts in every hemisphere
> And if I'm not the type of guy you like to circumvent
> Just remember not to love me when I disa-
>
> I graduated at the top
> I like to take advantage of the bourgeoisie
> So if you have a fantasy of being a queen
> Maybe you should blow a couple bucks on me

> ### Припев [12]
> Going once, going twice
> Won't these gentlemen suffice
> Any here? Any there?
> Any motions anywhere?
> Going twice going thrice
> Guess this means we'll go four times a lady

> ### Бридж [18]
> I promise I'll be all you'll need, ever need
> You'll never have to shop around (don't shop around)
> And I'll give you all you'll ever need, ever need
> Don't worry I will never let you down, let you down
> Don't worry I will never let you down

> ### Solo[4] + АУТРО[8] + [2] бам бам бам
> So many ladies are wanting for mates
> And the prospects are good but the date's never great
> Over here over there over where? Anywhere
> They're too busy with winning the bidding to care
> And he's sold

## Участники

- Вокал
- Подпевки
- Ритм-гитара
- Соло-гитара
- Бас-гитара
- Синтезатор