# Саморазвитие и методы саморазвития Сфера деятельности

- [[Мой психологический портрет]]

## Подсферы деятельности

- [[Гигиена продуктивной жизни]]
- [[Мотивация]]
- [[Развитие памяти Сфера деятельности]]
- [[Тайм-мэнеджмент Сфера деятельности]]
- [[Дисциплина Сфера деятельности]]
- [[Развитие интеллекта]]
- [[Нетворкинг]]
- [[Проект развития работы с Obsidian]]
- [[Самообразование]]

## Конспекты

- [[Знание]]
- [[Как изменить мышление]]
- [[Как изменить привычку]]
- [[Регистратор ритма]]
- [[Постоянство важнее непостоянной интенсивности]]
- [[Как правильно ставить цели]]
- [[Как превратить жизнь в игру, в которую не страшно проиграть]]

## Статьи 

- [Как читать книги - статья о книге "Как читать книги"](https://vc.ru/books/77161-kak-chitat-knigi-razbor-knigi)

## Видео

- [Давайте учить мастерству, а не получению оценок - Сал Кхан(Khan Academy)](https://youtu.be/-MTRxRO5SRA)
- [Как пять слов могут дать вам то, что вы хотите](https://youtu.be/L9UIF852Boo)
- [Как быть интересным и стать экстраординарным(или нет)](https://youtu.be/srmtGrEIYBs)
- [7 things that (quickly) cured my procrastination](https://youtu.be/7yLOth7SWAY)
- [Меняя правила | Дэйв Эспри](https://youtu.be/dwLDcErU57M)
- [Как работать по 4 часа в неделю | Тимоти Феррис](https://youtu.be/MfSG_iSwU6o)
- [Атомные привычки.  Как приобрести хорошие привычки и избавиться от плохих  | Джеймс Клир](https://youtu.be/byu-LCoeENQ)
- [Иди туда, где страшно. Преодолей свой страх | Джим Лоулесс](https://youtu.be/YOvZ39N4cBg )
- [ПОВЫСИТЬ ЭФФЕКТИВНОСТЬ ОБУЧЕНИЯ. Как учиться быстрее и качественнее?](https://youtu.be/ruNMS07ru8c)
- [Marty Lobdell - Study Less Study Smart](https://youtu.be/IlU-zDU6aQ0)
- [Being your Own Life Coach | John Muldoon | TEDxShanghaiAmericanSchoolPuxi](https://youtu.be/vLFxOOEyhUE)
- [5 steps to designing the life you want  | Bill Burnett | TEDxStanford](https://youtu.be/SemHh0n19LA)

## Каналы на YT

- [Khan Academy на русском](https://youtube.com/c/KhanAcademyRussian)
- [Khan Academy](https://youtube.com/c/khanacademy)

#Саморазвитие 

Дата создания: 2021-07-14 04: