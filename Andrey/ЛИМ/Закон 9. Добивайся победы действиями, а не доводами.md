**Закон 9. Добивайся победы действиями, а не доводами**

Даже самые лучшие аргументы любого спора остаются всего-навсего словами. Другое дело — поступки и демонстрация наглядного. Дайте оппонентам в буквальном смысле ощутить физически, что вы имеете в виду — это куда более мощные аргументы, чем любые слова.

#48ЗаконовВласти