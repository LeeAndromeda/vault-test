# Notion

Перед работой важно создать для себя рабочее место, и Notion (ноушен) на сегодняшний день — это лучший сервис для работы с текстом. Именно на этом сервисе и базируется данный курс. Сервисом можно пользоваться как из браузера, так и с помощью фирменных приложений:

**Android** — [](https://play.google.com/store/apps/details?id=notion.id)[https://play.google.com/store/apps/details?id=notion.id](https://play.google.com/store/apps/details?id=notion.id)

**iOS** — [](https://apps.apple.com/ru/app/notion-notes-projects-docs/id1232780281)[https://apps.apple.com/ru/app/notion-notes-projects-docs/id1232780281](https://apps.apple.com/ru/app/notion-notes-projects-docs/id1232780281)

На данный момент русского языка в сервисе нет, только английский и корейский, но он активно развивается, так что со временем наверняка появится. Пользоваться Notion можно абсолютно бесплатно — ограничений на количество блоков и текста в нем нет.

Если вам понадобится загружать какие-то тяжелые файлы, то придется переключить аккаунт на платный тариф — Personal → Personal Pro. Ознакомиться с тарифами можно вот на этом скриншоте:

![https://s3-us-west-2.amazonaws.com/secure.notion-static.com/82a7e808-7265-4dd8-9cdd-52fa25d02f39/Untitled.png](https://s3-us-west-2.amazonaws.com/secure.notion-static.com/82a7e808-7265-4dd8-9cdd-52fa25d02f39/Untitled.png)

Я использую "Personal Pro", который выходит мне в год на 48$. Это, при курсе рубля к доллару — 74 рубля, 3550~ рублей. Да, не дешево, но, как я уже сказал, для начала вам хватит и бесплатной версии. Теперь давайте разберемся, как эта махина вообще работает.

# Создание материала

Новая страница создается с помощью вот этих кнопок:

![https://s3-us-west-2.amazonaws.com/secure.notion-static.com/9dbc445e-a7d6-47fe-8ac9-e3e4f9045aa3/Untitled.png](https://s3-us-west-2.amazonaws.com/secure.notion-static.com/9dbc445e-a7d6-47fe-8ac9-e3e4f9045aa3/Untitled.png)

![https://s3-us-west-2.amazonaws.com/secure.notion-static.com/6705a515-508a-474b-9e74-0045906a4607/Untitled.png](https://s3-us-west-2.amazonaws.com/secure.notion-static.com/6705a515-508a-474b-9e74-0045906a4607/Untitled.png)

Можно кликнуть на любую, эффект будет одинаковый. Пред вами откроется окно, в котором и будет происходить вся ваша магия:

![https://s3-us-west-2.amazonaws.com/secure.notion-static.com/cead4b9f-b0c5-438c-81e1-9e543318d83f/Untitled.png](https://s3-us-west-2.amazonaws.com/secure.notion-static.com/cead4b9f-b0c5-438c-81e1-9e543318d83f/Untitled.png)

Дальше у вас будет 2 пути:

1.  Сделать эту страницу под один материал;
2.  Сделать эту страницу в виде путеводителя.

Вот так, например, у меня сделана страница, где добавлены отдельные блоки по дням недели, куда я и пишу статьи для Яндекс Дзена:

![https://s3-us-west-2.amazonaws.com/secure.notion-static.com/cc5a981f-73af-46bb-ba51-762b64003eaa/Untitled.png](https://s3-us-west-2.amazonaws.com/secure.notion-static.com/cc5a981f-73af-46bb-ba51-762b64003eaa/Untitled.png)

Удобно, лаконично и максимально практично.

Функций у Notion очень много, включая практически полноценные базы данных, которые отлично заменяют эксель. Есть также календари, борды, галереи и таймлайны, с помощью которых можно оптимизировать и упорядочить всю работу.

Эти функции в ближайшее время вам на вряд ли понадобятся — для начала освойте структуризацию текстовых материалов.

---

На предыдущую страницу — [Вдохновение](https://www.notion.so/924eeb914fe44ab2b3b37f2f5e0635f2) | На следующую страницу — [Теплые тексты](https://www.notion.so/41f062c79d974e4db14abb6941a10f37)