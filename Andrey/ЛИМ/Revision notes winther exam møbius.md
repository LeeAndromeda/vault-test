- [ ] Watch 3Blue1Brown on Linear Algebra

### Noter
- Enhedscirklen ![[Pasted image 20211129101616.png]]
- Ganger man med I roterer man med 90 grader
- Ganger man med komplekst konjugerede svarer det til Re^2 +Im^2
- Procentuel vækst skal man trække 100% fra.
- Ganger man komplekst tal på eksponentiel form med -1 svarer det til at gange hele det komplekse tal med -1 (tænk på enhedscirklen).
- Er et komplekst tal rod i et andengradspolynomium, er dens konjugerede også rod.
- Nedstigning ![[Pasted image 20211129142643.png]]
- Differentier exp ![[Pasted image 20211129145927.png]] 
- Tan differentieret = sec^2
- Sin differentieret = cos
- cos differentieret = -sin
- f^-1(f(x))=x
- Differentiation af omvendt funktion ![[Pasted image 20211129153353.png]]
- I en regulær hvis rang =n
- Et lineær system har endten 0, en, eller uendeligt mange løsninger
- (A.B)^T=A^T*B^T
- Areap parallelogram = Determinant (2X2)
- Skalarprodukt ![[IMG_20211129_174432.jpg]]
- Determinant regneregler ![[Pasted image 20211129181645.png]] ![[Pasted image 20211129181907.png]]
- Singulær er det modsatte af regulær
- Tjek om en vektor tilhører billedrummet for en afbildningsmatrix ved at se om totalmatrix A.x har en løsning
- Ved monomiebase skriver man (1,x,x^2) ned ad en søjle.
- Det gælder for egenværdier, at summen af dem, der tilhører den samme matrice, skal være lig summen af sporet af matricen
- Similær matrice: A og B har samme egenværdier

### To review:
- [ ] Differentiation af omvendte funktioner
- [x] 23
- [x] 58
- [x] 63
- [x] Nedstigningssætningen
- [x] Spørgsmål 12
- [x] Spørgsmål ål 65


#math #DTU