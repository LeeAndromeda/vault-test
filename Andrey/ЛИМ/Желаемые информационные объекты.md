---

Tags: 
aliases: 
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

Тип: конспект
Описание: -

День: 11 # Не трогать!
Месяц: 11 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Friday 11th November 2022 15:16:42

---
# [[Желаемые информационные объекты]]

**Желаемые информационные объекты** - информация, которую было бы полезно или интересно потребить, но нет никаких серьезных вынуждающих факторов для этого. 

## Приложения

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

> [!question]- Соурслист
> 1. 


---
#ПланированиеЗнаний #ВнедрениеЗнаний/ЖелаемоеЗнание