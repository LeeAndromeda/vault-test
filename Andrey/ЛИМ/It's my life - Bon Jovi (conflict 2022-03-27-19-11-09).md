
This ain't a song for the broken-hearted
No silent prayer for faith-departed
And I ain't gonna be just a face in the crowd
You're gonna hear my voice when I shout it out loud

**C5**
It's my life
............**G#5**.......**D#5**
It's now or never
............................**A#5**...........**C5**
But I ain't gonna live forever
.....................**G#5**........................**A#5**
I just want to live while I'm alive
**5.1-5.2**.......**C5**
(It's my life)
........................**G#5**...................**D#5**
My heart is like an open highway
................................**A#5**..............**C5**
Like Frankie said, "I did it my way"
.......................**G#5**....................**A#5**
I just want to live while I'm alive
**5.1**
It's 
**5.2**
my 

**Проигрыш**
life

Yeah, this is for the ones who stood their ground
For Tommy and Gina, who never backed down
Tomorrow's getting harder, make no mistake
Luck ain't even lucky, got to make your own breaks

**C5**
It's my life
............**G#5**.......**D#5**
It's now or never
............................**A#5**...........**C5**
But I ain't gonna live forever
.....................**G#5**........................**A#5**
I just want to live while I'm alive
**5.1-5.2**.......**C5**
(It's my life)
........................**G#5**...................**D#5**
My heart is like an open highway
................................**A#5**..............**C5**
Like Frankie said, "I did it my way"
.......................**G#5**....................**A#5**
I just want to live while I'm alive
**5.1**
It's 
**5.2**
my 

**Соло**(G#5-A#5-G#5-A#5) **C#5**

Better stand tall when they're calling you out
Don't bend, don't break, baby, don't back down

**C5**
It's my life
............**G#5**.......**D#5**
And it's now or never
............................**A#5**...........**C5**
'Cause I ain't gonna live forever
.....................**G#5**........................**A#5**
I just want to live while I'm alive
**5.1-5.2**.......**C5**
(It's my life)
............**G#5**.......**D#5**
My heart is like an open highway
............................**A#5**...........**C5**
Like Frankie said, "I did it my way"
.....................**G#5**........................**A#5**
I just want to live while I'm alive
**5.1-5.2**.......**C5**
(It's my life)
............**G#5**.......**D#5**
And it's now or never
............................**A#5**...........**C5**
I ain't gonna live forever
.....................**G#5**........................**A#5**
I just want to live while I'm alive
**5.1-5.2**.......**C5**
(It's my life)
............**G#5**.......**D#5**
My heart is like an open highway
............................**A#5**...........**C5**
Like Frankie said, "I did it my way"
.....................**G#5**........................**A#5**
I just want to live while I'm alive
**5.1-5.2**.......**silence**
'Cause it's my life

## Разобранные партии

https://www.songsterr.com/a/wsa/bon-jovi-its-my-life-bass-tab-s3048

1. Вокал
2. Соло-гитара
3. Ритм-гитара
4. Бас гитара
5. Барабаны
- El. Piano
- Хор