# Не рискуй большим, чем можешь позволить потерять
В идеале ставка не должна превышать 5% от капитала. Но даже сам игровой капитал это четко отведенная сумма из твоего бюджета. Больше нее ты обязан не вкладывать, чтобы не потерять неигровые деньги. Будь терпелив.

### Смотрите также:
[[Индивидуальная психология по Элдеру]](Индивидуальная психология по Элдеру.md)

#АЭлдер #Трейдинг