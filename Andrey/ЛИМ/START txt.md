# START

Хотите научиться писать тексты, которые, словно магнит, будут притягивать внимание обывателей? Хотите дополнительно зарабатывать с помощью текстов столько, сколько зарабатывают некоторые бизнесы? Тогда этот курс то, что вам нужно.

TEDREX — это симбиот, который содержит в себе информацию как для простого текста (статей, постов в блоги), так и для продающего текста. Изучив его от корки до корки, вы научитесь писать любые тексты на любые темы, а также строить продажи там, где никто бы и не додумался.

**Изучать курс следует в порядке распределения блоков.**

---

На следующую страницу — [Почему именно буковки?](https://www.notion.so/a2fca2b952414a3fb4e03ac280a12c78)