---
Тип: Задача
Описание: -
Исполнители: 
Проект: PKM

Дисциплина: Саморазвитие
Tags: Задача
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

День: 09 # Не трогать!
Месяц: 11 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Wednesday 9th November 2022 22:10:17

день_завершения: нет
месяц_завершения: 12
год_завершения: 2022
дата_завершения: 2022-12-нет

Выполнено: нет
Результат: 
---

```dataview
TABLE день_завершения as "День истечения", месяц_завершения as "Месяц истечения", Проект, Выполнено, Результат, Количество_выходящих_задач as "Количество выходящих задач"
WHERE contains(file.name, "Разработать другие полезные виды задач для ежедневных записей и работы с проектами")
limit 1
```
# [[Разработать другие полезные виды задач для ежедневных записей и работы с проектами]]

#Входящие 

## Результат:



## Следующая задача:

#Входящие 

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```


---
#PKM/obsidian 
 