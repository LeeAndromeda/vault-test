## Разобранные партии
#### 1-2 такт 779
Oh, I miss the misery

#### Вступление

![[IMG_20220329_161252.jpg]]

#### Куплет

![[Screenshot_2022-03-29-16-13-35-388_com.brave.browser.jpg]]

I've been a mess since you stayed
I've been a wreck since you changed
Don't let me get in your way
I miss the lies and the pain
The fights that keep us awake
I'm telling you

#### Припев

![[Screenshot_2022-03-29-16-14-15-350_com.brave.browser.jpg]]

I miss the bad things
The way you hate me
I miss the screaming
The way that you blame me
Miss the phone calls
When it's your fault
I miss the late nights
Don't miss you at all
I like the kick in the face
And the things you do to me
I love the way that it hurts
I don't miss you, I miss the misery

#### Куплет 2

![[Screenshot_2022-03-29-16-15-05-438_com.brave.browser.jpg]]

I've tried but I just can't take it
I'd rather fight than just fake it ('cause I like it rough)
You know that I've had enough
I dare ya to call my bluff
Can't take too much of a good thing
I'm telling you

#### Припев

I miss the bad things
The way you hate me
I miss the screaming
The way that you blame me
Miss the phone calls
When it's your fault
I miss the late nights
Don't miss you at all
I like the kick in the face
And the things you do to me
I love the way that it hurts
I don't miss you, I miss the misery

#### Breakdown

![[Screenshot_2022-03-29-16-17-05-558_com.brave.browser.jpg]]

Just know that I'll make you hurt
(I miss the lies and the pain what you did to me)
When you tell me you'll make it worse
(I'd rather fight all night than watch the TV)
I hate that feeling inside
You tell me how hard you'll try
But when we're at our worst
I miss the misery

#### бридж

![[IMG_20220329_162124.jpg]]

I miss the bad things
The way you hate me
I miss the screaming
The way that you blame me

I miss the rough sex
Leaves me a mess
I miss the feeling of pains in my chest
Miss the phone calls
When it's your fault
I miss the late nights
Don't miss you at all
I like the kick in the face
And the things you do to me
I love the way that it hurts

![[IMG_20220329_164254.jpg]]

I don't miss you, I miss the misery
I don't miss you, I miss the misery
I don't miss you, I miss the misery


https://www.songsterr.com/a/wsa/halestorm-i-miss-the-misery-bass-tab-s389141

1. Вокал
2. Бэк-вокал
3. Соло-гитара
4. Ритм-гитара
5. Бас гитара
6. Барабаны