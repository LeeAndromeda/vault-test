---
Тип: Задача
Описание: -
Исполнители: 
Проект: ВнедрениеЗнаний

Дисциплина: #Входящие 
Tags: Задача
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: 15 # Не трогать!
Месяц: 02 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-02-15

дата_завершения: "2023-нет-нет"

Выполнено: нет
Результат: 
---

```dataview
TABLE день_завершения as "День истечения", месяц_завершения as "Месяц истечения", Проект, Выполнено, Результат, Количество_выходящих_задач as "Количество выходящих задач"
WHERE Дата_создания = this.Дата_создания
limit 1
```
# [[Work smart, not hard]]

> [!example]- Чеклист знания
> - Получение источников и ссылок
> 	- [ ] Получение одной ссылки
> 	- [ ] Получение ссылок на разные источники
> - Конспектирование
> 	- [ ] Достаточное для использования конспектирование
> 	- [ ] Полное конспектирование
> - Создание справок
> 	- [ ] Справки практического применения
> 	- [ ] Список терминов
> 	- [ ] Определение терминов из списка терминов
> 	- [ ] Список связанных тем(если нет внутри текста)
> - Внедрение в базу знаний
> 	- [ ] Создание связей с оригинальной страницей (этой)
> 	- [ ] Разбиение на подтемы
> 	- [ ] Атомизация - (по необходимости) разбиение конспекта по подтемам на страницы
> - [ ] Внедрение в практику
> 

**Содержание потом вырезается, все содержимое конспекта удаляется и содержание вставляется по шаблону входящего конспекта.**

# Содержание 

Work Smart, Not Hard.


kollektrazum@gmail.com
There is a prase that says - work smart, not hard. What books or articles explain this prahse in detail, explain gow to do it and gives examples?

The phrase "work smart, not hard" is a common saying that emphasizes the importance of efficiency and effectiveness in achieving goals. There are many books and articles that explore this concept in detail, and provide practical advice on how to apply it in different areas of life. Here are a few recommendations:

"The 4-Hour Work Week" by Timothy Ferriss: This book is a classic in the productivity genre, and offers a step-by-step guide on how to achieve more with less effort. Ferriss shares his own experiences and provides case studies of successful entrepreneurs who have applied his principles to create more free time and financial independence.

"Essentialism: The Disciplined Pursuit of Less" by Greg McKeown: In this book, McKeown argues that success in any field requires focusing on the essential things and eliminating the non-essential. He provides practical strategies for prioritizing your time, energy, and resources, and shows how to say no to distractions and low-value activities.

"The Lean Startup" by Eric Ries: This book is a must-read for anyone interested in entrepreneurship or innovation. Ries outlines a methodology for building successful businesses by testing assumptions, iterating quickly, and learning from failure. He emphasizes the importance of working smart, not hard, by focusing on the most important metrics and avoiding wasted effort.

"Deep Work" by Cal Newport: This book explores the idea of working deeply and achieving a state of flow, where you can produce high-quality work in less time. Newport provides practical advice on how to eliminate distractions, prioritize your work, and cultivate the habits that lead to deep work.

These books offer a range of perspectives on the idea of working smart, not hard, and provide actionable advice for applying these principles in your own life. By focusing on the most important tasks and eliminating distractions, you can achieve more in less time, and enjoy greater success and fulfillment.






ChatGPT Feb 13 Version. Free Research Preview. Our goal is to make AI systems more natural and safe to interact with. Your feedback will help us improve.



> [!question]- Соурслист
> 1. 
---

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

---
#ВнедрениеЗнаний 
#Саморазвитие/Эффективность

---

[^def]: термин
[^que]: вопрос