# [[Анализ Foster the people - The Truth]]

Куплет начинается с повторяющихся всплесков довольно острого синтезатора, похожего на жужжание тока, а так же с очень ритмичных постукиваний ударных в каждую восьмую долю с выделением акцентами слабых долей, а так же с воздушного, но пока частотно-сосрелоточенного, наполняющего спектр голоса, выполняющего сразу и мелодическую роль, и роль наполнения.

Переход наполнения выполнен в виде акцентированной тишины, в которой звучит только голос, говорящий идею этой песни - "And I, I only want the truth". 

#МузыкальныйАнализ 