---

Tags: 
aliases: 
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

Тип: конспект
Описание: -

День: 11 # Не трогать!
Месяц: 11 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Friday 11th November 2022 15:18:30

---
# [[Возможно интересующий информационный объект]]

**Возможно интересующий информационный объект** - информация, которая может быть полезна, однако ее ценность и содержание содержат некоторую неопределенность.

## Приложения

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

> [!question]- Соурслист
> 1. 


---
#ПланированиеЗнаний #ВнедрениеЗнаний/ВозможноИнтересующееЗнание