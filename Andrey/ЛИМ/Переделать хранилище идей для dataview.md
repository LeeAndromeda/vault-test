---
Тип: Задача
Описание: -
Исполнители: 
Проект: 

Дисциплина: PKM
Tags: Задача
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

День: 06 # Не трогать!
Месяц: 11 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Sunday 6th November 2022 18:38:24

день_завершения: 9
месяц_завершения: 03
год_завершения: 2023
дата_завершения: 2023-1-9

Выполнено: да
Результат: 
---

```dataview
TABLE день_завершения as "День истечения", месяц_завершения as "Месяц истечения", Проект, Выполнено, Результат, Количество_выходящих_задач as "Количество выходящих задач"
WHERE contains(file.name, "Переделать хранилище идей для dataview")
limit 1
```
# [[Переделать хранилище идей для dataview]]

[[Хранилище идей old]] 
[[Хранилище идей]]

## Результат:



## Следующая задача:

#Входящие 

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

---
#PKM/obsidian 