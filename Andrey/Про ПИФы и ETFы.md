# Про ПИФы и ETFы

**Инвестиционный пай** — (в соответствии с Федеральным законом N 156-ФЗ «Об инвестиционных фондах» от 29 ноября 2001 г.) именная ценная бумага, удостоверяющая долю её владельца в праве собственности на имущество, составляющее паевой инвестиционный фонд, право требовать от управляющей компании надлежащего доверительного управления паевым инвестиционным фондом, право на получение денежной компенсации при прекращении договора доверительного управления паевым инвестиционным фондом со всеми владельцами инвестиционных паев этого паевого инвестиционного фонда (прекращении паевого инвестиционного фонда). (https://ru.wikipedia.org/wiki/%D0%98%D0%BD%D0%B2%D0%B5%D1%81%D1%82%D0%B8%D1%86%D0%B8%D0%BE%D0%BD%D0%BD%D1%8B%D0%B9_%D0%BF%D0%B0%D0%B9?wprov=sfla1)

## Что такое ETF?

ETF (Exchange Traded Fund) - биржевой фонд, паи или акции которого торгуются на бирже. 

ETF обладает признаками фондов, т.е. отражает стоимость какого-то набора активов.
Также ETF обладает признаками акции, т.е. цена пая меняется в режиме реального времени.

Суть ETF.
ETF копирует различные фондовые индексы, в которые могут входить акции, облигации, товарные активы и др.
Наборы этих активов могут быть объединены по различным признакам: по отраслям, странам, регионам, классам активов и т.д.
Стоимость ETF равна стоимости базовых ценных бумаг фондового индекса, который повторяет ETF

Какие активы входят в ETF всегда можно узнать на сайте провайдера фондового индекса или на сайтах компаний, создающих эти ETF.

Основные отличия ETF от ПИФов.
- стоимость ETF меняется в режиме реального времени;
- нельзя купить дробное кол-во ETF;
- нельзя обменивать паи ETF;
- ETF можно купить только на бирже;
- ETF могут выплачивать дивиденды;
- у ETF низкие комиссионные за управление.

## Плюсы и минусы инвестирования в ETF

**Плюсы:**
- использование ETFs при составлении инвестиционного портфеля открывает практически безграничные возможности диверсификации рисков;
- ETFs отличаются низкой комиссией за управление.
Минусы:
- Большое количество ETFs и похожих инструментов за рубежом;
- Языковой барьер при инвестировании через зарубежного брокера;
- Очень маленькое количество ETFs на Московской бирже.

**Где и как можно купить ETFs?**
- на Московской бирже через российского брокера;
- на Санкт-Петербургской бирже через российского брокера;
- на зарубежных биржах через дочерние компании российских брокеров;
- на зарубежных биржах через иностранных брокеров.

[КК Финансовая Независимость YT канал](КК%20Финансовая%20Независимость%20YT%20канал.md)

#инвестиции