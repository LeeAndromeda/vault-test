# Список плагинов, которые будут использоваться в улучшении научного процесса

- Annotator
- Graph Analysis
- Homepage
- Journey
- Media Extended
- Obsidian charts
- Obsidian leaflet
- Random structural diary
- Recall
- Vocabulary view

## Annotator

Плагин для чтения и комментирования файло формата PDF и EPUB.

Файл открывается для аннотации следующей командой: 

```md
---
annotation-target: https://arxiv.org/pdf/2104.13478.pdf
---
```

Чтобы открыть режим аннотации, нужно эту комманду ввести в отдельном файле.

- [[Пример аннотации]]

Будет использоваться для конспектирования книг в Obsidian

> ## Graph Analysis
> 
> Почему-то не робит
> 
> ### Типы Анализов
> 
> 1. Similarity
> 2. Link Prediction
> 3. Co-Citations
> 4. Community Detection
> 
> ### 1. Схожесть
> 
> **Схожесть** - это мера того, насколько похожи записи, в основе которой лежит сравнение их связей(ссылок) в графе. 
> 
> > Связано:[[Jaccard Similarity measure]]
> 
> ### 2. Предсказание ссылок
> 
> **Предсказание ссылок** - это вероятность того, что два конспекта долджны быть соединены ссылкой, в основе которой лежат другие их связи на графе.
> 
> ### 3. Соупоминания
> 
> **Соупоминание** - это ссылки на два разных файла, находящиеся в одном файле.
> 
> ### 4. Обнаружение сообществ
> 
> Эти алгоритмы пытаются найти группы схожих конспектов.


## Journey

Очень интересный плагин для поиска кратчайшего пути по ссылкам, тегам, обратным ссылкам(настраивается) между двумя записями. Из таких «путешствий», в честь которых и назван плагин, можно почерпнуть много интересного.

> ## Media Extended

## Obsidian Charts

1. Open command palette
2. Enter "Obsidian charts"
3. Use

```chart
type: polarArea
labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
series:
  - title: Russia
    data: [12, 15, 16, 17, 14, 13, 19, 21]
  - title: toto
    data: [15, 15, 21, 0, 0, 0, 3, 75]
  - title: Afrcia
    data: [1, 4, 9, 16, 25, 36, 49, 64]
tension: 0.39
width: 86%
labelColors: true
fill: true
beginAtZero: false
```

## Obsidian Leaflet

> ### ВАЖНАЯ ИНФОРМАЦИЯ ПО ИСПОЛЬЗОВАНИЮ ПЛАГИНА
>
> Создав метку на картинке, важно занести ее в код страницы в меню нажатия правой кнопки мыши, иначе изменение меток повлияет на все итерации Leaflet в различных конспектах. 
> 
> Вышеуказанную операцию назовем **закреплением  LFL-метки**.
> 
> После закрепления LFL-метки она пропадет с остальных итераций Leaflet, а её редактирование может быть осуществлено только в режиме редактора obsidian в блоке кода плагина, в котором она была закреплена.

```leaflet
id: leaflet-map
image: [[Диапазоны Оркестровых.jpg]]
height: 500px
lat: 50
long: 50
minZoom: 1
maxZoom: 10
defaultZoom: 5
unit: meters
scale: 1
marker: default,-0.19111327826976776,0.5769275348895402,Малая флейта *
darkMode: false
```

> ## Random structural diary

## Recall

Плагин для интервального повторения.

Есть две основные комманды:

1. Start/stop tracking note

> Заносит запись в список повторяемых

2. Review

> Начинает проверку

Используются различные алгоритмы. Рекоммендую ознакомиться с описанием плагина.

## Vocabulary view

```vocaview-list1
	word1: explanation1
	word2: explanation2
	word3: explanation3
	word4: explanation4
```

### Blocktypes

There are 3 types of block available:

-   **list**: Preview the words as a list. All words are shown.
-   **choice**: Preview the words in a single choice queston style. One randomly picked word at a time.
-   **card**: Preview the words as a card. One randomly picked word at a time.

### Subtypes

For each block type, 3 subtypes are available:

-   1: Show word, hide explanation.
-   2: Show explanation, hide word.
-   3: Randomly mix subtype 1 and 2.

#Worldbuilding #obsidian #туториал 