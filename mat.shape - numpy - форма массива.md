---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 20 # Не трогать!
Месяц: 11 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-11-20

---
# [[mat.shape - numpy - форма массива]]


Это свойство объекта `numpy.ndarray`, которое хранит в себе информацию о количестве элементов для вектора, о количестве строк и столбцов для матрицы и т.д.

![[Pasted image 20231120115419.png]]
## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. Otus


---
#Python/NumPy 

---

[^def]: термин
[^que]: вопрос