> [!bg-plate]
> Plot a complex function with zeros at ![](https://reference.wolfram.com/language/ref/Files/ComplexPlot.en/18.png) and poles at ![](https://reference.wolfram.com/language/ref/Files/ComplexPlot.en/19.png):
> 
> In[1]:=
> 
> ![](https://reference.wolfram.com/language/ref/Files/ComplexPlot.en/I_1.png)
> 
> `ComplexPlot[(z^2 + 1)/(z^2 - 1), {z, -2 - 2 I, 2 + 2 I}]`
> 
> `https://wolfram.com/xid/0bdo3xzoi-kiedlx`
> 
> Out[1]=
> 
> ![](https://reference.wolfram.com/language/ref/Files/ComplexPlot.en/O_1.png)
> 
> Include a legend showing how the colors vary from ![](https://reference.wolfram.com/language/ref/Files/ComplexPlot.en/20.png) to ![](https://reference.wolfram.com/language/ref/Files/ComplexPlot.en/21.png):
> 
> In[1]:=
> 
> ![](https://reference.wolfram.com/language/ref/Files/ComplexPlot.en/I_2.png)
> 
> `ComplexPlot[(z^2 + 1)/(z^2 - 1), {z, -2 - 2 I, 2 + 2 I}, PlotLegends -> Automatic]`
> 
> `https://wolfram.com/xid/0bdo3xzoi-02fsmi`
> 
> Out[1]=
> 
> ![](https://reference.wolfram.com/language/ref/Files/ComplexPlot.en/O_2.png)
> 
> Use color shading to highlight features of the function:
> 
> In[1]:=
> 
> ![](https://reference.wolfram.com/language/ref/Files/ComplexPlot.en/I_3.png)
> 
> `ComplexPlot[(z^2 + 1)/(z^2 - 1), {z, -2 - 2 I, 2 + 2 I}, ColorFunction -> "CyclicLogAbsArg"]`
> 
> `https://wolfram.com/xid/0bdo3xzoi-bh16bh`
> 
> Out[1]=
> 
![](https://reference.wolfram.com/language/ref/Files/ComplexPlot.en/O_3.png)


#Mathematica 