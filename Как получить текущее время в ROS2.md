---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 19 # Не трогать!
Месяц: 05 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-05-19

---
# [[Как получить текущее время в ROS2]]


Для этого вам необходимо заемнить вот эту часть - `std::shared_ptr<rclcpp::Node>()` - на ваш указатель на нод.

```cpp
rosmsg.header.set__stamp(std::shared_ptr<rclcpp::Node>()->get_clock()->now());
```

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#ROS2

---

[^def]: термин
[^que]: вопрос