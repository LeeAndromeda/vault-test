---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 21 # Не трогать!
Месяц: 11 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-11-21

---
# [[numpy.cov - ковариация двух рядов]]


Ковариация - это статистическая мера, количественно определяющая степень совместного изменения двух переменных. Она дает представление о направлении (положительном или отрицательном) и силе линейной связи между переменными. Формула для выборочной ковариации между двумя переменными XX и YY имеет вид:

cov(X,Y)=∑i=1N(Xi-Xˉ)(Yi-Yˉ)N-1cov(X,Y)=N-1∑i=1N(Xi-Xˉ)(Yi-Yˉ)

В этой формуле вычисляется среднее произведение разностей между отдельными точками данных и их соответствующими средними. Результат масштабируется на N-1N-1 для учета изменчивости выборки.

Ковариационная матрица расширяет эту концепцию на несколько переменных. Она представляет собой квадратную матрицу, каждый элемент которой отражает ковариацию между соответствующими парами переменных. Положительная ковариация указывает на то, что переменные имеют тенденцию к совместному увеличению или уменьшению, в то время как отрицательная ковариация предполагает обратную зависимость.

Функция numpy.cov эффективно вычисляет ковариационную матрицу. При применении к двумерному массиву каждая строка или столбец представляет собой переменную, а функция возвращает матрицу ковариаций. Диагональные элементы матрицы соответствуют дисперсиям отдельных переменных, а внедиагональные элементы представляют собой ковариации между парами переменных.

Таким образом, ковариация является фундаментальным статистическим инструментом для понимания того, как изменяются переменные, а ковариационная матрица представляет эти отношения в краткой и информативной форме.

## Пример

Рассмотрим пример в контексте робототехники, где у нас есть данные о положении двух роботов в двумерном пространстве с течением времени. С помощью numpy.cov мы рассчитаем ковариационную матрицу и визуализируем ее с помощью тепловой карты в Matplotlib.

```python
import numpy as np
import matplotlib.pyplot as plt

# Generate example data for two robots' x and y positions with high covariance
np.random.seed(42)
time_steps = 100
robot1_x = np.linspace(0, 10, time_steps) + np.random.normal(0, 1, time_steps)
robot1_y = 2 * robot1_x + np.random.normal(0, 2, time_steps)

# Increase the dependency between robot2_x and robot2_y
robot2_x = 0.8 * robot1_x + np.random.normal(0, 1, time_steps)
robot2_y = -0.8 * robot1_y + np.random.normal(0, 1, time_steps)

# Stack variables into a 2D array
robot_positions = np.vstack((robot1_x, robot1_y, robot2_x, robot2_y))

# Calculate the covariance matrix
cov_matrix = np.cov(robot_positions)

# Visualize the covariance matrix as a heatmap
plt.imshow(cov_matrix, cmap='seismic', interpolation='none')
plt.colorbar(label='Covariance')
plt.title('Covariance Matrix of Robot Positions with High Covariance')
plt.xticks(np.arange(4), ['Robot1_X', 'Robot1_Y', 'Robot2_X', 'Robot2_Y'])
plt.yticks(np.arange(4), ['Robot1_X', 'Robot1_Y', 'Robot2_X', 'Robot2_Y'])

# Show the plot
plt.show()
```

![[Pasted image 20231121172130.png]]

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Python/NumPy 

---

[^def]: термин
[^que]: вопрос