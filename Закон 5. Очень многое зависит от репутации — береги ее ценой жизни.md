**Закон 5. Очень многое зависит от репутации — береги ее ценой жизни**

Репутация — краеугольный камень власти, сокровище, которое необходимо накапливать и тщательно хранить. Репутацию можно построить на одном безукоризненном качестве, оно станет чем-то вроде визитной карточки. Не заботясь о своем образе, вы заставляете других решать за вас. Сделайте свою репутацию непоколебимой. Один из мощных инструментов в играх власти — атака на репутацию соперника, особенно если он сильнее. Найдя брешь в его репутации, предоставьте общественному мнению расправиться с ней. Из этого закона нет исключений, репутация важна при любых обстоятельствах.

#48ЗаконовВласти