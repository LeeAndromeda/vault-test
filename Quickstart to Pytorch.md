---
Тип: Задача
alias: [Pytorch быстрый старт]
Описание: -
Исполнители: 
Проект: ВнедрениеЗнаний

Tags: Задача
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: 16 # Не трогать!
Месяц: 06 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-06-16

дата_завершения: "2023-06-23"

Выполнено: нет
Результат: 
---

# [[Quickstart to Pytorch]]

> [!danger] Обязательный чеклист
> - [ ] Получение одной ссылки
> - [ ] Расставить теги 


> [!example]+  Чеклист знания
> - Получение источников и ссылок
> 	- [ ] Получение ссылок на разные источники
> - Конспектирование
> 	- [ ] Достаточное для использования конспектирование
> 	- [ ] Полное конспектирование
> - Создание справок
> 	- [ ] Справки практического применения
> 	- [ ] Список терминов
> 	- [ ] Определение терминов из списка терминов
> 	- [ ] Список связанных тем(если нет внутри текста)
> - Внедрение в базу знаний
> 	- [ ] Создание связей с оригинальной страницей (этой)
> 	- [ ] Разбиение на подтемы
> 	- [ ] Атомизация - (по необходимости) разбиение конспекта по подтемам на страницы
> - [ ] Внедрение в практику
> 

<mark style="background: #FF5582A6;">**Содержание потом вырезается, все содержимое конспекта удаляется и содержание вставляется по шаблону входящего конспекта.**</mark> 

# Содержание 

> [!info]- Code from the tutorial
> ```python 
> import torch
> from torch import nn
> from torch.utils.data import DataLoader
> from torchvision import datasets
> from torchvision.transforms import ToTensor
> 
> 
> 
> # Download training data from open datasets.
> training_data = datasets.FashionMNIST(
>     root="data",
>     train=True,
>     download=True,
>     transform=ToTensor(),
> )
> 
> # Download test data from open datasets.
> test_data = datasets.FashionMNIST(
>     root="data",
>     train=False,
>     download=True,
>     transform=ToTensor(),
> )
> 
> # Define the batch size
> batch_size = 64
> 
> # Create data loaders.
> train_dataloader = DataLoader(training_data, batch_size=batch_size)
> test_dataloader = DataLoader(test_data, batch_size=batch_size)
> 
> 
> for X, y in test_dataloader:
>     print(f"Shape of X [N, C, H, W]: {X.shape}")
>     print(f"Shape of y: {y.shape} {y.dtype}")
>     break
> 
> 
> # Get cpu, gpu or mps device for training.
> device = (
>     "cuda"
>     if torch.cuda.is_available()
>     else "mps"
>     if torch.backends.mps.is_available()
>     else "cpu"
> )
> print(f"Using {device} device")
> 
> 
> 
> 
> # Define model
> class NeuralNetwork(nn.Module):
>     def __init__(self):
>         super().__init__()
>         self.flatten = nn.Flatten()
>         self.linear_relu_stack = nn.Sequential(
>             nn.Linear(28*28, 512),
>             nn.ReLU(),
>             nn.Linear(512, 512),
>             nn.ReLU(),
>             nn.Linear(512, 10)
>         )
> 
>     # Define how data goes through the model
>     def forward(self, x):
>         x = self.flatten(x)
>         logits = self.linear_relu_stack(x)
>         return logits
> 
> 
> # Load the model to the device
> model = NeuralNetwork().to(device)
> print(model)
> 
> # Choos the loss function and the optimizers
> loss_fn = nn.CrossEntropyLoss()
> optimizer = torch.optim.SGD(model.parameters(), lr=1e-3)
> 
> 
> 
> # Define training function
> def train(dataloader, model, loss_fn, optimizer):
>     size = len(dataloader.dataset)
>     model.train()
>     for batch, (X, y) in enumerate(dataloader):
>         X, y = X.to(device), y.to(device)
> 
>         # Compute prediction error
>         pred = model(X)
>         loss = loss_fn(pred, y)
> 
>         # Backpropagation
>         loss.backward()
>         optimizer.step()
>         optimizer.zero_grad()
> 
>         if batch % 100 == 0:
>             loss, current = loss.item(), (batch + 1) * len(X)
>             print(f"loss: {loss:>7f}  [{current:>5d}/{size:>5d}]")
> 
> 
> 
> # Define testing function
> def test(dataloader, model, loss_fn):
>     size = len(dataloader.dataset)
>     num_batches = len(dataloader)
>     model.eval()
>     test_loss, correct = 0, 0
>     with torch.no_grad():
>         for X, y in dataloader:
>             X, y = X.to(device), y.to(device)
>             pred = model(X)
>             test_loss += loss_fn(pred, y).item()
>             correct += (pred.argmax(1) == y).type(torch.float).sum().item()
>     test_loss /= num_batches
>     correct /= size
>     print(f"Test Error: \n Accuracy: {(100*correct):>0.1f}%, Avg loss: {test_loss:>8f} \n")
> 
> 
> 
> # Load the network
> model = NeuralNetwork().to(device)
> model.load_state_dict(torch.load("model.pth"))
> print("Model loaded!")
> 
> 
> # Train the network
> epochs = 3
> for t in range(epochs):
>     print(f"Epoch {t+1}\n-------------------------------")
>     train(train_dataloader, model, loss_fn, optimizer)
>     test(test_dataloader, model, loss_fn)
> print("Done!")
> 
> # Save the network
> torch.save(model.state_dict(), "model.pth")
> print("Saved PyTorch Model State to model.pth")
> 
> 
> # Use the model
> classes = [
>     "T-shirt/top",
>     "Trouser",
>     "Pullover",
>     "Dress",
>     "Coat",
>     "Sandal",
>     "Shirt",
>     "Sneaker",
>     "Bag",
>     "Ankle boot",
> ]
> 
> model.eval()
> x, y = test_data[0][0], test_data[0][1]
> with torch.no_grad():
>     x = x.to(device)
>     pred = model(x)
>     predicted, actual = classes[pred[0].argmax(0)], classes[y]
>     print(f'Predicted: "{predicted}", Actual: "{actual}"')
> ```

## NN development pipeline in Pytorch

### 1. Loading data

1. `Dataset`
	1. `torch.utils.data.Dataset`
	2. `Dataset` stores the samples and their corresponding labels
2. `DataLoader`
	1. `torch.utils.data.DataLoader`
	2. `DataLoader` wraps an iterable around the `Dataset`

***Every TorchVision `Dataset` includes two arguments: `transform` and `target_transform` to modify the samples and labels respectively.***

***We pass the `Dataset` as an argument to `DataLoader`.***

This wraps an iterable over our dataset, and supports automatic batching, sampling, shuffling and multiprocess data loading. Here we define a batch size of 64, i.e. each element in the dataloader iterable will return a batch of 64 features and labels.

Read more about [loading data in PyTorch](https://pytorch.org/tutorials/beginner/basics/data_tutorial.html).

#### Kinds of data

> [!wow] Этот источник содержит направляющую информацию
> Этот источник соедержит ссылку на источник по теме, которая затрагивается или используется в данной, но не объясняется на достаточном уровне. Он может послужить как необходимым знанием, так и богатым источником контекста
> 
> #ВнедрениеЗнаний/РасширяющееЗнание/Тропинка

PyTorch offers domain-specific libraries such as [TorchText](https://pytorch.org/text/stable/index.html), [TorchVision](https://pytorch.org/vision/stable/index.html), and [TorchAudio](https://pytorch.org/audio/stable/index.html), all of which include datasets. For this tutorial, we will be using a TorchVision dataset.

```python
import torch
from torch import nn
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision.transforms import ToTensor
```

The `torchvision.datasets` module contains `Dataset` objects for many real-world vision data like CIFAR, COCO ([full list here](https://pytorch.org/vision/stable/datasets.html)). In this tutorial, we use the FashionMNIST dataset.

### 2. Creating a model

> [!wow] Этот источник содержит направляющую информацию
> Этот источник соедержит ссылку на источник по теме, которая затрагивается или используется в данной, но не объясняется на достаточном уровне. Он может послужить как необходимым знанием, так и богатым источником контекста
> 
> #ВнедрениеЗнаний/РасширяющееЗнание/Тропинка

- To create a neural network in Pytorch, we create a class that inherits from nn.Module. 
- Then we define the layers in the `__init__()` function.
- Then we define how data goes through the network in the `forward()` function
- Then, if available, we move the network to a GPU or MPS to accelerate the learning process and the operational process

> [!warning]
> To use the model, we pass it the input data. This executes the model’s `forward`, along with some [background operations](https://github.com/pytorch/pytorch/blob/270111b7b611d174967ed204776985cefca9c144/torch/nn/modules/module.py#L866). ***<mark style="background: #FFB86CA6;">Do not call `model.forward()` directly!</mark> ***


### 3. Optimize the model parameters

> [!wow] Этот источник содержит направляющую информацию
> Этот источник соедержит ссылку на источник по теме, которая затрагивается или используется в данной, но не объясняется на достаточном уровне. Он может послужить как необходимым знанием, так и богатым источником контекста
> 
> #ВнедрениеЗнаний/РасширяющееЗнание/Тропинка

- For that two key concepts are used:
	- a **[loss function](https://pytorch.org/docs/stable/nn.html#loss-functions)**
	- an **[optimizer](https://pytorch.org/docs/stable/optim.html)**

Then we need to define a `train()` function and a `test()` function.

In a single training loop the model makes predictions on the training dataset(fed to it in **batches**) and *backpropagates* the **prediction error** to adjust the model's parameters.

The testing function tests the model against the training dataset and ensures that the model continues learning.

The training process then goes in iterations(**epochs**). During each epoch the model learns parameters to make better predictions. We print the models *accuracy* and *loss* to be able to watch it's development. We would like to see accuracy increase and loss decrease with each epoch.

Read more about [Training your model](https://pytorch.org/tutorials/beginner/basics/optimization_tutorial.html).

### 4. Save the model

A common way to save the model is to serialize its internal state dictionary:

```python
torch.save(model.state_dict(), "model.pth")
print("Saved PyTorch Model State to model.pth")
```

### 5. Load the model

The process of loading the model includes recreating its structure and loading the weights into it:
```python
model = NeuralNetwork().to(device)
model.load_state_dict(torch.load("model.pth"))
```

This model is now ready to make predictions.

Read more about [Saving & Loading your model](https://pytorch.org/tutorials/beginner/basics/saveloadrun_tutorial.html).

## Смотрите также:


> [!question]- Соурслист
> 1. https://pytorch.org/tutorials/beginner/basics/quickstart_tutorial.html
> 2. [[Quickstart to Pytorch.pdf]]

> [!quote]- Контекст
> ```dataview
> LIST 
> FROM !outgoing([[]])
> WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
> ```

---
#ВнедрениеЗнаний 
#pytorch 

---

[^def]: термин
[^que]: вопрос