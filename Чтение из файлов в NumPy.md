---
Тип: Задача
Описание: -
Исполнители: 
Проект: ВнедрениеЗнаний

Tags: Задача
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: 28 # Не трогать!
Месяц: 03 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-03-28

дата_завершения: "2023-нет-нет"

Выполнено: нет
Результат: 
---

# [[Чтение из файлов в NumPy]]

> [!danger] Обязательный чеклист
> - [ ] Получение одной ссылки
> - [ ] Расставить теги 


> [!example]+  Чеклист знания
> - Получение источников и ссылок
> 	- [ ] Получение ссылок на разные источники
> - Конспектирование
> 	- [ ] Достаточное для использования конспектирование
> 	- [ ] Полное конспектирование
> - Создание справок
> 	- [ ] Справки практического применения
> 	- [ ] Список терминов
> 	- [ ] Определение терминов из списка терминов
> 	- [ ] Список связанных тем(если нет внутри текста)
> - Внедрение в базу знаний
> 	- [ ] Создание связей с оригинальной страницей (этой)
> 	- [ ] Разбиение на подтемы
> 	- [ ] Атомизация - (по необходимости) разбиение конспекта по подтемам на страницы
> - [ ] Внедрение в практику
> 

<mark style="background: #FF5582A6;">**Содержание потом вырезается, все содержимое конспекта удаляется и содержание вставляется по шаблону входящего конспекта.**</mark> 

# Содержание 

> [!usage-info]-
> - `np.loadtxt("sbux.csv", usecols=(0,1,4), skiprows=1, delimiter=",", dtype={'names': ('date', 'open', 'close'), 'formats': ('datetime64[D]', 'f4', 'f4')})`
> 	- `"sbbux.csv"` - имя файла
> 	- `usecols=(0,1,4)`- указывает используемые столбцы файла
> 	- `skiprows=1` - указывает, сколько строк от начала файла пропустить
> 	- `delimiter=","` - указывает какой символ использовать в качестве разделителя
> 	- `dtype={'names': ('date', 'open', 'close'), 'formats': ('datetime64[D]', 'f4', 'f4')})`- указывает, какие типы данных используются в столбцах 

## np.loadtxt



```python
	numpy.loadtxt(fname,dtype=<class 'float'>,comments='#',delimiter=None,converters=None,skiprows=0,usecols=None,unpack=False,ndmin=0,encoding='bytes',max_rows=None,*,quotechar=None,like=None)
```

Load data from a text file.

### Parameters:

- **`fname`**: file, str, pathlib.Path, list of str, generator
	- File, filename, list, or generator to read. If the filename extension is `.gz` or `.bz2`, the file is first decompressed. Note that generators must return bytes or strings. The strings in a list or produced by a generator are treated as lines.
- **`dtype`**: data-type, optional
	- Data-type of the resulting array; default: float. If this is a structured data-type, the resulting array will be 1-dimensional, and each row will be interpreted as an element of the array. In this case, the number of columns used must match the number of fields in the data-type.
- **`comments`**: str or sequence of str or None, optional
	- The characters or list of characters used to indicate the start of a comment. None implies no comments. For backwards compatibility, byte strings will be decoded as ‘latin1’. The default is ‘#’.
- **`delimiter`**: str, optional
	- The character used to separate the values. For backwards compatibility, byte strings will be decoded as ‘latin1’. The default is whitespace.
- **`converters`**: dict or callable, optional
	- Converter functions to customize value parsing. If _converters_ is callable, the function is applied to all columns, else it must be a dict that maps column number to a parser function. See examples for further details. Default: None.
- **`skiprows`**: int, optional
	- Skip the first _skiprows_ lines, including comments; default: 0.
- **`usecols`**: int or sequence, optional
	- Which columns to read, with 0 being the first. For example, `usecols = (1,4,5)` will extract the 2nd, 5th and 6th columns. The default, None, results in all columns being read.
- **`unpack`**: bool, optional
	- If True, the returned array is transposed, so that arguments may be unpacked using `x, y, z = loadtxt(...)`. When used with a structured data-type, arrays are returned for each field. Default is False.
- **`ndmin`**: int, optional
	- The returned array will have at least _ndmin_ dimensions. Otherwise mono-dimensional axes will be squeezed. Legal values: 0 (default), 1 or 2.
- **`encoding`**: str, optional
	- Encoding used to decode the inputfile. Does not apply to input streams. The special value ‘bytes’ enables backward compatibility workarounds that ensures you receive byte arrays as results if possible and passes ‘latin1’ encoded strings to converters. Override this value to receive unicode arrays and pass strings as input to converters. If set to None the system default is used. The default value is ‘bytes’.
- **`max_rows`**: int, optional
	- Read _max_rows_ rows of content after _skiprows_ lines. The default is to read all the rows. Note that empty rows containing no data such as empty lines and comment lines are not counted towards _max_rows_, while such lines are counted in _skiprows_.
- **`quotechar`**: unicode character or None, optional
	- The character used to denote the start and end of a quoted item. Occurrences of the delimiter or comment characters are ignored within a quoted item. The default value is `quotechar=None`, which means quoting support is disabled.
	- If two consecutive instances of _quotechar_ are found within a quoted field, the first is treated as an escape character. See examples.
- **`like`**: array_like, optional
	- Reference object to allow the creation of arrays which are not NumPy arrays. If an array-like passed in as `like` supports the `__array_function__` protocol, the result will be defined by it. In this case, it ensures the creation of an array object compatible with that passed in via this argument.
- **`out`**: ndarray
	- Data read from the text file.
## Смотрите также:

> [!question]- Соурслист
> 1. https://numpy.org/doc/stable/reference/generated/numpy.loadtxt.html

> [!quote]- Контекст
> ```dataview
> LIST 
> FROM !outgoing([[]])
> WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
> ```

---
#ВнедрениеЗнаний 
#Python/NumPy 

---

[^def]: термин
[^que]: вопрос