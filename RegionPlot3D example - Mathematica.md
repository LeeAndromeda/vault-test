Plot a 3D region:

In[1]:=

![](https://reference.wolfram.com/language/ref/Files/RegionPlot3D.en/I_1.png)

`RegionPlot3D[x^2 + y^3 - z^2 > 0, {x, -2, 2}, {y, -2, 2}, {z, -2, 2}]`

`https://wolfram.com/xid/0d6g6quw2w-k2y`

Out[1]=

![](https://reference.wolfram.com/language/ref/Files/RegionPlot3D.en/O_1.png)

Plot multiple regions:

In[1]:=

![](https://reference.wolfram.com/language/ref/Files/RegionPlot3D.en/I_2.png)

`RegionPlot3D[{x + y + z < -2, x + y + z > 2}, {x, -2, 2}, {y, -2, 2}, {z, -2, 2}]`

`https://wolfram.com/xid/0d6g6quw2w-79v8l1`

Out[1]=

![](https://reference.wolfram.com/language/ref/Files/RegionPlot3D.en/O_2.png)

Plot 3D regions defined by logical combinations of inequalities:

In[1]:=

![](https://reference.wolfram.com/language/ref/Files/RegionPlot3D.en/I_3.png)

`https://wolfram.com/xid/0d6g6quw2w-dwx3gb`

Out[1]=

![](https://reference.wolfram.com/language/ref/Files/RegionPlot3D.en/O_3.png)

Use simple styling of region boundaries:

In[1]:=

![](https://reference.wolfram.com/language/ref/Files/RegionPlot3D.en/I_4.png)

`RegionPlot3D[x y z < 1, {x, -5, 5}, {y, -5, 5}, {z, -5, 5}, PlotStyle -> Directive[Yellow, Opacity[0.5]], Mesh -> None]`

`https://wolfram.com/xid/0d6g6quw2w-ch6vth`

Out[1]=

![](https://reference.wolfram.com/language/ref/Files/RegionPlot3D.en/O_4.png)