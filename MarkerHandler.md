---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 28 # Не трогать!
Месяц: 09 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-09-28

---
# [[MarkerHandler]]


```cpp
// Класс для работы с маркерами
class MarkerHandler {
    private:
    visualization_msgs::Marker marker;
    ros::Publisher marker_pub;
    ros::NodeHandle* nh_pointer;

    public:
    std::string module_name;

    MarkerHandler()
    {
        marker.header.stamp = ros::Time::now();
        module_name = "default";
    }

    // MarkerHandler(std::string pub_topic) 
    // {
    //     marker.header.stamp = ros::Time::now();
    //     marker_pub = rvizor.advertise<visualization_msgs::Marker>(pub_topic, 1000);
    //     module_name = "default";
    // }

    void _init(ros::NodeHandle* nh, std::string pub_topic)
    {
        nh_pointer = nh;
        advertise_to_topic(pub_topic);
    }

    void advertise_to_topic(std::string pub_topic)
    {
        // marker_pub = rvizor.advertise<visualization_msgs::Marker>(pub_topic, 1000);
        marker_pub = nh_pointer->advertise<visualization_msgs::Marker>(pub_topic, 1000);
    }

    void set_frame(std::string frame_id)
    {
        marker.header.frame_id = frame_id;
    }

    void set_namespace(std::string namespace_str)
    {
        marker.ns = namespace_str;
    }

    void set_id(int id)
    {
        marker.id = id;
    }

    void set_type(int enum_number)
    {
        /*
            ARROW = 0u,
            CUBE = 1u,
            SPHERE = 2u,
            CYLINDER = 3u,
            LINE_STRIP = 4u,
            LINE_LIST = 5u,
            CUBE_LIST = 6u,
            SPHERE_LIST = 7u,
            POINTS = 8u,
            TEXT_VIEW_FACING = 9u,
            MESH_RESOURCE = 10u,
            TRIANGLE_LIST = 11u,
        */
        if (enum_number == 0)
        {
            marker.type = visualization_msgs::Marker::ARROW;
            marker.action = visualization_msgs::Marker::ADD;
        } 

        else if (enum_number == 1)
        {
            marker.type = visualization_msgs::Marker::CUBE;
            marker.action = visualization_msgs::Marker::ADD;
        } 

        else if (enum_number == 2)
        {
            marker.type = visualization_msgs::Marker::SPHERE;
            marker.action = visualization_msgs::Marker::ADD;
        } 

        else if (enum_number == 3)
        {
            marker.type = visualization_msgs::Marker::CYLINDER;
            marker.action = visualization_msgs::Marker::ADD;
        } 

        else if (enum_number == 4)
        {
            marker.type = visualization_msgs::Marker::LINE_STRIP;
            marker.action = visualization_msgs::Marker::ADD;
        } 

        else if (enum_number == 5)
        {
            marker.type = visualization_msgs::Marker::LINE_LIST;
            marker.action = visualization_msgs::Marker::ADD;
        } 

        else if (enum_number == 6)
        {
            marker.type = visualization_msgs::Marker::CUBE_LIST;
            marker.action = visualization_msgs::Marker::ADD;
        } 

        else if (enum_number == 7)
        {
            marker.type = visualization_msgs::Marker::SPHERE_LIST;
            marker.action = visualization_msgs::Marker::ADD;
        } 

        else if (enum_number == 8)
        {
            marker.type = visualization_msgs::Marker::POINTS;
            marker.action = visualization_msgs::Marker::ADD;
        } 

        else if (enum_number == 9)
        {
            marker.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
            marker.action = visualization_msgs::Marker::ADD;
        } 

        else if (enum_number == 10)
        {
            marker.type = visualization_msgs::Marker::MESH_RESOURCE;
            marker.action = visualization_msgs::Marker::ADD;
        } 

        else if (enum_number == 11)
        {
            marker.type = visualization_msgs::Marker::TRIANGLE_LIST;
            marker.action = visualization_msgs::Marker::ADD;
        } 

        else ROS_ERROR_STREAM("NO SUCH POSITION IN THE LIST" << std::endl);
    }


    void set_pose(double x, double y, double z, tf2::Quaternion q)
    {
        marker.pose.position.x = x;
        marker.pose.position.y = y;
        marker.pose.position.z = z;
        marker.pose.orientation.x = q.getX();
        marker.pose.orientation.y = q.getY();
        marker.pose.orientation.z = q.getZ();
        marker.pose.orientation.w = q.getW();
    }

    void set_scale(float x, float y, float z)
    {
        marker.scale.x = x;
        marker.scale.y = y;
        marker.scale.z = z;
    }

    void set_color(float alpha, float red, float green, float blue)
    {
        marker.color.a = alpha;
        marker.color.r = red;
        marker.color.g = green;
        marker.color.b = blue;
    }

    void publish_marker()
    {
        marker_pub.publish(marker);
    }

    void set_module_name(std::string new_name)
    {
        module_name = new_name;
    }

    void debug_info(std::string new_module_name)
    {
        set_module_name(new_module_name);
        //DEBUG rinfo
        //****************************************************************************************************
        if(1)
        {
            ROS_INFO_STREAM(std::endl << "_________________________________" << std::endl << "_________________________________" << std::endl 
            << "FUNCTION NAME: " << module_name << std::endl 
            << "VARIABLES: " << std::endl 
            << "frame id -->" << marker.header.frame_id << std::endl 
            << "id -->" << marker.id << std::endl 
            << "type -->" << marker.type << std::endl 
            << "pose x -->" << marker.pose.position.x << std::endl 
            << "pose y -->" << marker.pose.position.y << std::endl 
            << "pose z -->" << marker.pose.position.z << std::endl 
            << "quaternion w -->" << marker.pose.orientation.w << std::endl 
            << "scale x -->" << marker.scale.x << std::endl 
            << "scale y -->" << marker.scale.y << std::endl 
            << "scale z -->" << marker.scale.z << std::endl 
            << "color a -->" << marker.color.a << std::endl 
            << "color r -->" << marker.color.r << std::endl 
            << "color g -->" << marker.color.g << std::endl 
            << "color b -->" << marker.color.b << std::endl 
            << "_________________________________" << std::endl << "_________________________________" << std::endl);
        }
        //****************************************************************************************************
    }
};

```


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#ROS 

---

[^def]: термин
[^que]: вопрос