---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 21 # Не трогать!
Месяц: 11 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-11-21

---
# [[numpy.corrcoef - коэффициент линейной корреляции двух переменных]]


Функция numpy.corrcoef в NumPy используется для вычисления коэффициента корреляции Пирсона между двумя или более переменными. Коэффициент корреляции Пирсона - это мера линейной связи между двумя переменными, варьирующаяся от -1 (идеальная отрицательная корреляция) до 1 (идеальная положительная корреляция), при этом 0 означает отсутствие корреляции.

Коэффициент корреляции Пирсона измеряет линейную корреляцию между двумя переменными. Линейная корреляция означает, что связь между переменными может быть хорошо аппроксимирована прямой линией.

Таким образом, если коэффициент корреляции Пирсона близок к 1, это свидетельствует о сильной положительной линейной корреляции, то есть при увеличении одной переменной другая имеет тенденцию к пропорциональному увеличению. Если коэффициент близок к -1, то он указывает на сильную отрицательную линейную корреляцию, что означает, что при увеличении одной переменной другая имеет тенденцию к пропорциональному уменьшению. Если коэффициент близок к 0, то это означает слабую линейную корреляцию или ее отсутствие.

Важно отметить, что коэффициент корреляции Пирсона чувствителен к линейным связям и может не отражать другие типы связей (нелинейные) между переменными. Для нелинейных связей могут быть более подходящими другие меры корреляции, например коэффициент ранговой корреляции Спирмена.

## Пример

```python
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

# Generate example data
np.random.seed(42)
data_size = 100
x = np.random.randn(data_size)
y = 2 * x + np.random.randn(data_size)  # y is linearly related to x with some random noise

# Calculate Pearson correlation coefficient
correlation_matrix = np.corrcoef(x, y)
pearson_corr = correlation_matrix[0, 1]

# Print the correlation coefficient
print(f"Pearson Correlation Coefficient: {pearson_corr:.2f}")

# Create a scatter plot using Seaborn
sns.set(style="whitegrid")
sns.scatterplot(x=x, y=y)
plt.title('Scatter Plot with Pearson Correlation Coefficient')

# Show the plot
plt.show()
### Output
Pearson Correlation Coefficient: 0.87
```

![[Pasted image 20231121164634.png]]

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Python/NumPy 

---

[^def]: термин
[^que]: вопрос