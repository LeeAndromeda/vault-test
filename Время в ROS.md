---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 01 # Не трогать!
Месяц: 02 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-02-01

---
# [[Время в ROS]]

> [! example] Список определений [^def]
>

> [! example] Список вопросов [^que]
> 

Для переменной времени используется `ros::Duration`:

- Иницализация:
	- `ros::Duration var(5.0); // 5 секунд`

%%## Приложения%%

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. http://wiki.ros.org/roscpp/Overview/Time


---
#ROS

---

[^def]: термин
[^que]: вопрос