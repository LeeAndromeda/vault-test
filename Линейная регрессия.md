---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 22 # Не трогать!
Месяц: 06 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-06-22

---
# [[Линейная регрессия]]


Статистический метод аппроксимации функции *двух переменных*[^1], подразумевающий построение формулы такой прямой, в которой ошибка между предсказанным значением функции и реальным значением функции для одного и того же аргумента во всех точках было бы минимальным.


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://www.youtube.com/watch?v=zPG4NjIkCjc


---
#Статистика 

---

[^1]: мб больше, хз
[^def]: термин
[^que]: вопрос