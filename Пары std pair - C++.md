---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 28 # Не трогать!
Месяц: 09 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-09-28

---
# [[Пары std pair - C++]]


Создание пары:

```cpp
    std::vector<std::pair<geometry_msgs::Point32, double>> pcl_with_angles;
    for (int i = 0; i < segment_scan.ranges.size(); ++i)
    {
        if (isfinite(segment_scan.ranges.at(i)))
        {
            geometry_msgs::Point32 p;
            p.x = segment_scan.ranges.at(i) * cos(segment_scan.angle_increment * i);
            p.y = segment_scan.ranges.at(i) * sin(segment_scan.angle_increment * i);
            
            double angle = i * segment_scan.angle_increment;

            pcl_with_angles.push_back(std::make_pair(p, angle));

            // pc.points.push_back(p);
        }
```

Обращение к элементам пары:

```cpp
 auto p = std::make_pair(1, 3.14); 
 std::cout << '(' << std::get<0>(p) << ", " << std::get<1>(p) << ")\n"; 
 std::cout << '(' << std::get<int>(p) << ", " << std::get<double>(p) << ")\n";
```

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. [[std get(std pair) - cppreference.com.pdf]]
> 2. [[std pair - cppreference.com.pdf]]


---
#cpp 

---

[^def]: термин
[^que]: вопрос