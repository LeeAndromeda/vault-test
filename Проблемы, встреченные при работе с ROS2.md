---

Tags: 
aliases: 
regex-to-search:  # "что-то" regex
regex-to-be-found: "[((.*)[Rr][Oo][Ss]2(.*))((.*)[Bb]ug(.*))((.*)[Бб]аг(.*))((.*)[Рр][Оо][Сс]2(.*))]" # "что-то" regex

Тип: конспект
Описание: -

День: 27 # Не трогать!
Месяц: 04 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-04-27

---
# [[Проблемы, встреченные при работе с ROS2]]

- [[Перегрузка сети и ОЗУ оверфлоу - особенности работы с CycloneDDS]]
- [[Разные источники времени - проблемы с ROS2]]

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#ROS2

---

[^def]: термин
[^que]: вопрос