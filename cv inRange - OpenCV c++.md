---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 19 # Не трогать!
Месяц: 07 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-07-19

---
# [[cv inRange - OpenCV c++]]

> [!def]+
> 
> ```cpp
> void cv::inRange 	
> ( 	
> 	InputArray src,
> 	InputArray lowerb,
> 	InputArray upperb,
> 	OutputArray dst 
> ) 	
> ```
> 
> > |  arg   |     meaning                               |
|--------|------------------------------------------------------|
> | src    | first input array.                                   |
| lowerb | inclusive lower boundary array or a scalar.          |
| upperb | inclusive upper boundary array or a scalar.          |
| dst    | output array of the same size as src and CV_8U type. |


Проверяет, лежат ли элементы массива `src` в пределах элементов массивов от `lowerb` до `upperb`. Результат сохраняется в массив `dst` - 1 при справедливости выражения и 0 в обратном случае. 

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://docs.opencv.org/3.4/d2/de8/group__core__array.html#ga48af0ab51e36436c5d04340e036ce981


---
#КомпьютерноеЗрение/opencv 
#cpp 

---

[^def]: термин
[^que]: вопрос