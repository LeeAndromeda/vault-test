---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 10 # Не трогать!
Месяц: 01 # Не трогать!
Год: 2023 # Не трогать!
Дата_создания: Tuesday 10th January 2023 16:22:49

---
# [[Передача в callback функцию ROS2 нескольких аргументов C++]]

В качестве примера возьму код, разработанный в рамках [[Рубин|Рубина]] [[Бридж сим-СУ|для решения задачи разработки бриджа системы управления и симулятора]]. 

> [!info]- Функция инициализации подписки
> ```cpp
> // Функция, которая добавляет пару айди-сабскрайбер в subscriber_list
> void _init_subscription(int anpa_id)
> {
> 	try
> 	{
> 		// int anpa_id_copy = anpa_id;
> 		std::function<void(const rubin_msgs::msg::Regulator::SharedPtr reg_in)> bound_cb_func = std::bind(&ControlSignalsParser::regulator_sub_cb, this, std::placeholders::_1, anpa_id);
> 		rclcpp::Subscription<rubin_msgs::msg::Regulator>::SharedPtr new_sub_ptr = __node_ptr->create_subscription<rubin_msgs::msg::Regulator>(anpa_id_topic + std::to_string(anpa_id) + control_signals_topic, 10, bound_cb_func, options);
> 		subscriber_list.insert(std::make_pair(anpa_id, new_sub_ptr));
> 		RCLCPP_INFO_STREAM(__node_ptr->get_logger(), "Initialised sub of anpa " << anpa_id << std::endl);
> 	}
> 	catch (const std::exception& e)
> 	{
> 		RCLCPP_ERROR_STREAM(__node_ptr->get_logger(), "Error while creating subscription: " << e.what());
> 	}
>}
>```

> [!note]- Функция колбэка
> ```cpp
void regulator_sub_cb(const rubin_msgs::msg::Regulator::SharedPtr reg_in, int anpa_id)
> {
> 	try
> 	{
> 		_real_cb(reg_in);
> 		c++;
> 		anpa_id = c;
> 	}
> 	catch (const std::exception& e)
> 	{
> 		RCLCPP_ERROR_STREAM(__node_ptr->get_logger(), "Exception in regulator callback: " << e.what());
> 	}
> }
> ```

Дело заключается в баге, с которым справляются таким костылем:

Обратите внимание на то, что в функции инициализации подписки колбэк сначала объявляется как `std::function` с типом данных `<void(const rubin_msgs::msg::Regulator::SharedPtr reg_in\)>` [^1], а затем инициализируется функцией `std::bind`, в которой мы уже добавляем необходимый аргумент после плэйсхолдера:

```cpp
std::function<void(const rubin_msgs::msg::Regulator::SharedPtr reg_in)> bound_cb_func = std::bind(&ControlSignalsParser::regulator_sub_cb, this, std::placeholders::_1, anpa_id);
rclcpp::Subscription<rubin_msgs::msg::Regulator>::SharedPtr new_sub_ptr = __node_ptr->create_subscription<rubin_msgs::msg::Regulator>(anpa_id_topic + std::to_string(anpa_id) + control_signals_topic, 10, bound_cb_func, options);
```

После такого объявления мы можем добавить необходимый аргумент в параметры функции, что и сделано в колбэке:

```cpp
void regulator_sub_cb(const rubin_msgs::msg::Regulator::SharedPtr reg_in, int anpa_id)

	try
	{
		_real_cb(reg_in);
		c++;
		anpa_id = c;
	}
```

На момент создания туториала данный код разрабатывался с целью демонстрации возможности проведения такого действия, потому над передаваемым дополнительным аргументом производится чисто символическая операция, однако для решения своих задач после указанных операций вы можете производить над аргументом любые необходимые манипуляции.

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://answers.ros.org/question/308386/ros2-add-arguments-to-callback/
> 2. https://github.com/ros2/rclcpp/issues/273


---
#Робототехника/ROS2 #cpp 

[^1]: Обратный слеш стоит для нормального отражения в Obsidian. При клпировании его необходимо убрать