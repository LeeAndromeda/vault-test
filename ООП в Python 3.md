---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 19 # Не трогать!
Месяц: 11 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-11-19

---
# [[ООП в Python 3]]

- [[Абстрактные классы в Python]]

## Базовая теория о классах и ООП

[[ООП в Python 3.canvas|Полотно]]

Определение класса:

```Python
class Name:
	pass
```

> [!info]- Конвенция наименований классов
> 

## Конструктор класса - метод `__init__(self)`

Атрибуты, которые должен иметь каждый экземпляр класса и которые можно переопределять при создании экземпляра, определяются в этом методе следующим образом:

```Python
class Dog:
    def __init__(self, name, age):
        self.name = name
        self.age = age
```

## Классовые аттрибуты

Атрибуты, которые должны быть одинаковыми у всех экземпляров класса, задаются до метода `__init__()` следующим образом:

```python
class Dog:
    # Class attribute
    species = "Canis familiaris"

    def __init__(self, name, age):
        self.name = name
        self.age = age
```


## Методы

Определяются следующим образом:

```python
class Dog:
    species = "Canis familiaris"

    def __init__(self, name, age):
        self.name = name
        self.age = age

    # Instance method
    def description(self):
        return f"{self.name} is {self.age} years old"

    # Another instance method
    def speak(self, sound):
        return f"{self.name} says {sound}"
```

## Наследование

Синтаксис наследования является следующим:

```Python
class JackRussellTerrier(Dog):
    pass

class Dachshund(Dog):
    pass

class Bulldog(Dog):
    pass
```

Чтобы определить тип существующего объекта, используется функция `type(obj)`. Чтобы определить, является ли объект экземпляром указанного класса, используется функция `isinstance(obj, class)`

### Полиморфизм

Чтобы переопределить родительский класс, мы просто объявляем уже существующую в родительском классе функцию, будто её еще нет. В случае, если мы хотим переиспользовать функционал, имеющийся в родительском классе, можно использовать метод `super()`. 

```python
class GoldenRetriever(Dog):
    def speak(self, sound="Bark"):
        return super().speak(sound)
```

## Доступ. Публичные и приватные 

Все методы и атрибуты по-умолчанию являются публичными. Приватные методы и атрибуты начинаются с двух нижних подчеркиваний.
## Смотрите также:

> [!question]- Соурслист
> 1. https://realpython.com/python3-object-oriented-programming/

> [!quote]- Контекст
> ```dataview
> LIST 
> FROM !outgoing([[]])
> WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
> ```

---
#python 

---

[^def]: термин
[^que]: вопрос