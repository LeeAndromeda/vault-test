---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 15 # Не трогать!
Месяц: 08 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-08-15

---
# [[Assertions - утверждения в GoogleTest]]


**Assertion** - утверждения, макросы с function-like синтаксисом,  проверка какого-то условия для класса или функции в GoogleTest. Если условие не выполняется, то GT пишет файл и местоположение объекта, который проверяется.

Утверждения существуют в парах - `ASSERT_*` и `EXPECT_*`. Первый тип вызывает фатальную ошибку и должен быть использован, если после провала проверки текущего объекта нет смысла продолжать работу. Второй тип позволит программе и тестам выполняться дальше и предпочитаем во всех остальных случаях применения.

> [!attention]- Заметка об ошибках кучи  при провале `ASSERT_*`
> Поскольку неудачный `ASSERT_*` возвращается из текущей функции немедленно, возможно, пропуская следующий за ним чистый код, он может привести к утечке данных. В зависимости от характера утечки, ее можно исправить, а можно и не исправлять - поэтому имейте это в виду, если помимо ошибок утверждений вы получаете ошибки проверки кучи.

Чтобы выдать пользовательское сообщение о сбое, достаточно передать его в макрос с помощью оператора << или последовательности таких операторов.

```cpp
ASSERT_EQ(x.size(), y.size()) << "Vectors x and y are of unequal length";

for (int i = 0; i < x.size(); ++i) {
  EXPECT_EQ(x[i], y[i]) << "Vectors x and y differ at index " << i;
}
```
## Смотрите также:

- [Assertions reference - GoogleTest](https://google.github.io/googletest/reference/assertions.html)

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. http://google.github.io/googletest/primer.html#assertions


---
#GoogleTest 

---

[^def]: термин
[^que]: вопрос