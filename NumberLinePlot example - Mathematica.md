> [!bg-plate]
> 
>Show the first 10 prime numbers on a number line:
>
>In[1]:=
>
>![](https://reference.wolfram.com/language/ref/Files/NumberLinePlot.en/I_1.png)
>
>`NumberLinePlot[Prime[Range[10]]]`
>
>`https://wolfram.com/xid/0d6h0f7v0y-vcw40b`
>
>Out[1]=
>
>![](https://reference.wolfram.com/language/ref/Files/NumberLinePlot.en/O_1.png)
>
>Display an interval:
>
>In[1]:=
>
>![](https://reference.wolfram.com/language/ref/Files/NumberLinePlot.en/I_2.png)
>
>`NumberLinePlot[Interval[{0, 1}]]`
>
>`https://wolfram.com/xid/0d6h0f7v0y-y77nm7`
>
>Out[1]=
>
>![](https://reference.wolfram.com/language/ref/Files/NumberLinePlot.en/O_2.png)
>
>Show where an inequality is true:
>
>In[1]:=
>
>![](https://reference.wolfram.com/language/ref/Files/NumberLinePlot.en/I_3.png)
>
>`NumberLinePlot[Sin[x] < Cos[x], {x, 0, 2 Pi}]`
>
>`https://wolfram.com/xid/0d6h0f7v0y-jaanzd`
>
>Out[1]=
>
>![](https://reference.wolfram.com/language/ref/Files/NumberLinePlot.en/O_3.png)
>
>Show an infinite interval:
>
>In[1]:=
>
>![](https://reference.wolfram.com/language/ref/Files/NumberLinePlot.en/I_4.png)
>
>`NumberLinePlot[Interval[{0, \[Infinity]}]]`
>
>`https://wolfram.com/xid/0d6h0f7v0y-76z483`
>
>Out[1]=
>
>![](https://reference.wolfram.com/language/ref/Files/NumberLinePlot.en/O_4.png)
>
>Show several sets on a single number line:
>
>In[1]:=
>
>![](https://reference.wolfram.com/language/ref/Files/NumberLinePlot.en/I_5.png)
>
>`NumberLinePlot[{Range[20], 2 Range[10], 3 Range[6], 5 Range[4], 7 Range[2]}]`
>
>`https://wolfram.com/xid/0d6h0f7v0y-nlu3hl`
>
>Out[1]=
>
>![](https://reference.wolfram.com/language/ref/Files/NumberLinePlot.en/O_5.png)

#Mathematica 