


---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 23 # Не трогать!
Месяц: 08 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-08-23

---
# [[GoogleCartographer - ROS пакет]]


## Установка

Установка выполняется по следующему гайду - [ссылка](https://google-cartographer-ros.readthedocs.io/en/latest/compilation.html).

### Встреченные проблемы

#### libasl-dev

1. В `~/catkin_ws/src/cartographer` откройте `package.xml` и удалите там строчку:
	```xml
	<depend>libabsl-dev</depend>
	```
2. В папке `~/catkin_ws/src/cartographer/scripts`  выполните следующую команду:
	```bash
	sudo ./install_abseil.sh 
	```
3. Продолжайте установку по гайду

#### Как установить картографер в воркспэйс, использующий catkin build

1. Переместите `cartographer`  и `cartographer_ros` из `~/catkin_ws/src`  в `src` вашего воркспэйса.
2. Переместите `install_isolated` из `~/catkin_ws/` в ваш воркспэйс
3. Используйте команду `catkin_make_isolated --install --use-ninja --pkg cartographer  cartographer_ros cartographer_rviz` для билда картографера
4. Каждый раз перед использованием картографера нужно помимо остального соурсить еще `<путь_к_вашему_воркспэйсу>/install_isolated/setup.bash`

#### Проблемы со sphinx

В этом случае у вас скорее всего уже стоит  `jinja2`. Вам нужно его удалить командой `pip3 uninstall jinja2` и установить более старую весрию командой `pip3 install jinja2==3.0.0`.

## Настройка

В лаунче `drone_essentials/cartographer_lidar_odom_gek.launch` можно указать `*.lua` файл с конифгурацие GC. Параметры GC указаны в источнике 1.


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://answers.ros.org/question/311263/odometry-with-cartographer/


---
#ROS

---

[^def]: термин
[^que]: вопрос