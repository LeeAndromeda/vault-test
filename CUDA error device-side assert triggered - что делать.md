---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 15 # Не трогать!
Месяц: 09 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-09-15

---
# [[CUDA error device-side assert triggered - что делать]]

В этом случае, надо попробовать переключить `device` на cpu и тогда  torch скорее всего выдаст более понятную ошибку.

В случае, если вам необходимо устроить дебаг именно для CUDA, вы можете запускать программу с установленной переменной среды:

```python
import os
os.environ['CUDA_LAUNCH_BLOCKING'] = "1"
```
## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#pytorch 

---

[^def]: термин
[^que]: вопрос