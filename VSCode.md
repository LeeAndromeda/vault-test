---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 09 # Не трогать!
Месяц: 01 # Не трогать!
Год: 2023 # Не трогать!
Дата_создания: Monday 9th January 2023 16:48:56

---
# [[VSCode]]

## Горячие клавиши

- [[Горячие клавиши VSCode для сворачивания и разворачивания классов и функций]]

## Расширения

### Todo tree



## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Программирование/СредыРазработки/IDE

[^def]: термин
[^que]: вопрос