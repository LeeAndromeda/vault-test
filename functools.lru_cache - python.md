---

Tags: 
aliases: [Кэширование результатов вычислений в Python]
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 09 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-10-09

---
# [[functools.lru_cache - python]]

Это декоратор, который кэширует результаты вычислений:

```python
from functools import lru_cache

@lru_cache(3) # Число - размер кэша
def calc(a):
    print(a, end=' -> ')
    return a ** 3


print(calc(2)) # 2
print(calc(2))
print(calc(2))
print(calc(3)) # 3 -> 2
print(calc(3))
print(calc(4)) # 4 -> 3 -> 2
print(calc(4))
print(calc(5)) # 5 -> 4 -> 3
print(calc(2)) # 2 -> 5 -> 4

### Output

2 -> 8
8
8
3 -> 27
27
4 -> 64
64
8
5 -> 125
8

```


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Python 

---

[^def]: термин
[^que]: вопрос