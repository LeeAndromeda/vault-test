---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 22 # Не трогать!
Месяц: 11 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-11-22

---
# [[Тау Кэнделла - коэффициент порядковой ассоциации - реализация в numpy]]

```python
import numpy as np 
import matplotlib.pyplot as plt

def sgn(num):
    if num >= 0:
        return 1
    else:
        return - 1

def kendall_tau(x, y):
    assert len(x) == len(y)
    n = len(x) 
    sum_x = 0
    for i in range(n - 1):
        sum_x += sgn(x[i] - x[i + 1])
    
    sum_y = 0
    for i in range(n - 1):
        sum_y += sgn(y[i] - y[i + 1])
    
    tau = sum_x * sum_y / (n * (n - 1))
    return tau

x = np.arange(1, 100)
y = -np.log(x)

print(kendall_tau(x, y))
plt.plot(x, y)
plt.show()

#####################
# Output
-0.98989898989899
```
![[Pasted image 20231122150520.png]]

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Python/NumPy 

---

[^def]: термин
[^que]: вопрос