# Мой лист

[[Список вдохновляющих материалов и литературы]]

[[Прайс-лист для гейм ГТД]]

---
>>## Кошелек
>>💠 - 625
>>🔵 - 5
>>🟣 - 1000
>>🔴 - 0
>>🟢 - 0
>>🟡 - 50
>>⚪ - 1202
>>⚫ - 5150
>>🟠 - 1780


#### Пари и выполненные просроченные задачи этой недели

## [[Отчёты по Гейм-ГТД]]

[[Мой лист#Накопительные счета]]
---

![[Список жизненных сфер, важных для моего развития]]
![[Распределение наград в гейм-гтд]]
![[Прайс-лист для гейм ГТД]]

---
## Накопительные счета

---
### Счёт "Music theorist"

Счёт на получение вечного доступа к книге "Теория музыки с пространством кратностей"

> ✅
>💠 - 690/690

Динамика изменения счёта

```chart
type: line
labels: [?, 19.12.21/1, 19.12.21/2, 19.12.21/3, 19.12.21/4, 22.12.2021, 23.12.2021, 05.01.2022]
series:
  - title: 💠
    data: [0, 156, 100, 138, 118, 318, 422, 690]
tension: 0.2
width: 100%
labelColors: false
fill: true
beginAtZero: true
```

---
### Счёт "Самодисциплина"

Счёт на получение вечного доступа к книге "Сила воли: как развить и укрепить"

>✅
>💠 - 790/790

Динамика изменения счёта:

```chart
type: line
labels: [15.01.2022/1, 15.01.2022/2]
series:
  - title: 💠
    data: [500, 790]
tension: 0.2
width: 100%
labelColors: false
fill: true
beginAtZero: true
```

---

#Гейм-ГТД