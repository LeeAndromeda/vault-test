# Проект развития работы с Obsidian

## Видео

- [Obsidian Advanced Techniques | Templates, Tagging, Folding, Embedding, and more](https://youtu.be/c6qfrRVUOO8)
- [How to achieve more while studying less - Obsidian & Conceptual Notes](https://youtu.be/MYJsGksojms)
- [Zettelkasten Note-Taking Method: Simply Explained](https://youtu.be/rOSZOCoqOo8)

## Отчеты

- [[Развитие работы с Obsidian. Итерация 1]]
- [[Leaflet-конспекты (LFL)]]

#obsidian 