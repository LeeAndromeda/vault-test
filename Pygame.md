---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 02 # Не трогать!
Месяц: 11 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-11-02

---
# [[Pygame]]


Pygame - это библиотека для разработки компьютерных игр и мультимедийных приложений на языке программирования Python. Она предоставляет различные инструменты и функции для создания графики, обработки звука, управления вводом пользователя и других аспектов игровой разработки.

- [[Шаблон программы на Pygame]]
- [[Создание экрана]]
- [[Рисование фигур в pygame]]

---

- [[Спрайты в Pygame]]
- [[Слои спрайтов в pygame]]
- [[Прозрачные спрайты в pygame]]

---

- [[Тайлсеты и тайловые карты в pygame]]

--- 

- [[Коллизии в pygame]]

## Смотрите также:
- [[Чат с ChatGPT по поводу изучения pygame]]
- [[Документация к Pygame]]
```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#python/pygame

---

[^def]: термин
[^que]: вопрос