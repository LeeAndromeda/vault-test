---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 22 # Не трогать!
Месяц: 04 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-04-22

---
# [[Изображения в OpenCV]]

## Базовое устройство

Каждая картинка в OpenCV - это 2Д или 3Д массив класса numpy.ndarray. Чтобы создать черно-белую картинку размером 3 на 3, можно использовать следующую команду:

```python
img = numpy.zeros((3, 3), dtype=numpy.uint8)
```

Если вывести картинку в консоль, мы увидим:

```
array([[0, 0, 0],  
[0, 0, 0],  
[0, 0, 0]], dtype=uint8)
```

Здесь каждый пиксель представлен 8-битным целым числом, он принимает значения 0-255, где 0 - черный, 255 - белый, а все, что между - оттенки серого.

## Устройство цветного изображения

Давайте теперь преобразуем это изображение в сине-зелено-красный (BGR) формат, используя
функцию [[cvtColor() OpenCV|cv2.cvtColor]]:
```python
img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
```
Давайте посмотрим, как изменилось изображение:
```python
array([[[0, 0, 0],
        [0, 0, 0],
        [0, 0, 0]],
       [[0, 0, 0],
        [0, 0, 0],
        [0, 0, 0]],
       [[0, 0, 0],
        [0, 0, 0],
        [0, 0, 0]]], dtype=uint8)
```

Как вы можете видеть, каждый пиксель теперь представлен трехэлементным массивом, причем каждое целое число представляет один из трех цветовых каналов: B, G и R, соответственно. Другие распространенные [[Цветовые пространства OpenCV|цветовые модели]], такие как [[Цветовая модель HSV в OpenCV|HSV]], будут представлены таким же образом, хотя и с различными значениями диапазонов. Например, значение оттенка в цветовой модели HSV имеет диапазон 0-180.

## Свойства картинки в формате [[Как создать numpy.ndarray из numpy.array|numpy.array]]

Наконец, мы можем получить доступ к свойствам numpy.array, как показано в следующем коде:  
```python
импортировать cv2  
img = cv2.imread('MyPic.png')  
print(img.shape)  
print(img.size)  
print(img.dtype)  
```


Эти три свойства определяются следующим образом:  
- **shape**: Это [[Коллекции (массивы) в Python|кортеж]], описывающий форму массива. Для изображения он содержит (по порядку) высоту, ширину и - если изображение цветное - количество каналов. По длине кортежа shape можно определить, является ли изображение полутоновым или цветным. Для полутонового изображения len(shape) == 2, а для цветного len(shape) == 3.  
- **size**: Это количество элементов в массиве. В случае полутонового изображения это число равно числу пикселей. В случае изображения BGR это число в три раза больше числа пикселей, поскольку каждый пиксель представлен тремя элементами (B, G и R).  
- **dtype**: Это тип данных элементов массива. Для изображения с 8-битным каналом типом данных является numpy.uint8.


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. [[LOCV4 Howse & Minichino.canvas]]


---
#КомпьютерноеЗрение/opencv  

---

[^def]: термин
[^que]: вопрос