---
Тип: Задача
alias: [Pytorch список скилов навыков умений, необходимых для]
Описание: -
Исполнители: 
Проект: pytorch

Tags: Задача
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: 16 # Не трогать!
Месяц: 06 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-06-16

дата_завершения: "2023-нет-нет"

Выполнено: нет
Результат: 
---

# [[Pytorch skill list]]

> [!danger] Обязательный чеклист
> - [ ] Получение одной ссылки
> - [ ] Расставить теги 


> [!example]+  Чеклист знания
> - Получение источников и ссылок
> 	- [ ] Получение ссылок на разные источники
> - Конспектирование
> 	- [ ] Достаточное для использования конспектирование
> 	- [ ] Полное конспектирование
> - Создание справок
> 	- [ ] Справки практического применения
> 	- [ ] Список терминов
> 	- [ ] Определение терминов из списка терминов
> 	- [ ] Список связанных тем(если нет внутри текста)
> - Внедрение в базу знаний
> 	- [ ] Создание связей с оригинальной страницей (этой)
> 	- [ ] Разбиение на подтемы
> 	- [ ] Атомизация - (по необходимости) разбиение конспекта по подтемам на страницы
> - [ ] Внедрение в практику
> 

<mark style="background: #FF5582A6;">**Содержание потом вырезается, все содержимое конспекта удаляется и содержание вставляется по шаблону входящего конспекта.**</mark> 

# Содержание 



## Смотрите также:

> [!question]- Соурслист
> 1. 

> [!quote]- Контекст
> ```dataview
> LIST 
> FROM !outgoing([[]])
> WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
> ```

---
#ВнедрениеЗнаний 
#pytorch 

---

[^def]: термин
[^que]: вопрос