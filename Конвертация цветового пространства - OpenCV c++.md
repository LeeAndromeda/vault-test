---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 18 # Не трогать!
Месяц: 07 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-07-18

---
# [[Конвертация цветового пространства - OpenCV c++]]

Во-первых, мы объявили две матрицы и два окна. Они предназначены для загрузки и отображения изображений. Затем мы загрузили наше изображение с именем `cat.jpg` в матрицу `myImage`.  После этого мы использовали `cvtColor(myImage, myImage_Converted, COLOR_RGB2GRAY)`. Эта строка преобразует цветовое пространство RGB изображения `myImage` в Grayscale и сохраняет его в матрице `myImage_Converted`.

В исходном виде функция `cvtColor()` выглядит следующим образом:

```cpp
cvtColor(Source Matrix, Destination Matrix, Color Space Conversion Code)
```

Пример:

```cpp
#include<iostream>
#include<opencv2/highgui/highgui.hpp>
#include<opencv2/imgproc/imgproc.hpp>
using namespace cv;
using namespace std;

int main(int argc, const char** argv) 
{
   Mat myImage;//declaring a matrix to load the image//
   Mat myImage_Converted;//declaring a matrix to store the converted image//  
   namedWindow("Actual_Image");//declaring window to show actual image//
   namedWindow("Converted_Image");//declaring window to show converted image//
   myImage = imread("cat.jpg");//loading the image in myImage matrix//
   cvtColor(myImage,myImage_Converted, COLOR_RGB2GRAY);//converting RGB to Grayscale//
   imshow("Actual_Image",myImage);//showing Actual Image//
   imshow("Converted_Image",myImage_Converted);//showing Converted Image//  
   waitKey(0);//wait for key stroke
   destroyAllWindows();//closing all windows
   return 0;
}
```

## Смотрите также:

1. [[Цветовые пространства OpenCV|Цветовые схемы OpenCV]]

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#КомпьютерноеЗрение/opencv 
#cpp 

---

[^def]: термин
[^que]: вопрос