1. Зайти в директорию `~/Firmware/Tools/sitl_gazebo/models/6qr`
```bash
cd ~/Firmware/Tools/sitl_gazebo/models/6qr 
```
2. Открыть файл `model.sdf`:
```bash
gedit model.sdf
```
3. Заменить в указанной строчке `qr_1` на `6qr`:
![[Pasted image 20231016223652.png]]