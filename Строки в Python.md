---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 03 # Не трогать!
Месяц: 02 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-02-03

---
# [[Строки в Python]]


## Методы строки

```Python
hello = 'hello'
print(hello.upper())
print(hello.lower())
print(hello.lower().count('ll'))
```

## Математические операции со строкой

### Умножение на целое число

```python
hello = 'hello'
y = 3

print(hello * y) # hellohellohello
```

### Сложение строк

```python
hello = 'hello'
y = yes
print(hello + y) # helloyes
```

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Входящие 

---

[^def]: термин
[^que]: вопрос