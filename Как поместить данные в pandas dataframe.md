---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 04 # Не трогать!
Месяц: 07 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-07-04

---
# [[Как поместить данные в pandas dataframe]]


```python
df = pd.DataFrame({
    'X1' : X[:, 0],
    'X2' : X[:, 1],
    "label" : y
})

# или

data_dict = {
    'X1' : X[:, 0],
    'X2' : X[:, 1],
    'label' : y
}

df2 = pd.DataFrame.from_dict(data=data_dict)

---

           X1        X2  label
0    0.754246  0.231481      1
1   -0.756159  0.153259      1
2   -0.815392  0.173282      1
3   -0.393731  0.692883      1
4    0.442208 -0.896723      0
..        ...       ...    ...
995  0.244054  0.944125      0
996 -0.978655 -0.272373      0
997 -0.136900 -0.810012      1
998  0.670362 -0.767502      0
999  0.281057  0.963824      0

[1000 rows x 3 columns]
```



## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. [[02_pytorch_classification.ipynb]]


---
#pandas 

---

[^def]: термин
[^que]: вопрос