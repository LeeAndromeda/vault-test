---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 15 # Не трогать!
Месяц: 08 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-08-15

---
# [[Терминология GoogleTest и почему ее стоит знать]]

- **Test** - тест
- **Test Case** - группа связанных тестов (устаревшее), тест(неиспользуемое???)
- **Test Suite** - та же группа связанных тестов, но на текущий момент является предпочтительным


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#GoogleTest 

---

[^def]: термин
[^que]: вопрос