---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 24 # Не трогать!
Месяц: 09 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-09-24

---
# [[Старые и новые методы форматирования текста в C++]]

Форматирование текста на C++ можно реализовать несколькими способами:  
  

- потоками ввода-вывода. В частности, через `std::stringstream` с помощью потоковых операций (таких как `operator <<`);
- функциями `printf`, в частности `sprintf`;
- с помощью библиотеки форматирования C++20, в частности `std::format` / `std::format_to`;
- с помощью сторонней библиотеки, в частности `{fmt}` (основа новой стандартной библиотеки форматирования).

  
Первые два варианта представляют старые способы. Библиотека форматирования, очевидно, является новым. Но какой из них лучше в плане производительности? Это я и решил выяснить.  
  

## Примеры

  
Для начала разберём простые примеры форматирования текста. Предположим, нам нужно отформатировать текст в виде `"severity=1,error=42,reason=access denied"`. Это можно сделать так:

- с помощью потоков:  
  

```cpp
int severity = 1;
unsigned error = 42;
reason = "access denied";

std::stringstream ss;
ss << "severity=" << severity
   << ",error=" << error
   << ",reason=" << reason;

std::string text = ss.str();
```

• с помощью `printf`:  
  

```cpp
int severity = 1;
unsigned error = 42;
reason = "access denied";

std::string text(50, '\0');
sprintf(text.data(), "severity=%d,error=%u,reason=%s", severity, error, reason);
```

  
• с помощью `format`:  
  

```cpp
int severity = 1;
unsigned error = 42;
reason = "access denied";

std::string text = std::format("severity={},error={},reason={}", severity, error, reason);

// либо

std::string text;
std::format_to(std::back_inserter(text), "severity={},error={},reason={}", severity, error, reason);
```

Вариант с `std::format` во многом похож на `printf`, хотя здесь вам не нужно указывать спецификаторы типов, такие как `%d`, `%u`, `%s`, только плейсхолдер аргумента `{}`. Естественно, спецификаторы типов доступны, и о них можно почитать [тут](https://en.cppreference.com/w/cpp/utility/format/formatter), но эта тема не относится к сути статьи.  
  
Вариант с `std::format_to` полезен для добавления текста, поскольку производит запись в выходной буфер через итератор. Это позволяет нам присоединять текст условно, как в примере ниже, где `reason` записывается в сообщение, только если содержит что-либо:  
  

```cpp
std::string text = std::format("severity={},error={}", severity, error);

if(!reason.empty())
  std::format_to(std::back_inserter(text), ",reason=", reason);
```
#Входящие 

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://habr.com/ru/companies/ruvds/articles/761910/


---
#Входящие 

---

[^def]: термин
[^que]: вопрос