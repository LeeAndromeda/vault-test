---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 08 # Не трогать!
Месяц: 01 # Не трогать!
Год: 2023 # Не трогать!
Дата_создания: Sunday 8th January 2023 23:50:14

---
# [[Встреченные ошибки в C++ и как с ними бороться]]

**Приложенное аудио описывает проблему.**

```cpp
valgrind ./dstar
```

## Приложения

![[Jan 8, 11.47 PM_.mp3]]

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://www.youtube.com/watch?v=S-4j2Byi8KM
> 2. https://www.youtube.com/watch?v=Sddn1UjzSAo


---
#Входящие 

[^def]: термин
[^que]: вопрос