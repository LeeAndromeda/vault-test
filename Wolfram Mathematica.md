---
number headings: first-level 1, max 6, _.1. .1.

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 22 # Не трогать!
Месяц: 02 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-02-22

---
# [[Wolfram Mathematica]]

## 2. Структуры данных

### 2.1. Список

- {1, 2, 3, 4, 5} - базовая форма
- {{1, 2}, {2, 3}, {3, 4}} - список списков
- Range[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10} - функция, которая возвращает список

#### 2.1.1. Операторы действий со списком

- `+` - каждый элемент второго списка складывает с соответствующим элементом первого списка. Так же работает вычитание
- `*` - из двух списков создает матрицу 2х2, где каждому элементу первого списка соответствует элемент второго

#### 2.1.2. Функции действий со списком

- `Transpose[]` - транспонирование матрицы. Если увас есть список из двух списков, то, транспонировав его, вы получите список пар.

### 2.2. Таблицы

#### 2.2.1. Table[]

Первый аргумент - выражение, которое составит итоговый список, а второй элемент диапазон зачений переменной.

![[Pasted image 20230222044400.png]]

#### 2.2.2. Grid[]

Применив эту функцию к функции `Table[]`, получите нормальную таблицу:

![[Pasted image 20230222044637.png]]


##### 2.2.2.1. Спецификаторы

- `Frame` - заключить ли элементы в рамку

## 3. Операторы

- `Func[]/@{some, list}` - применить функцию слева к каждому элементу списка справа
- `arg // Func` - применяет функцию справа к аргументу слева

## 4. Условные выражения

Для этого используется функция `If[]`. Базовый синтаксис такой:

```Wolfram
If[conditional_expression, corresponding_result_value_or_expr, default_value_or_epxr]
```

## 5. Графики (Визуализация)

Для пострения графиков обычно используются различные версии функции `Plot[]`, работающие по одним и тем же конвенциям.

### 5.1. Диапазон аргумента

```Wolfram
Plot[F[x], {x, 0, 5}] (*Вот это диапазон*)
```

### 5.2. График двух функций

```Wolfram
Plot[{F[x], G[x]}, {x, 0, 5}, GridLines -> Automatic]
```

### 5.3. Ползунок изменения переменной
```Wolfram
Manipulate[
 	Plot[f[x, k], {x, 0, 5}, AspectRatio -> 1, PlotRange -> 10] ,
 	{k, 0, 5}
 ]
```

### 5.4. Объединение графиков

Для этого используется команда `Show[]`:

```Wolfram
Show[{plot1, plot2}, PlotStyle -> {Opacity[0.5]}]
```

![[UnitedGraphs.pdf]]

### 5.5. Легенда для пользовательского объекта

Для этого используется команада `Legended[]`:

```Wolfram
Legended[
 	Show[{plot1, plot2}, PlotStyle -> {Opacity[0.5]},  
  AxesLabel -> {"x", "y", "z"}],
SwatchLegend[{Purple, Cyan}, {"Решение есть", "Решения нет"}]
 ]
```

![[Pasted image 20230223085413.png]]

z### 5.6. Виды графиков

- 1д график!
	- [[NumberLinePlot example - Mathematica|NumberLinePlot]]
- 2д график
	- Обычный
		- [[Plot example - Mathematica|Plot]]
	- График от двух переменных. Функция - цвет
		- Обычный
			- [[DensityPlot example - Mathematica|DensityPlot]]
		- С комплексными числами в цвете
			- [[ComplexPlot example - Mathematica|ComplexPlot]]
	- График с закрашиванием зон
		- [[RegionPlot example - Mathematica|RegionPlot]]
	- Параметрический график
		- [[ParametricPlot example - Mathematica|ParametricPlot]]
	- График с точками
		- [[ListPlot example - Mathematica|ListPlot]] - принимает точки
		- [[DiscretePlot example - Mathematica|DiscretePlot]] - принимает функцию и рисует с определенным интервалом
			- ExtentSize - ширина рисуемой точки(линии)
			- ExtentMarkers - что нарисовать на краях линии
	- Гистограмма
		- [[Histogram example - Mathematica|Histogram]]
	- График из 2D массива или картинка
		- [[ArrayPlot example - Mathematica|ArrayPlot]]
	- График рельефа
		- [[ReliefPlot example - Mathematica|ReliefPlot]]
	- График графа
		- [[GraphPlot example - Mathematica|GraphPlot]]
- 3д график
	- Обычный
		- [[Plot3D example - Mathematica|Plot3D]]
	- С комплексными числами в цвете
		- [[ComplexPlot3D example - Mathematica|ComplexPlot3D]]
	- График 3д пространств(регионов)
		- [[RegionPlot3D example - Mathematica|RegionPlot3D]]
	- Параметрический график
		- [[ParametricPlot3D example - Mathematica|ParametricPlot3D]]


### 5.7. Спецификаторы

- GridLines - сетка
	- Можно задать линии сетки в определенных координатах
		- GridLines -> {{-1}, { 7}}
- PlotPoints - разрешение графика
- PlotRange - область допустимых значений функции
- AxesLabels - наименование осей
- PlotLabel - название графика
- **PlotStyle** - кастомизация графика
	- Цвет
		- Любой из цветов радуги английским словом(Red, Green ...)
		- `RGBColor[r, g, b]`
		- `CMYKColor`
	- Opacity - прозрачность
	- Spuclarity - отражаемость поверхности
	- Dashing - прерывистые линии
	- PointSize - размер точек
	- EdgeForm - стиль ребер
	- FaceForm - стиль сторон
	- GrayLevel
	- Hue
	- Directive
- PlotLegends - создает описание графиков мультиграфика
	- Automatic
	- {expr1, expr2}
	- `Placed[{expr1, expr2}, where]`
- Mesh - Сетка трехмерного графика
- **ExlusionsStyle** - стилизация исключенных частей на графике
- ColorFunction - выбирает цветовую гамму графика
- Filling - настраивает заполнение графика
- FaceGrids - настраивает сетку стороны 3Д пространства
- ViewPoint - точка зрения на 3Д график
	- Вы можете самостоятельно задать координаты точки зрения на график, даже используя координаты типа $\infty$,чтобы получать проекции на плоскости
- 

#### 5.7.1. Функция [Graphics](https://reference.wolfram.com/language/ref/Graphics.html)/[Graphics3D](https://reference.wolfram.com/language/ref/Graphics3D.html)

#### 5.7.2. Углубленное изучение конкретных вопросов построения графиков

##### 5.7.2.1. Построение проекций трехмерного графика

- [[Построение проекций трехмерного графика]]

## 6. Функции и модули

### 6.1. Определение своих функций

![[Pasted image 20230222031149.png]]
Аргументы записываются через запятую с нижним подчеркиванием после имени. В случае, если вы хотите указать тип аргумента, он указывается после нижнего подчеркивания. 

Функция определяется знаком `:=`, после чего идет ее определение.

Функции могут определяться рекурсивно.

### 6.2. Определение своих модулей(классов)

![[Pasted image 20230222031545.png]]

Можно обьявлять локальные переменные. Возвращаемым значением модуля является последняя выполняемая строчка.

## 7. Встроенные функции

### 7.1. Алгебра

- `FullSimplify[]` - попытается упростить выражение
- `Solve[]` - Решает [[Wolfram Mathematica#Solve|уравнения]]
- `NSolve[]` - решает уравнения численно
- `DSolve[exprssns, funcs, indep_var]` - решает (системы) дифференциальные уравнения. В следущем файле пример работы с `DSolve[]` - [[SolvingDifferentialEquations.nb]]
- `Reduce[]` -  так же решает уравнения.
	- В случае если ответ дан в форме `a || b` нужно читать $a \cup b$
- `Sum[]` - [[Wolfram Mathematica#Sum|суммирует]] значения выражения
- `Accumulate[]` - [[Wolfram Mathematica#Accumulate|нарастающая]] сумма
- `FunctionDomain[]` - находит область определения функции
- `Simplify[]` - упрощает выражение

#### 7.1.1. Solve

Базовый синтаксис таков:

```Wolfram
Solve[expr, var]
```

Если вы хотите решить систему уравнений, то нужно систему выразить списком:

```Wolfram
Solve[{expr1, expr2, expr3}, {var1, var2, var3}]
```

##### 7.1.1.1. Спецификаторы

- **Quartics** - запись ответа с абсолютной точностью

#### 7.1.2. Sum

![[Pasted image 20230222043337.png]]


#### 7.1.3. Accumulate

![[Pasted image 20230222043840.png]]

### 7.2. [[Комплексные числа]]

- `ComplexExpand[]` - описывает полную комплексную форму аргумента 

### 7.3. Логика

- `Exists[x, expr]` - утверждает, что существует значение x, для которого выражение является истиной. [Используй](https://reference.wolfram.com/language/ref/Resolve.html) `Resolve[]` для нахождения значений x.

### 7.4. Операции над множествами

- `Union[]` - объединение
- `Intersection[]` - пересечение
- `Complement[]` - дополнение (разность множеств)
- `SymmetricDifference[]` - симметрическая разность

- [Subsets](https://reference.wolfram.com/language/ref/Subsets.html) — find all possible subsets of a set (the powerset)
- [Tuples](https://reference.wolfram.com/language/ref/Tuples.html) — find all possible tuples of n elements from a set
- [Groupings](https://reference.wolfram.com/language/ref/Groupings.html) — find all possible groupings of elements

#### 7.4.1. Testing Relations between Sets

- [IntersectingQ](https://reference.wolfram.com/language/ref/IntersectingQ.html) - возвращает true, если множества пересекаются 
- [DisjointQ](https://reference.wolfram.com/language/ref/DisjointQ.html)  - если не пересекаются - true
- [SubsetQ](https://reference.wolfram.com/language/ref/SubsetQ.html) - true, если лист 2 - подмножество листа 1

https://reference.wolfram.com/language/guide/OperationsOnSets.html

### 7.5. [Случайные числа](https://reference.wolfram.com/language/tutorial/RandomNumberGeneration.html)

Для этого используются функции семейства Random:

- RandomReal
- RandomInteger
- RandomComplex
- RandomPrime

### 7.6. Системные

- `Thread[CustomFunc[Range[n]]]` - позволяет применить пользовательскую функцию к каждому элементу списка
- `Clear[expr]` - очищает значение данного выражения

### 7.7. Пользовательские функции

##### 7.7.1.1. [Кусочно определенные функции](https://reference.wolfram.com/language/ref/Piecewise.html)

## 8. Воркфлоус

### 8.1. Работа с матрицами

#### 8.1.1. Определение матрицы

Определить матрицу можно обычным [[#1.1. Список|списком]]. А можно и более приятным способом.

##### 8.1.1.1. Определение матрицы горячими клавишами

Перед тем, как определить матрицу, желательно создать две курглые скобки

- **Ctrl + Enter** - создает новую строку матрицы(причем даже если вы не в м атрице)
- **Ctrl + ,** - создает новый столбец матрицы

#### 8.1.2. Изображение матрицы

##### 8.1.2.1. MatrixForm

Если ваша матрица хранится в форме списка, используйте функцию MatrixForm, чтобы увидеть ее в форме матрицы. **Выход MatrixForm не используется для определения матриц, а объекты, которые в итоге получаются, нужно только тдля визуального представления матрицы.**

<mark style="background: #FF5582A6;">Внимание!</mark> Не используйте  `// MatrixForm` при присвоении значения новой матрице, так как это влияет на дальенейшие вычисления, связанные с этой матрицей.  

#### 8.1.3. Матричные операции

- **[[Умножение матриц]]** - есть два способа
	1. `Dot[mx1, mx2]` - функция
	2. `mx1.mx2` - точка
- Поэлементное умножение матриц - mx1 \* mx2
- *Возведение матрицы в степень* - `MatrixPower[mx, power]` - **матричное** умножение `power` - 1 раз. В Mathematica $A ^{2}$ означает $A * A$
- Возведение элементов матриц в степень - mx^2
- Извлечение элемента матрицы:
	- `Part[mx, row, column]` - базовый синтаксис, можно использовать больше измерений матрицы 
	- `mx[[row column]]` - то же самое
- Извлечение строки или столбца <mark style="background: #FFF3A3A6;">внимание на синтаксис</mark>:
	- `Transpose[{mx[[All, column]}]` - весь столбец
	- `{mx[[row, All]}` - вся строка
- Вертикальное соединение матриц - `Join[mx1, mx2]`
- Горизонтальное соединение матриц - `Transpose[Join[Transpose[mx1], Transpose[mx2]]]`

### 8.2. [[#6.1. Алгебра|Решение дифференциальных уравнений]]

### 8.3. Работа с множествами



### 8.4. [Создание видео в Mathematica](https://www.youtube.com/watch?v=S03e6dwM100&list=PLxdnSsBqCrrE4j99TtW_zdyED2IVgbBUd&index=5)

## 9. Решение типовых задач

### 2D проекция 3D фигуры

https://mathematica.stackexchange.com/questions/164663/2d-projection-of-a-3d-surface

### Проекция 3D кривой на 2D

https://mathematica.stackexchange.com/questions/114051/projection-of-a-3d-curve-to-2d

### Получение координат пересечения двух поверхностей

https://mathematica.stackexchange.com/questions/113956/how-to-obtain-the-coordinates-of-the-intersection-line-of-two-surfaces

## 10. Связано:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://www.youtube.com/watch?v=2bCv1CFKGr4
> 2. https://reference.wolfram.com/language/


---
#Маетматика/ПО

---

[^def]: термин
[^que]: вопрос