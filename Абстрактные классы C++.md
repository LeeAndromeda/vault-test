---
Тип: Задача
Описание: -
Исполнители: 
Проект: ВнедрениеЗнаний

Tags: Задача
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: 31 # Не трогать!
Месяц: 03 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-03-31

дата_завершения: "2023-нет-нет"

Выполнено: нет
Результат: 
---

# [[Абстрактные классы C++]]

> [!danger] Обязательный чеклист
> - [ ] Получение одной ссылки
> - [ ] Расставить теги 


> [!example]+  Чеклист знания
> - Получение источников и ссылок
> 	- [ ] Получение ссылок на разные источники
> - Конспектирование
> 	- [ ] Достаточное для использования конспектирование
> 	- [ ] Полное конспектирование
> - Создание справок
> 	- [ ] Справки практического применения
> 	- [ ] Список терминов
> 	- [ ] Определение терминов из списка терминов
> 	- [ ] Список связанных тем(если нет внутри текста)
> - Внедрение в базу знаний
> 	- [ ] Создание связей с оригинальной страницей (этой)
> 	- [ ] Разбиение на подтемы
> 	- [ ] Атомизация - (по необходимости) разбиение конспекта по подтемам на страницы
> - [ ] Внедрение в практику
> 

<mark style="background: #FF5582A6;">**Содержание потом вырезается, все содержимое конспекта удаляется и содержание вставляется по шаблону входящего конспекта.**</mark> 

# Содержание 

**Абстрактный класс** - это класс, который содержит хотя бы одну pure-virtual функцию. Экземпляр абстрактного класса создать нельзя, однако от него можно наследовать. В таком случае наследник должен иметь свои определения pure-virtual функций, иначе он тоже будет считаться абстрактным классом. 

Абстрактные классы используются для создания обобщений, которые не могут существовать сами по себе, но определяют общие черты более конкретных представителей своего рода. Например, класс `Shape` не может существовать сам по себе и его следует создавать абстрактным, так как его наследники `Circle`, `Square`, `Triangle` и т.д. все будут иметь свойства, присущие классу `Shape`, например площадь, периметр и т.д.

**[[Pure virtual функции C++|Pure-virtual функция]]** - это виртуальная функция, объявленная следующим синтаксисом:

```cpp
// An abstract class
class Test
{   
    // Data members of class
public:
    // Pure Virtual Function
    virtual void show() = 0;
   /* Other members */
};
```

Она имеет [[Спецификатор virtual C++|спецификатор `virtual`]] и приравнивается к нулю.

## Смотрите также:

> [!question]- Соурслист
> 1. https://www.geeksforgeeks.org/pure-virtual-functions-and-abstract-classes/
> 2. https://en.cppreference.com/w/cpp/language/abstract_class

> [!quote]- Контекст
> ```dataview
> LIST 
> FROM !outgoing([[]])
> WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
> ```

---
#ВнедрениеЗнаний 
#cpp


---