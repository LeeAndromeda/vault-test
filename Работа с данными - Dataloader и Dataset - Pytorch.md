---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 04 # Не трогать!
Месяц: 07 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-07-04

---
# [[Работа с данными - Dataloader и Dataset - Pytorch]]

- [[Как создать собственный датасет в pytorch]]

The PyTorch DataLoader class is a powerful tool for preparing, managing, and serving data to deep learning networks. It provides various functionalities to streamline data loading and preprocessing, making your code more readable and maintainable.

## Гиперпараметры 

- `batch_size: int` - размер батча
- `shuffle: bool` - перемешивать ли данные
- `num_workers` - сколько ядер использовать для загрузки

## Table of Contents
- [What Does a PyTorch DataLoader Do?](#what-does-a-pytorch-dataloader-do)
- [Understanding the PyTorch DataLoader Class](#understanding-the-pytorch-dataloader-class)
- [Creating and Using a PyTorch DataLoader](#creating-and-using-a-pytorch-dataloader)
- [Accessing Data and Targets in a PyTorch DataLoader](#accessing-data-and-targets-in-a-pytorch-dataloader)
- [Loading Data to a GPU (CUDA) With a PyTorch DataLoader](#loading-data-to-a-gpu-cuda-with-a-pytorch-dataloader)

## What Does a PyTorch DataLoader Do?
The PyTorch DataLoader class allows you to:
1. Define a dataset: Specify the source and access method for your data.
2. Batch the data: Set the number of samples per batch for more manageable training and testing.
3. Shuffle the data: Shuffle the data during loading to increase representativeness and prevent skewness.
4. Support multi-processing: Utilize multiple processors to load data concurrently, saving time during training and testing.
5. Merge datasets: Optionally merge multiple datasets together for convenience.
6. Load data directly on CUDA tensors: Load data onto CUDA before returning, enabling GPU acceleration.

## Understanding the PyTorch DataLoader Class
The PyTorch DataLoader class has several parameters that control its behavior. Here are some important ones:
- `dataset`: Specifies the PyTorch Dataset from which to load the data.
- `batch_size`: Determines the number of samples to load in each batch.
- `shuffle`: Specifies whether to shuffle the data at each epoch.
- `sampler`: Defines how samples are drawn from the dataset (cannot be used with shuffling).
- `num_workers`: Controls the number of subprocesses used for loading data.

## Creating and Using a PyTorch DataLoader
To create a PyTorch DataLoader, you can use a built-in dataset. Here's an example using the MNIST dataset:
```python
from torchvision.datasets import MNIST
from torch.utils.data import DataLoader

# Downloading and Saving MNIST
data_train = MNIST('~/mnist_data', train=True, download=True, transform=transforms.ToTensor())

# Creating Data Loader
data_loader = DataLoader(data_train, batch_size=20, shuffle=True)
```

> [!info]- Parameters of the DataLoader
> `shuffle` - bool, if to shuffle the dataset befor each epoch
> `batch_size` - int, number of elements to process in an epoch


## Accessing Data and Targets in a PyTorch DataLoader

You can access the data and targets in a PyTorch DataLoader by iterating over the batches. Here's an example:

```python
for idx, (data, target) in enumerate(data_loader):
    # Accessing data and target for each batch
    print(data[0])
    print(target[0])
    break  # Only printing the first batch
```

## Loading Data to a GPU (CUDA) With a PyTorch DataLoader

To load data onto a GPU (typically CUDA) using a PyTorch DataLoader, you can utilize the `to` method. Here's an example:

```python
import torch

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

for idx, (data, target) in enumerate(data_loader):
    data = data.to(device)
    target = target.to(device)
```

This allows your code to dynamically choose between CPU and GPU devices, making it more flexible.

By utilizing the PyTorch DataLoader and Dataset classes effectively, you can streamline your data loading process, make training and testing more manageable, and take advantage of GPU acceleration.

## Дополнительно

### Как получить один элемент из Dataloader'а

```python
img, label = next(iter(train_dataloader))
```

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://datagy.io/pytorch-dataloader/


---
#pytorch 
#chatgpt 

---

[^def]: термин
[^que]: вопрос