---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 01 # Не трогать!
Месяц: 08 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-08-01

---
# [[Установка OpenCV на Linux]]


 Quick start
Build core modules
# Install minimal prerequisites (Ubuntu 18.04 as reference)
```bash
sudo apt update && sudo apt install -y cmake g++ wget unzip
```
# Download and unpack sources
```bash
wget -O opencv.zip https://github.com/opencv/opencv/archive/4.x.zip
unzip opencv.zip
```
# Create build directory
```bash
mkdir -p build && cd build
```
# Configure
```bash
cmake ../opencv-4.x
```
# Build
```bash
cmake --build .
```

Build with opencv_contrib
# Install minimal prerequisites (Ubuntu 18.04 as reference)
```bash
sudo apt update && sudo apt install -y cmake g++ wget unzip
```
# Download and unpack sources
```bash
wget -O opencv.zip https://github.com/opencv/opencv/archive/4.x.zip
wget -O opencv_contrib.zip https://github.com/opencv/opencv_contrib/archive/4.x.zip
unzip opencv.zip
unzip opencv_contrib.zip
```
# Create build directory and switch into it
```bash
mkdir -p build && cd build
```
# Configure
```bash
cmake -DOPENCV_EXTRA_MODULES_PATH=../opencv_contrib-4.x/modules ../opencv-4.x
```
# Build
```bash
cmake --build .
```


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Входящие 

---

[^def]: термин
[^que]: вопрос