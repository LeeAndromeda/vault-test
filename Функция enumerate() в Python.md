---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 16 # Не трогать!
Месяц: 06 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-06-16

---
# [[Функция enumerate() в Python]]


The `enumerate()` function in Python is a built-in function that takes an iterable object (e.g., list, tuple, string) as input and returns an enumerate object. The enumerate object contains pairs of the original iterable's elements along with their corresponding index values.

Here's an example:

pythonCopy Code

`fruits = ['apple', 'banana', 'orange'] for i, fruit in enumerate(fruits):     print(i, fruit)`

Output:

Copy Code

`0 apple 1 banana 2 orange`

In this example, the `enumerate()` function is used to iterate over the `fruits` list and obtain the index value of each element. The `for` loop assigns the index value (`i`) and the element (`fruit`) to two separate variables. These variables are then printed using the `print()` function.

The `enumerate()` function can also take an optional second argument which specifies the starting value for the index. For example, `enumerate(fruits, 1)` would start the index at 1 instead of the default value of 0.

The `enumerate()` function is a useful tool when you need to loop through an iterable object and keep track of the index values at the same time.


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Python 

---

[^def]: термин
[^que]: вопрос