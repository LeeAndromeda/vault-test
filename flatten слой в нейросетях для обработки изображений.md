---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 14 # Не трогать!
Месяц: 08 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-08-14

---
# [[flatten слой в нейросетях для обработки изображений]]


**Flatten слой** используется потому, что для создания полностью соединенной ([fully interconnected](https://www.tutorialspoint.com/what-is-fully-interconnected-network-topology)) связи между входным и следующим слоем необходим вектор, а не матрица. В общем случае, слой flatten необходим для всех многомерных входных данных.


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Нейросети 
#КомпьютерноеЗрение

---

[^def]: термин
[^que]: вопрос