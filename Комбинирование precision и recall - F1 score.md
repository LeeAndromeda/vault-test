




---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 11 # Не трогать!
Месяц: 08 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-08-11

---
# [[Комбинирование precision и recall - F1 score]]


Для того, чтобы сделать сбалансированную модель классификации, которая будет работать, учитывая и [[Precision - количество верных срабатываний к количеству всех срабатываний - метрика оценки нейронных сетей|precision]], и [[Recall - покрытие искомых случаев - метрика оценки нейронных сетей|recall]], чаще всего используется F1 score - [[Среднее гармоническое|гармоническое среднее]] precision и recall, учитывающее оба параметра и [не пропускающее экстремальные значения](https://stackoverflow.com/questions/26355942/why-is-the-f-measure-a-harmonic-mean-and-not-an-arithmetic-mean-of-the-precision?).

Например, классификатор с precision 1,0 и recall 0,0 имеет простое среднее значение 0,5, а F1 score 0. F1 score дает равный вес обоим показателям и является частным примером общей метрики Fβ, где β может быть скорректирована для придания большего веса либо recall, либо precision. (Существуют и другие метрики, позволяющие объединить precision и recall, например, [Geometric Mean](https://en.wikipedia.org/wiki/Fowlkes%E2%80%93Mallows_index?и) от precision и recall, но оценка F1 является наиболее часто используемой). Если мы хотим создать сбалансированную модель классификации с оптимальным соотношением recall и precision, то мы стараемся максимизировать показатель F1.


## Смотрите также:
- [[Confusion matrix - визуализация точности классификации]]
```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://willkoehrsen.github.io/statistics/learning/beyond-accuracy-precision-and-recall/


---
#Нейросети 

---

[^def]: термин
[^que]: вопрос