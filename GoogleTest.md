---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 15 # Не трогать!
Месяц: 08 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-08-15

---
# [[GoogleTest]]

> [!info]-
> GoogleTest - это среда тестирования, разработанная командой Testing Technology с учетом специфических требований и ограничений Google. Работаете ли вы в Linux, Windows или Mac, пишете ли вы код на языке C++, GoogleTest может вам помочь. Он поддерживает любые виды тестов, а не только модульные.

- [[Терминология GoogleTest и почему ее стоит знать]]
- [[Базовый процесс написания тестов с GoogleTest]]
- [[Assertions - утверждения в GoogleTest]]
- [[Простые тесты на GoogleTest]]
- [[Быстрый старт CMake и GoogleTest]]


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. http://google.github.io/googletest/primer.html


---
#GoogleTest
#cpp 

---

[^def]: термин
[^que]: вопрос