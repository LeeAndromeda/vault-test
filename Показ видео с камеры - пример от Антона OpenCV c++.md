---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 18 # Не трогать!
Месяц: 07 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-07-18

---
# [[Показ видео с камеры - пример от Антона OpenCV c++]]

Для этого используется класс [[cv VideoCapture - OpenCV c++|cv::VideoCapture]].

```cpp
#include "opencv2/opencv.hpp"
#include <iostream>

int main()
{
    cv::Mat frame;

    cv::VideoCapture cap;
    cap.open("/dev/video0");    // запускает стриминг с камеры под индексом 0
    cap.set(cv::CAP_PROP_FRAME_HEIGHT, 720);
    cap.set(cv::CAP_PROP_FRAME_WIDTH, 1280);
    cap.set(cv::CAP_PROP_FPS, 60);
    
    while(true)
    {
        cap.read(frame);

        cv::imshow("this is you, smile! :)", frame);

        if( cv::waitKey(10) == 27 ) 
            break; // stop capturing by pressing ESC 
    }
    return 0;
}
```


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://github.com/LeekaiDel/drone_ws/tree/for_sim/src/packageInDev/cv_cpp_test


---
#КомпьютерноеЗрение/opencv 
#cpp 

---

[^def]: термин
[^que]: вопрос