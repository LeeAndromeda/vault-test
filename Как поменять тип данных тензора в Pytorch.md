---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 19 # Не трогать!
Месяц: 06 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-06-19

---
# [[Как поменять тип данных тензора в Pytorch]]


You can change the datatypes of tensors using [`torch.Tensor.type(dtype=None)`](https://pytorch.org/docs/stable/generated/torch.Tensor.type.html) where the `dtype` parameter is the datatype you'd like to use.

First we'll create a tensor and check it's datatype (the default is `torch.float32`).
```python
In [47]:

# Create a tensor and check its datatype
tensor = torch.arange(10., 100., 10.)
tensor.dtype

Out[47]:

torch.float32
```
Now we'll create another tensor the same as before but change its datatype to `torch.float16`.
```python
In [48]:

# Create a float16 tensor
tensor_float16 = tensor.type(torch.float16)
tensor_float16

Out[48]:

tensor([10., 20., 30., 40., 50., 60., 70., 80., 90.], dtype=torch.float16)
```
And we can do something similar to make a `torch.int8` tensor.
```python
In [49]:

# Create a int8 tensor
tensor_int8 = tensor.type(torch.int8)
tensor_int8

Out[49]:

tensor([10, 20, 30, 40, 50, 60, 70, 80, 90], dtype=torch.int8)
```
## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#pytorch 

---

[^def]: термин
[^que]: вопрос