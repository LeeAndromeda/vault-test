---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 10 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-10-10

---
# [[Inachevé - Род существительных во французском языке]]


Во французском языке у существительных два рода - мужской и женский. 

К мужскому роду во французском языке относятся названия дней недели (le lundi), месяцев (le mars) и времён года (le printemps). Параллельно заметим, что с весной говорят au printemps, а с остальными — en été, en automne, en hiver).

Как правило, к мужскому роду относятся названия деревьев — le sapin (ель), le bouleau (берёза), le tilleul (липа). Аналогично названия металлов — le fer (железо), le cuivre (медь), l’acier (сталь), l’argent (серебро, деньги). Исключение: la fonte (чугун). Также к мужскому роду относятся все наименования частей речи: un nom (существительное), un adjectif (прилагательное), un verbe (глагол). А вот к женскому роду относятся наименования наук la botanique (ботаника), la médecine (медицина), исключение: le droit (право)

Особо надо отметить, что во французском языке есть ряд абстрактных существительных с суффиксом -eur , которые относятся к женскому роду — la douleur (боль), la chaleur (жара), la pâleur (бледность) etc.

Отдельный класс составляют слова мужского рода с окончанием -il, которое читается как \[il\]: un fil (волокно, нитка), un cil (ресница, ресничка), un péril (опасность), le brésil (кампешевое дерево; Бразилия), le grésil (град), un nombril (пупок, в последних двух словах конечная -l может читаться, а может и не читаться) или вообще не читается: un fusil (ружьё), un sourcil (бровь), le persil (петрушка; заметим, что faire son persil имеет жаргонное значение — заниматься проституцией)

## Таблица окончаний-суффиксов, указывающих род

Здесь указаны вперемешку существительные и прилагательные, так как они легко могут переходят друг в друга. Например italien означает, как итальянец, так и итальянский.


### Мужской род

- -age	
	- le vitrage (остекление) (кроме plage, image, cage, page (страница) и другие, где -age входит в состав корня)
- -ard	
	- le tard (позднота), le vieillard (старик)
- -aire	
	- le salaire (зарплата), un anniversaire (годовщина) + un air (воздух), un éclair (молния, сверкание). Примечание: прилагательное nécessaire (необходимый) имеет одну форму для fém./masc. (уже с -e на конце) а прилагательное clair/claire (светлый, ясный) — две.
- -eau	
	- le manteau (пальто), le gâteau (пирог), НО: eau (вода) ж.р., peau (кожа) ж.р.
- -ème	
	- (слова греческого происхождения) le problème (проблема), le poème (поэма), le thème (тема), le système (система)
- -gramme	
	- (опять греки постарались) le télé gramme (телеграмма), le diagramme (диаграмма). НО une é pigramme (эпиграмма) ж.р.
- -isme	
	- le socialisme (социализм), le tourisme (туризм)
- -ment	
	- le gouvernement (правительство)
- -ot	
	- le grelot (бубенчик)
- -on	
	- le chaperon (шапочка, капюшон)


### Женский род

- -ade	
	- la façade (лицевая сторона), la promenade(прогулка)
- -aie	
	- la raie (межа)
- -aison	
	- la comparaison (сравнение), la raison (причина, довод)
- -ance	
	- la connaissance (знание, познание)
- -esse	
	- la vitesse (скорость)
- -ine	
	- la marine (морской флот)
- -ison	
	- la guérison (выздоровление)
- -itude	
	- l’attitude (отношение; поза в балете)
- -oison	
	- la cloison (перегородка), НО poison (яд) — м. р.
- -ose	
	- l’hypnose (гипноз), la chose (вещь)
- -sion	
	- la commission (комиссия)
- -tion	
	- natation (плавание), révolution (революция)
- -ure	
	- coiffure (прическа) confiture (варенье)


### Таблица созвучных окончаний, которые дают мужской и женский род 


![[Screenshot_2023-10-11-00-06-19-442_com.brave.browser.jpg]]![[Screenshot_2023-10-11-00-06-25-021_com.brave.browser.jpg]]




## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://www.le-francais.ru/oh-la-la/genre/


---
#Французский 

---

[^def]: термин
[^que]: вопрос