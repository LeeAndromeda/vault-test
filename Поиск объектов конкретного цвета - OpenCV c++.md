
---
Тип: Проект 
название_проекта: "(.*)[Nn][Vv][Ss][Ii][Bb][Cc][Vv](.*)" # "(.*)Проект(.*)"
Проект: нет
Описание: -
Дисциплина: #Входящие 
Tags: Проект
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: 18 # Не трогать!
Месяц: 07 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-07-18
Дата_создания: 2023-07-23, 14-27

дата_завершения: "2023-07-28"

Выполнено: да
Результат: 
---


# [[Поиск объектов конкретного цвета - OpenCV c++]]

> [!abstract]- Материалы
> - [[Хронологичская документация проекта nvsibcv]]
> - [[OpenCV C++ map.canvas|OpenCV C++ map]]
> 	- Алгоритмы поиска контура
> - [Anisotropic image segmentation by a gradient structure tensor](https://docs.opencv.org/4.x/d4/d70/tutorial_anisotropic_image_segmentation_by_a_gst.html)
> - [[Цветовые пространства OpenCV|Цветовые схемы OpenCV]]
> - [[HSV диапазоны цветов]]
> ```dataview
> LIST
> FROM (#ВнедрениеЗнаний/РасширяющееЗнание or #ВнедрениеЗнаний/РасширяющееЗнание/Тропинка) and #tag
> ```

> [!example]+ Подпроекты
> ```dataview
> TABLE Описание, день_завершения as "День завершения", месяц_завершения as "Месяц завершения", Исполнители, Выполнено
> FROM #Проект and !"Templates"
> WHERE Тип = "Проект" and regexmatch(this.название_проекта, Проект) and Выполнено = "нет"
> SORT год_завершения ASC, месяц_завершения ASC, день_завершения ASC
> ```

> [!hint]+ Задачи
> 
> > [!hint]+ Текущие по проекту
> > ```dataview
> > TABLE Описание, дата_завершения as "Дата завершения", Исполнители, Выполнено
> > FROM #Задача/РаботаИдет and !"Templates"
> > WHERE regexmatch(this.название_проекта, Проект) and Выполнено = "нет"
> > SORT дата_завершения ASC
> >```
>
> > [!example]- В очереди по проекту
> > ```dataview
> > TABLE Описание, дата_завершения as "Дата завершения", Исполнители, Выполнено
> > FROM #Задача/вОчереди and !"Templates"
> > WHERE regexmatch(this.название_проекта, Проект) and Выполнено = "нет"
> > SORT дата_завершения ASC
> >```
> 
> > [!faq]+ Ансорт
> > ```dataview
> > TABLE Описание, дата_завершения as "Дата завершения", Исполнители, Выполнено
> > FROM #Задача and !"Templates"
> > WHERE regexmatch(this.название_проекта, Проект) and Выполнено = "нет"
> > SORT дата_завершения ASC
> >```
> 
> > [!success]- Выполненные
> > ```dataview
> > TABLE Описание, дата_завершения as "Дата завершения", Исполнители, Выполнено
> > FROM #Задача  and !"Templates"
> > WHERE regexmatch(this.название_проекта, Проект) and Выполнено = "да"
> > SORT дата_завершения ASC
> >```



## Участники проекта

```dataview
LIST
FROM !"Obsidian/Templates" and !"Templates" and #Человек 
WHERE regexmatch(this.название_проекта, Проект) or regexmatch(Проект, this.название_проекта)
```

## Смотрите также

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
---

#Задача 
#КомпьютерноеЗрение/opencv 
#cpp 

---

