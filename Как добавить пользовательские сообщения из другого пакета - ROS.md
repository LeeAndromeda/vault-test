---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 21 # Не трогать!
Месяц: 06 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-06-21

---
# [[Как добавить пользовательские сообщения из другого пакета - ROS]]

Добавьте пакет, в котором находятся сообщения,в find_package:
```cmake
find_package(catkin REQUIRED COMPONENTS
  roscpp 
  drone_msgs
)
``` 

Затем, привяжите этот пакет к вашему исполняемому файлу как зависимость:
```cmake
add_executable(contour_planner_node src/move_along_contour.cpp)
target_link_libraries(contour_planner_node ${catkin_LIBRARIES})
add_dependencies(contour_planner_node ${catkin_EXPORTED_TARGETS})
```

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Входящие 

---

[^def]: термин
[^que]: вопрос