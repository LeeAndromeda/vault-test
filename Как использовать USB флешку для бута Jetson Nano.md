---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 07 # Не трогать!
Месяц: 12 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-12-07

---
# [[Как использовать USB флешку для бута Jetson Nano]]

**QSPI-NOR** - флеш память на самом джетсоне.

Когда Jetpack проходит процесс подготовки системы, он передает информацию с sd карты памяти в QSPI-NOR.

Если sd карты нет, он попытается найти rootfs(корень файловой системы) на USB носителях.



## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://jetsonhacks.com/2021/03/10/jetson-nano-boot-from-usb/


---
#Jetson/Nano 

---

[^def]: термин
[^que]: вопрос