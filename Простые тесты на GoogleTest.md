---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 15 # Не трогать!
Месяц: 08 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-08-15

---
# [[Простые тесты на GoogleTest]]


## Тест-функции

```cpp
TEST(TestSuiteName, TestName) {
  ... test body ...
}
```

> [!info]-
> 1. Use the `TEST()` macro to define and name a test function. These are ordinary C++ functions that don’t return a value.
> 2. In this function, along with any valid C++ statements you want to include, use the various GoogleTest assertions to check values.
> 3. The test’s result is determined by the assertions; if any assertion in the test fails (either fatally or non-fatally), or if the test crashes, the entire test fails. Otherwise, it succeeds.

For example, let’s take a simple integer function:

```cpp
int Factorial(int n);  // Returns the factorial of n
```

A test suite for this function might look like:

```cpp
// Tests factorial of 0.
TEST(FactorialTest, HandlesZeroInput) {
  EXPECT_EQ(Factorial(0), 1);
}

// Tests factorial of positive numbers.
TEST(FactorialTest, HandlesPositiveInput) {
  EXPECT_EQ(Factorial(1), 1);
  EXPECT_EQ(Factorial(2), 2);
  EXPECT_EQ(Factorial(3), 6);
  EXPECT_EQ(Factorial(8), 40320);
}
```

> [!info]+
> GoogleTest группирует результаты тестирования по тестовым наборам, поэтому логически связанные тесты должны находиться в одном тестовом наборе; другими словами, первый аргумент их TEST() должен быть одинаковым. В приведенном выше примере мы имеем два теста HandlesZeroInput и HandlesPositiveInput, которые принадлежат к одному тестовому набору FactorialTest.

При именовании тестовых наборов и тестов следует придерживаться тех же правил, что и при именовании функций и классов.

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. http://google.github.io/googletest/primer.html#simple-tests


---
#GoogleTest 

---

[^def]: термин
[^que]: вопрос