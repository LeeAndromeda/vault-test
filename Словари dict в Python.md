---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 03 # Не трогать!
Месяц: 02 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-02-03

---
# [[Словари dict в Python]]

Работает по приницпу ключ : значение.

```Python
x = {'key' : 4}

x['key2'] = 5
x[2] = 8
x[True] = [2, 4, 62]
```

### Ключ есть в словаре?

```Python
x = dict()
print('key' in x)
```

## Вывод словаря

Обычно из этих частей стоит создавать список:

```Python
x = {'key' : 4}
  
x['key2'] = 5
x[2] = 8
x[True] = [2, 4, 62]
  
print(list(x.keys()))
# [4, 5, 8, [2, 4, 62]]
```

### Вывести ключи

```Python
x = {'key' : 4}
  
x['key2'] = 5
x[2] = 8
x[True] = [2, 4, 62]
  
print(x.keys())
# dict_values([4, 5, 8, [2, 4, 62]])
```

### Вывести значения

```Python
x = {'key' : 4}
  
x['key2'] = 5
x[2] = 8
x[True] = [2, 4, 62]
  
print(x.values())
# dict_keys(['key', 'key2', 2, True])
```

## Удаление ключа со значением

```Python
x = {'key' : 4}
x['key2'] = 5
x[2] = 8
x[True] = [2, 4, 62]
  
print(x.values())
  
del x['key']
  
print(x.values())
# dict_values([4, 5, 8, [2, 4, 62]])
# dict_values([5, 8, [2, 4, 62]])
```

## Итерация через словарь

```Python
x = {'key' : 4}
x['key2'] = 5
x[2] = 8
x[True] = [2, 4, 62]
  
for key, value in x.items():
print(key, value)
# key 4
# key2 5
# 2 8
# True [2, 4, 62]

x = {'key' : 4}
x['key2'] = 5
x[2] = 8
x[True] = [2, 4, 62]
  
for key, value in x.items():
print(key, x[key])
# key 4
# key2 5
# 2 8
# True [2, 4, 62]
```

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Входящие 

---

[^def]: термин
[^que]: вопрос
