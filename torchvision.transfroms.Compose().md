---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 09 # Не трогать!
Месяц: 09 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-09-09

---
# [[torchvision.transfroms.Compose()]]

`torchvision.transforms.Compose()` - это функция, которая позволяет объединять несколько преобразований из библиотеки `torchvision` в одно преобразование. 

Она принимает список преобразований в качестве аргумента и возвращает новое преобразование, которое применяет все преобразования из списка последовательно.

Например:

```python
# Write transform for image
data_transform = transforms.Compose([
    # Resize the images to 64x64
    transforms.Resize(size=(64, 64)),
    # Flip the images randomly on the horizontal
    transforms.RandomHorizontalFlip(p=0.5), # p = probability of flip, 0.5 = 50% chance
    # Turn the image into a torch.Tensor
    transforms.ToTensor() # this also converts all pixel values from 0 to 255 to be between 0.0 and 1.0 
])
```

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Входящие 

---

[^def]: термин
[^que]: вопрос