---

Tags: 
aliases: [Не запускается линукс Linux, оболочка Linux]
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 20 # Не трогать!
Месяц: 07 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-07-20

---
# [[Как зайти в консоль, если не перезагрузилась оболочка - Linux]]


**Ctrl** + **Alt** + **F4**


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. [[Иванов Антон Сергеевич|Антон]]


---
#Linux 

---

[^def]: термин
[^que]: вопрос