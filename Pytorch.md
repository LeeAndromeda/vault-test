---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 25 # Не трогать!
Месяц: 05 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-05-25

---
# [[Pytorch]]

---
## Общая теория нейронных сетей через призму Pytorch

---
### Слои

- [[torch.nn.Linear линейный слой - Pytorch]]
- [[torch.nn.Conv2d сверточный слой- Pytorch]]
- [[torch.nn.MaxPool2d пуллинг слой - Pytorch]]

---

### Функции активации

- [[torch.nn.Sigmoid()]]
- [[torch.nn.Softmax()]]


---
### База

---

- [[Гиперпараметры модели - Pytorch]]
- [[Написание модели - pytorch]]
- [[Loss-function - Pytorch]]
- [[Различные метрики для оценки классифкации в Pytorch]]
- [[Оптимайзеры - Pytorch]]
- [[Подготовка данных к обучению]]
	- [[Работа с данными - Dataloader и Dataset - Pytorch]]
	- [[Типичная структура датасета для классификации изображений, представленная иерархией папок]]
	- [[Функция создания датасета из стандартной структуры директорий для датасета изображений - torchvision.datasets.ImageFolder()]]
	- [[Пример создания собственной функции для загрузки датасета]]
- [[Сохранение и загрузка модели - Pytorch]]

---

- [[Как улучшить результаты работы модели с точки зрения модели]]

---

### Задачи и решения

- [[Бинарная классификация в Pytorch]]

---
### Тензоры
---

#### Общие сведения 

- [[Наименование скаляров, векторов, матриц и тензоров в Pytorch]]
- [[Свойства скаляров, векторов, матриц и тензоров в Pytorch]]
- [[Форма тензоров (массивов) в Python]]
- [[Как отключить back propagation для модели Pytorch, чтобы использовать ее без обучения]]

---

#### Работа с тензорами в Pytorch

- [[Типы данных тензоров в Pytorch]]
- [[Как поменять тип данных тензора в Pytorch]]
- [[Получение данных из тензоров в Pytorch]]
- [[Генерация рандомных тензоров в Pytorch]]
- [[Арифметические операции с тензорами в Pytorch]]
- [[Изменение формы тензора в Pytorch]]
- [[Numpy и обратная совместимость - Pytorch]]

---
#### Случайность и воспроизведение результатов

- [[torch.manual_seed(seed)]]

---

#### Действия над тензорами torch.exp() и т.д.

- [[torch.exp()]]
- [[torch.sum()]]

---

### Вопросы оптимизации

- [[Как лучше представлять изображения для Pytorch]]


---

### Проблемные ситуации

- [[Что делать, если модель переобучается]]
- [[Какую модель стоит использовать в зависимости от вида данных]]
- [[torchinfo.summary() - как понять, что происходит с данными внутри вашей модели]]
- [[CUDA error device-side assert triggered - что делать]]

---

## Воркфлоус

- [[Как сделать свою модель пошагово - Pytorch]]
- [[Работа с изображениями - torchvision.transforms]]
- [[Создание датасета для обучения сети для компьютерного зрения]]



---


## Документация

- [[torch.tensor()]]
- [[torch.arange()]]
- [[torch.randn()]]
- [[Какие датасеты уже есть в pytorch]]

---

- [Форум Pytorch](https://discuss.pytorch.org/)
- [Cheatsheat](https://pytorch.org/tutorials/beginner/ptcheat.html)


---


## Черновое

- [[Quickstart to Pytorch]]
- [[PyTorch Cheat Sheet — PyTorch Tutorials 2.0.1+cu117 documentation.pdf]]

---

## Устройства

- [[Запуск нейронной сети на определенном устройстве - Pytorch]]
---
## Курсы

- [PyTorch за сутки](https://youtu.be/Z_ikDlimN6A)
- [[Pytorch skill list]]


## Смотрите также:

- [[Pytorch Lightning]]
```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]+ Соурслист
> 1. https://www.learnpytorch.io/00_pytorch_fundamentals/


---
#pytorch

---

[^def]: термин
[^que]: вопрос