---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 09 # Не трогать!
Месяц: 09 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-09-09

---
# [[rosdep - ROS]]


**rosdep** - пакет для дозагрузки зависимостей для необходимых пакетов ROS.

## Случаи применения

### 1. Сборка исходников пакета, загруженных с Github/GitLab/etc.

Для этого используется следующая команда: 

```shell
rosdep install --from-paths src --ignore-src -r
```

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#ROS

---

[^def]: термин
[^que]: вопрос