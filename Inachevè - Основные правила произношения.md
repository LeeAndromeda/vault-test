---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 10 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-10-10

---
# [[Inachevè - Основные правила произношения]]


1. Все гласные в слове должны произноситься
2. Ударение всегда на последнем произнесенном слоге
3. Обычно согласные на конце слов не произносятся за исключением *c*, *f*, *l*, которые обычно произносятся
4. Если слово заканчивается согласной, а следующее начинается с гласной или беззвучной «h», то последняя согласная присоединяется к следующей гласной.



- [[Исчерпывающий справочник по произношению французских звуков]]

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://preply.com/ru/blog/kak-vyuchit-frantsuzskij/


---
#Французский

---

[^def]: термин
[^que]: вопрос