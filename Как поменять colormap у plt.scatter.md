---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 04 # Не трогать!
Месяц: 07 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-07-04

---
# [[Как поменять colormap у plt.scatter]]


Используя данные из [[02_pytorch_classification.ipynb]], это делается следующим образом:

```python
plt.scatter(x=X[:, 0],
            y=X[:, 1],
            c=y,
            cmap=plt.cm.RdYlBu);
plt.show()
```

## Какие бывают colormap

[[Choosing Colormaps in Matplotlib — Matplotlib 3.7.1 documentation.pdf]]

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#mathplotlib 

---

[^def]: термин
[^que]: вопрос