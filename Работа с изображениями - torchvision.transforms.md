---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 09 # Не трогать!
Месяц: 09 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-09-09

---
# [[Работа с изображениями - torchvision.transforms]]


- [[torchvision.transfroms.Compose()]]


# Все основные встроенные трансформации
## Geometry[](https://pytorch.org/vision/stable/transforms.html#geometry)

|   |   |
|---|---|
|[`Resize`](https://pytorch.org/vision/stable/generated/torchvision.transforms.Resize.html#torchvision.transforms.Resize "torchvision.transforms.Resize")(size[, interpolation, max_size, ...])|Resize the input image to the given size.|
|[`v2.Resize`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.Resize.html#torchvision.transforms.v2.Resize "torchvision.transforms.v2.Resize")(size[, interpolation, max_size, ...])|[BETA] Resize the input to the given size.|
|[`v2.ScaleJitter`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.ScaleJitter.html#torchvision.transforms.v2.ScaleJitter "torchvision.transforms.v2.ScaleJitter")(target_size[, scale_range, ...])|[BETA] Perform Large Scale Jitter on the input according to ["Simple Copy-Paste is a Strong Data Augmentation Method for Instance Segmentation"](https://arxiv.org/abs/2012.07177).|
|[`v2.RandomShortestSize`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.RandomShortestSize.html#torchvision.transforms.v2.RandomShortestSize "torchvision.transforms.v2.RandomShortestSize")(min_size[, max_size, ...])|[BETA] Randomly resize the input.|
|[`v2.RandomResize`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.RandomResize.html#torchvision.transforms.v2.RandomResize "torchvision.transforms.v2.RandomResize")(min_size, max_size[, ...])|[BETA] Randomly resize the input.|
|[`RandomCrop`](https://pytorch.org/vision/stable/generated/torchvision.transforms.RandomCrop.html#torchvision.transforms.RandomCrop "torchvision.transforms.RandomCrop")(size[, padding, pad_if_needed, ...])|Crop the given image at a random location.|
|[`v2.RandomCrop`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.RandomCrop.html#torchvision.transforms.v2.RandomCrop "torchvision.transforms.v2.RandomCrop")(size[, padding, ...])|[BETA] Crop the input at a random location.|
|[`RandomResizedCrop`](https://pytorch.org/vision/stable/generated/torchvision.transforms.RandomResizedCrop.html#torchvision.transforms.RandomResizedCrop "torchvision.transforms.RandomResizedCrop")(size[, scale, ratio, ...])|Crop a random portion of image and resize it to a given size.|
|[`v2.RandomResizedCrop`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.RandomResizedCrop.html#torchvision.transforms.v2.RandomResizedCrop "torchvision.transforms.v2.RandomResizedCrop")(size[, scale, ratio, ...])|[BETA] Crop a random portion of the input and resize it to a given size.|
|[`v2.RandomIoUCrop`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.RandomIoUCrop.html#torchvision.transforms.v2.RandomIoUCrop "torchvision.transforms.v2.RandomIoUCrop")([min_scale, max_scale, ...])|[BETA] Random IoU crop transformation from ["SSD: Single Shot MultiBox Detector"](https://arxiv.org/abs/1512.02325).|
|[`CenterCrop`](https://pytorch.org/vision/stable/generated/torchvision.transforms.CenterCrop.html#torchvision.transforms.CenterCrop "torchvision.transforms.CenterCrop")(size)|Crops the given image at the center.|
|[`v2.CenterCrop`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.CenterCrop.html#torchvision.transforms.v2.CenterCrop "torchvision.transforms.v2.CenterCrop")(size)|[BETA] Crop the input at the center.|
|[`FiveCrop`](https://pytorch.org/vision/stable/generated/torchvision.transforms.FiveCrop.html#torchvision.transforms.FiveCrop "torchvision.transforms.FiveCrop")(size)|Crop the given image into four corners and the central crop.|
|[`v2.FiveCrop`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.FiveCrop.html#torchvision.transforms.v2.FiveCrop "torchvision.transforms.v2.FiveCrop")(size)|[BETA] Crop the image or video into four corners and the central crop.|
|[`TenCrop`](https://pytorch.org/vision/stable/generated/torchvision.transforms.TenCrop.html#torchvision.transforms.TenCrop "torchvision.transforms.TenCrop")(size[, vertical_flip])|Crop the given image into four corners and the central crop plus the flipped version of these (horizontal flipping is used by default).|
|[`v2.TenCrop`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.TenCrop.html#torchvision.transforms.v2.TenCrop "torchvision.transforms.v2.TenCrop")(size[, vertical_flip])|[BETA] Crop the image or video into four corners and the central crop plus the flipped version of these (horizontal flipping is used by default).|
|[`Pad`](https://pytorch.org/vision/stable/generated/torchvision.transforms.Pad.html#torchvision.transforms.Pad "torchvision.transforms.Pad")(padding[, fill, padding_mode])|Pad the given image on all sides with the given "pad" value.|
|[`v2.Pad`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.Pad.html#torchvision.transforms.v2.Pad "torchvision.transforms.v2.Pad")(padding[, fill, padding_mode])|[BETA] Pad the input on all sides with the given "pad" value.|
|[`v2.RandomZoomOut`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.RandomZoomOut.html#torchvision.transforms.v2.RandomZoomOut "torchvision.transforms.v2.RandomZoomOut")([fill, side_range, p])|[BETA] "Zoom out" transformation from ["SSD: Single Shot MultiBox Detector"](https://arxiv.org/abs/1512.02325).|
|[`RandomRotation`](https://pytorch.org/vision/stable/generated/torchvision.transforms.RandomRotation.html#torchvision.transforms.RandomRotation "torchvision.transforms.RandomRotation")(degrees[, interpolation, ...])|Rotate the image by angle.|
|[`v2.RandomRotation`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.RandomRotation.html#torchvision.transforms.v2.RandomRotation "torchvision.transforms.v2.RandomRotation")(degrees[, interpolation, ...])|[BETA] Rotate the input by angle.|
|[`RandomAffine`](https://pytorch.org/vision/stable/generated/torchvision.transforms.RandomAffine.html#torchvision.transforms.RandomAffine "torchvision.transforms.RandomAffine")(degrees[, translate, scale, ...])|Random affine transformation of the image keeping center invariant.|
|[`v2.RandomAffine`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.RandomAffine.html#torchvision.transforms.v2.RandomAffine "torchvision.transforms.v2.RandomAffine")(degrees[, translate, scale, ...])|[BETA] Random affine transformation the input keeping center invariant.|
|[`RandomPerspective`](https://pytorch.org/vision/stable/generated/torchvision.transforms.RandomPerspective.html#torchvision.transforms.RandomPerspective "torchvision.transforms.RandomPerspective")([distortion_scale, p, ...])|Performs a random perspective transformation of the given image with a given probability.|
|[`v2.RandomPerspective`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.RandomPerspective.html#torchvision.transforms.v2.RandomPerspective "torchvision.transforms.v2.RandomPerspective")([distortion_scale, p, ...])|[BETA] Perform a random perspective transformation of the input with a given probability.|
|[`ElasticTransform`](https://pytorch.org/vision/stable/generated/torchvision.transforms.ElasticTransform.html#torchvision.transforms.ElasticTransform "torchvision.transforms.ElasticTransform")([alpha, sigma, ...])|Transform a tensor image with elastic transformations.|
|[`v2.ElasticTransform`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.ElasticTransform.html#torchvision.transforms.v2.ElasticTransform "torchvision.transforms.v2.ElasticTransform")([alpha, sigma, ...])|[BETA] Transform the input with elastic transformations.|
|[`RandomHorizontalFlip`](https://pytorch.org/vision/stable/generated/torchvision.transforms.RandomHorizontalFlip.html#torchvision.transforms.RandomHorizontalFlip "torchvision.transforms.RandomHorizontalFlip")([p])|Horizontally flip the given image randomly with a given probability.|
|[`v2.RandomHorizontalFlip`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.RandomHorizontalFlip.html#torchvision.transforms.v2.RandomHorizontalFlip "torchvision.transforms.v2.RandomHorizontalFlip")([p])|[BETA] Horizontally flip the input with a given probability.|
|[`RandomVerticalFlip`](https://pytorch.org/vision/stable/generated/torchvision.transforms.RandomVerticalFlip.html#torchvision.transforms.RandomVerticalFlip "torchvision.transforms.RandomVerticalFlip")([p])|Vertically flip the given image randomly with a given probability.|
|[`v2.RandomVerticalFlip`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.RandomVerticalFlip.html#torchvision.transforms.v2.RandomVerticalFlip "torchvision.transforms.v2.RandomVerticalFlip")([p])|[BETA] Vertically flip the input with a given probability.|

## Color[](https://pytorch.org/vision/stable/transforms.html#color)

|   |   |
|---|---|
|[`ColorJitter`](https://pytorch.org/vision/stable/generated/torchvision.transforms.ColorJitter.html#torchvision.transforms.ColorJitter "torchvision.transforms.ColorJitter")([brightness, contrast, ...])|Randomly change the brightness, contrast, saturation and hue of an image.|
|[`v2.ColorJitter`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.ColorJitter.html#torchvision.transforms.v2.ColorJitter "torchvision.transforms.v2.ColorJitter")([brightness, contrast, ...])|[BETA] Randomly change the brightness, contrast, saturation and hue of an image or video.|
|[`v2.RandomPhotometricDistort`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.RandomPhotometricDistort.html#torchvision.transforms.v2.RandomPhotometricDistort "torchvision.transforms.v2.RandomPhotometricDistort")([brightness, ...])|[BETA] Randomly distorts the image or video as used in [SSD: Single Shot MultiBox Detector](https://arxiv.org/abs/1512.02325).|
|[`Grayscale`](https://pytorch.org/vision/stable/generated/torchvision.transforms.Grayscale.html#torchvision.transforms.Grayscale "torchvision.transforms.Grayscale")([num_output_channels])|Convert image to grayscale.|
|[`v2.Grayscale`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.Grayscale.html#torchvision.transforms.v2.Grayscale "torchvision.transforms.v2.Grayscale")([num_output_channels])|[BETA] Convert images or videos to grayscale.|
|[`RandomGrayscale`](https://pytorch.org/vision/stable/generated/torchvision.transforms.RandomGrayscale.html#torchvision.transforms.RandomGrayscale "torchvision.transforms.RandomGrayscale")([p])|Randomly convert image to grayscale with a probability of p (default 0.1).|
|[`v2.RandomGrayscale`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.RandomGrayscale.html#torchvision.transforms.v2.RandomGrayscale "torchvision.transforms.v2.RandomGrayscale")([p])|[BETA] Randomly convert image or videos to grayscale with a probability of p (default 0.1).|
|[`GaussianBlur`](https://pytorch.org/vision/stable/generated/torchvision.transforms.GaussianBlur.html#torchvision.transforms.GaussianBlur "torchvision.transforms.GaussianBlur")(kernel_size[, sigma])|Blurs image with randomly chosen Gaussian blur.|
|[`v2.GaussianBlur`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.GaussianBlur.html#torchvision.transforms.v2.GaussianBlur "torchvision.transforms.v2.GaussianBlur")(kernel_size[, sigma])|[BETA] Blurs image with randomly chosen Gaussian blur.|
|[`RandomInvert`](https://pytorch.org/vision/stable/generated/torchvision.transforms.RandomInvert.html#torchvision.transforms.RandomInvert "torchvision.transforms.RandomInvert")([p])|Inverts the colors of the given image randomly with a given probability.|
|[`v2.RandomInvert`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.RandomInvert.html#torchvision.transforms.v2.RandomInvert "torchvision.transforms.v2.RandomInvert")([p])|[BETA] Inverts the colors of the given image or video with a given probability.|
|[`RandomPosterize`](https://pytorch.org/vision/stable/generated/torchvision.transforms.RandomPosterize.html#torchvision.transforms.RandomPosterize "torchvision.transforms.RandomPosterize")(bits[, p])|Posterize the image randomly with a given probability by reducing the number of bits for each color channel.|
|[`v2.RandomPosterize`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.RandomPosterize.html#torchvision.transforms.v2.RandomPosterize "torchvision.transforms.v2.RandomPosterize")(bits[, p])|[BETA] Posterize the image or video with a given probability by reducing the number of bits for each color channel.|
|[`RandomSolarize`](https://pytorch.org/vision/stable/generated/torchvision.transforms.RandomSolarize.html#torchvision.transforms.RandomSolarize "torchvision.transforms.RandomSolarize")(threshold[, p])|Solarize the image randomly with a given probability by inverting all pixel values above a threshold.|
|[`v2.RandomSolarize`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.RandomSolarize.html#torchvision.transforms.v2.RandomSolarize "torchvision.transforms.v2.RandomSolarize")(threshold[, p])|[BETA] Solarize the image or video with a given probability by inverting all pixel values above a threshold.|
|[`RandomAdjustSharpness`](https://pytorch.org/vision/stable/generated/torchvision.transforms.RandomAdjustSharpness.html#torchvision.transforms.RandomAdjustSharpness "torchvision.transforms.RandomAdjustSharpness")(sharpness_factor[, p])|Adjust the sharpness of the image randomly with a given probability.|
|[`v2.RandomAdjustSharpness`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.RandomAdjustSharpness.html#torchvision.transforms.v2.RandomAdjustSharpness "torchvision.transforms.v2.RandomAdjustSharpness")(sharpness_factor[, p])|[BETA] Adjust the sharpness of the image or video with a given probability.|
|[`RandomAutocontrast`](https://pytorch.org/vision/stable/generated/torchvision.transforms.RandomAutocontrast.html#torchvision.transforms.RandomAutocontrast "torchvision.transforms.RandomAutocontrast")([p])|Autocontrast the pixels of the given image randomly with a given probability.|
|[`v2.RandomAutocontrast`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.RandomAutocontrast.html#torchvision.transforms.v2.RandomAutocontrast "torchvision.transforms.v2.RandomAutocontrast")([p])|[BETA] Autocontrast the pixels of the given image or video with a given probability.|
|[`RandomEqualize`](https://pytorch.org/vision/stable/generated/torchvision.transforms.RandomEqualize.html#torchvision.transforms.RandomEqualize "torchvision.transforms.RandomEqualize")([p])|Equalize the histogram of the given image randomly with a given probability.|
|[`v2.RandomEqualize`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.RandomEqualize.html#torchvision.transforms.v2.RandomEqualize "torchvision.transforms.v2.RandomEqualize")([p])|[BETA] Equalize the histogram of the given image or video with a given probability.|

## Composition[](https://pytorch.org/vision/stable/transforms.html#composition)

|   |   |
|---|---|
|[`Compose`](https://pytorch.org/vision/stable/generated/torchvision.transforms.Compose.html#torchvision.transforms.Compose "torchvision.transforms.Compose")(transforms)|Composes several transforms together.|
|[`v2.Compose`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.Compose.html#torchvision.transforms.v2.Compose "torchvision.transforms.v2.Compose")(transforms)|[BETA] Composes several transforms together.|
|[`RandomApply`](https://pytorch.org/vision/stable/generated/torchvision.transforms.RandomApply.html#torchvision.transforms.RandomApply "torchvision.transforms.RandomApply")(transforms[, p])|Apply randomly a list of transformations with a given probability.|
|[`v2.RandomApply`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.RandomApply.html#torchvision.transforms.v2.RandomApply "torchvision.transforms.v2.RandomApply")(transforms[, p])|[BETA] Apply randomly a list of transformations with a given probability.|
|[`RandomChoice`](https://pytorch.org/vision/stable/generated/torchvision.transforms.RandomChoice.html#torchvision.transforms.RandomChoice "torchvision.transforms.RandomChoice")(transforms[, p])|Apply single transformation randomly picked from a list.|
|[`v2.RandomChoice`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.RandomChoice.html#torchvision.transforms.v2.RandomChoice "torchvision.transforms.v2.RandomChoice")(transforms[, p])|[BETA] Apply single transformation randomly picked from a list.|
|[`RandomOrder`](https://pytorch.org/vision/stable/generated/torchvision.transforms.RandomOrder.html#torchvision.transforms.RandomOrder "torchvision.transforms.RandomOrder")(transforms)|Apply a list of transformations in a random order.|
|[`v2.RandomOrder`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.RandomOrder.html#torchvision.transforms.v2.RandomOrder "torchvision.transforms.v2.RandomOrder")(transforms)|[BETA] Apply a list of transformations in a random order.|

## Miscellaneous[](https://pytorch.org/vision/stable/transforms.html#miscellaneous)

|   |   |
|---|---|
|[`LinearTransformation`](https://pytorch.org/vision/stable/generated/torchvision.transforms.LinearTransformation.html#torchvision.transforms.LinearTransformation "torchvision.transforms.LinearTransformation")(transformation_matrix, ...)|Transform a tensor image with a square transformation matrix and a mean_vector computed offline.|
|[`v2.LinearTransformation`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.LinearTransformation.html#torchvision.transforms.v2.LinearTransformation "torchvision.transforms.v2.LinearTransformation")(...)|[BETA] Transform a tensor image or video with a square transformation matrix and a mean_vector computed offline.|
|[`Normalize`](https://pytorch.org/vision/stable/generated/torchvision.transforms.Normalize.html#torchvision.transforms.Normalize "torchvision.transforms.Normalize")(mean, std[, inplace])|Normalize a tensor image with mean and standard deviation.|
|[`v2.Normalize`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.Normalize.html#torchvision.transforms.v2.Normalize "torchvision.transforms.v2.Normalize")(mean, std[, inplace])|[BETA] Normalize a tensor image or video with mean and standard deviation.|
|[`RandomErasing`](https://pytorch.org/vision/stable/generated/torchvision.transforms.RandomErasing.html#torchvision.transforms.RandomErasing "torchvision.transforms.RandomErasing")([p, scale, ratio, value, inplace])|Randomly selects a rectangle region in a torch.Tensor image and erases its pixels.|
|[`v2.RandomErasing`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.RandomErasing.html#torchvision.transforms.v2.RandomErasing "torchvision.transforms.v2.RandomErasing")([p, scale, ratio, value, ...])|[BETA] Randomly select a rectangle region in the input image or video and erase its pixels.|
|[`Lambda`](https://pytorch.org/vision/stable/generated/torchvision.transforms.Lambda.html#torchvision.transforms.Lambda "torchvision.transforms.Lambda")(lambd)|Apply a user-defined lambda as a transform.|
|[`v2.Lambda`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.Lambda.html#torchvision.transforms.v2.Lambda "torchvision.transforms.v2.Lambda")(lambd, *types)|[BETA] Apply a user-defined function as a transform.|
|[`v2.SanitizeBoundingBox`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.SanitizeBoundingBox.html#torchvision.transforms.v2.SanitizeBoundingBox "torchvision.transforms.v2.SanitizeBoundingBox")([min_size, labels_getter])|[BETA] Remove degenerate/invalid bounding boxes and their corresponding labels and masks.|
|[`v2.ClampBoundingBox`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.ClampBoundingBox.html#torchvision.transforms.v2.ClampBoundingBox "torchvision.transforms.v2.ClampBoundingBox")()|[BETA] Clamp bounding boxes to their corresponding image dimensions.|
|[`v2.UniformTemporalSubsample`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.UniformTemporalSubsample.html#torchvision.transforms.v2.UniformTemporalSubsample "torchvision.transforms.v2.UniformTemporalSubsample")(num_samples)|[BETA] Uniformly subsample `num_samples` indices from the temporal dimension of the video.|

## Conversion[](https://pytorch.org/vision/stable/transforms.html#conversion)

Note

Beware, some of these conversion transforms below will scale the values while performing the conversion, while some may not do any scaling. By scaling, we mean e.g. that a `uint8` -> `float32` would map the [0, 255] range into [0, 1] (and vice-versa).

|   |   |
|---|---|
|[`ToPILImage`](https://pytorch.org/vision/stable/generated/torchvision.transforms.ToPILImage.html#torchvision.transforms.ToPILImage "torchvision.transforms.ToPILImage")([mode])|Convert a tensor or an ndarray to PIL Image - this does not scale values.|
|[`v2.ToPILImage`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.ToPILImage.html#torchvision.transforms.v2.ToPILImage "torchvision.transforms.v2.ToPILImage")|alias of [`ToImagePIL`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.ToImagePIL.html#torchvision.transforms.v2.ToImagePIL "torchvision.transforms.v2._type_conversion.ToImagePIL")|
|[`v2.ToImagePIL`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.ToImagePIL.html#torchvision.transforms.v2.ToImagePIL "torchvision.transforms.v2.ToImagePIL")([mode])|[BETA] Convert a tensor or an ndarray to PIL Image - this does not scale values.|
|[`ToTensor`](https://pytorch.org/vision/stable/generated/torchvision.transforms.ToTensor.html#torchvision.transforms.ToTensor "torchvision.transforms.ToTensor")()|Convert a PIL Image or ndarray to tensor and scale the values accordingly.|
|[`v2.ToTensor`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.ToTensor.html#torchvision.transforms.v2.ToTensor "torchvision.transforms.v2.ToTensor")()|[BETA] Convert a PIL Image or ndarray to tensor and scale the values accordingly.|
|[`PILToTensor`](https://pytorch.org/vision/stable/generated/torchvision.transforms.PILToTensor.html#torchvision.transforms.PILToTensor "torchvision.transforms.PILToTensor")()|Convert a PIL Image to a tensor of the same type - this does not scale values.|
|[`v2.PILToTensor`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.PILToTensor.html#torchvision.transforms.v2.PILToTensor "torchvision.transforms.v2.PILToTensor")()|[BETA] Convert a PIL Image to a tensor of the same type - this does not scale values.|
|[`v2.ToImageTensor`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.ToImageTensor.html#torchvision.transforms.v2.ToImageTensor "torchvision.transforms.v2.ToImageTensor")()|[BETA] Convert a tensor, ndarray, or PIL Image to [`Image`](https://pytorch.org/vision/stable/generated/torchvision.datapoints.Image.html#torchvision.datapoints.Image "torchvision.datapoints.Image") ; this does not scale values.|
|[`ConvertImageDtype`](https://pytorch.org/vision/stable/generated/torchvision.transforms.ConvertImageDtype.html#torchvision.transforms.ConvertImageDtype "torchvision.transforms.ConvertImageDtype")(dtype)|Convert a tensor image to the given `dtype` and scale the values accordingly.|
|[`v2.ConvertDtype`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.ConvertDtype.html#torchvision.transforms.v2.ConvertDtype "torchvision.transforms.v2.ConvertDtype")([dtype])|[BETA] Convert input image or video to the given `dtype` and scale the values accordingly.|
|[`v2.ConvertImageDtype`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.ConvertImageDtype.html#torchvision.transforms.v2.ConvertImageDtype "torchvision.transforms.v2.ConvertImageDtype")|alias of [`ConvertDtype`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.ConvertDtype.html#torchvision.transforms.v2.ConvertDtype "torchvision.transforms.v2._meta.ConvertDtype")|
|[`v2.ToDtype`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.ToDtype.html#torchvision.transforms.v2.ToDtype "torchvision.transforms.v2.ToDtype")(dtype)|[BETA] Converts the input to a specific dtype - this does not scale values.|
|[`v2.ConvertBoundingBoxFormat`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.ConvertBoundingBoxFormat.html#torchvision.transforms.v2.ConvertBoundingBoxFormat "torchvision.transforms.v2.ConvertBoundingBoxFormat")(format)|[BETA] Convert bounding box coordinates to the given `format`, eg from "CXCYWH" to "XYXY".|

## Auto-Augmentation[](https://pytorch.org/vision/stable/transforms.html#auto-augmentation)

[AutoAugment](https://arxiv.org/pdf/1805.09501.pdf) is a common Data Augmentation technique that can improve the accuracy of Image Classification models. Though the data augmentation policies are directly linked to their trained dataset, empirical studies show that ImageNet policies provide significant improvements when applied to other datasets. In TorchVision we implemented 3 policies learned on the following datasets: ImageNet, CIFAR10 and SVHN. The new transform can be used standalone or mixed-and-matched with existing transforms:

|   |   |
|---|---|
|[`AutoAugmentPolicy`](https://pytorch.org/vision/stable/generated/torchvision.transforms.AutoAugmentPolicy.html#torchvision.transforms.AutoAugmentPolicy "torchvision.transforms.AutoAugmentPolicy")(value)|AutoAugment policies learned on different datasets.|
|[`AutoAugment`](https://pytorch.org/vision/stable/generated/torchvision.transforms.AutoAugment.html#torchvision.transforms.AutoAugment "torchvision.transforms.AutoAugment")([policy, interpolation, fill])|AutoAugment data augmentation method based on ["AutoAugment: Learning Augmentation Strategies from Data"](https://arxiv.org/pdf/1805.09501.pdf).|
|[`v2.AutoAugment`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.AutoAugment.html#torchvision.transforms.v2.AutoAugment "torchvision.transforms.v2.AutoAugment")([policy, interpolation, fill])|[BETA] AutoAugment data augmentation method based on ["AutoAugment: Learning Augmentation Strategies from Data"](https://arxiv.org/pdf/1805.09501.pdf).|
|[`RandAugment`](https://pytorch.org/vision/stable/generated/torchvision.transforms.RandAugment.html#torchvision.transforms.RandAugment "torchvision.transforms.RandAugment")([num_ops, magnitude, ...])|RandAugment data augmentation method based on ["RandAugment: Practical automated data augmentation with a reduced search space"](https://arxiv.org/abs/1909.13719).|
|[`v2.RandAugment`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.RandAugment.html#torchvision.transforms.v2.RandAugment "torchvision.transforms.v2.RandAugment")([num_ops, magnitude, ...])|[BETA] RandAugment data augmentation method based on ["RandAugment: Practical automated data augmentation with a reduced search space"](https://arxiv.org/abs/1909.13719).|
|[`TrivialAugmentWide`](https://pytorch.org/vision/stable/generated/torchvision.transforms.TrivialAugmentWide.html#torchvision.transforms.TrivialAugmentWide "torchvision.transforms.TrivialAugmentWide")([num_magnitude_bins, ...])|Dataset-independent data-augmentation with TrivialAugment Wide, as described in ["TrivialAugment: Tuning-free Yet State-of-the-Art Data Augmentation"](https://arxiv.org/abs/2103.10158).|
|[`v2.TrivialAugmentWide`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.TrivialAugmentWide.html#torchvision.transforms.v2.TrivialAugmentWide "torchvision.transforms.v2.TrivialAugmentWide")([num_magnitude_bins, ...])|[BETA] Dataset-independent data-augmentation with TrivialAugment Wide, as described in ["TrivialAugment: Tuning-free Yet State-of-the-Art Data Augmentation"](https://arxiv.org/abs/2103.10158).|
|[`AugMix`](https://pytorch.org/vision/stable/generated/torchvision.transforms.AugMix.html#torchvision.transforms.AugMix "torchvision.transforms.AugMix")([severity, mixture_width, ...])|AugMix data augmentation method based on ["AugMix: A Simple Data Processing Method to Improve Robustness and Uncertainty"](https://arxiv.org/abs/1912.02781).|
|[`v2.AugMix`](https://pytorch.org/vision/stable/generated/torchvision.transforms.v2.AugMix.html#torchvision.transforms.v2.AugMix "torchvision.transforms.v2.AugMix")([severity, mixture_width, ...])|[BETA] AugMix data augmentation method based on ["AugMix: A Simple Data Processing Method to Improve Robustness and Uncertainty"](https://arxiv.org/abs/1912.02781).|


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://pytorch.org/vision/stable/transforms.html


---
#pytorch/РаботаСИзображениями

---

[^def]: термин
[^que]: вопрос