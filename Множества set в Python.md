---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 03 # Не трогать!
Месяц: 02 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-02-03

---
# [[Множества set в Python]]

```Python
x = set() # Если вы создаете пустое множество, нужно использовать этот конструктор, иначе вы создадите dictionary
s = {4, 534, 3, 3}
s.add(5) # Добавляет элемент
s.remove(5) # Удаляет ВСЕ такие элементы
print(4 in s, 1 in s) # True False 
```

## Операции с множествами

```Python
s = {4, 1, 3, 2}
s2 = {0, 5, 1, 6}
  
print(s.union(s2))
# {0, 1, 2, 3, 4, 5, 6}
```

```Python
s = {4, 1, 3, 2}
s2 = {0, 5, 1, 6}
  
print(s.difference(s2))
# {2, 3, 4}
```

```Python
s = {4, 1, 3, 2}
s2 = {0, 5, 1, 6}
  
print(s.intersection(s2))
# {1}
```

```Python
s = {4, 1, 3, 2}
s2 = {0, 5, 1, 6}
  
print(s.symmetric_difference(s2))
# {0, 2, 3, 4, 5, 6}
```

#Входящие 

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Входящие 

---

[^def]: термин
[^que]: вопрос