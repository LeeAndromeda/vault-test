---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 21 # Не трогать!
Месяц: 11 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-11-21

---
# [[numpy.var - дисперсия в массиве]]

```python
import numpy as np
import matplotlib.pyplot as plt

# Create an array of data
data = np.random.rand(100) * 100

# Calculate the variance of the data
variance = np.var(data)

# Create a histogram to visualize the data distribution
plt.hist(data, bins=range(1, 100), edgecolor='black', alpha=0.7)

# Add labels and title
plt.xlabel('Value')
plt.ylabel('Frequency')
plt.title('Histogram of Data with Variance')

# Add a text annotation for the variance
plt.annotate(f'Variance: {variance:.2f}', xy=(8, 2), xytext=(8, 3),
             arrowprops=dict(facecolor='black', shrink=0.05))

# Show the plot
plt.show()
print(data)
```

![[Pasted image 20231121163148.png]]

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Python/NumPy 

---

[^def]: термин
[^que]: вопрос