---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 01 # Не трогать!
Месяц: 12 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-12-01

---
# [[Ссылка на Ubutnu 20 для Jetson]]

https://github.com/Qengineering/Jetson-Nano-Ubuntu-20-image

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Jetson #Ubuntu 

---

[^def]: термин
[^que]: вопрос