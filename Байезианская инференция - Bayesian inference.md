---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 22 # Не трогать!
Месяц: 11 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-11-22

---
# [[Байезианская инференция - Bayesian inference]]


Предположим, что процесс генерирует независимые и одинаково распределенные события $E_n, n=1,2,3, \ldots$, но вероятностное распределение неизвестно. Пусть пространство событий $\Omega$ представляет текущее состояние уверенности в этом процессе. Каждая модель представлена событием $M_m$. Условные вероятности $P(E_n \mid M_m)$ задаются для определения моделей. $P(M_m)$ - это степень уверенности в $M_m$. Перед первым шагом вывода ${P(M_m)}$ - это набор начальных априорных вероятностей. Они должны суммироваться до 1, но в остальном могут быть произвольными.

Предположим, что процесс наблюдается за генерацией $E \in{E_n}$. Для каждого $M \in{M_m}$ априорная вероятность $P(M)$ обновляется до апостериорной вероятности $P(M \mid E)$. Согласно теореме Байеса:[5]

$$ P(M \mid E)=\frac{P(E \mid M)}{\sum_m P\left(E \mid M_m\right) P\left(M_m\right)} \cdot P(M) . $$

После наблюдения дополнительных доказательств эту процедуру можно повторить.

## Что такое $\Omega$

In the context of Bayesian inference, the symbol $\Omega$ represents the "event space" or the "sample space." In simpler terms, you can think of $\Omega$ as the set of all possible outcomes or states of the system that you are observing or modeling.

For example, if you are flipping a fair coin, the event space $\Omega$ would contain two possible outcomes: heads (H) or tails (T). So, $\Omega = {H, T}$. If you are rolling a six-sided die, $\Omega$ would be the set of numbers {1, 2, 3, 4, 5, 6}. In more complex scenarios, $\Omega$ might represent a range of possible continuous values.

In the context of Bayesian inference, $\Omega$ is used to represent the set of all possible states of belief about a process. Each specific state or outcome within $\Omega$ is associated with a certain probability, and the goal is to update these probabilities based on observed evidence using Bayesian updating.

## Что такое модель $M_m$ и что делает Байезианская инференция

In Bayesian inference, a "model" is a specific hypothesis or assumption about the underlying process that generates the observed data. Each model is represented by an event, denoted as $M_m$, where $m$ is an index that distinguishes different models. These models are alternative ways to explain or represent the data generation process.

To make it more concrete, let's consider an example. Suppose you are trying to model the outcome of a coin flip. You might have two models:

1. Model $M_1$: The coin is fair, with an equal probability of landing on heads or tails.
2. Model $M_2$: The coin is biased, with a higher probability of landing on heads.

Here, $M_1$ and $M_2$ represent two different hypotheses or models about how the coin behaves. $M_1$ suggests that the coin is fair, while $M_2$ suggests that the coin is biased towards heads.

The probabilities $P(E_n | M_m)$ specify how likely it is to observe a particular event $E_n$ given the assumption that model $M_m$ is true. For example, $P(H | M_1)$ might be 0.5 (50%) if the coin is fair, but $P(H | M_2)$ might be 0.8 (80%) if the coin is biased.

The prior probability $P(M_m)$ represents the initial degree of belief or probability assigned to each model before observing any data. This could be based on prior knowledge, experience, or subjective beliefs.

As new evidence (data) is observed, Bayesian inference updates these probabilities using Bayes' theorem to obtain the posterior probabilities $P(M_m | E)$, which represent the updated beliefs about the models given the observed data. The process is iterative and can be repeated as more evidence becomes available.

> Does this mean that bayesian inference updates Models' correctness probabilities until some model could be considered ture?

In Bayesian inference, the goal is not necessarily to declare one specific model as "true" or "false" but rather to update our beliefs about the probability of each model given the observed data. It provides a framework for updating and refining our beliefs as we gather more evidence.

The updating process involves going from prior probabilities $P(M_m)$ to posterior probabilities $P(M_m | E)$ based on Bayes' theorem. These posterior probabilities represent our updated beliefs about the likelihood of each model given the observed data.

It's important to note that in many practical situations, we may never assign a probability of exactly 0 or 1 to a model. Even if one model becomes very likely based on the evidence, there is usually some small probability assigned to alternative models. This reflects the uncertainty inherent in real-world processes.

In summary, Bayesian inference helps us update and refine our beliefs about different models based on evidence, but it doesn't necessarily lead to a definitive declaration that one model is absolutely true. It's a probabilistic framework that allows us to quantify and update our uncertainty as we gather more information.

## Что такое $P(E_n \mid M_m)$ ?

$P(E_n \mid M_m)$ represents the conditional probability of observing event $E_n$ given that model $M_m$ is true. In simpler terms, it tells us the likelihood of a particular outcome $E_n$ under the assumption that a specific model $M_m$ accurately describes the underlying process.

The values of $P(E_n \mid M_m)$ are specified based on the characteristics and assumptions of the models. These probabilities are often derived from prior knowledge, data, or domain expertise. In many cases, they are not directly observed but are part of the model specification.

Let's consider an example with the coin flip scenario:

- Model $M_1$: Fair coin, so $P(H \mid M_1) = 0.5$ (equal probability of heads and tails).
- Model $M_2$: Biased coin with a higher probability of heads, so $P(H \mid M_2) = 0.8$ (80% chance of heads).

In more complex scenarios, specifying these probabilities might involve statistical analysis, experimental data, or expert judgment. The key idea is that these probabilities express the assumptions made about how the data is generated under each model.

During Bayesian updating, these probabilities play a crucial role in determining how much the observed data influences our beliefs about the different models. The likelihood term in Bayes' theorem, $P(E_n \mid M_m)$, quantifies the agreement between the model's predictions and the observed data.

## Что такое $P(E \mid M)$ в числителе формулы?

In the formula for Bayesian inference, $P(E \mid M)$ in the numerator represents the likelihood of observing the evidence $E$ given that model $M$ is true. This term quantifies how well the model explains or predicts the observed data.

To break it down further:
- $P(E \mid M)$ is the probability of observing the evidence $E$ under the assumption that the specific model $M$ is the true or correct model.
- The numerator $P(E \mid M) \cdot P(M)$ represents the joint probability of both observing the evidence and the prior belief in the model.
- The denominator $\sum_m P\left(E \mid M_m\right) P\left(M_m\right)$ is a normalizing factor, ensuring that the probabilities add up to 1 .

So, when you calculate $P(M \mid E)$, you are updating your belief in the likelihood of model $M$ being true based on how well it explains the observed evidence. If the evidence is highly probable under a particular model, that model's posterior probability will be higher after the update. If the evidence is unlikely under a model, its posterior probability will decrease.

> So to correctly apply bayesian inference i have to apply it to all models?

Yes, that's correct. Bayesian inference involves updating beliefs about all possible models based on the observed evidence. The formula for Bayesian updating includes a sum over all possible models in the denominator, ensuring that the probabilities of all models are taken into account when updating.

The key steps in Bayesian inference are as follows:
1. Prior Probability $(P(M))$ : Start with an initial belief or probability assigned to each model before observing any evidence. This is called the prior probability.
2. Likelihood $(P(E \mid M)$ ): Specify the likelihood of observing the evidence given each model. This represents how well each model explains the observed data.
3. Update Using Bayes' Theorem: Apply Bayes' theorem to update the prior probabilities to posterior probabilities based on the observed evidence.
$$
P(M \mid E)=\frac{P(E \mid M)}{\sum_m P\left(E \mid M_m\right) P\left(M_m\right)} \cdot P(M)
$$
4. Repeat as Necessary: If more evidence becomes available, repeat the process to further refine the posterior probabilities.

The sum in the denominator accounts for all possible models, ensuring that the posterior probabilities are properly normalized. The result is an updated set of probabilities for each model given the observed evidence.

This process allows for a dynamic and iterative refinement of beliefs as more information becomes available. It's a fundamental principle in Bayesian statistics for dealing with uncertainty and updating probabilities based on data.


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Математика/ТеорияИгр 

---

[^def]: термин
[^que]: вопрос