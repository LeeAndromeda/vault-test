---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 30 # Не трогать!
Месяц: 11 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-11-30

---
# [[Таблица команд ROS и их аналогов в ROS2]]


| ROS 1 Command                | ROS 2 Command                  |
|------------------------------|--------------------------------|
| roscd <package>              | ros2 pkg prefix <package>      |
| rosls <package>              | ros2 pkg executables <package> |
| rospack find <package>       | ros2 pkg prefix <package>      |
| roscd                        | ros2 cd                        |
| roslaunch <package> <file>   | ros2 launch <package> <file>   |
| rostopic list                | ros2 topic list                |
| rostopic echo <topic>        | ros2 topic echo <topic>        |
| rosnode list                 | ros2 node list                 |
| rosnode info <node>          | ros2 node info <node>          |
| rosservice list              | ros2 service list              |
| rosservice info <service>    | ros2 service type <service>    |
| rosparam list                | ros2 param list                |
| rosparam get <param>         | ros2 param get <param>         |
| rosparam set <param> <value> | ros2 param set <param> <value> |
| roswtf                       | ros2 doctor                    |



## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#ROS #ROS2 

---

[^def]: термин
[^que]: вопрос