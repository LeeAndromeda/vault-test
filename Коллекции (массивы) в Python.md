---

Tags: 
aliases: [кортеж, tuple, array]
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 03 # Не трогать!
Месяц: 02 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-02-03

---
# [[Коллекции (массивы) в Python]]

По сути это аналог `std::vector` для C++, но с возможностью хранить различные типы данных.


```Python
x = [4, True, 'hi']
y = 'hi'
print(len(x), len(y)) # 3 2
x.extend([4, 5, 5]) #push_back
print(len(x)) # 6
print(x.pop()) # gets the last element of the list and then deletes it

n = 0

print(x.pop(n)) # Сделает то же самое с нулевым элементом

x[0] = 1 # Делает нулевой элемент равным 1
```

[[Форма тензоров (массивов) в Python]]

## Копирование и обращение

```Python
x = [4, True, 'hi']
y = x # Обращение к реальному объекту, т.е. ссылка
y = x[:] # Копирование списка
```

Для копирования одноуровневых объектов(не по ссылке), нужно пользоваться функцией `copy()`. Для объектов с вложенными контэйнерами нужно пользоваться `deepcopy()`:

```python
from copy import copy, deepcopy

a = copy(b)
a = deepcopy(b)
```

## Tuples

```Python
x = (0, 0, 2, 2) # Immutable, не может быть изменено, только для чтения
```


## Смотрите также:

- [[Slice оператор в Python]]

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Python 

---

[^def]: термин
[^que]: вопрос