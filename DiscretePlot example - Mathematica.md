> [!bg-plate]
>Plot a sequence:
>
>In[1]:=
>
>![](https://reference.wolfram.com/language/ref/Files/DiscretePlot.en/I_1.png)
>
>`DiscretePlot[PrimePi[k], {k, 1, 50}]`
>
>`https://wolfram.com/xid/0tzxz1j2-k3j031`
>
>Out[1]=
>
>![](https://reference.wolfram.com/language/ref/Files/DiscretePlot.en/O_1.png)
>
>In[2]:=
>
>![](https://reference.wolfram.com/language/ref/Files/DiscretePlot.en/I_2.png)
>
>`DiscretePlot[MoebiusMu[k], {k, 1, 50}]`
>
>`https://wolfram.com/xid/0tzxz1j2-chmh54`
>
>Out[2]=
>
>![](https://reference.wolfram.com/language/ref/Files/DiscretePlot.en/O_2.png)
>
>Plot several sequences:
>
>In[1]:=
>
>![](https://reference.wolfram.com/language/ref/Files/DiscretePlot.en/I_3.png)
>
>`https://wolfram.com/xid/0tzxz1j2-gc6m59`
>
>In[2]:=
>
>![](https://reference.wolfram.com/language/ref/Files/DiscretePlot.en/I_4.png)
>
>`Table[PDF[BinomialDistribution[50, p], k], {p, {0.3, 0.5, 0.8}}]; DiscretePlot[Evaluate[%], {k, 1, 50}]`
>
>`https://wolfram.com/xid/0tzxz1j2-oo29l`
>
>Out[2]=
>
>![](https://reference.wolfram.com/language/ref/Files/DiscretePlot.en/O_3.png)
>
>Show a Riemann sum approximation to the area under a curve:
>
>In[1]:=
>
>![](https://reference.wolfram.com/language/ref/Files/DiscretePlot.en/I_5.png)
>
>`Show[DiscretePlot[Sin[t], {t, 0, 2 Pi, Pi/6}, ExtentSize -> Full], Plot[Sin[t], {t, 0, 2 Pi}]]`
>
>`https://wolfram.com/xid/0tzxz1j2-l88b6`
>
>Out[1]=
>
>![](https://reference.wolfram.com/language/ref/Files/DiscretePlot.en/O_4.png)
>
>With bars to the left and right of the sample points:
>
>In[2]:=
>
>![](https://reference.wolfram.com/language/ref/Files/DiscretePlot.en/I_6.png)
>
>`Table[DiscretePlot[Sin[t], {t, 0, 2 Pi, Pi/6}, ExtentSize -> a, PlotMarkers -> "Point", AxesOrigin -> {0, 0}], {a, {Left, Right}}]`
>
>`https://wolfram.com/xid/0tzxz1j2-3d9mex`
>
>Out[2]=
>
>![](https://reference.wolfram.com/language/ref/Files/DiscretePlot.en/O_5.png)
>
>Use legends to identify functions:
>
>In[1]:=
>
>![](https://reference.wolfram.com/language/ref/Files/DiscretePlot.en/I_7.png)
>
>`data = Table[ PDF[BinomialDistribution[50, p], k], {p, {0.3, 0.5, 0.8}}];`
>
>`https://wolfram.com/xid/0tzxz1j2-x2k3z8`
>
>In[2]:=
>
>![](https://reference.wolfram.com/language/ref/Files/DiscretePlot.en/I_8.png)
>
>`DiscretePlot[Evaluate@data, {k, 1, 50}, PlotLegends -> {0.3, 0.5, 0.8}]`
>
>`https://wolfram.com/xid/0tzxz1j2-lue5dw`
>
>Out[2]=
>
![](https://reference.wolfram.com/language/ref/Files/DiscretePlot.en/O_6.png)