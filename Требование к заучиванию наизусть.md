---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 04 # Не трогать!
Месяц: 05 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-05-04

---
# [[Требование к заучиванию наизусть]]


Лучший способ понять и почувствовать, что ты хорошо выучил что-то - это твоя способность быстро и четко это устно повторить или объяснить. Если ты начинаешь запинаться, задумываться, это показывает тебе, какие места в твоей памяти этой информации хромают.


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#память 

---

[^def]: термин
[^que]: вопрос