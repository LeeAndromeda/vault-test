> [!bg-plate]
>Plot a parametric curve:
>
>Copy to clipboard.
>
>In[1]:=
>
>![](https://reference.wolfram.com/language/ref/Files/ParametricPlot.en/I_1.png)
>
>`ParametricPlot[{Sin[u], Sin[2 u]}, {u, 0, 2 Pi}]`
>
>`https://wolfram.com/xid/0i1m5ycpu-yz6`
>
>Out[1]=
>
>![](https://reference.wolfram.com/language/ref/Files/ParametricPlot.en/O_1.png)
>
>Plot a parametric region:
>
>In[1]:=
>
>![](https://reference.wolfram.com/language/ref/Files/ParametricPlot.en/I_2.png)
>
>`ParametricPlot[ r^2 { Sqrt[t] Cos[t], Sin[t]}, {t, 0, 3 Pi/2}, {r, 1, 2}]`
>
>`https://wolfram.com/xid/0i1m5ycpu-srn`
>
>Out[1]=
>
>![](https://reference.wolfram.com/language/ref/Files/ParametricPlot.en/O_2.png)
>
>Plot several parametric curves with a legend:
>
>In[1]:=
>
>![](https://reference.wolfram.com/language/ref/Files/ParametricPlot.en/I_3.png)
>
>`ParametricPlot[{{2 Cos[t], 2 Sin[t]}, {2 Cos[t], Sin[t]}, {Cos[t], 2 Sin[t]}, {Cos[t], Sin[t]}}, {t, 0, 2 Pi}, PlotLegends -> "Expressions"]`
>
>`https://wolfram.com/xid/0i1m5ycpu-2gsco1`
>
>Out[1]=
>
>![](https://reference.wolfram.com/language/ref/Files/ParametricPlot.en/O_3.png)
>
>Plot overlapping regions:
>
>In[1]:=
>
>![](https://reference.wolfram.com/language/ref/Files/ParametricPlot.en/I_4.png)
>
>`ParametricPlot[{{2 r Cos[t], r Sin[t]}, {r Cos[t], 2 r Sin[t]}}, {t, 0, 2 Pi}, {r, 0, 1}, Mesh -> False]`
>
>`https://wolfram.com/xid/0i1m5ycpu-rawijp`
>
>Out[1]=
>
![](https://reference.wolfram.com/language/ref/Files/ParametricPlot.en/O_4.png)