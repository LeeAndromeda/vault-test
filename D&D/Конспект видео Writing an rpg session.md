#  Writing an rpg session

> ### When writing a campaign, less is more.

Always try to find an in-game reason to explain something to the players. It feels more natural and immersive.

## The three tools Runesmith uses in his sessions
1. Scene descriptions
2. Important NPC
3. What if?'s

## Location details (1)

Variety is better than thorough planning when writing for D&D. Write special things about the locations and then improvise detail during the game. Most details aren't needed outside of combat, unless you work better with them prepared.

###  The NPC (2)

Usually a follower or advisor. A helpful tool for new DM's, because they know what to do next. Runesmith likes to vary his, keeping each one lasting 2-4 sessions.

### The Hypotheticals (3)

Know your platers. Plan a few variable outcomes. You can never plan for everything but planning helps. Knowing what players will do next helps with pacing, as well as instigating stagnant players.

> # THE MOST IMPORTANT PART OF THE NOTE ↓

> ### Game designers checklist:
> Each time someone plays, they should be able to:
> - go somewhere cool
> - talk to someone interesting
> - learn something new
> - fight something
> - get a reward

> # THE MOST IMPORTANT PART OF THE NOTE ↑

## The Rule of Three

Runesmith: a writing principle that suggests a trio of events or characters is more humorous, satisfying or effective than other numbers.

## Subversion

Undermining people's expectations and beliefs in an attempt to educate, humor or surprise the subject.

## Random goods and resources

Players will get them based on dice rolls. They are random after all.

## Never marry yourself on encounters

Don't overmake sidequests. 

# Plan for session
## Step 1. What do the players want?

Understanding what the players want and figuring out how to provide it.

## Step 2. Putting obstacles

Putting road blocks on their way.

> The antagonist can be not only a man but a group of people.

> ## Stories are like balls of clay. Don't waste material on things you don't need to make. Every stroy element is a resource and shouldn't be refined too early.

***A story is never done, and everything left unssid should remain a mystery. - Hulk Hogan, 1786***

# Связано:

[Writing an rpg session YT video by Runesmith](https://youtu.be/34HEM82HoW4)