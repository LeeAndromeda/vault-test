---
Тип: Задача
Описание: -
Исполнители: 
Проект: ВнедрениеЗнаний

Tags: Задача
aliases: ["congruence"]
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: 28 # Не трогать!
Месяц: 02 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-02-28

дата_завершения: "2023-нет-нет"

Выполнено: нет
Результат: 
---

# [[Конгруэнтность]]

> [!example]- Чеклист знания
> - Получение источников и ссылок
> 	- [ ] Получение одной ссылки
> 	- [ ] Получение ссылок на разные источники
> - Конспектирование
> 	- [ ] Достаточное для использования конспектирование
> 	- [ ] Полное конспектирование
> - Создание справок
> 	- [ ] Справки практического применения
> 	- [ ] Список терминов
> 	- [ ] Определение терминов из списка терминов
> 	- [ ] Список связанных тем(если нет внутри текста)
> - Внедрение в базу знаний
> 	- [ ] Создание связей с оригинальной страницей (этой)
> 	- [ ] Разбиение на подтемы
> 	- [ ] Атомизация - (по необходимости) разбиение конспекта по подтемам на страницы
> - [ ] Внедрение в практику
> 

<mark style="background: #FF5582A6;">**Содержание потом вырезается, все содержимое конспекта удаляется и содержание вставляется по шаблону входящего конспекта.**</mark> 


# Содержание 

## [Wikipedia](https://en.wikipedia.org/wiki/Congruence_relation)

In abstract algebra, a congruence relation (or simply congruence) is an equivalence relation on an algebraic structure (such as a group, ring, or vector space) that is compatible with the structure in the sense that algebraic operations done with equivalent elements will yield equivalent elements. Every congruence relation has a corresponding quotient structure, whose elements are the equivalence classes (or congruence classes) for the relation.

## Смотрите также:

> [!question]- Соурслист
> 1. https://en.wikipedia.org/wiki/Congruence_relation



> [!quote]- Контекст
> ```dataview
> LIST 
> FROM !outgoing([[]])
> WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
> ```

---
#ВнедрениеЗнаний 
#Входящие 

---

[^def]: термин
[^que]: вопрос