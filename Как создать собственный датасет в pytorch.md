---

Tags: 
aliases: [Как создать свой dataset class в pytorch]
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 04 # Не трогать!
Месяц: 07 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-07-04

---
# [[Как создать собственный датасет в pytorch]]

Чтобы импортировать абстрактный класс датасета,нужно прописать следующую строчку:

```python
from torch.utils.data import Dataset, DataLoader
```

Каждый датасет должен быть реализован классом, наследующим от абстрактного класса pytorch.utils.data.Dataset. Кастомный датасет должен переопределять следующие методы:
- `__len__` so that `len(dataset)` returns the size of the dataset.
- `__getitem__` to support the indexing such that `dataset[i]` can be used to get iith sample.

На этом обязательные требования к датасету заканчиваются, но полезно и почти обязательно так же продумать, как хранить и загружать данные в него.

## Пример

[[Creating a custom Dataset and Dataloader in Pytorch by Vineeth S Subramanyam Analytics Vidhya Medium.pdf]]


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Входящие 

---

[^def]: термин
[^que]: вопрос