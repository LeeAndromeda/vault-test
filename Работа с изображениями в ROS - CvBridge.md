---
tags: 
aliases:
  - cv_bridge
regex-to-search: 
regex-to-be-found: NoRgxTBFKey
Тип: конспект
Описание:
  - 
День: 2
Месяц: 10
Год: 2023
дата_создания: 2023-10-02
---
# [[Работа с изображениями в ROS - CvBridge]]

# C++
## Перевод из cv::Mat в sensor_msgs::Image

```cpp
cv::Mat img;
cv_bridge::CvImage img_bridge;

std_msgs::Header header;
header.frame_id = "optical";

sensor_msgs::Image img_msg;

img_bridge = cv_bridge::CvImage(header, sensor_msgs::image_encodings::BGR8, img);
img_bridge.toImageMsg(img_msg);
```

## Перевод из sensor_msgs::Image в  CvImage
```cpp
    sensor_msgs::Image msg;
    cv::Mat img;
    cv_bridge::CvImagePtr img_bridge;
    
    img_bridge = cv_bridge::toCvCopy(msg,sensor_msgs::image_encodings::BGR8);
    img = img_bridge->image;
```

## Таблица кодировок цветов
```cpp
  if (source->encoding == "CV_8UC1")
	  source_typed->encoding = enc::MONO8;
  
  else if (source->encoding == "16UC1")
	  source_typed->encoding = enc::MONO16;
  
  else if (source->encoding == "CV_8UC3")
	  source_typed->encoding = enc::BGR8;
  
  else if (source->encoding == "CV_8UC4")
	  source_typed->encoding = enc::BGRA8;
  
  else if (source->encoding == "CV_16UC3")
	  source_typed->encoding = enc::BGR8;

  else if (source->encoding == "CV_16UC4")
	  source_typed->encoding = enc::BGRA8;
```

# Python

## sensor_msgs::Image -> cv::Mat

```python
from cv_bridge import CvBridge
bridge = CvBridge()
cv_image = bridge.imgmsg_to_cv2(image_message, desired_encoding='passthrough')
```

## cv::Mat->sensor_msgs::Image

```python
from cv_bridge import CvBridge
bridge = CvBridge()
image_message = bridge.cv2_to_imgmsg(cv_image, encoding="passthrough")
```

## Кодировки

|   |

```python
mono8: CV_8UC1, grayscale image
mono16: CV_16UC1, 16-bit grayscale image
bgr8: CV_8UC3, color image with blue-green-red color order
rgb8: CV_8UC3, color image with red-green-blue color order
bgra8: CV_8UC4, BGR color image with an alpha channel
rgba8: CV_8UC4, RGB color image with an alpha channel 
```

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. [[Ящишин Олег Игоревич]]
> 2. [CvBridge](http://wiki.ros.org/cv_bridge/Tutorials/ConvertingBetweenROSImagesAndOpenCVImagesPython)


---
#КомпьютерноеЗрение/opencv 

---

[^def]: термин
[^que]: вопрос