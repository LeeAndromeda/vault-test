---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 23 # Не трогать!
Месяц: 03 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-03-23

---
# [[ZMQ socket poll или сабскрайбер с таймаутом]]
> [!usage-help]- Практическая инструкция
> 1. Всю внутренность потока, в котором запускается сабскрайбер оберните в `while(true)`
> 2. Создайте `zmq::pollitem_t items[]` и проинициализируйте его вашим сокетом следующим образом:
> ```cpp
> 	zmq::pollitem_t items [] = {
> 			{subscriber, 0, ZMQ_POLLIN, 0},
> 			// {subscriber, 0, ZMQ_POLLOUT, 5000}
> 		};
> ```
> - Где `subscriber` - ваш объект сокета, первый 0 - должен всегда изначально быть нулем, `ZMQ_POLLIN` или `ZMQ_POLLOUT` - флаг, того, принимает или отправляет ваш сокет данные, [[zmq socket pollitem_t словами ChatGPT|и последнее число - вообще говоря, таймаут ивента(получения или передачи)]], но я в этом не уверен, так как это написал ChatGPT, а в официальной документации это не описано :) .
> 3. После создайте переменную статуса потока `bool restart_thread = false;` и запихните всю остальную логику в `while(!restart_thread)`
> 4. Далее создайте и проинициализируйте объект `zmq_poll(items, 1, 5000)`, где `items` это ваш `zmq::pollitem_t`, 1 - количество сокетов в `items`, 5000 - таймаут в миллисекундах. Это точно таймаут,он в коде и используется.
> 5. Далее для каждого сокета (в примере он один) создаете `if (items[0].revents & ZMQ_POLLIN)`, где 0 - позиция вашего сокета в `items[]`. В теле этого `if` вы и реализуете получение сообщения и взаимодействие с интерфейсом логики. После `if` напишите `else`, в котором будет описаны действия программы, в случае наступления таймаута. Там поднимайте ваш флаг статуса потока `restart_thread = true`.


В коллауте находится мой рабочий пример кода с бриджа на [[Рубин]]. Следует обратить внимание на отличия от обычного [[zmq шаблоны Паблишер и Сабскрайбер#Сабскрайбер|zmq сабскрайбера]], а точнее даже на `zmq::pollitem_t items[]`. Так же данная реализация имеет отлов исключений.

> [!example]- Пример кода
> ```cpp
> void nav_and_sensors_bridge(zmq::context_t *anpa_ctx, std::shared_ptr<AnpaParser> anpa_parser)
> {
> 	while (true)
> 	{
> 		// Prepare subscriber
> 		zmq::socket_t subscriber(*anpa_ctx, ZMQ_SUB);
> 		subscriber.set(zmq::sockopt::subscribe, "ANPAGroup");
> 		  
> 		subscriber.connect(sim_address);
> 		  
> 		// DEBUG: Most of the following code has been reworked to make a timeout
> 		  
> 		zmq::pollitem_t items [] = {
> 			{subscriber, 0, ZMQ_POLLIN, 0},
> 			// {subscriber, 0, ZMQ_POLLOUT, 5000}
> 		};
> 		
> 		bool restart_thread = false;
> 		RCLCPP_INFO_STREAM(anpa_parser->get_pointer_to_node()->get_logger(), "restart thread = " << restart_thread << std::endl);
> 		  
> 		while (!restart_thread)
> 		{
> 			try
> 			{
> 				std::vector<zmq::message_t> recv_msgs;
> 				zmq::poll(items, 1, 5000);
> 			
> 				if (items[0].revents & ZMQ_POLLIN)
> 				{
> 					zmq::recv_result_t result = zmq::recv_multipart(subscriber, std::back_inserter(recv_msgs), zmq::recv_flags::none);
> 				  
> 					RubinComm::ANPAGroup anpa_group;
> 
> 					anpa_group.ParseFromArray(recv_msgs[1].data(), recv_msgs[1].size());
> 				  
> 					if (anpa_group.anpas().size() != 0)
> 					{
> 						anpa_parser->proccess_the_anpa_group(anpa_group);
> 					}
> 				}
> 				else
> 				{
> 					for(int i = 0; i < 100; ++i)
> 					{
> 						RCLCPP_ERROR_STREAM(anpa_parser->get_pointer_to_node()->get_logger(), "WARNING, NAV SUB TIMED OUT");
> 					}
> 					restart_thread = true;
> 				}
> 				  
> 			}
> 			catch (zmq::error_t e)
> 			{
> 				RCLCPP_ERROR_STREAM(anpa_parser->get_pointer_to_node()->get_logger(), "CAUGHT ZMQ EXCEPTION: " << e.what() << std::endl);
> 			}
> 			catch (std::exception e)
> 			{
> 				RCLCPP_ERROR_STREAM(anpa_parser->get_pointer_to_node()->get_logger(), "CAUGHT STD EXCEPTION: " << e.what() << std::endl);
> 			}
> 		}
> 	}
> }
> ```
## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. http://api.zeromq.org/2-1:zmq-poll
> 2. http://zguide2.zeromq.org/cpp:mspoller
> 3. [[zmq socket pollitem_t словами ChatGPT]]


---
#ZMQ 

---

[^def]: термин
[^que]: вопрос