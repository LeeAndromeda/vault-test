---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 19 # Не трогать!
Месяц: 11 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-11-19

---
# [[Абстрактные классы в Python]]

## Как объявляются абстрактные класс?

Основой абстрактных классов в питоне являются `abc.ABC` и `abc.abstractmethod`. 
- `ABC` - это базовый абстрактный класс в Python,от которого вы наследуете тот класс, который хотите сделать абстрактным. 
- `@abstractmethod` - это декоратор, которым вы декорируете [[Спецификатор virtual C++|виртуальные функции]]

### Пример

```python
# Python program showing
# abstract base class work
from abc import ABC, abstractmethod
 
 
class Polygon(ABC):
 
    @abstractmethod
    def noofsides(self):
        pass
 
 
class Triangle(Polygon):
 
    # overriding abstract method
    def noofsides(self):
        print("I have 3 sides")
 
 
class Pentagon(Polygon):
 
    # overriding abstract method
    def noofsides(self):
        print("I have 5 sides")
 
 
class Hexagon(Polygon):
 
    # overriding abstract method
    def noofsides(self):
        print("I have 6 sides")
 
 
class Quadrilateral(Polygon):
 
    # overriding abstract method
    def noofsides(self):
        print("I have 4 sides")
 
 
# Driver code
R = Triangle()
R.noofsides()
 
K = Quadrilateral()
K.noofsides()
 
R = Pentagon()
R.noofsides()
 
K = Hexagon()
K.noofsides()

### Output
I have 3 sides
I have 4 sides
I have 5 sides
I have 6 sides
```

## Конкретные методы абстрактного класса

Конкретные классы содержат только конкретные (обычные) методы, в то время как абстрактные классы могут содержать как конкретные, так и абстрактные методы. Конкретный класс предоставляет реализацию абстрактных методов, абстрактный базовый класс также может предоставить реализацию, вызывая методы через `super()`.

### Пример

```python
# Python program invoking a 
# method using super()
 
import abc
from abc import ABC, abstractmethod
 
class R(ABC):
    def rk(self):
        print("Abstract Base Class")
 
class K(R):
    def rk(self):
        super().rk()
        print("subclass ")
 
# Driver code
r = K()
r.rk()
```

## Абстрактные свойства классов

Абстрактные свойства определяются декоратором `abc.abstractproperty`. Свойства, помеченные этим декоратором в абстрактном классе, <mark style="background: #FF5582A6;">должны иметь геттер и сеттер фнукции</mark>.

### Пример

```python
from abc import ABC, abstractmethod, abstractproperty
 
class Shape(ABC):
    def __init__(self, shape_name):
        self.shape_name = shape_name
    
    @abstractproperty
    def name(self):
        pass
     
    @abstractmethod
    def draw(self):
        pass


class Circle(Shape):
    def __init__(self):
        super().__init__("circle")
 
    @property
    def name(self):
        return self.shape_name
 	def draw(self):    
        print("Drawing a Circle")
```

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://www.geeksforgeeks.org/abstract-classes-in-python/
> 2. https://www.scaler.com/topics/abstract-class-in-python/


---
#Python 

---

[^def]: термин
[^que]: вопрос