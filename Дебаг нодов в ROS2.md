---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 03 # Не трогать!
Месяц: 03 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-03-03

---
# [[Дебаг нодов в ROS2]]

## Description

This is a small tutorial on how to debug a ROS2 C++ node usign VSCode.

## Requirements

This implementation was done using:

-   [ROS2 Foxy](https://index.ros.org/doc/ros2/Installation/Foxy/)
-   [Ubuntu 20.04](https://releases.ubuntu.com/20.04/)
-   [Visual Code 1.55.2](https://code.visualstudio.com/)
-   [C/C++ VSCode extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools)

## Debug

Once you have your C++ code correctly implemented (at least compile), the First thing to do is to **compile the package exporting the symbols** (allow the breakpoints where you want to stop the code):
```bash
cd ros_ws

colcon build --symlink-install --cmake-args -DCMAKE_BUILD_TYPE=RelWithDebInfo

source install/setup.bash
```

> [!hint] Просто скопировать
> ```shell
>colcon build --symlink-install --cmake-args -DCMAKE_BUILD_TYPE=RelWithDebInfo
>```


Second, we have to **launch the [GDB](https://en.wikipedia.org/wiki/GNU_Debugger) Server** for debbuging the CPP Code. Here, we will use a localhost:port for creating the server. Choose any free port that you want.

```shell
ros2 run --prefix 'gdbserver localhost:3000' package_name executable_name
```

Third, we have to **create a launch.json on VSCode**. In other words, we will create a custom debugging configuration. In our case, create a GDB client and connect to the server.

1) Open VSCode on your workspace.
2) Go to your side bar, 'Run and Debug' section.
3) Add a new configuration (Select C++ enviroment or any other)
4) On your launch.json file, put the following information

Launch.json file:
```json
    {
        "version": "0.2.0",
        "configurations": [
            {
                "name": "C++ Debugger",
                "request": "launch",
                "type": "cppdbg",
                "miDebuggerServerAddress": "localhost:3000",
                "cwd": "/",
                "program": "[build-path-executable]"
            }
        ]
    }
```

-   **name** - Custom name of your debugger configuration
-   **request** - In this case we want to _launch_ the client
-   **type** - _cppdbg_ for c++ debugging
-   **miDebuggerServerAddress** - path_server:port
-   **cwd** - Where to find all the required files. We use root because ROS, the package, and other required files are distributed along the entire PC.
-   **program** - Change [build-path-executable] by your executable build file. You can find this path on the console when you launch the server.

Lastly, use the VSCode buttons and panels option to debug correctly your code.

This implementation was found by [Alejandro Duarte](https://github.com/JADC362) and [Jeison Garcia](https://github.com/Jeisongarcia9713).


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://gist.github.com/JADC362/a4425c2d05cdaadaaa71b697b674425f


---
#ROS2 #cpp #vscode/debug

---

[^def]: термин
[^que]: вопрос