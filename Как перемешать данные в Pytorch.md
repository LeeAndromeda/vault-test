---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 10 # Не трогать!
Месяц: 08 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-08-10

---
# [[Как перемешать данные в Pytorch]]


В pytorch датасет, представленный объектом `list` из Python, легко разделить на случайным образом сформированные два объекта заданной длинны. Делается это при помощи `torch.utils.data.random_split`:

> [!def]+ `random_split()`
> `torch.utils.data.random_split(_dataset_, _lengths_, _generator=<torch._C.Generator object>_)` - Randomly split a dataset into non-overlapping new datasets of given lengths.
> 
If a list of fractions that sum up to 1 is given, the lengths will be computed automatically as floor(frac * len(dataset)) for each fraction provided.
> 
> After computing the lengths, if there are any remainders, 1 count will be distributed in round-robin fashion to the lengths until there are no remainders left.
> 
> Optionally fix the generator for reproducible results, e.g.:
> Parameters:
> 
> - **dataset** ([_Dataset_](https://pytorch.org/docs/stable/data.html#torch.utils.data.Dataset "torch.utils.data.Dataset")) – Dataset to be split
> - **lengths** (_sequence_) – lengths or fractions of splits to be produced
> - **generator** ([_Generator_](https://pytorch.org/docs/stable/generated/torch.Generator.html#torch.Generator "torch.Generator")) – Generator used for the random permutation.
> 
> Return type:
> 	[_List_](https://docs.python.org/3/library/typing.html#typing.List "(in Python v3.11)")[[_Subset_](https://pytorch.org/docs/stable/data.html#torch.utils.data.Subset "torch.utils.data.dataset.Subset")[_T_]]

## Пример 

```python
X, y = create_spirals()

print(f"X: {X[:5]}, Y: {y[:5]}")

# Create train and test splits
train_size = int(len(X) * 0.8)
test_size = int(len(X) * 0.2)

spirals_dset = SpiralsDataset(X, y)

train_dset, test_dset = torch.utils.data.random_split(spirals_dset, [train_size, test_size])

print(f"train_dset is: {train_dset[:5]}\n\ntest_dset is: {test_dset[:5]}")*
```

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://pytorch.org/docs/stable/data.html#torch.utils.data.random_split
> 2. [[Караев Артем]]


---
#pytorch 
#Нейросети 

---

[^def]: термин
[^que]: вопрос