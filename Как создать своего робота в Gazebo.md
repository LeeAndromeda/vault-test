


---
Тип: Задача
Описание: -
Исполнители: 
Проект: ВнедрениеЗнаний

Дисциплина: #Входящие 
Tags: Задача
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: 27 # Не трогать!
Месяц: 01 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-01-27

дата_завершения: "2023-нет-нет"

Выполнено: нет
Результат: 
---

```dataview
TABLE день_завершения as "День истечения", месяц_завершения as "Месяц истечения", Проект, Выполнено, Результат, Количество_выходящих_задач as "Количество выходящих задач"
WHERE Дата_создания = this.Дата_создания
limit 1
```
# [[Как создать своего робота в Gazebo]]

- Получение источников и ссылок
	- [ ] Получение одной ссылки
	- [ ] Получение ссылок на разные источники
- Конспектирование
	- [ ] Достаточное для использования конспектирование
	- [ ] Полное конспектирование
- Создание справок
	- [ ] Справки практического применения
	- [ ] Список терминов
	- [ ] Определение терминов из списка терминов
	- [ ] Список связанных тем(если нет внутри текста)
- Внедрение в базу знаний
	- [ ] Создание связей с оригинальной страницей (этой)
	- [ ] Разбиение на подтемы
	- [ ] Атомизация - (по необходимости) разбиение конспекта по подтемам на страницы
- [ ] Внедрение в практику

%%
## Результат:



## Следующая задача:

==#Входящие ==
%%

**Содержание потом вырезается, все содержимое конспекта удаляется и содержание вставляется по шаблону входящего конспекта.**

# Содержание 

%%
> [!tldr]- Практическая справка
> 
%%
## Формат модели робота

Роботы в Gazebo хранятся в формате sdf. 

### Компоненты робота в sdf

- Линки
	- Коллижены
	- Визуалы
	- Инерциалы
	- Сенсоры
	- Свет
- Джоинты
- Плагины

> **Links:** A link contains the physical properties of one body of the model. This can be a wheel, or a link in a joint chain. Each link may contain many collision and visual elements. Try to reduce the number of links in your models in order to improve performance and stability. For example, a table model could consist of 5 links (4 for the legs and 1 for the top) connected via joints. However, this is overly complex, especially since the joints will never move. Instead, create the table with 1 link and 5 collision elements.
> 
> > **Collision:** A collision element encapsulates a geometry that is used for collision checking. This can be a simple shape (which is preferred), or a triangle mesh (which consumes greater resources). A link may contain many collision elements.
> > 
> > **Visual:** A visual element is used to visualize parts of a link. A link may contain 0 or more visual elements.
> > 
> > **Inertial:** The inertial element describes the dynamic properties of the link, such as mass and rotational inertia matrix.
> > 
> > **Sensor:** A sensor collects data from the world for use in plugins. A link may contain 0 or more sensors.
> > 
> > **Light:** A light element describes a light source attached to a link. A link may contain 0 or more lights.
> 
> **Joints:** A joint connects two links. A parent and child relationship is established along with other parameters such as axis of rotation, and joint limits.
> 
> **Plugins:** A plugin is a shared library created by a third party to control a model.

## Построение модели


%%## Приложения%%


> [!question]- Соурслист
> 1. https://classic.gazebosim.org/tutorials?tut=build_model&cat=build_robot
> 2. https://gazebosim.org/docs/garden/building_robot
> 3. https://www.youtube.com/watch?v=_qQAfTmB5wc
> 4. Creating an URDF with solid works - https://www.youtube.com/watch?v=PIxHKowQ-wc
> 5. Creating an URDF with SolidWorks another - https://www.youtube.com/watch?v=anMymsvJKJI
> 6. Туториалы от AriculatedRobotics
> 	1. The ROS transform system (TF) - https://www.youtube.com/watch?v=QyvHhY4Y_Y8
> 	2. Describing a robot with URDF - https://www.youtube.com/watch?v=CwdbsvcpOHM
> 	3. Simulating robots with Gazebo - https://www.youtube.com/watch?v=laWn7_cj434
> 	4. Creating a rough 3d robot model with URDF - https://www.youtube.com/watch?v=BcjHyhV0kIs
> 	5. Driving your virtual robot - https://www.youtube.com/watch?v=IjFcr5r0nMs

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

---
#ВнедрениеЗнаний 
#Gazebo 

---

[^def]: термин
[^que]: вопрос