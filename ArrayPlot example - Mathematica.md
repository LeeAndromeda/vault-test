Plot an array of numbers:

In[1]:=

![](https://reference.wolfram.com/language/ref/Files/ArrayPlot.en/I_1.png)

`ArrayPlot[{{1, 0, 0, 0.3}, {1, 1, 0, 0.3}, {1, 0, 1, 0.7}}]`

`https://wolfram.com/xid/0cqzvyzm-vjk`

Out[1]=

![](https://reference.wolfram.com/language/ref/Files/ArrayPlot.en/O_1.png)

Give explicit color directives to specify colors for individual cells:

In[1]:=

![](https://reference.wolfram.com/language/ref/Files/ArrayPlot.en/I_2.png)

`https://wolfram.com/xid/0cqzvyzm-x0p`

Out[1]=

![](https://reference.wolfram.com/language/ref/Files/ArrayPlot.en/O_2.png)

Specify overall color rules:

In[1]:=

![](https://reference.wolfram.com/language/ref/Files/ArrayPlot.en/I_3.png)

`ArrayPlot[{{1, 0, 0, 0.3}, {1, 1, 0, 0.3}, {1, 0, 1, 0.7}}, ColorRules -> {1 -> Pink, 0 -> Yellow}]`

`https://wolfram.com/xid/0cqzvyzm-fw2`

Out[1]=

![](https://reference.wolfram.com/language/ref/Files/ArrayPlot.en/O_3.png)

Include a mesh:

In[1]:=

![](https://reference.wolfram.com/language/ref/Files/ArrayPlot.en/I_4.png)

`ArrayPlot[{{1, 0, 0, 0.3}, {1, 1, 0, 0.3}, {1, 0, 1, 0.7}}, Mesh -> True]`

`https://wolfram.com/xid/0cqzvyzm-h1e`

Out[1]=

![](https://reference.wolfram.com/language/ref/Files/ArrayPlot.en/O_4.png)

Plot a table of data:

In[1]:=

![](https://reference.wolfram.com/language/ref/Files/ArrayPlot.en/I_5.png)

`ArrayPlot[RandomReal[1, {10, 20}]]`

`https://wolfram.com/xid/0cqzvyzm-gt5`

Out[1]=

![](https://reference.wolfram.com/language/ref/Files/ArrayPlot.en/O_5.png)

Use a standard blend as a color function:

In[1]:=

![](https://reference.wolfram.com/language/ref/Files/ArrayPlot.en/I_6.png)

`ArrayPlot[RandomReal[1, {10, 20}], ColorFunction -> "Rainbow"]`

`https://wolfram.com/xid/0cqzvyzm-63dqy`

Out[1]=

![](https://reference.wolfram.com/language/ref/Files/ArrayPlot.en/O_6.png)