---

Tags: 
aliases: [Как выражается изображение в opencv в c++]
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 17 # Не трогать!
Месяц: 07 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-07-17

---
# [[cv Mat - OpenCV c++]]


`cv::Mat`  - это тип данных, в котором хранятся изображения в OpenCV. Он состоит из `header`, который хранит данные о том, какие размеры у изображения, [[Цветовые пространства OpenCV|каким образом(цветовое пространство)]] и где он хранится в памяти, и ссылки на тело матрицы, которая выражает изображение.

## Копирование объектов

Так как объекты `cv::Mat` **хранят ссылки на матрицы изображений**, операторы копирования копируют не саму матрицу, а ссылку на нее, так что несколько объектов одновременно могут иметь доступ к самой матрице. 

Чтобы копировать и матрицу тоже применяют операции `cv::Mat::clone()` и `cv::Mat::copyTo()`:
```cpp
Mat F = A.clone();
Mat G;
A.copyTo(G);
```

`cv::Mat` в общем виде является классом для работы с матрицами, так что вы можете найти ему и много других применений.

## Вывод матрицы

`cv::Mat` поддерживает вывод матрицы в консоль через оператор `<<`:

```cpp
Mat M(2,2, CV_8UC3, Scalar(0,0,255));
cout << "M = " << endl << " " << M << endl << endl;
```

> [!attention]
> Вывод поддерживают только двумерные матрицы!

## Явное создание `cv::Mat`

> [!note]-
> 
> In the [Load, Modify, and Save an Image](https://docs.opencv.org/4.x/db/d64/tutorial_load_save_image.html) tutorial you have already learned how to write a matrix to an image file by using the [cv::imwrite()](https://docs.opencv.org/4.x/d4/da8/group__imgcodecs.html#gabbc7ef1aa2edfaa87772f1202d67e0ce) function. However, for debugging purposes it's much more convenient to see the actual values. You can do this using the << operator of _Mat_. Be aware that this only works for two dimensional matrices.
> 
> Although _Mat_ works really well as an image container, it is also a general matrix class. Therefore, it is possible to create and manipulate multidimensional matrices. You can create a Mat object in multiple ways:
> 
> - [cv::Mat::Mat](https://docs.opencv.org/4.x/d3/d63/classcv_1_1Mat.html#a2c4229732da267f1fe385458af3896d8) Constructor
> 
> Mat M(2,2, [CV_8UC3](https://docs.opencv.org/4.x/d1/d1b/group__core__hal__interface.html#ga88c4cd9de76f678f33928ef1e3f96047), [Scalar](https://docs.opencv.org/4.x/dc/d84/group__core__basic.html#ga599fe92e910c027be274233eccad7beb)(0,0,255));
> 
> cout << "M = " << endl << " " << M << endl << endl;
> 
> ![MatBasicContainerOut1.png](https://docs.opencv.org/4.x/MatBasicContainerOut1.png)
> 
> For two dimensional and multichannel images we first define their size: row and column count wise.
> 
> Then we need to specify the data type to use for storing the elements and the number of channels per matrix point. To do this we have multiple definitions constructed according to the following convention:
> 
> CV_[The number of bits per item][Signed or Unsigned][Type Prefix]C[The channel number]
> 
> For instance, _CV_8UC3_ means we use unsigned char types that are 8 bit long and each pixel has three of these to form the three channels. There are types predefined for up to four channels. The [cv::Scalar](https://docs.opencv.org/4.x/dc/d84/group__core__basic.html#ga599fe92e910c027be274233eccad7beb) is four element short vector. Specify it and you can initialize all matrix points with a custom value. If you need more you can create the type with the upper macro, setting the channel number in parenthesis as you can see below.
> 
> - Use C/C++ arrays and initialize via constructor
> 
> int sz[3] = {2,2,2};
> 
> Mat L(3,sz, [CV_8UC](https://docs.opencv.org/4.x/d1/d1b/group__core__hal__interface.html#ga78c5506f62d99edd7e83aba259250394)(1), Scalar::all(0));
> 
> The upper example shows how to create a matrix with more than two dimensions. Specify its dimension, then pass a pointer containing the size for each dimension and the rest remains the same.
> 
> - [cv::Mat::create](https://docs.opencv.org/4.x/d3/d63/classcv_1_1Mat.html#a55ced2c8d844d683ea9a725c60037ad0) function:
> 
> M.create(4,4, [CV_8UC](https://docs.opencv.org/4.x/d1/d1b/group__core__hal__interface.html#ga78c5506f62d99edd7e83aba259250394)(2));
> 
> cout << "M = "<< endl << " " << M << endl << endl;
> 
> ![MatBasicContainerOut2.png](https://docs.opencv.org/4.x/MatBasicContainerOut2.png)
> 
> You cannot initialize the matrix values with this construction. It will only reallocate its matrix data memory if the new size will not fit into the old one.
> 
> - MATLAB style initializer: [cv::Mat::zeros](https://docs.opencv.org/4.x/d3/d63/classcv_1_1Mat.html#a56daa006391a670e9cb0cd08e3168c99) , [cv::Mat::ones](https://docs.opencv.org/4.x/d3/d63/classcv_1_1Mat.html#a5e10227b777425407986727e2d26fcdc) , [cv::Mat::eye](https://docs.opencv.org/4.x/d3/d63/classcv_1_1Mat.html#a458874f0ab8946136254da37ba06b78b) . Specify size and data type to use:
> 
> Mat E = Mat::eye(4, 4, [CV_64F](https://docs.opencv.org/4.x/d1/d1b/group__core__hal__interface.html#ga30a562691cc5987bc88eb7bb7a8faf2b));
> 
> cout << "E = " << endl << " " << E << endl << endl;
> 
> Mat O = Mat::ones(2, 2, [CV_32F](https://docs.opencv.org/4.x/d1/d1b/group__core__hal__interface.html#ga4a3def5d72b74bed31f5f8ab7676099c));
> 
> cout << "O = " << endl << " " << O << endl << endl;
> 
> Mat Z = Mat::zeros(3,3, [CV_8UC1](https://docs.opencv.org/4.x/d1/d1b/group__core__hal__interface.html#ga81df635441b21f532fdace401e04f588));
> 
> cout << "Z = " << endl << " " << Z << endl << endl;
> 
> ![MatBasicContainerOut3.png](https://docs.opencv.org/4.x/MatBasicContainerOut3.png)
> 
> - For small matrices you may use comma separated initializers or initializer lists (C++11 support is required in the last case):
> 
> Mat C = (Mat_<double>(3,3) << 0, -1, 0, -1, 5, -1, 0, -1, 0);
> cout << "C = " << endl << " " << C << endl << endl;
> 
> C = (Mat_<double>({0, -1, 0, -1, 5, -1, 0, -1, 0})).reshape(3);
> 
> cout << "C = " << endl << " " << C << endl << endl;
> 
> ![MatBasicContainerOut6.png](https://docs.opencv.org/4.x/MatBasicContainerOut6.png)
> 
> - Create a new header for an existing _Mat_ object and [cv::Mat::clone](https://docs.opencv.org/4.x/d3/d63/classcv_1_1Mat.html#a03d2a2570d06dcae378f788725789aa4) or [cv::Mat::copyTo](https://docs.opencv.org/4.x/d3/d63/classcv_1_1Mat.html#a33fd5d125b4c302b0c9aa86980791a77) it.
> 
> Mat RowClone = C.row(1).clone();
> 
> cout << "RowClone = " << endl << " " << RowClone << endl << endl;
> 
> ![MatBasicContainerOut7.png](https://docs.opencv.org/4.x/MatBasicContainerOut7.png)
> 
> Note
> 
> You can fill out a matrix with random values using the [cv::randu()](https://docs.opencv.org/4.x/d2/de8/group__core__array.html#ga1ba1026dca0807b27057ba6a49d258c0) function. You need to give a lower and upper limit for the random values:
> 
> Mat R = Mat(3, 2, [CV_8UC3](https://docs.opencv.org/4.x/d1/d1b/group__core__hal__interface.html#ga88c4cd9de76f678f33928ef1e3f96047));
> 
> [randu](https://docs.opencv.org/4.x/d2/de8/group__core__array.html#ga1ba1026dca0807b27057ba6a49d258c0)(R, Scalar::all(0), Scalar::all(255));

## [[Итерирование через cv Mat]]

> [!important]
> Обязательно посмотрите!


---
## Смотрите также:
- [[Итерирование через cv Mat]]
```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://docs.opencv.org/4.x/d6/d6d/tutorial_mat_the_basic_image_container.html


---
#КомпьютерноеЗрение/opencv 
#cpp 

---

[^def]: термин
[^que]: вопрос