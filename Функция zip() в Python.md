---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 05 # Не трогать!
Месяц: 03 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-03-05

---
# [[Функция zip() в Python]]


Функция zip() принимает на входе несколько итерируемых объектов (iterable) или итераторов (iterators) и поэлементно группирует в кортежи. Функция останавливается, когда заканчиваются элементы в одном из источников. Относится к неперезапускаемым генераторам.

Пример:

```Python
user_names = ['Anna', 'Bob', 'Claire', 'Daniel']
user_age = [20, 25, 30]
zipped_values = zip(user_names, user_age)

print(list(zipped_values)) # => [('Anna', 20), ('Bob', 25), ('Claire', 30)]
# для Daniel возраста не осталось, функция остановилась
print(list(zipped_values)) # => []
# т.к. функция неперазапускаемая, повторный вызов вернул пустой список
```


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Python

---

[^def]: термин
[^que]: вопрос