---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 06 # Не трогать!
Месяц: 12 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-12-06

---
# [[Разбор проблемы с установкой ОС для Jetson Nano, используемых на роботах Wheeltec]]


Судя по тому, что сказано [здесь](https://forums.developer.nvidia.com/t/jetson-nano-not-booting/193417/5), через NVIDIA SDK Manager можно какие-то действия проводитиь и с самим джетсоном, причем вместе с флешкой. На этой почве у меня возникла мысль, на китайских джетсонах стоит что-то,что паодвязано с системами на флешках.

[Тут ](https://forums.developer.nvidia.com/t/jetson-nano-not-booting/193417/7) указана ссылка на мануальный режим восстановления в джетсоне.

[На форуме в посте по часто встречающимся проблемам](https://forums.developer.nvidia.com/t/debug-tips-when-you-hit-any-issue-fail-to-boot-keep-reboot-etc/177457) указано, что SDKManager должен стоять на x64 Ubuntu 18.04. Так же говорится про какой-то QSPI-NOR. Похоже,что это прошивка самого джетсона.

> [!info]- ChatGPT о QSPI-NOR
>In the context of the Jetson Nano, QSPI-NOR refers to Quad Serial Peripheral Interface - Non-Volatile Memory. Let's break down these terms:
>
>1. **Quad Serial Peripheral Interface (QSPI):** QSPI is a type of serial communication protocol that allows for high-speed data transfer between a master device (like a microcontroller or processor) and a peripheral device (such as a memory chip). "Quad" in QSPI indicates that it uses four communication lines (clock and three data lines) for faster data transfer compared to standard SPI.
>    
>2. **Non-Volatile Memory (NOR):** NOR is a type of non-volatile storage technology, which means that it retains data even when power is turned off. NOR flash memory is commonly used for storing firmware, boot code, and other critical system data.
>    
>
>On the Jetson Nano, QSPI-NOR is likely used for storing essential firmware, boot loaders, and possibly other critical system software. The Quad SPI interface provides faster data transfer rates compared to traditional SPI, making it suitable for reading large amounts of data quickly during the boot process.
>
>In embedded systems like the Jetson Nano, QSPI-NOR is often employed for its speed, reliability, and non-volatile characteristics, making it well-suited for storing important system software.
> --- 
>In the context of our conversation, what does this line mean? "SDKmanager will reflash the QSPI-NOR over your jetson module. So if your board crashed in bootoloader stage, it will be recovered."
>
>ChatGPT
>
>In the context of your conversation, the line is referring to the process facilitated by the SDKmanager tool, which is used for managing software on NVIDIA's Jetson modules. Let's break down the statement:
>
>1. **SDKmanager:** This is a software development kit (SDK) manager provided by NVIDIA. It is a tool that helps developers set up and manage the software on their Jetson modules. It can handle tasks such as flashing firmware, installing software packages, and managing the development environment.
>    
>2. **Reflash the QSPI-NOR:** The term "reflash" means to overwrite or update the contents of the Quad Serial Peripheral Interface - Non-Volatile Memory (QSPI-NOR). This is a type of non-volatile memory often used for storing critical firmware and bootloader code on embedded systems like the Jetson module.
>    
>3. **Jetson module:** In this context, it refers to the Jetson Nano module or a similar module in the Jetson family of products. These modules are embedded computing platforms designed by NVIDIA for AI and machine learning applications.
>    
>4. **Crashed in bootloader stage:** If the boot process of the Jetson module fails at the bootloader stage (the initial software that starts the operating system), it can render the module unusable. This might happen due to corrupted firmware or other issues.
>    
>5. **It will be recovered:** The line suggests that using SDKmanager to reflash the QSPI-NOR can recover a Jetson module that has crashed during the bootloader stage. By overwriting the QSPI-NOR with a known good firmware image, the tool helps restore the module to a functional state, allowing it to boot up properly.
>    
>
>In summary, the statement is explaining that SDKmanager can be used to recover a Jetson module that has encountered issues during the bootloader stage by reprogramming the QSPI-NOR with a fresh and correct firmware image.

Судя по всему,проблему можно будет решить работой через SDKManager.

> [!info]- Таблица поддерживаемых систем - SDKManager
> ![[Pasted image 20231206170903.png]]

Возможно мне понадобится еще Ubuntu 20.04. Пока попробую на Ubuntu 22.

Нашел какой-то `jetsonUtilities`. Возможно пригодится в будущем. ->https://stackoverflow.com/questions/72359090/how-to-find-the-jetpack-version-of-nvidia-jetson-device

Придется использовать Ubuntu 18.04, потому что Jetson Nano поддерживает только Jetpack 4\*, а он ставится только с этой версии.

Можно рассмотреть вариант просто использовать чистую систему Jetson на другой убунте, но тогда надо понять вообще, как работать с SDKManager и прошивать Jetson Nano.

[Эта статья](https://askubuntu.com/questions/1230886/ubuntu-18-04-and-20-04-on-the-same-computer) описывает процесс установки ubunutu 20 и 18 рядом друг с другом. Можно применить в подготовке у себя.

[Эта статья](https://askubuntu.com/questions/88384/how-can-i-repair-grub-how-to-get-ubuntu-back-after-installing-windows) описывает процесс восстановления Grub. На всякий случай.

## 2023-12-07

Нашел [видео](https://jetsonhacks.com/2021/03/10/jetson-nano-boot-from-usb/), в котором описывается процесс подготовки Jetson Nano к запуску с флешки. [[Как использовать USB флешку для бута Jetson Nano]]

Выяснилось, что большинство вариантов Jetson Nano идут с QSPI-NOR памятью, а вариант на машинках wheeltec скорее всего имеет emmc память и не использует для бута sd карту. То есть с этим могут быть связаны все проблемы. Так же я попробовал забутить оригинальную систему с другого порта и у меня получилось.

emmc - https://onpad.ru/catalog/cubie/nvidia/3304.html

Вариантов 2:
1. Попробовать поиграться с настройками кастомного образа
2. Поставить оригинальный образ и попытаться обновить его до ubuntu 20.04

На форумах нашел упоминание системы xubuntu 20, которую можно поставить на jetson nano b01. ->https://forums.developer.nvidia.com/t/cant-install-ubuntu-20-04-on-jetson-nano-developer-kit-model-b01/160527

Теперь вариантов 3:
1. Попробовать поиграться с настройками кастомного образа
2. Попробовать по гайду поставить образ xubuntu
3. Поставить оригинальный образ и попытаться обновить его до ubuntu 20.04

Нашел docker контэйнеры с ros2 различных версий. Можно  попробовать запустить его на ubuntu 18.04. -> 
- https://github.com/dusty-nv/jetson-containers/tree/master/packages/ros
- https://forums.developer.nvidia.com/t/when-will-jetpack-move-to-ubuntu-20-04/142517/8?u=yyootttaa


Теперь вариантов 4:

1. Попробовать поиграться с настройками кастомного образа
2. Попробовать по гайду поставить образ xubuntu
3. Поставить оригинальный образ и попробовать запустить контэйнер с ros2
4. Поставить оригинальный образ и попытаться обновить его до ubuntu 20.04

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 



---
#Jetson/Nano

---

[^def]: термин
[^que]: вопрос