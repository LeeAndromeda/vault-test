---

Tags: 
aliases: 
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

Тип: конспект
Описание: -

День: 21 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Friday 21st October 2022 17:20:03

---
# [[MarkdownExport/Наследование классов в C++]]

В C ++ есть несколько типов наследования:

-   публичный (`public`)- публичные (`public`) и защищенные (`protected`) данные наследуются без изменения уровня доступа к ним;
-   защищенный (`protected`) — все унаследованные данные становятся защищенными;
-   приватный (`private`) — все унаследованные данные становятся приватными.

Наследование выполняется слеющим синтаксисом:

```cpp
//  Определение класса Device
// ...

class Computer: public Device {/* Тело класса Computer */};

```

[[Множественное наследование в C++]] является темой, которая заслуживает отдельного внимания, так как вызывает очень много сложностей проектирования программ, большая часть из которых оставлена на решение программисту.

## Инициализация супер-класса[^1] в конструкторе субкласса
```cpp
class ControlSignalsParser : public brBase::ROS2ZMQBridge<RubinComm::RegulatorComplex, RubinComm::ANPAGroup, anpa_regulator_msgs::msg::GoalVel, BridgeNode>
{
	public:
	ControlSignalsParser(std::shared_ptr<BridgeNode> node_p) : ROS2ZMQBridge(node_p)
	{};
```
## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

> [!question]- Соурслист
> 1. https://habr.com/ru/post/445948/


---
#cpp 

[^1]: Родительского класса