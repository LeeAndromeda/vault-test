---

Tags: 
aliases: 
regex-to-search: "(.*)[Сс]пецифик(.*)[Дд]оступ(.*)"
regex-to-be-found: "((.*)[Cc][(++)(pp)](.*))|((.*)[Сс]пецифик(.*)[Дд]оступ(.*))"

Тип: конспект
Описание: -

День: 21 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Friday 21st October 2022 16:29:08

---
# [[MarkdownExport/Спецификаторы доступа классов cpp]]

**В языке C++ есть 3 уровня доступа**:

   **спецификатор public** делает члены открытыми;

   **спецификатор private** делает члены закрытыми;

   **спецификатор protected** открывает доступ к членам только для дружественных и дочерних классов (детально об этом на соответствующем уроке).

## Типы наследования 

### Публичное наследование

| Access specifier in base class | Access specifier when inherited <mark style="background: #ADCCFFA6;">publicly</mark> |
| ------------------------------ | ---------------------------------------- |
| <mark style="background: #ADCCFFA6;">Public</mark>         --->                | <mark style="background: #ADCCFFA6;">Public</mark>                                   |
| <mark style="background: #D2B3FFA6;">Protected</mark>      --->                | <mark style="background: #D2B3FFA6;">Protected</mark>                                |
| <mark style="background: #FF5582A6;">Private</mark>        --->                | <mark style="background: #CACFD9A6;">Inaccessible</mark>                             |

В работе с классом ничего не меняется. Приватные методы родительского класса исчезают из поля зрения ребенка, значит они должны использоваться исключительно для упрощения внутренней работы класса.

## Защищенное наследование

| Access specifier in base class | Access specifier when inherited <mark style="background: #D2B3FFA6;">protectedly</mark> |
|--------------------------------|---------------------------------------------|
| <mark style="background: #ADCCFFA6;">Public</mark>     --->                    | <mark style="background: #D2B3FFA6;">Protected</mark>                                   |
| <mark style="background: #D2B3FFA6;">Protected</mark>      --->                | <mark style="background: #D2B3FFA6;">Protected</mark>                                   |
| <mark style="background: #FF5582A6;">Private</mark>      --->                  | <mark style="background: #CACFD9A6;">Inaccessible</mark>                                |

Защищенное наследование означает, что будете использовать родительский класс только для обустройства работы своего класса и предоставоять детям возможность работать с вашим родителем.

### Приватное наследование

| Access specifier in base class | Access specifier when inherited <mark style="background: #FF5582A6;">privately</mark> |
|--------------------------------|-------------------------------------------|
| <mark style="background: #ADCCFFA6;">Public</mark>            --->             | <mark style="background: #FF5582A6;">Private</mark>                                   |
| <mark style="background: #D2B3FFA6;">Protected</mark>         --->             | <mark style="background: #FF5582A6;">Private</mark>                                   |
| <mark style="background: #FF5582A6;">Private</mark>           --->             | <mark style="background: #CACFD9A6;">Inaccessible</mark>                              |

Приватное наследование используется, когда вы наследуете от вспомогательного класса, который нужен вам и пользователю только на внутренней кухне вашего класса.

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

> [!question]- Соурслист
> 1. https://ravesli.com/urok-113-klassy-obekty-i-metody-klassov/
> 2. http://cppstudio.com/post/439/
> 3. Inheritance and access specifiers - https://www.learncpp.com/cpp-tutorial/inheritance-and-access-specifiers/


---
#cpp