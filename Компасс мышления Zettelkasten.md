


---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 06 # Не трогать!
Месяц: 05 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-05-06

---
# [[Компасс мышления Zettelkasten]]

**Компасс мышления [[Zettelkasten]]** - это метод определения связей идеи с другими идеями, который помогает построить собственную систему конспектов. Он заключается в следующем:

Ты берешь идею и как бы помещаешь ее на плоскость информации, а затем обдумываешь ее в четыре разные стороны.

1. Север - откуда эта идея пришла
2. Запад - на что похожа эта идея
3. Восток - что противоположно этой идее
4. Юг - к чему эта идея может вести

Пример:

![[Zettelkasten compass example.png]]

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://www.youtube.com/watch?v=5O46Rqh5zHE


---
#Zettelkasten
#Саморазвитие


---

[^def]: термин
[^que]: вопрос