# Правила игры в стратежку тезисно

## Карта

Карта состоит из **провинций**. Провинции бывают **морские**, **наземные** и **побережные**.

Соседние провинции соединены **путем**. Пути бывают **проходимые**, **труднопроходимые**[^3] и **непроходимые**. Если между провинциями есть горы, то путь непроходим. Если между провинциями есть река, то путь труднопроходим. Провинция так же считается труднопроходимой, если в ней находится количество юнитов другого игрока, соизмеримое с вашей группировкой.

Соизмеримость проверяется по следующей формуле:

$$\frac{UV_2}{UV_1}>0,3$$

> Провинции имеют разные ландшафтные типы.

На провинциях могут располагаться **ресурсы** и **здания**. Они занимают **места**. Мест в каждой провинции всего пять.

### Ресурсы

> #### [[Новая теория ресурсов]]

#### Модернизированная Классическая теория ресурсов

Существует пять основных видов ресурсов:
1. Кирпичи
2. Еда
3. Железо
4. Алмазы
5. Исследования
6. Валюта

Четыре первых вида ресурсов добываются добытчиками на источниках. Пятый вид производится институтами. Шестой вид производят города(250/ход) и Корпорации.



## Юниты

> ### [[Новая теория юнитов]]

### Модернизированная классическая теория юнитов

### Группы юнитов

Юниты, обладающие ненулевой атакой должны группироваться по универсальной стоимости, иначе они не смогут передвигаться.  

Формула проста:

Суммарная универсальная стоимость > 60 единиц

## Здания
Здания являются неподвижными единицами, выполняющими свои особые функции. Зачастую здания либо что-то производят, либо дают какие-то преимущества юнитам, которые их используют.

#### Теория зданий

Здания могут:

- являться центрами городов - 100 универсальной стоимости
- Добывать ресурсы - (среднее арифметическое от количества за ход(для добытчиков 5 ус))
- Превращать одни ресурсы в другие (50 * (1 + количество обрабатываемых видов за один ход / 2) ус)
- Являться "пристанищем" для юнитов, нуждающихся в базе - (100 ус)
- создавать юниты - (40 * (1 + уровень технологий / 3))
- давать бонусы атаки и защиты рядом стоящим воискам (суммарный бонус * 10 ус)
- ускорять перемещение (20 ус)
## Бой

> **Фаза боя и передвижений является первой фазой игры и каждого хода**

Перед фазой боя определяется инициатива. Далее указываются юниты, которые не будут двигаться в этом ходу. Игрок выбирает, окопался(х1,8 защита) этот юнит или в засаде(х1.8 атака).

Перед началом выполнения действий юнитом, игрок должен выбрать поведение юнита:
- Обычное (никаких изменений в характеристиках)
- Марш-бросок - подвижность юнита а этом ходу х1,5, защита х0,75
- Маневрирование - атака х0,75, маневренность х1,5
- Наступление - маневренность х0,75, атака х1,5

Игроки ходят по очереди: каждый игрок ходит всеми юнитами, пока не закончатся юниты, способные к действию.

Бой случается, если два юнита стоят на одной провинции и игрок, совершающий ход, решает атаковать другого.

Расчет боя проходит по следующему плану:
1. Определение боевых группировок
2. Выбор видов атаки(если необходимо)
3. Выбор распределений
4. Добавление коэффициентов поведения юнита
5. Расчет коэффициентов защиты
6. Рассчет и добавление коэффициентов маневренности
7. Броски кубиков
8. Рассчет потерь

#### 1. Определение боевых группировок

![[Pasted image 20220105031045.png]]

Боевые группировки состоят из боевых единиц(один вид юнитов во всем его количестве внутри боевой группировки). Боевая группировка должна в себя включать юнитов на универсальную стоимость от 60 единиц. Если универсальная стоимость группировки меньше, значит в бою она неделима. Потолок универсальной стоимости группировки - 2500 единиц универсальной стоимости.

#### 2. Выбор видов атаки

Некоторые юниты не могут использовать все свое оружие одновременно. Для таких юнитов нужно выбирать используемый в этом бою вид атаки.

#### 3. Выбор распределений

Каждая боевая группировка может распределить свой урон, как захочет по боевым единицам в пределах выбранной ей вражеской боевой группировки. Однако боевая группировка распределяет свой урон дискретными долями по 10%(т.е. 10%, 20%, 30% и т.д.)

#### 4. Добавление коэффициентов поведения юнита

В начале хода юнитом вы выбирали его поведение. Если вы не выбирали и юнит не окопан/не в засаде, его поведение автоматически определяется как обычное.

#### 5. Рассчет коэффициентов защиты

Защита юнита от определенного вида атаки = базовая защита * коэффициент [^69] 

Урон, нанесенный одним из видов атаки не суммируется с другими(Вам нужно превысить защиту врага одним видом атаки).

#### 6. Рассчет и добавление коэффициентов маневренности

Коэффициент маневренности в бою = точность атаки - маневренность юнита

Если он равен нулю или отрицателен, юнит получает урон в полном объеме. Если нет, то получившееся число вычитается из единицы - такой урон в долях получает юнит (минимум 0 урона).

> Позиции юнитов

#### 7. Броски кубиков

Каждая сторона боя бросает два четырехгранных кубика. Первый отражает успешность атак, второй защит. Каждый игрок вычитает из своей атаки вражескую защиту(результаты кубиков) и получает число, значение которого далее смотрит по шкале эффективности атаки. Он получает коэффициент эффективности атаки, на который домножает весь нанесенный врагу урон.

#### 8. Рассчет потерь

 В итоге каждая боевая единица, получившая урон, должна провериться на наличие потерь. 

 Это делается по следующему плану:
 1. Подписывание нанесенного боевой единице урона по каждому виду атаки отдельно
 2. Применение коэффициента эффективности атаки
 3. Применение коэффициентов поведения юнита
 4. Применение коэффициента маневренности
 5. Деление урона по видам атаки на соответствующие виды защиты с примененным коэффициентом защиты
 6. Целое число, полученное от деления, является количеством уничтоженных в бою юнитов


### Боевые характеристики юнитов

#### Атака и ее виды

>Атака - максимальный урон, который может нанести юнит за бой. Атаки бывают различными и делятся на **группы** и **виды**. Так же любая атака делится на подвиды **ближний бой** и **дальний бой** и **направления**(воздух, земля/вода, подводное).
>
 >Атака - максимальный урон, который может нанести юнит за бой. Атаки классифицируются по **дальности**, **уровням**, **видам** и **подвидам**. Каждый вид урона обладает **уровнем мощи** в зависимости от 

Базовая атака - урон, который нанесет юнит атакой с коэффициентом 1.

Атака делится на **уровни**:  воздушная, наземная/наводная, подводная. Каждый юнит обладает **коэффициентами атаки** по уровням атаки.

В сражении юнит, имеющий возможность атаковать различными видами атаки, имеет возможность атаковать только одним, если не указано обратного.

>##### Ближний бой
>
>- Дробящий
>- Пронзающий
>- Режущий





>**Группа физический урон**
>
>- Дробящий
>- Пронзающий
>- Режущий
>
>**Группа элементальный урон**
>
>- Огневой
>- Холодовой
>- Электрический
>- Токсиновый
>- Радиоактивный
>- Электромагнитный
>- Взрывной
>
>**Группа фентези**
>
>- Псионический
>- Плазменный
>- Лазерный
>- Гравитационный
>- Импульсно-энергетический
>- Воид

#### Защита и коэффициенты защиты

**Защита** - это количество урона, которое может выдержать юнит. 

**Коэффициенты защиты** - это множители защиты, которые влияют на защиту от определенных видов атаки. Они имеют такое же деление, как и виды атаки. 

Коэффициенты защиты указываются для всех видов атаки для каждого юнита за исключением электромагнитной атаки для юнитов, не имеющих электроники. Если КЗ не указан, значит он принимается за единицу. Коэффициент защиты 1 означает, что юнит подвержен этому виду урона настолько же, насколько среднестатистический человек.


#### Маневренность

**Маневренность** - способность юнита уклоняться от атаки. Маневренность равная единице - маневренность среднестатистического человека.

#### Точность атаки

**Точность атаки** - способность юнита попадать по цели. Точность равная единице - точность среднестатичстического человека.

## Технологии

Планируется ввести возможность создавать свои расы с юнитами, для которых нужно будет писать свой кодекс. Основной и единственной на данный момент расой является *Раса людей*. Так же поощряется ветвление одной расы по вариантам новых уровней технологий. В случае ветвления, раса получает не все плюшки уровня технологий сразу, а приобретает только один из вариантов этого ут, остальные варианты можно докупить по цене, равной оригинальной цене, деленной на порядковый номер раза покупки этого ут.

> 1 уровень технологий:
> 1 уровень добычи
> мечи, луки, катапульты, грузовые корабли, осадные лестницы, конница, пикинеры, тяжёлые рыцари
> церкви, замки, мастерские

> 2 уровень технологий:
> 2 уровень добычи
> мушкеты, конница, пушки, дробь, первая артиллерия, боевые корабли, пушки на кораблях
> крепости, университеты,  мануфактуры, железная дорога для паровозов
> сплавы, порох

> 3 уровень технологий
> 3 уровень добычи
> Огнестрельное оружие, гранаты, танки, машины, самолёты, дирижабли, железные корабли, артиллерия, газовые снаряды, пламенные бомбардировки, железные корабли, подводные лодки
> стратегические крепости, институты, фабрики, железная дорога для скоростных поездов, статичные ПВО, аэропорты
> больше сплавов, нефть

> 4 уровень технологий
> 4 уровень добычи
> продвинутое огнестрельное оружие, продвинутые гранаты, продвинутая артиллерия, продвинутые самолёты, продвинутые корабли, авианосцы, дальние ракеты, ядерные ракеты, ядерный след, мобильные ПВО
> военные базы, научно исследовательские институты, заводы, железная дорога для сверхскоростных поездов, ракетная установка, ПРО, атомные электростанции, гидроэлектростанции, ветряки, полет в космос

> 5 уровень технологий
> киборги и роботы, ии, боевые роботы, ядерный синтез, сражения умов.

#Стратежка

[^1]: по чему он передвигается
[^2]: подразумевается количество орудий в случае, если они не могут стрелять все вместе. Если могут, то оно равно 1.
[^3]: трудно проходимость означает, что после передвижения на эту провинцию юнит заканчивает передвижение в этом ходу
[^69]: ???

## Дипломатические состояния



## Идеи
### Корпорации

Вводится ресурс: валюта

Города теперь приносят валюту: столица 250, обычные 100.

> Курс обмена валюты на ресурсы:
> 10 кирпичей = 50 валюты
> 10 еды = 50 валюты
> 10 железа =75 валюты
> 10 алмазов = 300 валюты
> **Обменивать валюту на ресурсы можно только этими долями или им кратными**

Государство может профинансировать создание корпорации. Когда корпорация создана, каждый ход она бросает к12 на успешность торгов. 

>Основать корпорацию стоит:
>2000 валюты × (количество уже основанных вами корпораций + 1) + стоимость постройки здания, если таковых нет
 >
 > Стоимость постройки здания подразделения: 30 к, 50 ж, 20 ал, 10 ед

 Корпорации различаются по **сферам экономической деятельности**. Сфер экономической деятельности 5: кирпичи, еда, железо, алмазы и технологии. Государство может создать по одной корпорации в каждой из сфер и не больше.

 > # Правило конкуренции
 > В одном и том же городе может находиться только одно структурное подразделение корпораций одной и той же сферы экономической деятельности

Подразделения различаются по **множителю финансовой мощи**.

На каждом уровне успешности торговли, кроме плохой, есть шанс получить новое подразделение корпорации. Место выбирает владелец корпорации. При открытии заграничного подразделения, вам необходимо заплатить 250 валюты государству, в котором вы открываете подразделение и построить здание подразделения, если нет свободных

На плохом уровне успешности корпорация получает шанс банкротства одного из подразделений. Если у вас нет, конкурентов, то выберите один из филиалов(если есть, а если нет, то штаб) - он обанкротился и больше не принадлежит корпорации, но здание остаётся и его не нужно заново строить; если у вас есть конкурентные корпорации, то они выбирают филиал, который банкротится по следующему принципу - если в стране-владельце корпорации-конкурента есть ваши филиалы, то он может выбирать только среди них, если их нет, то он может выбрать любой филиал(штаб или заграничный штаб, если он последний в какой-то стране).

Вы так же можете полностью или для конкретных корпораций/государств запретить иностранные корпорации в своем государстве. В таком случае все иностранные подразделения этих корпораций закрываются, здания освобождаются и теперь ваши корпорации могут их занимать.

**Виды подразделений**:
1. Штаб(50)
2. Филиал(25)
3. Заграничный штаб(35)
4. Заграничный филиал(15)
5. Иностранный штаб(15)
6. Иностранный филиал(10)

> ## Таблица значений результатов
> 11-12 налаживает торговое сотрудничество с другой корпорацией,  успешные торги
> 9-10 успешные торги
> 5-8 нормальные торги
> 3-4 плохие торги
> 1-2 - банкротится одно из подразделений(50%) либо прекращается торговое сотрудничество с другой корпорацией(50%), плохие торги

> # Правило государственной аккредитации/санкций
> Вы можете потратить 1000 валюты для того, чтобы повысить результат, выпавший на кубике, на единицу. Можно делать до максимума.
> Так же вы можете потратить 2000 валюты для того, чтобы понизить результат, выпавший на кубике у корпорации вашего оппонента, на единицу. Можно делать до -4 и **только перед броском оппонента**.


> ## Таблица результатов торгов
> - Очень успешные торги: прибыль подразделения - 3×мнозитель, шанс на новое подразделение (50%)
> - нормальные торги: прибыль подразделения - 2×множитель, шанс на новое подразделение(%25)
> - плохие торги: прибыль подразделения - 1×множитель

> ## Порядок хода с дополнением Корпорации
> 1. Расчет доходности корпораций
	> 1. Решения по санкциям
	> 2. Броски успешности торговли
	> 3. Решения по аккредитации
	> 4. Рассчет прибыли
> 2. Рассчет прибыли по ресурсам
> 3. Строительство
> 4. Боевая фаза
	> 1. Броски инициативы
	> 2. Передвижения
	> 3. Рассчет сражений