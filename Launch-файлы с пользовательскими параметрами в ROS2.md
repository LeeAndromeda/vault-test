---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 22 # Не трогать!
Месяц: 03 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-03-22

---
# [[Launch-файлы с пользовательскими параметрами в ROS2]]

## Создание лаунч файла



В вашем пакете создайте папку `launch` и создайте в ней ваш лаунч файл. Придумайте ему имя, а расширение должно быть `*.launch.py`.

В ROS2 лаунч файлы пишутся на питоне. Для этого используются библиотеки `launch` и `launch_ros`, но не полностью:

```python
from launch import LaunchDescription
import launch_ros.actions
```

Для описания самого лаунча, нам необходимо определить функцию `generate_launch_description()`. В ней мы создаем объект класса `LaunchDescription`, который мы импортировали:

```python
def generate_launch_description():
	ld = LaunchDescription()
```

Используя этот объект, добавляем необходимые ноды в лаунч с помощью команды `LaunchDescription.add_action()`. Например:

```python
	# Бридж Симулятор-СУ
	ld.add_action(
		launch_ros.actions.Node(
			package="sim_su_bridge", executable="sim_su_bridge_node",
			parameters=sim_prms.params, emulate_tty=True, output="screen"
		)
	),
	
	# Бридж Пульт-СУ
	ld.add_action(
		launch_ros.actions.Node(
			package="pu_su_bridge", executable="pu_su_bridge_node",
			parameters=pu_prms.params, emulate_tty=True, output="screen"
		)
	)
```

> [!seealso]- Опции при укзании нода
> Если вы делаете лаунч файл, который запускает несколько нодов одного типа, то вам могут пригодиться опции `namespace` и `remappings`:
> ```python
>from launch import LaunchDescription
>from launch_ros.actions import Node
>
>def generate_launch_description():
>    return LaunchDescription([
>        Node(
>            package='turtlesim',
>            namespace='turtlesim1',
>            executable='turtlesim_node',
>            name='sim'
>        ),
>        Node(
>            package='turtlesim',
>            namespace='turtlesim2',
>            executable='turtlesim_node',
>            name='sim'
>        ),
>        Node(
>            package='turtlesim',
>            executable='mimic',
>            name='mimic',
>            remappings=[
>                ('/input/pose', '/turtlesim1/turtle1/pose'),
>                ('/output/cmd_vel', '/turtlesim2/turtle1/cmd_vel'),
>            ]
>        )
>    ])
> ```


И в самом конце функции добавляем возврат объекта `LaunchDescription`:
```python
return ld
```

> [!example]- Полный пример лаунч файла
> ```python
> from launch import LaunchDescription
> import launch_ros.actions
> from bridge_params import SimBridgeParams, ControlBridgeParams
>   
> def generate_launch_description():
> 	ld = LaunchDescription()
> 	
> 	# Бридж Симулятор-СУ
> 	ld.add_action(
> 		launch_ros.actions.Node(
> 			package="sim_su_bridge", executable="sim_su_bridge_node",
> 			parameters=sim_prms.params, emulate_tty=True, output="screen"
> 		)
> 	),
> 	# Бридж Пульт-СУ
> 	ld.add_action(
> 		launch_ros.actions.Node(
> 			package="pu_su_bridge", executable="pu_su_bridge_node",
> 			parameters=pu_prms.params, emulate_tty=True, output="screen"
> 		)
> 	)
> 	return ld
> ```

Лаунч файл готов. Теперь надо настроить файл `setup.py` нашего пакета.

## Настройка setup.py

Большую часть файла можно просто скопировать из шаблона:

> [!tldr]- Шаблон setup.py
> ```python
>from setuptools import find_packages, setup
>from glob import glob
>import os
>package_name = 'augv_launches'
>
>setup(
>    name=package_name,
>    version='0.0.0',
>    packages=find_packages(exclude=['test']),
>    data_files=[
>        ('share/ament_index/resource_index/packages',
>            ['resource/' + package_name]),
>        ('share/' + package_name, ['package.xml']),
>        (os.path.join('share', package_name), glob('launch/*.launch.py')),
>    ],
>    install_requires=['setuptools'],
>    zip_safe=True,
>    maintainer='leev',
>    maintainer_email='yyootttaa@gmail.com',
>    description='TODO: Package description',
>    license='TODO: License declaration',
>    tests_require=['pytest'],
>    entry_points={
>        'console_scripts': [
>        ],
>    },
>)
>```


А поменять в нем нужно будет только `package_name` так, чтобы он совпадал с именем пакета из `package.xml`. Например: 

```python
package_name = 'beginner_tutorials'
```

Теперь билдим наш пакет с лаунчем, соурсим воркспэйс и запускаем лаунч через:
```shell
ros2 launch package_name launch_name.launch.py
```
## Добавление параметров

### Файл параметров

В папке `launch`  создайте файл `*_params.py`, который будет содержать ваши параметры. 

Параметры задаются питоновским классом. Определите ваш класс параметров и добавьте ему функцию `__init__()`:
```python
class SimBridgeParams:
	def __init__(self) -> None:
```

Далее определяйте параметры следующим образом:
```python
	def __init__(self) -> None:
		self.anpa_topic = "/anpa_"
		self.regulator_topic = "/regulator"
		self.bottom_sonar = "/sonar/front"
```

После того, как определили все необходимые параметры, создайте [[Словари dict в Python|словарь]] параметров, чтобы определить их значения для ROS2:
```python
	def __init__(self) -> None:
		self.anpa_topic = "/anpa_"
		self.regulator_topic = "/regulator"
		self.bottom_sonar = "/sonar/front"
		
		self.params = [
			{"bottom_sonar": self.bottom_sonar},
			{"front_sonar" : self.front_sonar},
			{"regulator_topic" : self.regulator_topic}
		]
```

Далее определите функцию `update_parameters()`, в которую запишите тот же самый словарь:
```python
	def update_parameters(self):
		self.params = [
			{"bottom_sonar": self.bottom_sonar},
			{"front_sonar" : self.front_sonar},
			{"regulator_topic" : self.regulator_topic}
		]
```

### Изменения в лаунче

В вашем лаунч файле добавьте импорт вашего файла параметров. Оттуда импортируйте созданный вами класс. Например:

```python
from bridge_params import SimBridgeParams
```

В `generate_launch_description()` создайте объект импортированного класса и заполните его поля. <mark style="background: #FF5582A6;">***Именно здесь вы заполняете те значения, которые хотите использовать в вашей программе.***</mark> Затем вызовите функцию `update_parameters()`:

```python
def generate_launch_description():
	ld = LaunchDescription()
	
	sim_prms = SimBridgeParams()
	sim_prms.anpa_topic = "/anpa_"
	sim_prms.regulator_topic = "/regulator"
	sim_prms.bottom_sonar = "/sonar/bottom"
	
	sim_prms.update_parameters()
```

### Изменения в setup.py

В `py_modules=[]` внесите созданный(-е) вами файл(-ы) следующим образом:

```python
setup(
	name=package_name,
	version='0.0.1',
	package_dir={'': 'launch'},
	py_modules=[
		'bridge_params',
	],
```

Т.е. например, если ваш файл параметров назывался `bridge_params.py`, то в `py_modules` внесите `bridge_params`.

В `data_files[]` по [[#Настройка setup.py|тому же принципу, что и строчка для лаунча из шаблона]], нужно добавить следующую строчку для файла параметров:

```python
data_files=[

	('share/ament_index/resource_index/packages', ['resource/' + package_name]),
	('share/' + package_name, ['package.xml']),
	(os.path.join('share', package_name), glob('launch/*.launch.py')),
	(os.path.join('share', package_name), glob('launch/bridge_params.py')), # Вот эту
],
```

### В самом ноде

#### [[C++]]

Чтобы получить заданное значение, вам нужен класс, который наследует от или явялется классом `Node`. Данный пример будет приведен на языке C++. Пусть вашим нодом будет объект `node`:

```cpp
// Сначала объявляем параметры
node.declare_parameter("anpa_topic", "/anpa_");
node.declare_parameter("regulator_topic", "/regulator");
node.declare_parameter("bottom_sonar", "/sonar/front");

// Затем создаем переменные для них (это необходимый шаг для следующей функции)
std::string default_anpa_topic = "/anpa_";
std::string default_regulator_topic = "/regulator";
std::string default_bottom_sonar = "/sonar/bottom";

// Затем получаем параметры. Первый аргуемнт - название параметра,
// второй аргумент - переменная, в которую будем получать значение,
// и третий аргумент - переменная-значение по-умолчанию, ее мы объявили
// в прошлом шаге.
node.get_parameter_or("anpa_topic", anpa_topic, default_anpa_topic);
node.get_parameter_or("regulator_topic", regulator_topic, default_regulator_topic);
node.get_parameter_or("bottom_sonar", bottom_sonar, default_bottom_sonar);
```

После этого мы билдим наш пакет с ланучем и все работает. Теперь при запуске лаунч файла параметры будут подтягиваться из него.

#### [[Python]]

Необходимо внести небольшие изменения в [[Простой паблишер-сабскрайбер ROS2(Python)|шаблон класса]]. Было:

```python
    def __init__(self):
        super().__init__('node_name')
```

Стало:

```python
	def __init__(self):
		super().__init__('node_name', allow_undeclared_parameters=True, automatically_declare_parameters_from_overrides=True)

		self.your_parameter_var = try_get(lambda: self.get_parameter("parameter_name"), "default_if_value_not_set")
```

Так же нужно определить саму функцию `try_get()`:

```python
def try_get(fn, default):
    try:
        val = fn().value
        if val is None:
            return default
        else:
            return val
    except:
        return default
```

## Смотрите также:

> [!question]- Соурслист
> 1. anpa_bringup
> 2. [[Бросалин Дмитрий Олегович]]

> [!quote]- Контекст
> ```dataview
> LIST 
> FROM !outgoing([[]])
> WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
> ```


---
#ВнедрениеЗнаний 
#ROS2 

---

[^def]: термин
[^que]: вопрос