---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 30 # Не трогать!
Месяц: 09 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-09-30

---
# [[cv QRCodeDetector - класс для определения QR кодов в OpenCV]]

# C++
## detectAndDecode

```cpp
cv::String cv::QRCodeDetector::detectAndDecode 	( 	
		InputArray  	img,
		OutputArray  	points = noArray(),
		OutputArray  	straight_qrcode = noArray() 
	) 	
```

Parameters:
- `img`	grayscale or color (BGR) image containing QR code.
- `points`	optional output array of vertices of the found QR code quadrangle. Will be empty if not found.
- `straight_qrcode`	The optional output image containing rectified and binarized QR code

## detectAndDecodeMulti

# Python

## detectAndDecode

Эта функция работает только тогда, когда в картинке один QR-код. 

```python
import cv2


while True:
    frame = cv2.imread("/home/leev/drone_ws/src/packageInDev/opencv_projects/graphical_code_detection/imgs/QR.png")
    frame2 = frame.copy()

    points = []

    qr_detector = cv2.QRCodeDetector()
    info, points, _ = qr_detector.detectAndDecode(frame)
    # retval, info, points, _ = qr_detector.detectAndDecodeMulti(frame)

    for i in range(len(points)):
        cv2.line(frame, tuple(map(lambda x: int(x), points[i][0])), tuple(map(lambda x: int(x), points[i][1])), (0, 0, 255), 5)
        cv2.line(frame, tuple(map(lambda x: int(x), points[i][1])), tuple(map(lambda x: int(x), points[i][2])), (0, 0, 255), 5)
        cv2.line(frame, tuple(map(lambda x: int(x), points[i][2])), tuple(map(lambda x: int(x), points[i][3])), (0, 0, 255), 5)
        cv2.line(frame, tuple(map(lambda x: int(x), points[i][3])), tuple(map(lambda x: int(x), points[i][0])), (0, 0, 255), 5)


    if (not len(frame) == 0):
        # cvtColor(frame, frame, COLOR_GRAY2BGR);
        cv2.imshow("objects2", frame)

    print(info)

    if( cv2.waitKey(5) >= 0 ):
        break # stop capturing by pressing ESC 
    
```
## detectAndDecodeMulti

Эта функция работает и с одним кодом, и с несколькими

```python
import cv2


while True:
    frame = cv2.imread("/home/leev/drone_ws/src/packageInDev/opencv_projects/graphical_code_detection/imgs/QR2.png")
    frame2 = frame.copy()

    points = []

    qr_detector = cv2.QRCodeDetector()
    # info, points, _ = qr_detector.detectAndDecode(frame)
    retval, info, points, _ = qr_detector.detectAndDecodeMulti(frame)

    for i in range(len(points)):
        cv2.line(frame, tuple(map(lambda x: int(x), points[i][0])), tuple(map(lambda x: int(x), points[i][1])), (0, 0, 255), 5)
        cv2.line(frame, tuple(map(lambda x: int(x), points[i][1])), tuple(map(lambda x: int(x), points[i][2])), (0, 0, 255), 5)
        cv2.line(frame, tuple(map(lambda x: int(x), points[i][2])), tuple(map(lambda x: int(x), points[i][3])), (0, 0, 255), 5)
        cv2.line(frame, tuple(map(lambda x: int(x), points[i][3])), tuple(map(lambda x: int(x), points[i][0])), (0, 0, 255), 5)


    if (not len(frame) == 0):
        # cvtColor(frame, frame, COLOR_GRAY2BGR);
        cv2.imshow("objects2", frame)

    print(info)

    if( cv2.waitKey(5) >= 0 ):
        break # stop capturing by pressing ESC 
    
```

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. [[OpenCV cv QRCodeDetector Class Reference.pdf]]


---
#cpp 
#КомпьютерноеЗрение/opencv 

---

[^def]: термин
[^que]: вопрос