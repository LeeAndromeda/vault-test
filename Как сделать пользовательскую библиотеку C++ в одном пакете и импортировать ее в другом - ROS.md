---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 08 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-10-08

---
# [[Как сделать пользовательскую библиотеку C++ в одном пакете и импортировать ее в другом - ROS]]


## Структура пакетов
![[Pasted image 20231008165316.png]]
- **test_lib** - пакет, в котором создается экспортируемая библиотека
	- `test_lib.hpp`, `test_lib.cpp` - файлы библиотеки, которую мы экспортируем. Там был реализован простой класс машинки, которая что-то выполняет функцией `Car::honk()`.
- **test_lib_import** - пакет, в котором импортируется библиотека

`car_honk.cpp` в обоих пакетах - файл, включающий нашу библиотеку и выполняющий функцию `Car::honk()` для демонстрации работоспособности подключенной библиотеки.

## Создание библиотеки в CMake

Процедура аналогична описанной в [[cmake создание shared library cpp|конспекте по созданию shared library]]:

```cmake
add_library(test_library SHARED src/test_lib.cc include/test_lib.hpp)
target_include_directories(test_library
PUBLIC
$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
$<INSTALL_INTERFACE:include>)


install(TARGETS test_library
		ARCHIVE DESTINATION ${CATKIN_GLOBAL_LIB_DESTINATION}
		LIBRARY DESTINATION ${CATKIN_GLOBAL_LIB_DESTINATION}
		RUNTIME DESTINATION ${CATKIN_GLOBAL_BIN_DESTINATION})
```

Важное отличие в том, что `ARCHIVE DESTINATION` и `LIBRARY DESTINATION` указаны `GLOBAL`, а не `PACKAGE`.

Перед строками создания библиотеки необходимо раскомментировать команду `catkin_package` и указать необходимые параметры:

```cmake
catkin_package(
  INCLUDE_DIRS include
  LIBRARIES test_library
  CATKIN_DEPENDS roscpp
#  DEPENDS system_lib
)
```

После [[catkin build|сборки воркспэйса]], библиотека будет доступна в других пакетах.

## Импортирование в CMake 

Импортирование происходит просто через команду `find_package()`, где вы указываете все пакеты ROS, от которых зависит текущий пакет. Например:

```cmake
find_package(catkin REQUIRED COMPONENTS
  roscpp
  test_lib_pkg
)
```

Затем в вашем коде нужно включить необходимую библиотеку, как любую другую:

```cpp
#include "test_lib.hpp" // Наша библиотека
#include <string>


int main ()
{
    Car car("Honk");
    car.honk();

    return 0;
}
```

Ну и конечно желательно указать зависимость от вашего пакета с библиотекой в `package.xml`.

## Смотрите также:

- [[Как создать свой модуль mymodule.h в C++]]
- [[cmake создание shared library cpp]]

---
#cpp 
#ROS 

---

[^def]: термин
[^que]: вопрос