---

Tags: 
aliases: [opencv cpp, imread, imwrite, imshow]
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 17 # Не трогать!
Месяц: 07 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-07-17

---
# [[Запись, загрузка и показ изображения в OpenCV c++]]


> [!def]+ Чтение картинки
> `imread()` - функция четния картинки из файла
> 
> **аргументы:**
> 	1. `std::string img_path` - путь к изображению
> 	2. `cv::ImreadModes flag` - флаг чтения
> 
> **возвращает:** `cv::Mat`

> [!def]+ Запись картинки
> `imwrite()` - функция записи картинки в файл
> 
> **аргументы:**
> 	1. `std::string path`
> 	2. `cv::Mat img`

> [!def]+ Функция показа картинки
> `imshow()` - показывает картинку в новом окне
> 
> **аргументы:**
> 	1. `cv::Mat img`

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#КомпьютерноеЗрение/opencv 
#cpp 

---

[^def]: термин
[^que]: вопрос