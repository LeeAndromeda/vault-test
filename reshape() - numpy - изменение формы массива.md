---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 20 # Не трогать!
Месяц: 11 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-11-20

---
# [[reshape() - numpy - изменение формы массива]]


Может быть вызвана, как через `numpy.reshape()`, так и как метод самого массива.

В первом случае синтаксис таков:

```python
numpy.reshape(array, new_shape_tuple, order="C")
```

`order` - порядок элементов в новом массиве. Обращайтесь к документации для понимания.

Во втором случае таков:

```python
a = numpy.arrange(n)
a.reshape(new_shape_tuple, order="C")
```

Обе функции будут работать, только если произведение измерений изначального массива равно произведению измерений нового.

Функция возвращает новый массив, а не меняет старый.

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Python/NumPy 

---

[^def]: термин
[^que]: вопрос