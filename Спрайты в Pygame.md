---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 07 # Не трогать!
Месяц: 11 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-11-07

---
# [[Спрайты в Pygame]]

## Базовые методы работы со спрайтами
### Загрузка изображения

```python
sprite_image = pygame.image.load("sprite.png")
```

### Пример создания кастомного класса для спрайтов

```python
class Sprite(pygame.sprite.Sprite):
    def __init__(self, x, y, image):
        super().__init__()
        self.image = image
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
```

### Отображение спрайта 

Чтобы отобразить спрайт на экране, используйте метод `blit()` поверхности `screen`. Установите координаты спрайта с помощью атрибутов `rect.x` и `rect.y`. Вот пример отображения спрайта:

```python
screen.blit(sprite.image, sprite.rect)
```

### Изменение положения спрайта

Чтобы изменить положение спрайта, обновите значения атрибутов `rect.x` и `rect.y`. Например, чтобы сдвинуть спрайт вправо на 5 пикселей:

```python
sprite.rect.x += 5
```

### Анимация спрайта

Для создания простой анимации спрайта мы можем использовать список изображений и переключать их в зависимости от времени. Вот пример кода для анимации спрайта:

```python
image_list = [image1, image2, image3]  # Список изображений
frame_index = 0  # Индекс текущего кадра

def animate_sprite():
    nonlocal frame_index
    frame_index = (frame_index + 1) % len(image_list)  # Переключение кадров
    sprite.image = image_list[frame_index]  # Установка текущего изображения спрайта
```

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://stepik.org/lesson/1030427/


---
#python/pygame 

---

[^def]: термин
[^que]: вопрос