---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 09 # Не трогать!
Месяц: 09 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-09-09

---
# [[Строковая функция получения родительской папки, в которой находится файл по заданному пути - Python]]


```python
# 3. Get image class from path name (the image class is the name of the directory where the image is stored)
image_class = random_image_path.parent.stem

print(random_image_path)
print(image_class)

# Output
data/pizza_steak_sushi/train/steak/2087958.jpg
steak
```


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. [[04_pytorch_custom_datasets.ipynb]]


---
#Python

---

[^def]: термин
[^que]: вопрос