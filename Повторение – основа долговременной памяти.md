### **6. Повторение – основа долговременной памяти**

Кратковременное запоминание информации облегчается, когда она подкреплена аудио- и видеоматериалами. А срок хранения информации в долговременной памяти увеличивается, когда человек несколько раз возвращается к ней для повторения. Воспоминания способны очень быстро тускнеть. Через несколько лет после события, память о котором была живой и яркой, мы с большим трудом можем припомнить подробности происшедшего. Как показывают исследования, когда забываются те или иные детали, мозг легко может создать ложные воспоминания, чтобы сохранить логическую связность сюжета. Этот феномен иногда ведет к искажению свидетельских показаний в ходе судебного разбирательства. Когда вы хотите что-либо надолго запомнить, отнеситесь к этой задаче серьезно. Если вы постоянно не выполняете домашние задания, то запомнить пройденный материал в ночь перед экзаменом у вас едва ли получится даже при всем желании. Усваивать новую информацию надо регулярно, часто, понемногу и с перерывами. Чтобы новое осталось в памяти надолго, необходимо сделать следующее.

-   Обдумать новую информацию в течение часа после ее получения.
-   Обсудить ее с другими людьми во всех подробностях.
-   Хорошо выспаться, а наутро освежить информацию в памяти, еще раз ее повторив.

#ПравилаМозга

