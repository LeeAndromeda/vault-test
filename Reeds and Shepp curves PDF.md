---
annotation-target: Reeds and Shepp.pdf
---

>%%
>```annotation-json
>{"created":"2023-02-27T10:37:29.703Z","text":"Допустимые","updated":"2023-02-27T10:37:29.703Z","document":{"title":"","link":[{"href":"urn:x-pdf:49df1509bf7a9e428f6678cc5bc145a1"},{"href":"vault:/Obsidian/Reeds and Shepp.pdf"}],"documentFingerprint":"49df1509bf7a9e428f6678cc5bc145a1"},"uri":"vault:/Obsidian/Reeds and Shepp.pdf","target":[{"source":"vault:/Obsidian/Reeds and Shepp.pdf","selector":[{"type":"TextPositionSelector","start":12254,"end":12264},{"type":"TextQuoteSelector","exact":"Admissible","prefix":"ralgorithmic implementation.2.  ","suffix":" paths.  For us,  the  state  of"}]}]}
>```
>%%
>*%%PREFIX%%ralgorithmic implementation.2.%%HIGHLIGHT%% ==Admissible== %%POSTFIX%%paths.  For us,  the  state  of*
>%%LINK%%[[#^kc9vsdr0qvp|show annotation]]
>%%COMMENT%%
>Допустимые
>%%TAGS%%
>
^kc9vsdr0qvp
