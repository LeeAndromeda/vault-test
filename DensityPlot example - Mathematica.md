> [!bg-plate]
> Plot a function:
> 
> In[1]:=
> 
> ![](https://reference.wolfram.com/language/ref/Files/DensityPlot.en/I_1.png)
> 
> `DensityPlot[Sin[x] Sin[y], {x, -4, 4}, {y, -3, 3}]`
> 
> `https://wolfram.com/xid/0i1oru36a-lgz`
> 
> Out[1]=
> 
> ![](https://reference.wolfram.com/language/ref/Files/DensityPlot.en/O_1.png)
> 
> Use a different color scheme and legend:
> 
> In[1]:=
> 
> ![](https://reference.wolfram.com/language/ref/Files/DensityPlot.en/I_2.png)
> 
> `DensityPlot[Sin[x] Sin[y], {x, -4, 4}, {y, -3, 3}, ColorFunction -> "SunsetColors", PlotLegends -> Automatic]`
> 
> `https://wolfram.com/xid/0i1oru36a-wkzhzs`
> 
> Out[1]=
> 
> ![](https://reference.wolfram.com/language/ref/Files/DensityPlot.en/O_2.png)
> 
> Create a contouring overlay mesh:
> 
> In[1]:=
> 
> ![](https://reference.wolfram.com/language/ref/Files/DensityPlot.en/I_3.png)
> 
> `DensityPlot[Sin[x] Sin[y], {x, -4, 4}, {y, -3, 3}, ColorFunction -> "CMYKColors", PlotPoints -> 35, MeshFunctions -> {#3 &, #3 &}, Mesh -> {Range[-1, 1, 0.4], Range[-0.8, 0.8, 0.4]}, MeshStyle -> Opacity[0.5, Black]]`
> 
> `https://wolfram.com/xid/0i1oru36a-ghfvvr`
> 
> Out[1]=
> 
> ![](https://reference.wolfram.com/language/ref/Files/DensityPlot.en/O_3.png)
> 
> Use a multi-panel layout to show multiple functions at the same time:
> 
> In[1]:=
> 
> ![](https://reference.wolfram.com/language/ref/Files/DensityPlot.en/I_4.png)
> 
> `DensityPlot[{Sin[x] Cos[y], Sin[x + y] Cos[x - y]}, {x, -5, 5}, {y, -5, 5}, PlotLayout -> "Row"]`
> 
> `https://wolfram.com/xid/0i1oru36a-8umatz`
> 
> Out[1]=
> 
![](https://reference.wolfram.com/language/ref/Files/DensityPlot.en/O_4.png)