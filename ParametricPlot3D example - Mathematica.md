Plot a parametric surface:

In[1]:=

![](https://reference.wolfram.com/language/ref/Files/ParametricPlot3D.en/I_1.png)

`ParametricPlot3D[{Cos[u], Sin[u] + Cos[v], Sin[v]}, {u, 0, 2 \[Pi]}, {v, -\[Pi], \[Pi]}]`

`https://wolfram.com/xid/0dqvdgokvlk-hgso0m`

Out[1]=

![](https://reference.wolfram.com/language/ref/Files/ParametricPlot3D.en/O_1.png)

Plot a parametric space curve:

In[1]:=

![](https://reference.wolfram.com/language/ref/Files/ParametricPlot3D.en/I_2.png)

`ParametricPlot3D[{Sin[u], Cos[u], u/10}, {u, 0, 20}]`

`https://wolfram.com/xid/0dqvdgokvlk-l26`

Out[1]=

![](https://reference.wolfram.com/language/ref/Files/ParametricPlot3D.en/O_2.png)

Plot multiple parametric surfaces:

In[1]:=

![](https://reference.wolfram.com/language/ref/Files/ParametricPlot3D.en/I_3.png)

`ParametricPlot3D[{{4 + (3 + Cos[v]) Sin[u], 4 + (3 + Cos[v]) Cos[u], 4 + Sin[v]}, {8 + (3 + Cos[v]) Cos[u], 3 + Sin[v], 4 + (3 + Cos[v]) Sin[u]}}, {u, 0, 2 Pi}, {v, 0, 2 Pi}, PlotStyle -> {Red, Green}]`

`https://wolfram.com/xid/0dqvdgokvlk-drdeyv`

Out[1]=

![](https://reference.wolfram.com/language/ref/Files/ParametricPlot3D.en/O_3.png)

Use simple styling of surfaces:

In[1]:=

![](https://reference.wolfram.com/language/ref/Files/ParametricPlot3D.en/I_4.png)

`ParametricPlot3D[ {Sin[u] Sin[v] + 0.05 Cos[20 v], Cos[u] Sin[v] + 0.05 Cos[20 u], Cos[v]}, {u, -\[Pi], \[Pi]}, {v, -\[Pi], \[Pi]}, MaxRecursion -> 4, PlotStyle -> {Orange, Specularity[White, 10]}, Axes -> None, Mesh -> None]`

`https://wolfram.com/xid/0dqvdgokvlk-8fclp`

Out[1]=

![](https://reference.wolfram.com/language/ref/Files/ParametricPlot3D.en/O_4.png)

Plot surfaces with cuts:

In[1]:=

![](https://reference.wolfram.com/language/ref/Files/ParametricPlot3D.en/I_5.png)

`ParametricPlot3D[{u Cos[v], u Sin[v], Im[(u Exp[I v]^5)^(1/5)]}, {u, 0, 2}, {v, 0, 2 Pi}, Mesh -> None, ExclusionsStyle -> {None, Red}, PlotStyle -> Directive[Green, Opacity[0.8], Specularity[White, 20]]]`

`https://wolfram.com/xid/0dqvdgokvlk-ejjwtr`

Out[1]=

![](https://reference.wolfram.com/language/ref/Files/ParametricPlot3D.en/O_5.png)