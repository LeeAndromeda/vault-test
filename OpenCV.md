---
Тип: Проект 
название_проекта: "(.*)[Oo]pen[Cc][Vv](.*)" # "(.*)Проект(.*)"
Проект: нет
Описание: -
Дисциплина: #Входящие 
Tags: Проект
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: 15 # Не трогать!
Месяц: 05 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-05-15
Дата_создания: 2023-05-23, 17-06

дата_завершения: "2023-нет-нет"

Выполнено: нет
Результат: 
приоритет: 20
---


# [[OpenCV]]


> [!info]- Что это?
> OpenCV (Open Source Computer Vision) - это бесплатная библиотека функций программирования с открытым исходным кодом, предназначенная в основном для компьютерного зрения в реальном времени. Она используется для обработки изображений и видео, обнаружения и распознавания объектов, машинного обучения и многого другого. OpenCV написана на C++ и поддерживает множество языков программирования, включая Python, Java и MATLAB. Он широко используется в различных областях, таких как робототехника, дополненная реальность, медицинская визуализация и наблюдение.
> 

> [!example]- Подпроекты
> ```dataview
> TABLE Описание, день_завершения as "День завершения", месяц_завершения as "Месяц завершения", Исполнители, Выполнено
> FROM #Проект and !"Templates"
> WHERE Тип = "Проект" and regexmatch(this.название_проекта, Проект) and Выполнено = "нет"
> SORT год_завершения ASC, месяц_завершения ASC, день_завершения ASC
> ```

> [!hint]- Задачи
> ```dataview
> TABLE Описание, дата_завершения as "Дата завершения", Исполнители, Выполнено
> FROM #Задача and !"Templates"
> WHERE regexmatch(this.название_проекта, Проект) and Выполнено = "нет"
> SORT дата_завершения ASC
> ```
> > [!success]- Выполненные
> > ```dataview
> > TABLE Описание, дата_завершения as "Дата завершения", Исполнители, Выполнено
> > FROM #Задача  and !"Templates"
> > WHERE regexmatch(this.название_проекта, Проект) and Выполнено = "да"
> > SORT дата_завершения ASC
> >```


## Общая теория 

 - [[UV система координат]]
 - [[Получение мировых координат точки на картинке]]

## Python


### Теория

- [[Чтение и запись файлов - imread(), imwrite() OpenCV]]
- [[Изображение в сырые байты и обратно]]
- [[Работа с картинкой через numpy.array]]
- [[Чтение и запись видео OpenCV]]
- [[Кодировка типа данных изображения - OpenCV]]
- [[Моменты изображения]]


### Литература

- [[Learning-OpenCV-4-Computer-Vision-with-Python-3-3rd-Ed]] 
	- #КомпьютерноеЗрение/opencv/sources/LOCV4HowseNMinichino
	- [[LOCV4 Howse & Minichino.canvas]]
- [[opencv-tutorial-readthedocs-io-en-latest]]
- [[numpy-user-guide]]

## C++

[[OpenCV C++ map.canvas|OpenCV C++ map]]

### Теория

- [[Запись, загрузка и показ изображения в OpenCV c++]]
- [[Показ видео с камеры - пример от Антона OpenCV c++]]
- [[Измерение времени в OpenCv c++]]
- [[cv Mat - OpenCV c++]]
- [[cv VideoCapture - OpenCV c++]]
- [[Конвертация цветового пространства - OpenCV c++]]
- [[cv inRange - OpenCV c++]]
- [[Кодировка типа данных изображения - OpenCV]]
- [[Моменты изображения]]
- [[Как вывести текст на изображении - OpenCV c++]]
- [[Как нарисовать линию в OpenCV c++]]

---
### Воркфлоус

- [[cv QRCodeDetector - класс для определения QR кодов в OpenCV]]
- [[Работа с изображениями в ROS - CvBridge]]


### Проекты

- [[Поиск объектов конкретного цвета - OpenCV c++]]

## Участники проекта

```dataview
LIST
FROM !"Obsidian/Templates" and !"Templates" and #Человек 
WHERE regexmatch(this.название_проекта, Проект) or regexmatch(Проект, this.название_проекта)
```

## Смотрите также
 - [[Установка OpenCV на Linux]]
```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
---

#КомпьютерноеЗрение/opencv 
#Саморазвитие 
#ВнедрениеЗнаний 


---

