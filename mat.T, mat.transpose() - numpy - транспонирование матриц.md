---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 20 # Не трогать!
Месяц: 11 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-11-20

---
# [[mat.T, mat.transpose() - numpy - транспонирование матриц]]



В случае применения `a.transpose()` без аргументов, возвращает матрицу с обратным порядком измерений:

```python
import numpy as np
  
a = np.arange(180)
a = a.reshape(6, 2, 5, 3)
print(a.shape)
  
a = a.transpose()
  
print(a.shape)

###

(6, 2, 5, 3)
(3, 5, 2, 6)
```

Если вы хотите определенным образом траспонировать матрицу, в аргументах введите новый порядок измерений относительно старого следующим образом:

```python
import numpy as np
  
a = np.arange(180)
a = a.reshape(6, 2, 5, 3)
print(a.shape)
  
a = a.transpose(2, 0, 3, 1)
 
print(a.shape)

###

(6, 2, 5, 3)
(5, 6, 3, 2)
```


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Python/NumPy 

---

[^def]: термин
[^que]: вопрос