---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 11 # Не трогать!
Месяц: 08 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-08-11

---
# [[Accuracy - отношение верных случаев к всем - метрика оценки нейронных сетей]]


Accuracy - отношение количества верно распознанных объектов к количеству всех объектов.


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://willkoehrsen.github.io/statistics/learning/beyond-accuracy-precision-and-recall/


---
#Нейросети 
#Статистика 

---

[^def]: термин
[^que]: вопрос