---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 07 # Не трогать!
Месяц: 09 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-09-07

---
# [[torch.nn.MaxPool2d пуллинг слой - Pytorch]]


`nn.MaxPool2d` - это слой для работы с двумерными данными(есть аналогичные слои для других количеств измерений), который в заданном ядре(kernel) берет элемент с максимальным значением и вставляет его в выходные данные.

## Гиперпараметры

- `kernel_size` - размер стороны ядра. Ядро является квадратом с заданной стороной.
- `stride` - шаг, с которым пермещается ядро. По умолчанию равен `kernel_size`

## Последствия работы слоя 

- Если `stride` равен `kernel_size`, размер каждого измерения входных данных делится на `kernel_size`
- Каждый элемент выходных данных является максимальным элементом из каждого рассмотренного слоем ядра. Таким образом извлекаются самые "явные" данные.


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#pytorch 

---

[^def]: термин
[^que]: вопрос