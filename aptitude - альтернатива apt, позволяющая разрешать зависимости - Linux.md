---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 09 # Не трогать!
Месяц: 09 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-09-09

---
# [[aptitude - альтернатива apt, позволяющая разрешать зависимости - Linux]]

**Aptitude** - это текстовый интерфейс для управления пакетами в операционной системе Linux. Он предоставляет пользователю возможность устанавливать, обновлять, удалять и искать пакеты в системе. Aptitude также предлагает функции автоматического разрешения зависимостей, что позволяет удобно управлять пакетами и их зависимостями. Он является альтернативой команде apt-get и обеспечивает более удобный и гибкий способ работы с пакетами в Linux.
## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Linux

---

[^def]: термин
[^que]: вопрос