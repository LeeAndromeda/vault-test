---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 03 # Не трогать!
Месяц: 07 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-07-03

---
# [[Pandas]]


- [[Pandas_Cheat_Sheet]]
- [[Как поместить данные в pandas dataframe]]
- [[Как вывести только начало или только конец датафрейма - Pandas]]
- [[Как узнать сколько и какие элементы находятся в столбце датафрейма]]

---

- [[pandas docs.pdf]]

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#pandas

---

[^def]: термин
[^que]: вопрос