---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 20 # Не трогать!
Месяц: 11 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-11-20

---
# [[Выделение части массива в numpy - слайсы]]

- `a[start:stop:step]` выбирает элементы из `a` с индексами от `start` до `stop` с шагом `step`, т.е. `a[start]`, `a[start + step]`, `a[start + 2*step]` и так далее, пока индексы меньше `stop`;
- отрицательные индексы:
	```python
	a = np.array([[1,2,3],[4,5,6],[7,8,9]])
	print(a[-2])
	print(a[-3])

	# [4 5 6]
	# [1 2 3]
	```
- поддерживается синтаксис многоточия: `a[..., 1]` выбирает элементы с любым индексом в первом измерении и с индексом, равным 1, во втором измерении:  `a[:, 1]` оказывается эквивалентно `a[..., 1]`;
- можно указать индексы сразу в нескольких измерениях, указав их через запятую в квадратных скобках (как `a[..., 1]` в примере выше).

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Python/NumPy 

---

[^def]: термин
[^que]: вопрос