---

Tags: 
aliases: [How to create a torch.Tensor from a numpy array/python list, Как создать torch.Tensor из массива numpy/списка python]
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 27 # Не трогать!
Месяц: 06 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-06-27

---
# [[torch.tensor()]]


---

6/27/2023, 8:56:43 PM
What does the torch.tensor function do? Why is it written without the brackets?

---

6/27/2023, 8:56:52 PM

torch.tensor is a function in PyTorch that creates a new tensor from an existing array-like object, such as a NumPy array or a Python list. The torch.tensor function returns a new PyTorch tensor object with the same data type and shape as the input array-like object.

In the code snippet provided, the torch.tensor function is used to convert each of the four NumPy arrays x_train, y_train, x_valid, and y_valid into PyTorch tensors. This is achieved by mapping the torch.tensor function over these arrays using the map() function, which applies the function to each element of the iterable and returns an iterator that yields the results.

Regarding the use of the torch.tensor function without brackets, this is because torch.tensor is a function object and can be passed around like any other object in Python. In the code snippet, the torch.tensor function is being passed as an argument to the map() function, which takes a function and an iterable as arguments. Therefore, it is not necessary to call the function with parentheses when passing it as an argument to another function.

---


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. ChatGPT
> 2. https://chatbot.theb.ai/#/chat/1687888506866


---
#pytorch 

---

[^def]: термин
[^que]: вопрос