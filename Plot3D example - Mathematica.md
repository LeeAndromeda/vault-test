Plot a function:

In[1]:=

![](https://reference.wolfram.com/language/ref/Files/Plot3D.en/I_1.png)

`Plot3D[Sin[x + y^2], {x, -3, 3}, {y, -2, 2}]`

`https://wolfram.com/xid/0ftul7s-xi5`

Out[1]=

![](https://reference.wolfram.com/language/ref/Files/Plot3D.en/O_1.png)

Plot several functions:

In[1]:=

![](https://reference.wolfram.com/language/ref/Files/Plot3D.en/I_2.png)

`Plot3D[{x^2 + y^2, -x^2 - y^2}, {x, -2, 2}, {y, -2, 2}, ColorFunction -> "RustTones"]`

`https://wolfram.com/xid/0ftul7s-t6beae`

Out[1]=

![](https://reference.wolfram.com/language/ref/Files/Plot3D.en/O_2.png)

Restrict the domain:

In[1]:=

![](https://reference.wolfram.com/language/ref/Files/Plot3D.en/I_3.png)

`Plot3D[{x^2 + y^2, -x^2 - y^2}, {x, -2, 2}, {y, -2, 2}, RegionFunction -> Function[{x, y, z}, x^2 + y^2 <= 4], BoxRatios -> Automatic]`

`https://wolfram.com/xid/0ftul7s-f8oe94`

Out[1]=

![](https://reference.wolfram.com/language/ref/Files/Plot3D.en/O_3.png)

Plot functions with branch cuts:

In[1]:=

![](https://reference.wolfram.com/language/ref/Files/Plot3D.en/I_4.png)

`Plot3D[Im[ArcSin[(x + I y)^4]], {x, -2, 2}, {y, -2, 2}, Mesh -> None, PlotStyle -> Directive[Yellow, Specularity[White, 20], Opacity[0.8]], ExclusionsStyle -> {None, Red}]`

`https://wolfram.com/xid/0ftul7s-01767`

Out[1]=

![](https://reference.wolfram.com/language/ref/Files/Plot3D.en/O_4.png)