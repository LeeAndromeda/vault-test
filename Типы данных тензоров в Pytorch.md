---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 13 # Не трогать!
Месяц: 06 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-06-13

---
# [[Типы данных тензоров в Pytorch]]

For now let's create a tensor with dtype=torch.float16.

```python
float_16_tensor = torch.tensor([3.0, 6.0, 9.0],
                               dtype=torch.float16) # torch.half would also work

float_16_tensor.dtype
torch.float16
```

![[Screenshot_2023-06-13-23-35-55-770_com.brave.browser.jpg]]
![[Screenshot_2023-06-13-23-36-00-763_com.brave.browser.jpg]]![[Screenshot_2023-06-13-23-36-06-505_com.brave.browser.jpg]]


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://pytorch.org/docs/stable/tensors.html#data-types


---
#pytorch 

---

[^def]: термин
[^que]: вопрос