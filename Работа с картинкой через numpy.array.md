


---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 22 # Не трогать!
Месяц: 04 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-04-22

---
# [[Работа с картинкой через numpy.array]]


![[Изображения в OpenCV#Базовое устройство]]

Соответствено, получая доступ к элементам этого массива и изменяя их, мы меняем саму картинку. Например, белый пиксель в верхнем-левом углу картинки:

```python
import cv2  
img = cv2.imread('MyPic.png')  
img[0, 0] = [255, 255, 255]
```

![[np.item() и np.itemset()]]

## Области интереса (Regions of interest или ROI)

С помощью слайсинга массивов мы можем присвоить область изображения переменной, определить вторую область, присвоить значение первой области ко второй(таким образом копируя часть изображения на другое местоположение в изображении).

```python
import cv2  
img = cv2.imread('MyPic.png')  
my_roi = img[0:100, 0:100]  
img[300:400, 300:400] = my_roi
```
> [!attention] Важно!
> Важно, чтобы две области совпадали по размерам


## Смотрите также:
- [[ndarray NumPy - общая теория]]
```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. [[LOCV4 Howse & Minichino.canvas]]
> 2. [[Learning-OpenCV-4-Computer-Vision-with-Python-3-3rd-Ed]]


---
#КомпьютерноеЗрение/opencv 
#Python/NumPy 

---

[^def]: термин
[^que]: вопрос