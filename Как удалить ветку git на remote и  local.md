---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 10 # Не трогать!
Месяц: 01 # Не трогать!
Год: 2023 # Не трогать!
Дата_создания: Tuesday 10th January 2023 17:11:10

---
# [[Как удалить ветку git на remote и  local]]

# Delete Local Branch

To delete the _**local**_ branch use one of the following:

```
git branch -d <branch_name>
git branch -D <branch_name>
```

-   The `-d` option is an alias for `--delete`, which only deletes the branch if it has already been fully merged in its upstream branch.
-   The `-D` option is an alias for `--delete --force`, which deletes the branch "irrespective of its merged status." [Source: `man git-branch`]
-   As of [Git v2.3](https://github.com/git/git/blob/master/Documentation/RelNotes/2.3.0.txt), `git branch -d` (delete) learned to honor the `-f` (force) flag.
-   You will receive an error if you try to delete the currently selected branch.

# Delete Remote Branch

As of [Git v1.7.0](https://github.com/gitster/git/blob/master/Documentation/RelNotes/1.7.0.txt), you can delete a _**remote**_ branch using

```
$ git push <remote_name> --delete <branch_name>
```

Starting with [Git v2.8.0](https://github.com/git/git/blob/master/Documentation/RelNotes/2.8.0.txt), you can also use `git push` with the `-d` option as an alias for `--delete`. Therefore, the version of Git you have installed will dictate whether you need to use the easier or harder syntax.

### Steps to delete remote Git branches

In review, the steps to delete remote Git branches are:

1.  Issue the `git push origin –delete branch-name` command, or use the vendor’s online UI to perform a branch deletion
2.  After the remote branch is deleted, then delete the remote tracking branch with the `git fetch origin –prune` command
3.  Optionally delete the local branch with the `git branch -d branch-name` command

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://stackoverflow.com/questions/2003505/how-do-i-delete-a-git-branch-locally-and-remotely
> 2. [Как удалить ветку](https://techrocks.ru/2021/09/04/removing-local-or-remote-branch-in-git/)
> 3. https://www.theserverside.com/blog/Coffee-Talk-Java-News-Stories-and-Opinions/delete-remote-Git-branches-github-gitlab-bitbucket-tracking-local


---
#git #GitHub #GitLab

[^def]: термин
[^que]: вопрос