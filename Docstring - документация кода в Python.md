---
Тип: Задача
Описание: -
Исполнители: 
Проект: ВнедрениеЗнаний

Tags: Задача
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: 02 # Не трогать!
Месяц: 05 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-05-02

дата_завершения: "2023-нет-нет"

Выполнено: нет
Результат: 
---

# [[Docstring - документация кода в Python]]

> [!danger] Обязательный чеклист
> - [ ] Получение одной ссылки
> - [ ] Расставить теги 


> [!example]+  Чеклист знания
> - Получение источников и ссылок
> 	- [ ] Получение ссылок на разные источники
> - Конспектирование
> 	- [ ] Достаточное для использования конспектирование
> 	- [ ] Полное конспектирование
> - Создание справок
> 	- [ ] Справки практического применения
> 	- [ ] Список терминов
> 	- [ ] Определение терминов из списка терминов
> 	- [ ] Список связанных тем(если нет внутри текста)
> - Внедрение в базу знаний
> 	- [ ] Создание связей с оригинальной страницей (этой)
> 	- [ ] Разбиение на подтемы
> 	- [ ] Атомизация - (по необходимости) разбиение конспекта по подтемам на страницы
> - [ ] Внедрение в практику
> 

<mark style="background: #FF5582A6;">**Содержание потом вырезается, все содержимое конспекта удаляется и содержание вставляется по шаблону входящего конспекта.**</mark> 

# Содержание 

```python
def kos_root():
    """Return the pathname of the KOS root directory."""
    global _kos_root
    if _kos_root: return _kos_root
    ...
```

Пример с аргументами и возвращаемым значением:

```python
def train(model, input_train, output_train, lossfn, optim):
    """Training function for my classification model

    Args:
        model (torch.nn.Module): model
        input_train: x training data
        output_train: y training data
        lossfn: loss function
        optim: optimizer

    Returns:
        float: accuracy
    """
```

## Смотрите также:

> [!question]- Соурслист
> 1. https://peps.python.org/pep-0257/

> [!quote]- Контекст
> ```dataview
> LIST 
> FROM !outgoing([[]])
> WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
> ```

---
#ВнедрениеЗнаний/НеобходимоеЗнание 
#Python 

---

[^def]: термин
[^que]: вопрос