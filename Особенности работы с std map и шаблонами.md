---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 07 # Не трогать!
Месяц: 02 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-02-07

---
# [[Особенности работы с std map и шаблонами]]

## error: cannot bind rvalue reference of type ‘&&’ to lvalue of type ‘’

```cpp
error: cannot bind rvalue reference of type ‘func_modes&&’ to lvalue of type ‘func_modes’
   41 |         knob_state_map.insert(std::make_pair<T, bool>(knobs_enum, false));

```

Встретившаяся ошибка такого плана решается следующим способом(указан в первом источнике):

```cpp
knob_state_map.insert(std::make_pair<functionsT, bool>(std::move(knobs_enum), false));
```

То есть для `std::pair` вместо простого `knobs_enum` нужно `std::move(knobs_enum)`.

## Смотрите также:
- [[std move cpp]]
```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://stackoverflow.com/questions/20583531/lvalue-to-rvalue-reference-binding


---
#Входящие 

---

[^def]: термин
[^que]: вопрос