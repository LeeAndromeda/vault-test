# Кем ты должен стать

Большинство людей, выбрав новую цель, спрашивают: отлично, цель у меня есть, и что мне теперь надо сделать< чтобы её достичь? Неплохой вопрос, но не его нужно задавать в первую очередь. Спросите: кем мне нужно стать?

То, зачем вы гонитесь, от вас ускользает, это похоже на ловлю бабочек. Успех надо привлечь, тогда он сам придет к тому, кто его достоин.

>Например, когда устал от...
>![[IMG_20220110_181049.jpg]]

#НакопительныйЭффект 