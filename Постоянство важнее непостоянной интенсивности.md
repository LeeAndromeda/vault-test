# Постоянство важнее непостоянной интенсивности

Представьте, что вы и я - пилоты и нам надо перелететь из Лос-Анджелеса на Манхэттен. Самолёты одинаковые, но вы несколько раз приземлились где-то между этими двумя пунктами, пока я летел по прямой. Даже если бы вы двигались со скоростью пятьсот миль в час, а я двести миль в час, я бы всё равно опередил вас с большим отрывом.

> Время и энергия, затраченные на бесконечные остановки, раз в десять увеличивают время путешествия.

Что еще более вероятно, вы даже не долетите до цели - в какой-то момент у вас кончится горючее (энергия, мотивация, вера, желание). Намного проще и менее энергозатратные взлететь один раз и перемещается с постоянной скоростью (даже намного медленнее, чем все остальные) на протяжении всего пути.

#НакопительныйЭффект 