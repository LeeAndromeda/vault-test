Plot a complex function with zeros at ![](https://reference.wolfram.com/language/ref/Files/ComplexPlot3D.en/18.png) and poles at ![](https://reference.wolfram.com/language/ref/Files/ComplexPlot3D.en/19.png):

In[1]:=

![](https://reference.wolfram.com/language/ref/Files/ComplexPlot3D.en/I_1.png)

`ComplexPlot3D[(z^2 + 1)/(z^2 - 1), {z, -2 - 2 I, 2 + 2 I}]`

`https://wolfram.com/xid/0pus33fgmw-kiedlx`

Out[1]=

![](https://reference.wolfram.com/language/ref/Files/ComplexPlot3D.en/O_1.png)

Include a legend showing how the colors vary from ![](https://reference.wolfram.com/language/ref/Files/ComplexPlot3D.en/20.png) to ![](https://reference.wolfram.com/language/ref/Files/ComplexPlot3D.en/21.png):

In[1]:=

![](https://reference.wolfram.com/language/ref/Files/ComplexPlot3D.en/I_2.png)

`ComplexPlot3D[(z^2 + 1)/(z^2 - 1), {z, -2 - 2 I, 2 + 2 I}, PlotLegends -> Automatic]`

`https://wolfram.com/xid/0pus33fgmw-02fsmi`

Out[1]=

![](https://reference.wolfram.com/language/ref/Files/ComplexPlot3D.en/O_2.png)

Use color shading to highlight features of the function:

In[1]:=

![](https://reference.wolfram.com/language/ref/Files/ComplexPlot3D.en/I_3.png)

`ComplexPlot3D[(z^2 + 1)/(z^2 - 1), {z, -2 - 2 I, 2 + 2 I}, ColorFunction -> "CyclicLogAbsArg"]`

`https://wolfram.com/xid/0pus33fgmw-lu9tqy`

Out[1]=

![](https://reference.wolfram.com/language/ref/Files/ComplexPlot3D.en/O_3.png)