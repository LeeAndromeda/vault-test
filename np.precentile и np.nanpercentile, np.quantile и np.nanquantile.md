---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 21 # Не трогать!
Месяц: 11 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-11-21

---
# [[np.precentile и np.nanpercentile, np.quantile и np.nanquantile]]

Это функции, которые принимают массив, а так же процентиль, на котором вы хотите получить значение элемента из переданного массива, даже если такого элемента нет.

`np.percentile` принимают целые числа от 1 до 100, а `np.quantile` вещественные от 0 до 1.

## Пример

```python
import numpy as np
import matplotlib.pyplot as pl

def fonc(x):
    y = 3 * x ** 2 - 4 * x + 5
    return y

x = np.arange(-100, 100, dtype=float)
x[(x > 50) & (x < 75)] = np.nan
y = fonc(x)

yp = np.nanpercentile(y, 60)

plt.scatter(x, y, s=3, c='red', zorder=1)
plt.scatter(np.array([60]), yp, s=10, c='green')
plt.plot(x, y, zorder=-1)
x
```

![[Pasted image 20231121162300.png]]
## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Python/NumPy 

---

[^def]: термин
[^que]: вопрос