

Модуль в С++ состоит из двух файлов - Заголовочного (с расширением .h) и с файла исходного кода (с расширением .cpp)  
У обоих файлов одинаковое имя. В заголовочном файле в основном пишут объявление классов, прототипы функций ну и все такое. А в файле исходного кода - реализацию всего того, что объявили в заголовочном файле.  
Пример  
**mymodule.h** - заголовочный файл  

Код:

```cpp
#ifndef MYMODULEH
#define MYMODULEH

class A
{
public:
      A(int);
      ~A();
      int getNumber() const;
private:
      int number;
};
#endif
```

**mymodule.cpp** - файл исходного кода  

Код:

```cpp
#include "mymodule.h"

A::A(int n)
{
      number = n;
}

A::~A()
{
}

int A::getNumber() const
{
      return number;
}
```

Запись в типа
```cpp
extern int Var;
```
указывает, что в одном из файлов с исходным кодом на верхнем уровне определена глобальная переменная:
```cpp
int Var;
```
Объявленную переменную можно использовать, даже если в данном файле с исходным кодом нет ее определения. Если несколько модулей используют одни и те же константы, они обычно определяются в заголовочном файле, а объявления констант не используются.

#Программирование #сpp 