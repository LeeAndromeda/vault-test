---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 03 # Не трогать!
Месяц: 02 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-02-03

---
# [[Исключения в Python]]


```Python
raise Exception('exc')
```

`try` `except` помогает справляться с исключениями не краша программу

```Python
try:
	x = 7 / 0
except Exception as e:
	print(e)
finally:
	print("finally")
# division by zero
# finally
```


## Смотрите также:

- [[Пользовательские исключения в Python]]

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Python 

---

[^def]: термин
[^que]: вопрос