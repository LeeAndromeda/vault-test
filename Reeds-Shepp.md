


---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 09 # Не трогать!
Месяц: 02 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-02-09

---
# [[Reeds-Shepp]]


## Глоссарий

**Касп (cusp)** - точка разворота или точка на пересечении двух кривых с определенной ориентацией, где первая кривая движения имеет противоположное направление движения относительно другой кривой. (Например - реверс, движение вперед)

**Круг единого размера(a circle of unit radius)** - круг минимально возможного радиуса в данных условиях.

## Теория

### Задача

Найти **кратчайший путь** в плоскости с заданными начальной и конечной точкой и их ориентациями. Дополнительным условием является ограничение - в каждой точке радиус [[Кривизна|кривизны]] $\geq 1$.

### Все возможные варианты примитивов по базовому слову

![[Pasted image 20230220160305.png|http://lavalle.pl/planning/node822.html]]

[Источник](http://lavalle.pl/planning/node822.html) 

### Интервалы $\theta$ разделяющие плоскость примитивов на полуплоскости валидности

![[Pasted image 20230220160633.png|https://www.di.ens.fr/jean-paul.laumond/promotion/chap3.pdf]]

[Источник](http://lavalle.pl/planning/node822.html) 

### Построение доменов слов

На [[Оптимальные траектории для неголономных мобильных роботов|странице 30]] указаны правила построения доменов.

## Таблица слов и их формулировок

```leaflet
id: leaflet-idgusydfvirjhthbioetjhugioejuh352g25f4152h1
31yk5tyjliertjhopiutu3094uy90u835098i
image: Pasted image 20230227151051.png
height: 500px
lat: 50
long: 50
minZoom: 1
maxZoom: 10
defaultZoom: 8
unit: meters
scale: 1
darkMode: false
```


В этой таблице в четвертом столбце, указано, где найти формулу для этого слова(в этой же статье). Таблица находится на [[Reeds and Shepp.pdf|странице 5]]. 8 секция находится на [[Reeds and Shepp.pdf|странице 23]].

Разделим таблицу на 9 секций(уже разделены вертикальными линиями в самой таблице) и пронумеруем их.

### Получение формул для слов

Для выраженя явных формул роще всего использовать нотацию $l ^{\pm}, r ^{\pm}, s ^{\pm}$.

Авторы утверждают, что выражать формулы для всех слов не является необходимым по причине того, что существуют так называемые **элементарные трансформации**(отражения).

#### Таймфлип трансформация

![[Timeflip transform 2023-02-27 15.30.57.excalidraw]]

Эта трансформация позволяет нам получить формулу для $l ^{-} r ^{+} s ^{+} l ^{+}$ из $l ^{+} r ^{-} s ^{-} l ^{-}$. 

В используемой для явных формул нотации timeflip меняет знаки у примитивов(+ на - и наоборот). В координатах эта тансформация меняет знак у $x$ и $\phi$. То есть, если оригинальный путь идет из $(0,0,0)$ к $(x, y, \phi)$, тогда трансформированный путь идет из $(0,0,0)$ к $(-x, y, -\phi)$.

Например, если у нас есть формулы, чтобы найти длины дуг $t, u, v$ пути формы $l ^{+} _{t} r ^{-} _{\pi/2} s ^{-} _{u}, l ^{-} _{v}$ из $(0,0,0)$ к $(-x, y, -\phi)$, то мы можем использовать их, чтобы найти длины дуг для пути формы $l ^{-} r ^{+} s ^{+} l ^{+}$ к $(x, y, \phi)$.

Авторы так же утверждают, что таймфлиппинг позволяет избавиться от всех формул, начинающихся со знака "-".

#### Отражение

![[Reflecting 2023-02-27 16.18.17.excalidraw]]

Эта трансформация сохраняет знаки у примитивов, но меняет сами буквы. Таким образом $l$ превращается в $r$ и наоборот. $s$ остается.

Т.е. мы можем получить формулы для $r ^{+} l ^{-} s ^{-} r ^{-}$ из формул для $l ^{+} r ^{-} s ^{-} l ^{-}$. Отражение меняет $(x, y, \phi)$ на $(x, -y, -\phi)$

Авторы утверждают, что отражение помогло им избавиться от всех формул, кроме тех, которые начинаются на ${l ^{+}}$.

#### Реверсивная трансформация

![[Backwards 2023-02-27 16.32.41.excalidraw]]

Реверсивная трансформация подразумевает выполнение тех же примитивов, что и в изначальной формуле, но в обратном порядке. Она позволяет для формул типа $l ^{-} s ^{-} r ^{-}l ^{+}$ получить формулы из формул типа $l ^{+} r ^{-} s ^{-} l ^{-}$. Так конечная точка из $(x, y, \phi)$ сдвигается в $(x \cos \phi + y \sin \phi,\ x \sin \phi - y \cos \phi,\ \phi)$. 

Данная трансформация позволяет избавиться еще от нескольких слов. Дальше рассматривается получение формул для оставшихся 9.

### Определение параметров оставшихся формул

1. В каждой формуле объект будет двигаться из $(0,0,0)$ в $(x,y,\phi)$. 
2. Запись $(r, \theta) = R(x,y)$ означает полярную трансформацию $r \cos \theta = x, r \sin \theta = y, r \geq 0, - \pi \leq \theta < \pi$. 
3. Запись $\phi = M(\theta)$ означает $\phi \equiv \theta \mod 2 \pi$ и $- \pi \leq \phi < \pi$.
4. <mark style="background: #FF5582A6;">Не понимаю, о чем речь.</mark> Quantities $T$ are discarded in the formulas below and may not be equal at different usages.
5. В каждом случае, когда есть путь-решение, мы получаем на выходе неизвестные сегменты длины $t, u, v$ и общую длину $L$. Если $L = \infty$, решения нет.

> [!info]- Коротко
> 
> ---
> $(r, \theta) = R(x,y)$
> -->
> 
> $r \cos \theta = x$,
> $r \sin \theta = y$,
> $r \geq 0$,
> $- \pi \leq \theta < \pi$
> ---
> $\phi = M(\theta)$ 
> --> 
> 
> $\phi \equiv \theta \mod 2 \pi$ , 
> $- \pi \leq \phi < \pi$
> ---

#### 8.1 $l ^{+} _{t} s ^{+} _{u} l ^{+} _{v}$

Определим
$$(u,t) = R(x- \sin \phi, y - 1 + \cos \phi)$$
и
 $$v = M(\phi - t), \text{   } L = |t|+|u|+|v|$$
Путь не является оптимальным, если:
 $$t \notin [0, \pi]\ ||\ v \notin [0,\pi]$$
##### Разбор

 $$(u,t) = R(x- \sin \phi, y - 1 + \cos \phi)$$
  $$\equiv$$
   $$
\begin{matrix}
	 u \cos t = x - \sin \phi \\ 
	 u \sin t = y - 1 - \cos \phi \\
	 u \geq 0 \\
	 - \pi \leq t < \pi
 \end{matrix} 
 \text{[1]}
 $$

 $$v = M(\phi - t)$$
  $$\equiv$$
 $$v = (\phi - t) \mod 2 \pi, \text{[2]}$$
Из уравнений 1 и 2 можно получить все необходимые переменные: $(t, u, v)$

#### 8.2 $l ^{+} _{t} s ^{+} _{u} r ^{+} _{v}$

Определим
 $$
 (u _{1}, t) 
 = 
 R(
	 x + \sin \phi, 
	 y - 1 - \cos \phi
 )
 $$

Если $u ^{2} _{1} < 4: L = \infty$;
Иначе: 
$u = \sqrt{u ^{2} _{1} - 4},$
$(T, \theta) = R(u,2),$
$t=M(t _{1} + \theta),$
$v = M(t - \phi),$
$L = |t|+|u|+|v|$

##### Разбор

 $$
 \begin{matrix}
	 u _{1} \cos t = x+ \sin \phi \\ 
	 u _{1} \sin t = y - 1 - \cos \phi \\
	 u _{1} \geq 0 \\
	 - \pi \leq t < \pi
 \end{matrix} 
 \text{[3]}
 $$

# Продолжить здесь

Следующий вопрос - [[Разработка трехмерного алгоритма связного движения на основе кривых Reeds-Shepp и Dubins#Вопросы|Шестой вопрос]]

## Смотрите также:

- [[Оператор modulo математика]]
- [[Reeds and Shepp curves PDF]]
- [[Разработка планировщика трехмерной траектории на основе кривых Reeds-Shepp]]

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://gieseanw.wordpress.com/2012/11/15/reeds-shepp-cars/
> 2. http://lavalle.pl/planning/node822.html
> 3. https://github.com/gkouros/reeds-shepp-paths-ros


---
#Робототехника/Планировщик

---

[^def]: термин
[^que]: вопрос