---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 27 # Не трогать!
Месяц: 11 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-11-27

---
# [[Тотальный разбор launch файлов в ROS2]]

## Создание launch файла

Лаунч файлы лежат в отдельных пакетах, которые созданы командой `ros2 pkg create --build-type ament_python <package_name>`. По стандарту, они пишутся на Python, хранятся в папке `launch` пакета и имеют имя по шаблону `*.launch.py`.

Для того, чтобы лаунч файлы работали, нужно добавить следующую строку в `setup.py` пакета:

**Шаблон `setup.py` для хранение лаунч файлов:**
```python
from setuptools import find_packages, setup
from glob import glob
import os

package_name = 'turn_on_launches'

setup(
    name=package_name,
    version='0.0.0',
    packages=find_packages(exclude=['test']),
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        (os.path.join('share', package_name), glob('launch/*.launch.py')), # Эта строка отвечает за добавление всех лаунч файлов, соответствующих шаблону имени
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='leev',
    maintainer_email='yyootttaa@gmail.com',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
        ],
    },
)
```

После настройки `setup.py`, необходимо сбилдить воркспэйс и их можно будет запускать.

## Создание launch файла

Для создания базовой структуры launch файла вам необходимо импортировать класс `LaunchDescription` (далее может быть назван "описание лаунча"):

```python
from launch import LaunchDescription
```

Лаунч файл создает объект этого класса, задавая все параметры в нем. Все это происходит в функции `generate_launch_description`, которую нам необходимо определить в нашем launch файле. Именно в ней реализуется вся необходимая логика. Можно сказать, что `generate_launch_description` является main функцией launch файла.

Потому определим ее:

```python
from launch import LaunchDescription

def generate_launch_description():
    ld = LaunchDescription()
    return ld
```

### Добавление нода 

Для добавления нода в описание лаунча, используется модуль `launch_ros.actions`. Из него мы используем объект `Node`. `actions` - это тип объектов, которые мы передаем в `LaunchDescription`. 

> [!hint]- Содерание launch_ros.actions
> ![[Pasted image 20231127170542.png]]


Конструктор объекта `Node` имеет следующие аргументы:
- `package` - имя пакета, из которого мы будем добавлять нод в описание лаунча
- `executable` - имя исполняемого файла, который мы будем брать из пакета `package`. Имя должно совпадать с тем, как вы назвали нод, при внесении его в `CMakeLists.txt` или `setup.py`
- `name` - имя, с которым будет инициализирован указанный нод. Оно является идентификатором нода в ROS2, и должно быть уникальным для ***каждого*** нода, являющегося запущенным в одной сети
- `namespace` - необязательный префикс, с помощью которого можно обеспечить уникальность имен запускаемых нодов или определить их принадлежность к определенной технической системе. Например, если в системе два одинаковых робота, то регулятор у них будет запускаться одного и того же типа, а отличаться будет `namespace`. Пускай каждый робот это turtlebot 3, тогда `namespace` на первом роботе может быть `turtle1`, а на втором `turtle2`. Тогда запущенные лаунчем ноды будут иметь имя `turtle1/{name}` и `turtle2/{name}`, где вместо `{name}` будет указанное значение имени нода.
- `parameters` - это параметры, которые будут передаваться в нод. Их мы рассмотрим в одном из следующих разделов
- `remappings` - это изменение имен топиков. Их мы тоже рассмотрим в одном из следующих разделов
- `ros_arguments` - это "консольные" аргументы, применяющиеся в ROS2. Они хорошо описаны в [этом туториале ROS2](https://docs.ros.org/en/foxy/How-To-Guides/Node-arguments.html). При помощи этого аргумента, вы можете передать консольные аргументы из лаунч файла
- `arguments` - другие аргументы нода
- `exec_name` - имя, которым будет назван процесс нода в системе. По дефолту - полное имя нода

Обязательно нужно заполнить аргументы `package`, `executable`, `name`. 

Этот объект с заполненными аргументами нужно передать в функцию `LaunchDescription.add_action()`, относительно объекта, который вы будете возвращать.

Заполним для примера "игрушечными" данными:

```python
from launch import LaunchDescription
import launch_ros.actions as actions

def generate_launch_description():
    ld = LaunchDescription()
    
    ld.add_action(actions.Node(
        package="dummy_pkg",
        executable="dummy_node",
        name="dummy_node"
    ))
    
    ld.add_action(actions.Node(
        package="dummy_pkg",
        executable="dummy_another_node",
        name="dummy_node2"
    ))
    
    return ld
```

### Добавление другого лаунч файла

Запуск лаунч файлов из других лаунч файлов может быть полезен, когда вы объеденияете запуск нескольких технических систем или ваша техническая система имеет громоздкий объем программ,необходимых для ее работы. 

Для того, чтобы запустить лаунч файл, вам сначала нужно его найти. Для этого будет использоваться функция `ament_index_python.packages.get_package_share_directory()`. Ее нужно импорировать следующей строчкой:

```python
from ament_index_python.packages import get_package_share_directory
```

Для работы с путями к файлам в Python используется модуль `os`. Его тоже нужно импортировать:
```python
import os
```

Для создания `action` лаунча, вам понадобятся следующие классы - `launch.actions.IncludeLaunchDescription` и `launch.launch_description_sources.PythonLaunchDescriptionSource`. Импортируем их следующими командами:

```python
from launch.actions import IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource
```

Теперь можно создать `action` импортируемого лаунча. Этот объект нам вернет конструктор класса `IncludeLaunchDescription`, начнем с него:

```python
launch_action = IncludeLaunchDescription()
```

В `IncludeLaunchDescription` нужно аргументом поместить объект источника лаунч файла. Это мы сделаем при помощи объекта `PythonLaunchDescriptionSource`:

```python
launch_action = IncludeLaunchDescription(
	PythonLaunchDescriptionSource()
)
```

В конструктор `PythonLaunchDescriptionSource` необходимо поместить список, где первый элемент будет путем к директории `share/launch` пакета, в котором находится искомый лаунч, второй аргумент - имя лаунча. Получившийся `action` можно добавить в `LaunchDescription`. Заполним для примера:

```python
launch_action = IncludeLaunchDescription(
	PythonLaunchDescriptionSource([os.path.join(
         get_package_share_directory('dummy_pkg'), 'launch'),
         '/dummy_iported.launch.py'])
)

ld.add_action(launch_action)
```

По итогу получаем такой лаунч файл:

```python
import os

from ament_index_python.packages import get_package_share_directory

import launch_ros.actions as actions

from launch import LaunchDescription
from launch.actions import IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource


def generate_launch_description():
    ld = LaunchDescription()
    
    
    ld.add_action(actions.Node(
        package="dummy_pkg",
        executable="dummy_node",
        name="dummy_node"
    ))
    
    ld.add_action(actions.Node(
        package="dummy_pkg",
        executable="dummy_another_node",
        name="dummy_node2"
    ))
    
    launch_action = IncludeLaunchDescription(
	PythonLaunchDescriptionSource([os.path.join(
         get_package_share_directory('dummy_pkg'), 'launch'),
         '/dummy_iported.launch.py']))
    
    ld.add_action(launch_action)
    
    return ld

```

## Использование дополнительных возможностей

### Параметры нодов

Параметры нодов - переменные, значение которых вы задаете на старте лаунча каждому ноду. Они задаются в форме массива словарей, где ключ будет строкой - именем параметра, а значение будет значением параметра. Для того чтобы нод получил передаваемые параметры, в него нужно будет внести некоторые изменения. Начнем с изменений в лаунч-файл.

Заполним "игрушечные" параметры для `dummy_node` в нашем лаунч файле:

```python
    ld.add_action(actions.Node(
        package="dummy_pkg",
        executable="dummy_node",
        name="dummy_node",
        parameters=[
            {"dummy_id": 0},
            {"dummy_name": 'abc'},
            {"dummy_coefficient": 3.14159}
        ]
    ))
```

Мы заполнили аргумент `parameters`, списком(массивом), хранящим три словаря, каждому из них задали ключ и значение, таким образом передали параметры ноду. 

Стоит отметить, что значение параметра может быть так же задано переменной, что позволяет передавать параметры в зависимости от каких-то внешних или внутренних условий лаунча:

```python
    pi = 3.14159
    
    ld.add_action(actions.Node(
        package="dummy_pkg",
        executable="dummy_node",
        name="dummy_node",
        parameters=[
            {"dummy_id": 0},
            {"dummy_name": 'abc'},
            {"dummy_coefficient": pi} # Тут теперь переменная. Во время исполнения лаунча она заменится на ее значение
        ]
    ))
```

> [!attention] Внимание!
> Несмотря на то, что параметры, передаваемые разным по типу нодам, можно назвать одинаково, лучше, чтобы каждый параметр имел уникальное имя. Это может быть важно в будущем, если вы захотите запускать текущий лаунч файл в другом лаунче и перезаписывать значения параметров.


#### В ноде

##### С++

Чтобы получить заданное значение, вам нужен класс, который наследует от или явялется классом `Node`. Данный пример будет приведен на языке C++. Пусть вашим нодом будет объект `node`:

```cpp
// Создаем переменные для хранения дефолтных значений параметров, на случай если нод будет запускаться не в лаунче
std::string default_anpa_topic = "/anpa_";
std::string default_regulator_topic = "/regulator";
std::string default_bottom_sonar = "/sonar/bottom";

// Теперь создаем переменные, которые будут хранить значение наших параметров
std::string anpa_topic = default_anpa_topic;
std::string regulator_topic = default_regulator_topic;
std::string bottom_sonar = default_bottom_sonar;

// Объявляем параметры
node.declare_parameter("anpa_topic", default_anpa_topic);
node.declare_parameter("regulator_topic", default_regulator_topic);
node.declare_parameter("bottom_sonar", default_bottom_sonar);

// Затем получаем параметры. Первый аргуемнт - название параметра,
// второй аргумент - переменная, в которую будем получать значение,
// и третий аргумент - переменная-значение по-умолчанию
node.get_parameter_or("anpa_topic", anpa_topic, default_anpa_topic);
node.get_parameter_or("regulator_topic", regulator_topic, default_regulator_topic);
node.get_parameter_or("bottom_sonar", bottom_sonar, default_bottom_sonar);
```

После выполнения функции `get_parameter_or` в ваших переменных для хранения значений параметров будут либо переданные значения в случае успешного  получения параметров из лаунча, либо дефолтные значения, если ничего получено не было.

##### Python

Для начала нужно определить саму функцию `try_get()`:

```python
def try_get(fn, default):
    try:
        val = fn().value
        if val is None:
            return default
        else:
            return val
    except:
        return default
```

Теперь нужно внести небольшие изменения в [[Простой паблишер-сабскрайбер ROS2(Python)|шаблон класса]]. Было:

```python
    def __init__(self):
        super().__init__('node_name')
```

Стало:

```python
	def __init__(self):
		super().__init__('node_name', allow_undeclared_parameters=True, automatically_declare_parameters_from_overrides=True)

		self.your_parameter_var = try_get(lambda: self.get_parameter("parameter_name"), "default_if_value_not_set")
```

Тут мы в функцию `try_get` первым аргументом передали функцию, которая пытается получить значение параметра, а вторым - дефолтное значение, если получить значение параметра извне не удалось

### Загрузка параметров из Yaml конфигов

**`yaml`** - формат хранения параметров, как `json`. Довольно удобный и распространенный. Помимо роса его, например, использует Obsidian для хранения метаданных конспектов. Если вам интересно больше про него прочитать - https://blog.skillfactory.ru/glossary/yaml/.

Здесь в лаунче конфиг передается в параметры. Для начала нам необходимо получить путь к конфигу, при помощи функции `get_package_share_directory`, импортируем ее:

```python
from ament_index_python.packages import get_package_share_directory
```

Так как в нашем примере она уже импортирована, в нем второй раз мы добавлять не будем.

Теперь получим путь к конфигу. По стандарту, `yaml` конфиги лежат в папке `config` пакета.

```python
   config = os.path.join(
      get_package_share_directory('dummy_pkg'),
      'config',
      'dummy_params.yaml'
      )
```

Теперь передадим параметры в нод:

```python
    ld.add_action(actions.Node(
        package="dummy_pkg",
        executable="dummy_node",
        name="dummy_node",
        parameters=[config] # Просто кладем сюда полученную переменную с путем к конфигу
    ))
```

А теперь посмотрим, как должен выглядеть сам файл параметров для нода:

```yaml
/turtlesim2/sim: # Эта строчка отвечает за то, чтобы параметры попали только в нод с указанным пространством имен и именем (в данном случае turtlesim2 - пространство имен, а sim - имя)
   ros__parameters: # Это обязательная строчка, которая помогает росу определить, что все что принадлежит к ней (по уровню табуляции) - это параметры, передаваемые ноду
      background_b: 255 # А вот так задаются параметры
      background_g: 86 
      background_r: 150
```

### Yaml конфиг для нескольких нодов (wildcards)

Договоримся, что **суммарное имя нода** - его пространство имен + заданное имя. Пример: `/turtlesim2/sim`.

Прошлый вариант `yaml` конфига может быть использован только в ноде, суммарное имя которого совпадает с заданным в `yaml` конфиге. А что, если мы хотим передавать параметры из конфига каждому ноду `executable` из пакета `package`? Для этого используется `wildcard`-синтаксис. В конфиге вместо указания суммарного имени нода мы пишем следующую кострукцию - `/**`. Тут все элементарно - `/` это просто символ, а `**` означает "любое значение". То есть, когда рос будет сравнивать суммарное имя запускаемых нодов с этой конструкцией в конфиге, каждый нод, которому передан этот конфиг, рос воспримет как подходящий и передаст ему значения.

Пример конфига:

```yaml
/**:
   ros__parameters:
      background_b: 255
      background_g: 86
      background_r: 150
```

### Перезапись параметров в запускаемых лаунч файлах

Договоримся, что лаунч файл, который вы запускаете в лаунч файле, назовем **вложенный лаунч файл**.

Если в своем лаунч файле вы запускаете вложенный лаунч файл, то вполне возможно, что вы захотите поменять значения параметров, передаваемых его нодам. Для этого в конструктор класса `IncludeLaunchDescription` в аргумент `launch_arguments` аргументом передать новые параметры.

```python
    pi = 3.14159
    
    launch_args = {"dummy_id": 0}.items()
    
    launch_action = IncludeLaunchDescription(
	PythonLaunchDescriptionSource([os.path.join(
        get_package_share_directory('dummy_pkg'), 'launch'),
        '/dummy_iported.launch.py']),
        launch_arguments=launch_args
    )
```

При этом в вашем вложенном лаунч файле данные параметры должны быть объявлены аргументами лаунча. Для этого нам понадобится импортировать два дополнительных объекта `launch.actions.DeclareLaunchArgument` и `launch.substitutions.LaunchConfiguration`:

```python
from launch.actions import DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration
```

`DeclareLaunchArgument` позволит нам объявить аргумент лаунча, а `LaunchConfiguration` получить его значение:

```python
ld.add_action(
	DeclareLaunchArgument(
		name='arg_name',
		default_value='some_default_val',
		description='Описание аргумента'
	)
)

Node(
 package='dummy_pkg',
 executable='dummy_node',
 name='dummy_node',
 parameters=[
	{'dummy_parameter': LaunchConfiguration('arg_name')}
 ]
),
```

### Пространства имен (namespaces)

Пространство имен - это префикс имени нода, который позволяет запускать ноды одного типа для разных роботов(технических систем и т.д.). Однако, если нодов много, то для каждого из них вручную задавать пространство имен неудобно. Для этого лучше использовать `launch_ros.actions.PushRosNamespace`.  Он позволит задать пространство имен для всех нодов, запускаемых в лаунче. 

Так же нам понадобится `launch.actions.GroupAction`, группирующие разные `action` в один. Импортируем их:

```python
from launch.actions import GroupAction
from launch_ros.actions import PushRosNamespace
```

А теперь можно задать просранство имен:

```python
    launch_action = IncludeLaunchDescription(
	PythonLaunchDescriptionSource([os.path.join(
        get_package_share_directory('dummy_pkg'), 'launch'),
        '/dummy_iported.launch.py'])
    )
    
    launch_with_namespace = GroupAction(
        actions=[
            PushRosNamespace('paste_your_namespace_here'),
            launch_action
        ]
    )
    
    ld.add_action(launch_with_namespace)
```

Таким образом, все ноды, запускаемые лаунчем `dummy_iported.launch.py` будут иметь суммарное имя следующего вида `/paste_your_namespace_here/{name}`.

### Переименование топиков (remapping)

Для переименования топиков нода, будет использоваться аргумент `remappings` из `launch_ros.actions.Node`. Туда мы будем передавать список(массив) кортежей с двумя элементами. Первый - новое имя топика, второй - старое имя топика. **Будьте внимательны!** Имена топиков должны быть абсолютными(полными) и включать в себе изначальный `/`. Например: 

```python
    ld.add_action(actions.Node(
        package="dummy_pkg",
        executable="dummy_node",
        name="dummy_node",
        parameters=[config],
        remappings=[
            ('/new_topic', '/dummy/old_topic'),
            ('/new_topic2', '/topic420')
        ]
    ))
```

### Лаунч файлы с rviz

Для этого нам необходимо будет в нашем лаунче запускать нод `rviz2`, передавая ему конфиг, который он будет использовать. Тут используются уже известные нам функции и понятия, поэтому просто приведу пример кода:

```python
import os

from ament_index_python.packages import get_package_share_directory

from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():
   rviz_config = os.path.join(
      get_package_share_directory('turtle_tf2_py'),
      'rviz',
      'turtle_rviz.rviz'
      )

   return LaunchDescription([
      Node(
         package='rviz2',
         executable='rviz2',
         name='rviz2',
         arguments=['-d', rviz_config]
      )
   ])
```

### Использование переменных среды

Здесь тоже по большей части используются известные нам понятия, поэтому сначала приведу пример, а затем мы его рассмотрим.

```python
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.substitutions import EnvironmentVariable, LaunchConfiguration
from launch_ros.actions import Node


def generate_launch_description():
   return LaunchDescription([
      DeclareLaunchArgument(
            'node_prefix',
            default_value=[EnvironmentVariable('USER'), '_'],
            description='prefix for node name'
      ),
      Node(
            package='turtle_tf2_py',
            executable='fixed_frame_tf2_broadcaster',
            name=[LaunchConfiguration('node_prefix'), 'fixed_broadcaster'],
      ),
   ])
```

Из нового здесь функция `EnvironmentVariable`, которая в аргумент принимает строку, совпадающую с именем переменной среды, значение которой мы пытаемся получить. В случае, если получить значение не удалось, она возвращает пустую строку.

# Краткая справка

## `LaunchDescription` - возвращаемый лаунч объект

### Импорт
```python
from launch import LaunchDescription
```

### Использование

```python
from launch import LaunchDescription

def generate_launch_description():
    ld = LaunchDescription()
    return ld
```

## `launch_ros.actions.Node` - добавление нода

### Импорт

```python
import launch_ros.actions import Node
```

### Использование 

```python
    ld = LaunchDescription()
    
    ld.add_action(actions.Node(
        package="dummy_pkg",
        executable="dummy_node",
        name="dummy_node"
    ))
```

## Добавление лаунч файла

### Импорты

```python
from ament_index_python.packages import get_package_share_directory
import os
from launch.actions import IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource
```

### Использование

```python
launch_action = IncludeLaunchDescription(
	PythonLaunchDescriptionSource([os.path.join(
         get_package_share_directory('dummy_pkg'), 'launch'),
         '/dummy_iported.launch.py'])
)

ld.add_action(launch_action)
```

## Параметры нодов

В лаунче:

```python
    ld.add_action(actions.Node(
        package="dummy_pkg",
        executable="dummy_node",
        name="dummy_node",
        parameters=[
            {"dummy_id": 0},
            {"dummy_name": 'abc'},
            {"dummy_coefficient": 3.14159}
        ]
    ))
```

В ноде C++ -> [[#С++]]
В ноде Python -> [[#Python]]

## `yaml` параметры

### Импорты

```python
from ament_index_python.packages import get_package_share_directory
```

### Использование

```python
   config = os.path.join(
      get_package_share_directory('dummy_pkg'),
      'config',
      'dummy_params.yaml'
      )

    ld.add_action(actions.Node(
        package="dummy_pkg",
        executable="dummy_node",
        name="dummy_node",
        parameters=[config] # Просто кладем сюда полученную переменную с путем к конфигу
    ))
```

### Формат

```yaml
/turtlesim2/sim: # Эта строчка отвечает за то, чтобы параметры попали только в нод с указанным пространством имен и именем (в данном случае turtlesim2 - пространство имен, а sim - имя)
   ros__parameters: # Это обязательная строчка, которая помогает росу определить, что все что принадлежит к ней (по уровню табуляции) - это параметры, передаваемые ноду
      background_b: 255 # А вот так задаются параметры
      background_g: 86 
      background_r: 150
```

## `yaml` конфиги для нескольких нодов (wildcards)

```yaml
/**:
   ros__parameters:
      background_b: 255
      background_g: 86
      background_r: 150
```

## Перезапись параметров в запускаемых лаунч файлах

### В основном лаунче

```python
    pi = 3.14159
    
    launch_args = [
        {"dummy_id": 0},
        {"dummy_name": 'abc'},
        {"dummy_coefficient": pi}
    ]
    
    launch_action = IncludeLaunchDescription(
	PythonLaunchDescriptionSource([os.path.join(
        get_package_share_directory('dummy_pkg'), 'launch'),
        '/dummy_iported.launch.py']),
        launch_arguments=launch_args
    )
```

### Во вложенном

#### Импорты

```python
from launch.actions import DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration
```

#### Использование

```python
ld.add_action(
	DeclareLaunchArgument(
		name='arg_name',
		default_value='some_default_val',
		description='Описание аргумента'
	)
)

Node(
 package='dummy_pkg',
 executable='dummy_node',
 name='dummy_node',
 parameters=[
	{'dummy_parameter': LaunchConfiguration('arg_name')}
 ]
),
```

## Пространства имен (namespaces)

### Импорты

```python
from launch.actions import GroupAction
from launch_ros.actions import PushRosNamespace
```

###  Использование

```python
    launch_action = IncludeLaunchDescription(
	PythonLaunchDescriptionSource([os.path.join(
        get_package_share_directory('dummy_pkg'), 'launch'),
        '/dummy_iported.launch.py'])
    )
    
    launch_with_namespace = GroupAction(
        actions=[
            PushRosNamespace('paste_your_namespace_here'),
            launch_action
        ]
    )
    
    ld.add_action(launch_with_namespace)
```

## Переименование топиков (remapping)

```python
    ld.add_action(actions.Node(
        package="dummy_pkg",
        executable="dummy_node",
        name="dummy_node",
        parameters=[config],
        remappings=[
            ('/new_topic', '/dummy/old_topic'),
            ('/new_topic2', '/topic420')
        ]
    ))
```

## Лаунч файлы с rviz

```python 
import os

from ament_index_python.packages import get_package_share_directory

from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():
   rviz_config = os.path.join(
      get_package_share_directory('turtle_tf2_py'),
      'rviz',
      'turtle_rviz.rviz'
      )

   return LaunchDescription([
      Node(
         package='rviz2',
         executable='rviz2',
         name='rviz2',
         arguments=['-d', rviz_config]
      )
   ])
```

## Использование переменных среды


```python
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.substitutions import EnvironmentVariable, LaunchConfiguration
from launch_ros.actions import Node


def generate_launch_description():
   return LaunchDescription([
      DeclareLaunchArgument(
            'node_prefix',
            default_value=[EnvironmentVariable('USER'), '_'],
            description='prefix for node name'
      ),
      Node(
            package='turtle_tf2_py',
            executable='fixed_frame_tf2_broadcaster',
            name=[LaunchConfiguration('node_prefix'), 'fixed_broadcaster'],
      ),
   ])
```


# Коневртация xml лаунчей ROS в python лаунчи ROS2

- `arg` -> `DeclareLaunchArgument` -> [[#Краткая справка#Перезапись параметров в запускаемых лаунч файлах]]
- `param` -> `DeclareLaunchArgument` ->[[#Краткая справка#Перезапись параметров в запускаемых лаунч файлах]]
- `group` -> `GroupAction` -> [[#Краткая справка#Пространства имен (namespaces)]]
- `include file=` -> `IncludeLaunchDescription` -> [[#Краткая справка#Добавление лаунч файла]]
	- `arg` -> `DeclareLaunchArgument` -> [[#Краткая справка#Перезапись параметров в запускаемых лаунч файлах]]
- `node` -> `launch_ros.actions.Node` -> [[#`launch_ros.actions.Node` - добавление нода]]
	- `remap` -> `launch_ros.actions.Node(remappings=)` -> [[#Краткая справка#Переименование топиков (remapping)]]
- `rosparam file` -> [[#Краткая справка#`yaml` параметры]]

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#ROS2 

---

[^def]: термин
[^que]: вопрос