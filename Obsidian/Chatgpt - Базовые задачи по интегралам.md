---
Тип: Задача
Описание: -
Исполнители: 
Проект: ВнедрениеЗнаний

Дисциплина: #Входящие 
Tags: Задача
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: 15 # Не трогать!
Месяц: 02 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-02-15

дата_завершения: "2023-нет-нет"

Выполнено: нет
Результат: 
---

```dataview
TABLE день_завершения as "День истечения", месяц_завершения as "Месяц истечения", Проект, Выполнено, Результат, Количество_выходящих_задач as "Количество выходящих задач"
WHERE Дата_создания = this.Дата_создания
limit 1
```
# [[Chatgpt - Базовые задачи по интегралам]]

> [!example]- Чеклист знания
> - Получение источников и ссылок
> 	- [ ] Получение одной ссылки
> 	- [ ] Получение ссылок на разные источники
> - Конспектирование
> 	- [ ] Достаточное для использования конспектирование
> 	- [ ] Полное конспектирование
> - Создание справок
> 	- [ ] Справки практического применения
> 	- [ ] Список терминов
> 	- [ ] Определение терминов из списка терминов
> 	- [ ] Список связанных тем(если нет внутри текста)
> - Внедрение в базу знаний
> 	- [ ] Создание связей с оригинальной страницей (этой)
> 	- [ ] Разбиение на подтемы
> 	- [ ] Атомизация - (по необходимости) разбиение конспекта по подтемам на страницы
> - [ ] Внедрение в практику
> 

**Содержание потом вырезается, все содержимое конспекта удаляется и содержание вставляется по шаблону входящего конспекта.**

# Содержание 


1.  $\displaystyle \int x, dx = \frac{x^2}{2} + C$
    
2.  $\displaystyle \int 2x^3, dx = \frac{1}{2}x^4 + C$
    
3.  $\displaystyle \int \frac{1}{x^2}, dx = -\frac{1}{x} + C$
    
4.  $\displaystyle \int \sin x, dx = -\cos x + C$
    
5.  $\displaystyle \int e^x, dx = e^x + C$
---
---
7.  $\displaystyle \int 3x^2, dx = x^3 + C$
    
7.  $\displaystyle \int \frac{1}{x}, dx = \ln|x| + C$
    
8.  $\displaystyle \int \cos x, dx = \sin x + C$
    
9.  $\displaystyle \int 5, dx = 5x + C$
    
10.  $\displaystyle \int \frac{1}{\sqrt{x}}, dx = 2\sqrt{x} + C$
    
11.  $\displaystyle \int \sec^2 x, dx = \tan x + C$
    
12.  $\displaystyle \int 4e^{3x}, dx = \frac{4}{3}e^{3x} + C$
    
13.  $\displaystyle \int \frac{1}{1+x^2}, dx = \arctan x + C$
    
14.  $\displaystyle \int 2\sin(3x), dx = -\frac{2}{3}\cos(3x) + C$
    
15.  $\displaystyle \int \frac{1}{(2x+1)^2}, dx = -\frac{1}{2(2x+1)} + C$
    
16.  $\displaystyle \int \sqrt{x}, dx = \frac{2}{3}x^{\frac{3}{2}} + C$
    
17.  $\displaystyle \int 2\cos(2x), dx = \sin(2x) + C$
    
18.  $\displaystyle \int \frac{1}{\sqrt{1-x^2}}, dx = \arcsin x + C$
    
19.  $\displaystyle \int e^x\sin x, dx = \frac{1}{2}(e^x\sin x - e^x\cos x) + C$
    
20.  $\displaystyle \int \frac{1}{2x+3}, dx = \frac{1}{2}\ln|2x+3| + C$

---
---

Certainly! The basic rules of integration involve applying certain operations to functions in order to find their antiderivatives. Here are some of the most important rules:

1.  The power rule: If $n\neq -1$, then $\displaystyle \int x^n, dx = \frac{1}{n+1}x^{n+1} + C$. This rule allows us to find the antiderivatives of polynomials of the form $x^n$.
    
2.  The constant multiple rule: $\displaystyle \int cf(x), dx = c\int f(x), dx$. This rule allows us to pull constants out of integrals.
    
3.  The sum and difference rule: $\displaystyle \int (f(x) + g(x)), dx = \int f(x), dx + \int g(x), dx$ and $\displaystyle \int (f(x) - g(x)), dx = \int f(x), dx - \int g(x), dx$. This rule allows us to split integrals of sums or differences into individual integrals.
    
4.  The substitution rule: If $u = g(x)$ and $f$ is a function such that $f(g(x))$ is continuous, then $\displaystyle \int f(g(x))g'(x), dx = \int f(u), du$. This rule allows us to change the variable of integration.
    
5.  The integration by parts rule: $\displaystyle \int u, dv = uv - \int v, du$. This rule allows us to integrate products of functions.
    

Some of the typical functions and their antiderivatives include:

1.  Polynomials: $\displaystyle \int x^n, dx = \frac{1}{n+1}x^{n+1} + C$, where $n$ is a non-negative integer.
    
2.  Exponential functions: $\displaystyle \int e^x, dx = e^x + C$.
    
3.  Trigonometric functions: $\displaystyle \int \sin x, dx = -\cos x + C$, $\displaystyle \int \cos x, dx = \sin x + C$, $\displaystyle \int \tan x, dx = -\ln|\cos x| + C$, and $\displaystyle \int \sec x, dx = \ln|\sec x + \tan x| + C$.
    
4.  Inverse trigonometric functions: $\displaystyle \int \frac{1}{\sqrt{1-x^2}}, dx = \arcsin x + C$ and $\displaystyle \int \frac{1}{1+x^2}, dx = \arctan x + C$.
    
5.  Rational functions: $\displaystyle \int \frac{1}{x}, dx = \ln|x| + C$, $\displaystyle \int \frac{1}{x^2+a^2}, dx = \frac{1}{a}\arctan\left(\frac{x}{a}\right) + C$, and $\displaystyle \int \frac{1}{(x-a)^n}, dx = -\frac{1}{(n-1)(x-a)^{n-1}} + C$, where $n$ is a positive integer and $a$ is a constant.
    

These are just a few examples of functions and their antiderivatives. There are many other functions and rules of integration that can be used to find antiderivatives, but these are a good starting point for basic integrals.


> [!question]- Соурслист
> 1. 
---

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

---
#ВнедрениеЗнаний 
#Математика/Интегралы
#chatgpt

---

[^def]: термин
[^que]: вопрос