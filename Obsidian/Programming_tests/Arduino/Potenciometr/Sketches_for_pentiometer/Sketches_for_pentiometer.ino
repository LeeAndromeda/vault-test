// // Бегущий огонек на пяти диодах и потенциометре
// int PIN_POT = A0;
// int led[] = {3, 4, 5, 6, 7};

// void setup()
// {
//   pinMode(PIN_POT, INPUT);
//   pinMode(led[0], OUTPUT);
//   pinMode(led[1], OUTPUT);
//   pinMode(led[2], OUTPUT);
//   pinMode(led[3], OUTPUT);
//   pinMode(led[4], OUTPUT);
//   Serial.begin(9600);
// }

// void loop ()
// {
//   int input = analogRead(PIN_POT);
//   Serial.println(input);
//   for (int i = 0; i < 5; i++)
//   {
//     digitalWrite(led[i], HIGH);
//     delay(input);
//     digitalWrite(led[i], LOW);
//     // delay(input);
//   }
// }

// Индикатор (загрузка) диоды и потенциометр
int PIN_POT = A0;
int led[] = {3, 4, 5, 6, 7};

void setup()
{
  pinMode(PIN_POT, INPUT);
  pinMode(led[0], OUTPUT);
  pinMode(led[1], OUTPUT);
  pinMode(led[2], OUTPUT);
  pinMode(led[3], OUTPUT);
  pinMode(led[4], OUTPUT);
  Serial.begin(9600);
}

void loop ()
{
  int input = analogRead(PIN_POT);
  Serial.println(input);

  input = map(input, 0, 1023, -1, 4);
  for (int i = 0; i <= input; i++)
  {
    digitalWrite(led[i], HIGH);
  }
  delay(100);
    for (int i = 0; i <= input; i++)
  {
    digitalWrite(led[i], LOW);
  }
}