int pinA = 1;
int pinB = 2;
int pinC = 3;
int pinD = 4;
int pinE = 5;
int pinF = 6;
int pinG = 7;
int pinDP = 8;
int pinG1 = 9;
int pinG2 = 10;

void setup() {
  // put your setup code here, to run once:
  pinMode(pinA, OUTPUT);
  pinMode(pinB, OUTPUT);
  pinMode(pinC, OUTPUT);
  pinMode(pinD, OUTPUT);
  pinMode(pinE, OUTPUT);
  pinMode(pinF, OUTPUT);
  pinMode(pinG, OUTPUT);
  pinMode(pinDP, OUTPUT);
  pinMode(pinG1, OUTPUT);
  pinMode(pinG2, OUTPUT);
  digitalWrite(pinG1, HIGH);
  digitalWrite(pinG2, HIGH);
  turn_off();
}

int n = 0;

void loop() {
  while (n <= 9)
  {
    light_number(n);
    delay(1000);
    turn_off();
    n = n + 1;
  }
  n = 0;
}

void one()
{
  digitalWrite(pinB, LOW);
  digitalWrite(pinC, LOW);
}

void two()
{
  digitalWrite(pinA, LOW);
  digitalWrite(pinB, LOW);
  digitalWrite(pinG, LOW);
  digitalWrite(pinE, LOW);
  digitalWrite(pinD, LOW);
}

void three()
{
  digitalWrite(pinA, LOW);
  digitalWrite(pinB, LOW);
  digitalWrite(pinG, LOW);
  digitalWrite(pinC, LOW);
  digitalWrite(pinD, LOW);
}

void four()
{
  digitalWrite(pinF, LOW);
  digitalWrite(pinB, LOW);
  digitalWrite(pinG, LOW);
  digitalWrite(pinC, LOW);
}

void five()
{
  digitalWrite(pinA, LOW);
  digitalWrite(pinF, LOW);
  digitalWrite(pinD, LOW);
  digitalWrite(pinG, LOW);
  digitalWrite(pinC, LOW);
}


void six()
{
  digitalWrite(pinA, LOW);
  digitalWrite(pinF, LOW);
  digitalWrite(pinG, LOW);
  digitalWrite(pinE, LOW);
  digitalWrite(pinD, LOW);
  digitalWrite(pinC, LOW);
}

void seven()
{
  digitalWrite(pinA, LOW);
  digitalWrite(pinB, LOW);
  digitalWrite(pinC, LOW);
}

void eight()
{  
  digitalWrite(pinA, LOW);
  digitalWrite(pinB, LOW);
  digitalWrite(pinF, LOW);
  digitalWrite(pinG, LOW);
  digitalWrite(pinE, LOW);
  digitalWrite(pinD, LOW);
  digitalWrite(pinC, LOW);
}

void nine()
{  
  digitalWrite(pinA, LOW);
  digitalWrite(pinB, LOW);
  digitalWrite(pinF, LOW);
  digitalWrite(pinG, LOW);
  digitalWrite(pinD, LOW);
  digitalWrite(pinC, LOW);
}

void zero()
{  
  digitalWrite(pinA, LOW);
  digitalWrite(pinB, LOW);
  digitalWrite(pinF, LOW);
  digitalWrite(pinE, LOW);
  digitalWrite(pinD, LOW);
  digitalWrite(pinC, LOW);
}

void light_number(int num)
{
  switch(num)
  {
    case 0: zero(); break;
    case 1: one(); break;
    case 2: two(); break;
    case 3: three(); break;
    case 4: four(); break;
    case 5: five(); break;
    case 6: six(); break;
    case 7: seven(); break;
    case 8: eight(); break;
    case 9: nine(); break;
  }
}

void turn_off()
{
  digitalWrite(pinA, HIGH);
  digitalWrite(pinB, HIGH);
  digitalWrite(pinF, HIGH);
  digitalWrite(pinG, HIGH);
  digitalWrite(pinE, HIGH);
  digitalWrite(pinD, HIGH);
  digitalWrite(pinC, HIGH);
  digitalWrite(pinDP, HIGH);
}

















