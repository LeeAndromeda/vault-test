int ena = 5;
int in1 = 13;
int in2 = 4;
int enb = 6;
int in3 = 7;
int in4 = 8;

int sens0 = A0;
int sens1 = A1;
int sens2 = A2;
int sens3 = A3;
int sens4 = 10;

void stop()
{
  analogWrite(ena, 0);
  analogWrite(enb, 0);
}

void go_forward(int speed)
{
  analogWrite(ena, speed);
  analogWrite(enb, speed);

  digitalWrite(in1, 1);
  digitalWrite(in2, 0);
  digitalWrite(in3, 1);
  digitalWrite(in4, 0);
}

void go_backwards(int speed)
{
  analogWrite(ena, speed);
  analogWrite(enb, speed);

  digitalWrite(in1, 0);
  digitalWrite(in2, 1);
  digitalWrite(in3, 0);
  digitalWrite(in4, 1);
}

void go_right(int speed)
{
  analogWrite(ena, speed);
  analogWrite(enb, speed);

  digitalWrite(in1, 0);
  digitalWrite(in2, 1);
  digitalWrite(in3, 1);
  digitalWrite(in4, 0);
}

void go_left(int speed)
{
  analogWrite(ena, speed);
  analogWrite(enb, speed);

  digitalWrite(in1, 1);
  digitalWrite(in2, 0);
  digitalWrite(in3, 0);
  digitalWrite(in4, 1);
}

void step(int direction, int speed)
{
  // direction 1 left, -1 right, 2 forward, -2 backwards
  if(direction == 1)
  {
    go_left(speed);
    delay(50);
    stop();
  }

  if(direction == -1)
  {
    go_right(speed);
    delay(100);
    stop();
  }
  
  if(direction == 2)
  {
    go_forward(speed);
    delay(100);
    stop();
  }

  if(direction == -2)
  {
    go_backwards(speed);
    delay(100);
    stop();
  }
}

void accelerate_forward(int &init_speed, int speed_goal, int incr_step, int incr_period)
{
  while (init_speed < speed_goal)
  {
    init_speed += incr_step;
    go_forward(init_speed);
    delay(incr_period);
  }

  if (init_speed > speed_goal)
  {
    init_speed = speed_goal;
  }

  go_forward(init_speed);
  delay(incr_period);
}

void accelerate_backwards(int &init_speed, int speed_goal, int incr_step, int incr_period)
{
  while (init_speed < speed_goal)
  {
    init_speed += incr_step;
    go_backwards(init_speed);
    delay(incr_period);
  }

  if (init_speed > speed_goal)
  {
    init_speed = speed_goal;
  }

  go_backwards(init_speed);
  delay(incr_period);
}

void slow_down_forward(int &init_speed, int speed_goal, int incr_step, int incr_period)
{
  while (init_speed > speed_goal)
  {
    init_speed -= incr_step;
    go_forward(init_speed);
    delay(incr_period);
  }

  if(init_speed < speed_goal)
  {
    init_speed = speed_goal;
  }

  go_forward(init_speed);
  delay(incr_period);
}

void slow_down_backwards(int &init_speed, int speed_goal, int incr_step, int incr_period)
{
  while (init_speed > speed_goal)
  {
    init_speed -= incr_step;
    go_backwards(init_speed);
    delay(incr_period);
  }

  if(init_speed < speed_goal)
  {
    init_speed = speed_goal;
  }

  go_backwards(init_speed);
  delay(incr_period);
}

void ride_along_line(int sensors[5])
{
  if(sensors[2] == 0 && sensors[3] == 0 && sensors[4] == 0)
  {
    step(1, 130);
  }

  if(sensors[4] == 1 || sensors[3] == 1)
  {
    step(-1, 150);
  }

  if(sensors[0] == 0 && sensors[1] == 0 && sensors[2] == 0)
  {
    step(-1, 150);
  }

  if((sensors[1] == 0 || sensors[3] == 0) && sensors[2] == 1)
  {
    step(2, 105);
  }
}

// // // // // // // // // // // // // // // // // // // // // // //
// // // // // // // // // // // // // // // // // // // // // // //
// // // // // // // // // // // // // // // // // // // // // // //
// // // // // // // // // // // // // // // // // // // // // // //
// // // // // // // // // // // // // // // // // // // // // // //

void setup() {
  // put your setup code here, to run once:
  pinMode(ena, OUTPUT);
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(enb, OUTPUT);
  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT);

  pinMode(sens0, INPUT);
  pinMode(sens1, INPUT);
  pinMode(sens2, INPUT);
  pinMode(sens3, INPUT);
  pinMode(sens4, INPUT);
  
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:


  int sensors[5];
  sensors[0] = digitalRead(sens0);
  sensors[1] = digitalRead(sens1);
  sensors[2] = digitalRead(sens2);
  sensors[3] = digitalRead(sens3);
  sensors[4] = digitalRead(sens4);

  ride_along_line(sensors);

  delay(150);
}
