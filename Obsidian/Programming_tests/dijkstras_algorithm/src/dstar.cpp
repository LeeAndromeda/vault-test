/* Алгоритм Дейкстры реализованный на С++ студентом РТбо2-41 Лещевым-Романенко Андреем
* Данный алгоритм имеет два режима работы:
* 1. На предустановленном графе - его графичское представление приложено
* 2. На случайно сгенерированном графе
*
* Для работы в выбранном режиме необходимо будет ввести соотвествующую цифру.
*
* Имейте в виду, что нумерация вершин начинается с нуля и заканчивается на размере массива - 1.
*
* Данная реализация не защищена от неправильных вводов, потому необходимо
* с осторожностью проверять вводимые данные, иначе возможна некорректная
* работа алгоритма и программы в целом.
*/


#include <iostream>
#include <iomanip>
#include <ctime>
#include <cstdlib>
#include <vector>

#define infinite 99999

std::vector<int> predef_graph = {0, 5, 3, 99999, 99999,
                                99999, 99999, 99999, 99999, 5,
0, 6, 99999, 99999, 3, 2, 99999, 99999, 3, 6, 0, 7, 99999, 8, 99999, 99999, 99999,
99999, 99999, 7, 0, 4, 9, 99999, 99999, 99999, 99999, 99999, 99999, 4, 0, 5, 99999, 
99999, 5, 99999, 3, 8, 9, 5, 0, 1, 10, 99999, 99999, 2, 99999, 99999, 99999, 1,
0, 11, 99999, 99999, 99999, 99999, 99999, 99999, 10, 11, 0, 3, 99999, 99999, 99999,
99999, 5, 99999, 99999, 3, 0
};



void generate_random_graph(std::vector<std::vector<int>>& given_graph);

void print_matrix_2d(std::vector<std::vector<int>> input_mx);

void print_matrix_1d(std::vector<int> input_mx);

void set_graph(std::vector<std::vector<int>> &given_graph);

class Dstar
{
    public:
        Dstar()
        {
            NO_PATH.push_back(-1);
        }

        void set_start_node(int start_node)
        {
            this->start_node = start_node;
            min_distance_from_start.at(start_node) = 0;
        }

        void set_end_node(int end_node)
        {
            this->end_node = end_node;
        }

        void set_node_number(int node_number)
        {
            this->node_number = node_number;
        }

        void init_adjacency_matrix(std::vector<std::vector<int>> adjacency_matrix)
        {
            this->adjacency_matrix = adjacency_matrix;
        }

        void init_min_distance_from_start()
        {
            this->min_distance_from_start.resize(node_number);
        }

        // get start_node
        int get_start_node()
        {
            return start_node;
        }

        // get end_node 
        int get_end_node()
        {
            return end_node;
        }

        
        void logic()
        {
            // Resize columns and lines in adjacency_matrix and shortest_path_to_node to match node_number
            adjacency_matrix.resize(node_number);
            shortest_path_to_node.resize(node_number);
            for (int i = 0; i < node_number; ++i)
            {
                adjacency_matrix.at(i).resize(node_number);
                // shortest_path_to_node.at(i).resize(node_number);
            }

            // Установить минимальное расстояние от старта для всех нодов бесконечным
            for (int i = 0; i < min_distance_from_start.size(); ++i)
            {
                min_distance_from_start.at(i) = infinite;
            }

            min_distance_from_start.at(start_node) = 0;

            std::vector<int> neighbours_of_start = find_neighbours_of(start_node);

            for (int i = 0; i < neighbours_of_start.size(); ++i)
            {
                min_distance_from_start.at(neighbours_of_start.at(i)) = adjacency_matrix.at(start_node).at(neighbours_of_start.at(i));
            }

            int current_node = start_node;

            while(current_node != end_node)
            {
                parse(find_neighbours_of(current_node), current_node);
            }
        }


        int get_distance_to_goal()
        {
            return min_distance_from_start.at(end_node);
        }


        std::vector<int> get_path_to_goal()
        {
            return shortest_path_to_node.at(end_node);
        }


    private:
        std::vector<std::vector<int>> adjacency_matrix;
        std::vector<std::vector<int>> shortest_path_to_node;
        std::vector<int> min_distance_from_start;
        std::vector<int> reviewed_nodes;
        int start_node;
        int end_node;
        int node_number;
        std::vector<int> NO_PATH;


        std::vector<int> find_neighbours_of(int node)
        {
            std::vector<int> neighbours;

            for (int i = 0; i < adjacency_matrix.size(); ++i)
            {
                if(adjacency_matrix.at(node).at(i) > 0 && adjacency_matrix.at(node).at(i) < infinite)
                {
                    neighbours.push_back(i);
                }
            }

            for (int i = 0; i < neighbours.size(); ++i)
            {
                for (int j = 0; j < reviewed_nodes.size(); ++j)
                {
                    if (neighbours.at(i) != -1)
                    {
                        // std::cout << "i = " << i << " j = " << j << std::endl;
                        // std::cout << "node = " << node << std::endl;
                        // std::cout << "neighbours size = " << neighbours.size() << std::endl;
                        // std::cout << "reviewed nodes size = " << reviewed_nodes.size() << std::endl;
                        if(neighbours.at(i) == reviewed_nodes.at(j))
                        {
                            // std::cout << "erasing " << i << " " << j << std::endl;
                            // neighbours.erase(neighbours.begin() + (i)); // - 1
                            neighbours.at(i) = -1;
                            // std::cout << "erased " << i << " " << j << std::endl;
                        }
                        // std::cout << "reviewed nodes: " << std::endl;
                        // print_matrix_1d(reviewed_nodes);
                        
                        // std::cout << "neighbours: " << std::endl;
                        // print_matrix_1d(neighbours);
                    }

                }
            }

            int counter = 0;

            while (counter < neighbours.size())
            {
                // std::cout << "step counter = " << counter << std::endl;
                if(neighbours.at(counter) == -1)
                {
                    neighbours.erase(neighbours.begin() + (counter));
                    if (counter != 0) counter--;
                    // std::cout << "counter--. Now counter = " << counter << std::endl;
                }
                else
                {
                    counter++;
                    // std::cout << "counter++. Now counter = " << counter << std::endl;
                }
                // print_matrix_1d(neighbours);
            }

            return neighbours;
        }

        void parse(std::vector<int> parsed_neighbours, int &parsed_node)
        {
            int operational_node;

            // Узнать Кратчайший путь до нода короче чем путь через рассматриваемый нод?
            for (int n = 0; n < parsed_neighbours.size(); ++n)
            {
                operational_node = parsed_neighbours.at(n);
                // std::cout << "oper node = " << operational_node << " " << "parsed node = " << parsed_node << std::endl;
                if (min_distance_from_start.at(parsed_node) + find_distance_between(parsed_node, operational_node) < min_distance_from_start.at(operational_node))
                {
                    std::vector<int> path_to_current_node = find_path_to(parsed_node);
                    path_to_current_node.push_back(parsed_node);
                    shortest_path_to_node.at(operational_node) = path_to_current_node;
                    min_distance_from_start.at(operational_node) = min_distance_from_start.at(parsed_node) + find_distance_between(parsed_node, operational_node);
                }
            }
            std::cout << "Рассчитываем... " << (float) reviewed_nodes.size() / (float) node_number * 100 << "%" << std::endl;
            if (!contains(reviewed_nodes, parsed_node)) reviewed_nodes.push_back(parsed_node);
            parsed_node = find_closest_to_start();
        }


        int find_distance_between(int node1, int node2)
        {
            if (adjacency_matrix.at(node1).at(node2) > 0)
            {
                return adjacency_matrix.at(node1).at(node2);
            }
            return infinite;
        }


        std::vector<int> find_path_to(int node)
        {
            if (shortest_path_to_node.at(node) != NO_PATH)
            {
                return shortest_path_to_node.at(node);
            }
            else
            {
                std::vector<int> empty;
                return empty;
            }
        }


        int find_closest_to_start()
        {
            int closest_node = 0;

            for (int i = 0; i < min_distance_from_start.size(); ++i)
            {
                if (min_distance_from_start.at(i) < min_distance_from_start.at(closest_node))
                {
                    closest_node = i;
                }
            }

            if (closest_node == end_node)
            {
                return end_node;
            }

            if (closest_node == start_node)
            {
                int increment = 1;
                    while (contains(reviewed_nodes, closest_node))
                    {
                        if (closest_node == min_distance_from_start.size() - 1)
                        {
                            increment = -1;
                        }
                        else if (closest_node == 0)
                        {
                            increment = 1;
                        }
                        closest_node = closest_node + increment;
                    }

            }

            for (int i = 0; i < min_distance_from_start.size(); ++i)
            {
                if (min_distance_from_start.at(i) <= min_distance_from_start.at(closest_node) && !contains(reviewed_nodes, i))
                {
                    closest_node = i;
                }
            }

            int a = closest_node;
            return a;
        }


        bool contains(std::vector<int> vector, int value)
        {
            for (int i = 0; i < vector.size(); ++i)
            {
                if (vector.at(i) == value)
                {
                    return true;
                }
            }
            return false;
        }
};

int main ()
{
    srand(time(0));

    int node_number;
    std::vector<std::vector<int>> adjacency_matrix;
    Dstar dstar;

    int choice = 0;

    while (choice != 2 && choice != 1)
    {
        std::cout << "Введите 1, чтобы использовать заготовленный граф, или 2, чтобы сгенерировать случайный граф --> ";
        std::cin >> choice;

        if (choice == 1)
        {
            // Заполнить матрицу заготовленными числами
            node_number = 9;

            adjacency_matrix.resize(node_number);
            for (int i = 0; i < adjacency_matrix.size(); ++i)
            {
                adjacency_matrix.at(i).resize(node_number);
            }
            set_graph(adjacency_matrix);
        }
        else if (choice == 2)
        {
            // Заполнить матрицу смежности случайными числами
            std::cout << "Введите количество вершин графа --> ";
            std::cin >> node_number;

            adjacency_matrix.resize(node_number);
            for (int i = 0; i < adjacency_matrix.size(); ++i)
            {
                adjacency_matrix.at(i).resize(node_number);
            }
            generate_random_graph(adjacency_matrix);
        }
        else std::cout << "Неверный ввод. Попробуйте еще раз." << std::endl;
    }
    // print_matrix_2d(adjacency_matrix);

    int start_node;
    int end_node;

    std::cout << "Введите начальную ноду --> ";
    std::cin >> start_node;

    std::cout << "Введите конечную ноду --> ";
    std::cin >> end_node;

    dstar.set_node_number(node_number);
    dstar.init_min_distance_from_start();
    dstar.init_adjacency_matrix(adjacency_matrix);
    dstar.set_start_node(start_node);
    dstar.set_end_node(end_node);


    dstar.logic();

    std::cout << "Shortest distance to goal is " << dstar.get_distance_to_goal() << std::endl;
    std::cout << "Path to goal is:" << std::endl << dstar.get_start_node() << std::endl;
    print_matrix_1d(dstar.get_path_to_goal());
    std::cout << dstar.get_end_node() << std::endl;

    return 0;
}









void set_graph(std::vector<std::vector<int>> &given_graph)
{
    int c = 0;
    for (int stolb = 0; stolb < given_graph.size(); ++stolb)
    {
        for (int stroka = 0; stroka < given_graph.at(stolb).size(); ++stroka)
        {
            // if (stolb != stroka)
            // {
            //     std::cout << "Введите значение для элемента в столбе " << stolb << " и строке " << stroka 
            //     << std::endl << "--> ";
            //     std::cin >> given_graph.at(stolb).at(stroka);
            // }
            // else 
            // {
            //     given_graph.at(stolb).at(stroka) = 0;
            // }
            given_graph.at(stolb).at(stroka) = predef_graph.at(c);
            c++;
        }
    }
}


void generate_random_graph(std::vector<std::vector<int>> &given_graph)
{
    for (int stolb = 0; stolb < given_graph.size(); ++stolb)
    {
        for (int stroka = 0; stroka < given_graph.at(stolb).size(); ++stroka)
        {
            if (stolb == stroka)
            {
                given_graph.at(stolb).at(stroka) = 0;
            }
            else
            {
                float weight = (float) rand() / RAND_MAX * 15.0 + 1; // 
                if (weight > 12.0)
                {
                    weight = infinite;
                }
                // std::cout << weight << std::endl;
                given_graph.at(stolb).at(stroka) = weight;
                given_graph.at(stroka).at(stolb) = weight;
            }
        }
    }
}


void print_matrix_1d(std::vector<int> input_mx)
{
    for (int i = 0; i < input_mx.size(); ++i)
    {
        std::cout << input_mx.at(i) << std::endl;
    }
}


void print_matrix_2d(std::vector<std::vector<int>> input_mx)
{
    for (int i = 0; i < input_mx.size(); ++i)
    {
        for (int j = 0; j < input_mx.at(i).size(); ++j)
        {
            std::cout << std::setw(6) << input_mx.at(i).at(j) << " ";
        }
        std::cout << std::endl;
    }
}