import numpy as np
import cv2

cam = cv2.VideoCapture(0)
cam2 = cv2.VideoCapture(1)

cam.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)

cam2.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
cam2.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)

while(1):
    s, orignal = cam.read()
    s2, orignal2 = cam2.read()
    cv2.imshow('original',orignal)
    cv2.imshow('original2',orignal2)

    if cv2.waitKey(1) & 0xFF == ord('w'):
        break  


cam.release()
cam2.release()
cv2.destroyAllWindows()