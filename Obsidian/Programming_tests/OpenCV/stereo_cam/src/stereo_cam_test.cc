#include "opencv2/opencv.hpp"
#include <iostream>
using std::cout;

int main()
{
    cv::Mat frame;
    cv::Mat frame1;
    cv::Mat frame2;
    cv::Mat frame3;

    cv::VideoCapture cap;
    cap.open("/dev/video0");    // запускает стриминг с камеры под индексом 0
    cap.set(cv::CAP_PROP_FRAME_HEIGHT, 480);
    cap.set(cv::CAP_PROP_FRAME_WIDTH, 1280);
    cap.set(cv::CAP_PROP_FPS, 15);
    // cap.set(cv::CAP_PROP_FPS, 60);


    cv::VideoCapture cap1;
    cap1.open("/dev/video1");    // запускает стриминг с камеры под индексом 0
    cap1.set(cv::CAP_PROP_FRAME_HEIGHT, 480);
    cap1.set(cv::CAP_PROP_FRAME_WIDTH, 640);
    cap1.set(cv::CAP_PROP_FPS, 15);


    cv::VideoCapture cap2;
    cap2.open("/dev/video2");    // запускает стриминг с камеры под индексом 0
    cap2.set(cv::CAP_PROP_FRAME_HEIGHT, 480);
    cap2.set(cv::CAP_PROP_FRAME_WIDTH, 640);
    cap2.set(cv::CAP_PROP_FPS, 15);



    cv::VideoCapture cap3;
    cap3.open("/dev/video3");    // запускает стриминг с камеры под индексом 0
    cap3.set(cv::CAP_PROP_FRAME_HEIGHT, 480);
    cap3.set(cv::CAP_PROP_FRAME_WIDTH, 640);
    cap3.set(cv::CAP_PROP_FPS, 15);



    while(true)
    {
        cap.read(frame);
        cap1.read(frame1);
        cap2.read(frame2);
        cap3.read(frame3);
        int cols, rows;
        cols = frame.cols;
        rows = frame.rows;

        cout << "Frame = 0" << "Cols = " << cols << "\nRows = " << rows << "\nDepth = " << frame.depth() << "\nChannels = " << frame.channels() << "\n\n\n";

        cols = frame1.cols;
        rows = frame1.rows;

        cout << "Frame = 1" << "Cols = " << cols << "\nRows = " << rows << "\nDepth = " << frame1.depth() << "\nChannels = " << frame1.channels() << "\n\n\n";

        cols = frame2.cols;
        rows = frame2.rows;

        cout << "Frame = 2" << "Cols = " << cols << "\nRows = " << rows << "\nDepth = " << frame2.depth() << "\nChannels = " << frame2.channels() << "\n\n\n";

        cols = frame3.cols;
        rows = frame3.rows;

        cout << "Frame = 3" << "Cols = " << cols << "\nRows = " << rows << "\nDepth = " << frame3.depth() << "\nChannels = " << frame3.channels() << "\n\n\n";

        // cv::imshow("this is you, smile! :)", frame);
        // cv::imshow("this is you, smile! :)1", frame1);
        // cv::imshow("this is you, smile! :)2", frame2);
        // cv::imshow("this is you, smile! :)3", frame3);

        if( cv::waitKey(10) == 27 ) 
            break; // stop capturing by pressing ESC 
    }
    return 0;
}