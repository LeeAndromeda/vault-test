# Нам надо сохранить соотношение сторон
# чтобы изображение не исказилось при уменьшении
# для этого считаем коэф. уменьшения стороны
import cv2
# Загружаем библиотеку opencv
img = cv2.imread('avto.png')
# возвращает массив NumPy, который содержитпредставление данных изображения avto.jpg
cv2.imshow('avto.png', img)
# Вывести изображение на экран
scale_percent = 50
# Процент от изначального размера
width = int(img.shape[1] * scale_percent / 100)
height = int(img.shape[0] * scale_percent / 100)
dim = (width, height)
resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
cv2.imshow('avto_resized.png', resized)
cv2.waitKey(0)