import cv2
import numpy as np

img = cv2.imread('avto.png')

px = img[32, 32] # доступ к пикселю цветного изображения
print(px)

blue = img[32, 32, 0] # доступ только к синему пикселю
print(blue)

img [32, 32] = [255, 255, 255]
print(img [32, 32])

# accessing RED value
print(img.item(10,10,2))
# modifying RED value
img.itemset((10,10,2),100)
print(img.item(10,10,2))


