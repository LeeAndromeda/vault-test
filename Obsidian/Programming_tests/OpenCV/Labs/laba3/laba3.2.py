import cv2
import numpy as np
# from matplotlib import pyplot as cv2
# cv2.subplot(121)
img = cv2.imread('Cop.jpg')
cv2.imshow("img", img) # Меняем формат BGR на RGB
# cv2.subplot(122)
Z = img.reshape((-1,3)) # изменить размер массива изображения. Где (- 1,3) - означает во сколько строк и столбцов уместить прежний массив. В данном случае 3 - означает в 3 столбца, а -1 это исключение, означает что мы даём python самому понять во сколько строк это можно уместить
Z = np.float32(Z) # преобразовать массив к float (числа с плавающей точкой)
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
# Критерии для остановки квантования. Тут: (Останавливаем квантование если достигли 10 итераций или если достигли точности в 1.0)
K = 4 # До какого количества цветов сократить изображение
ret,label,center=cv2.kmeans(Z,K,None,criteria,10,cv2.KMEANS_RANDOM_CENTERS) # Функция квантования цветов
# Преобразуем изображение к изначальному виду:
center = np.uint8(center) # Преобразование матрицы к формату uint8 (от 0 до 255)
res = center[label.flatten()] # Сворачиваем массив нескольких матриц в одну
res2 = res.reshape((img.shape)) # Изменение формы массива под форму массива оригинального изображения
# Выводим изображение на экран:
cv2.imshow("quantized", res2)
cv2.waitKey(0)