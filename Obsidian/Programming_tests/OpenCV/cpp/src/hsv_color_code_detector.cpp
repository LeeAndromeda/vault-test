#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include <iostream>
using namespace cv;

const int max_value_H = 360/2;
const int max_value = 255;
const String window_capture_name = "Video Capture";
const String window_detection_name = "Object Detection";

int low_hue = 0, low_saturation = 0, low_value = 0;
int high_hue = max_value_H, high_saturation = max_value, high_value = max_value;


static void on_low_H_thresh_trackbar(int, void *)
{
    low_hue = min(high_hue-1, low_hue);
    setTrackbarPos("Low H", window_detection_name, low_hue);
}


static void on_high_H_thresh_trackbar(int, void *)
{
    high_hue = max(high_hue, low_hue+1);
    setTrackbarPos("High H", window_detection_name, high_hue);
}


static void on_low_S_thresh_trackbar(int, void *)
{
    low_saturation = min(high_saturation-1, low_saturation);
    setTrackbarPos("Low S", window_detection_name, low_saturation);
}


static void on_high_S_thresh_trackbar(int, void *)
{
    high_saturation = max(high_saturation, low_saturation+1);
    setTrackbarPos("High S", window_detection_name, high_saturation);
}


static void on_low_V_thresh_trackbar(int, void *)
{
    low_value = min(high_value-1, low_value);
    setTrackbarPos("Low V", window_detection_name, low_value);
}


static void on_high_V_thresh_trackbar(int, void *)
{
    high_value = max(high_value, low_value+1);
    setTrackbarPos("High V", window_detection_name, high_value);
}


int main(int argc, char* argv[])
{
    // VideoCapture cap(argc > 1 ? atoi(argv[1]) : 0);
    VideoCapture cap;
    cap.open("/dev/video2");    // запускает стриминг с камеры под индексом 0
    cap.set(CAP_PROP_FRAME_HEIGHT, 720);
    cap.set(CAP_PROP_FRAME_WIDTH, 1280);
    cap.set(CAP_PROP_FPS, 10);

    namedWindow(window_capture_name);
    namedWindow(window_detection_name);
    // Trackbars to set thresholds for HSV values
    createTrackbar("Low H", window_detection_name, &low_hue, max_value_H, on_low_H_thresh_trackbar);
    createTrackbar("High H", window_detection_name, &high_hue, max_value_H, on_high_H_thresh_trackbar);
    createTrackbar("Low S", window_detection_name, &low_saturation, max_value, on_low_S_thresh_trackbar);
    createTrackbar("High S", window_detection_name, &high_saturation, max_value, on_high_S_thresh_trackbar);
    createTrackbar("Low V", window_detection_name, &low_value, max_value, on_low_V_thresh_trackbar);
    createTrackbar("High V", window_detection_name, &high_value, max_value, on_high_V_thresh_trackbar);
    Mat frame, frame_HSV, frame_threshold;

    while (true) 
    {
        cap >> frame;
        if(frame.empty())
        {
            break;
        }
        
        // Convert from BGR to HSV colorspace
        cvtColor(frame, frame_HSV, COLOR_BGR2HSV);
        // Detect the object based on HSV Range Values
        inRange(frame_HSV, Scalar(low_hue, low_saturation, low_value), Scalar(high_hue, high_saturation, high_value), frame_threshold);
        // Show the frames
        imshow(window_capture_name, frame);
        imshow(window_detection_name, frame_threshold);
        resizeWindow(window_capture_name, 480, 320);

        char key = (char) waitKey(30);
        if (key == 'q' || key == 27)
        {
            break;
        }
    }

    std::cout << "- Нижний диапазон:\n\t- Hue: " << low_hue << "\n\t- Saturation: " << low_saturation << "\n\t- Value: " << low_value << std::endl;
    std::cout << "- Верхний диапазон:\n\t- Hue: " << high_hue << "\n\t- Saturation: " << high_saturation << "\n\t- Value: " << high_value << std::endl;
    
    return 0;
}