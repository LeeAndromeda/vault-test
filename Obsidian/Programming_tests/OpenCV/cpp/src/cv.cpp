#include "opencv2/opencv.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include <iostream>

using namespace cv;



// #################################################
// ###############################################
// ########       DECLARATIONS           #######
// ###############################################
// #################################################




struct Vector3_8U
{
    uchar x;
    uchar y;
    uchar z;
};



struct ColorRange
{
    Vector3_8U upper_bound;
    Vector3_8U lower_bound;
};


struct ColorSetups
{
    ColorRange red;
    ColorRange orange;
    ColorRange yellow;
    ColorRange green;
    ColorRange blue;
    ColorRange indigo;
    ColorRange violet;
};




/**
 * @brief Gets contours of an image
 * 
 * @param in_img BGR image
 * @return Mat that contains contours GRAYSCALE
 */
Mat contours_canny(Mat& in_img);


/**
 * @brief Recognizes segments on the frame image using the contour image as a hint
 * 
 * @param contour grayscale image, contating hint contours
 * @param frame image to find segments on
 * @return Mat CV_8UC3
 */
Mat watershed_from_contour(Mat& contour, Mat& frame);

/**
* @param img
* @param dilation_elem 0 - rectangle, 1 - cross, 2 - ellipse
* @param dilation_size
*/
void dilate_img(Mat& img, int dilation_elem, int dilation_size);

Mat quantize(Mat& img, int depth);


/**
* @brief Sets matrix values to the passed args
*/
void set_cv_8uc3_mat_to(Mat& mat, uchar c1, uchar c2, uchar c3);


/**
* @param img cv::Mat with CV_8UC3 data type
* 
* @returns cv::Mat with CV_8UC1 data type
*/
Mat threshold_color_hsv(Mat& img, uchar hue_b, uchar sat_b, uchar val_b, uchar hue_u, uchar sat_u, uchar val_u);


/**
 * @brief Returns image, that only contains the mentioned color
 * 
 * @param img 
 * @return Mat 
 */
Mat remove_lights_hsv(Mat& img);


/**
 * @brief Limits channels of a 3-channel image to the corresponding values
 * 
 * @param img 
 * @param l1 
 * @param l2 
 * @param l3 
 * @return Mat limited image
 */
Mat limit_channels(Mat& img, uchar l1, uchar l2, uchar l3);


/**
 * @brief Erosion func
 * 
 * @param img image to erode
 * @param kernel_size 
 * @param erosion_size 
 */
void erode_img(Mat& img, int kernel_size, int erosion_size);





/**
 * @brief Returns moments of an image
 * 
 * @param canny_output 
 * @return std::vector<Moments> 
 */
std::vector<Moments> find_moments(Mat& canny_output);



/**
 * @brief Returns moments that belong to a contour with largest area
 * 
 * @param ms moments vector
 * @param out_moments output moments var
 * @return Moments 
 */
Moments find_moment_largest_area(std::vector<Moments> ms, Moments& out_moments);



/**
 * @brief 
 * 
 * @param moments moments to search center coords for
 * @return Point center coords
 */
Point find_center_of_moment(Moments moments);




/**
 * @brief 
 * 
 * @param img BGR CV_8U C == 3 image
 * @param color color range to search for in HSV format
 * @param moments variable TO STORE moments of the contour image. IT SHOULD BE EMPTY OR WITH UNIMPORTANT DATA
 * @return Mat contours of found color GRAYSCALE
 */
Mat find_color(Mat& img, ColorRange color, std::vector<Moments>& moments);




















// #################################################
// ###############################################
// ########            MAIN           #########


int main()
{
    Mat frame;

    VideoCapture cap;
    cap.open("/dev/video1");    // запускает стриминг с камеры под индексом 0
    cap.set(CAP_PROP_FRAME_HEIGHT, 720);
    cap.set(CAP_PROP_FRAME_WIDTH, 1280);
    cap.set(CAP_PROP_FPS, 10);

    double last_time = (double)getTickCount();

    while(true)
    {
        cap.read(frame);
        // double time_since_last = ((double)getTickCount() - last_time);
        // last_time = getTickCount();

        CV_Assert(frame.depth() == CV_8U);
        CV_Assert(frame.channels() == 3);

// Lows:
// Hue: 161
// Saturation: 163
// Value: 37
// Highs:
// Hue: 180
// Saturation: 255
// Value: 255

        ColorRange red;
        red.lower_bound.x = 161;
        red.lower_bound.y = 163;
        red.lower_bound.z = 37;

        red.upper_bound.x = 180;
        red.upper_bound.y = 255;
        red.upper_bound.z = 255;

        std::vector<Moments> red_moments;

        Mat red_contour = find_color(frame, red, red_moments);
        Moments largest_red_area_mmnt;
        find_moment_largest_area(red_moments, largest_red_area_mmnt);
        Point red_cntr = find_center_of_moment(largest_red_area_mmnt);

        double size = sqrt(largest_red_area_mmnt.m00);
        drawMarker(frame, red_cntr, Scalar(255, 0, 0), 4, size, 2);

        std::string text;
        text.append("X: ");
        text.append(std::to_string(red_cntr.x));
        text.append("\nY: ");
        text.append(std::to_string(red_cntr.y));

        putText(frame, text, red_cntr ,FONT_HERSHEY_COMPLEX, 8, Scalar(0, 0, 0), 2);


        // Mat frame_8uc3(frame, CV_8UC3);

        // imshow("frame", frame);
        // cvtColor(frame, frame, COLOR_BGR2HSV);
        // frame = remove_lights_hsv(frame);
        // cvtColor(frame, frame, COLOR_HSV2BGR);
        // frame = limit_channels(frame, 255, 255, 180);
        // Mat rgb_frame;
        // cvtColor(frame, rgb_frame, COLOR_HSV2BGR);

        // Mat red = threshold_color_hsv(frame, 26, 46, 0, 49, 255, 255);

            // Mat red = threshold_color_hsv(frame, 161, 150, 30, 200, 255, 255);

            // dilate_img(red, 2, 3);
            // erode_img(red, 3, 3);
            // Mat red_countours = contours_canny(red);

            // std::vector<Moments> moms = find_moments(red_countours);
            // Moments mnt;
            // find_moment_largest_area(moms, mnt);

            // double x_cntr = (double)mnt.m10 / mnt.m00;
            // double y_cntr = (double)mnt.m01 / mnt.m00;

            // // double nx = x_cntr / mnt.m00;
            // // double ny = y_cntr / mnt.m00;

            // // double width = 2 * sqrt(nx);
            // // double height = 2 * sqrt(ny);

            // double size = sqrt(mnt.m00);

            // Point rect_pos(x_cntr, y_cntr);
            // drawMarker(frame, rect_pos, Scalar(255, 0, 0), 4, size, 2);

        // cvtColor(frame, frame, COLOR_HSV2BGR);
        // cvtColor(red_countours, red_countours, COLOR_GRAY2BGR);
        // frame = frame + red_countours;
        imshow("red_countours", frame);


        if( waitKey(5) >= 0 ) 
            break; // stop capturing by pressing ESC 
    }
    return 0;
}


// ########            MAIN           #########
// ###############################################
// #################################################



























// #################################################
// ###############################################
// ########        DEFINITIONS         #########
// ###############################################
// #################################################



Mat contours_canny(Mat& in_img)
{
    Mat src, src_gray, dst;
    int kernel_size = 3;
    int scale = 1;
    int delta = 0;
    int ddepth = CV_8UC3;
    const char* window_name = "Laplace Demo";

    // Reduce noise by blurring with a Gaussian filter ( kernel size = 3 )
    erode_img(in_img, 3, 3);
    dilate_img(in_img, 2, 3);
    GaussianBlur( in_img, src, Size(3, 3), 0, 0, BORDER_DEFAULT );
    src_gray = in_img;
    if (in_img.channels() != 1)
    {
        cvtColor(in_img, src_gray, COLOR_BGR2GRAY); // Convert the image to grayscale
    } 
    Mat abs_dst;
    // Laplacian( src_gray, dst, ddepth, kernel_size, scale, delta, BORDER_DEFAULT );
    Canny(src_gray, dst, 150, 200, 3, true);


    // converting back to CV_8U
    convertScaleAbs( dst, abs_dst );
    // imshow( window_name, abs_dst );
    // dilate_img(abs_dst, 2, );

    Mat kernel = (Mat_<char>(3,3) <<
    1, 1, 1,
    1, -8, 1,
    1, 1, 1);

    // cvtColor(abs_dst, abs_dst, COLOR_GRAY2BGR); 

    filter2D(abs_dst, abs_dst, ddepth, kernel);

    return abs_dst;
}


void dilate_img(Mat& img, int dilation_elem, int dilation_size)
{
    int dilation_type = 0;
    if( dilation_elem == 0 ){ dilation_type = MORPH_RECT; }
    else if( dilation_elem == 1 ){ dilation_type = MORPH_CROSS; }
    else if( dilation_elem == 2) { dilation_type = MORPH_ELLIPSE; }

    Mat element = getStructuringElement( dilation_type,
    Size( 2*dilation_size + 1, 2*dilation_size+1 ),
    Point( dilation_size, dilation_size ) );
    dilate( img, img, element );
    // imshow( "Dilation Demo", img );
}




void erode_img(Mat& img, int kernel_size, int erosion_size)
{
    int dilation_type = 0;
    Mat kernel(Size(kernel_size, kernel_size), CV_8U);

    Mat element = getStructuringElement( dilation_type,
    Size( 2*erosion_size + 1, 2*erosion_size+1 ),
    Point( erosion_size, erosion_size ) );
    // dilate( img, img, element );
    erode(img, img, kernel);
    // imshow( "Dilation Demo", img );
}








Mat watershed_from_contour(Mat& contour, Mat& frame)
{
    Mat contour_8u;
    CV_Assert(contour.channels() == 1);
    contour.convertTo(contour_8u, CV_8U);

    std::vector<std::vector<Point>> contours;
    findContours(contour_8u, contours, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);


    Mat markers = Mat::zeros(contour.size(), CV_32S);

    for (int i = 0; i < contours.size(); i++)
    {
        drawContours(markers, contours, static_cast<int>(i), Scalar(static_cast<int>(i)+1), -1);
    }

    // CV_Assert(contour_8u.depth() == CV_8U);

    watershed(frame, markers);
    Mat segments;
    markers.convertTo(segments, CV_8UC3);
    return segments;
}






Mat quantize(Mat& img, int depth)
{
    unsigned char table[256];

    int nRows = img.rows;
    int nCols = img.cols * 3;

    Mat qimg;
    img.convertTo(qimg, CV_8U);

    CV_Assert(qimg.depth() == CV_8U);

    for (int i = 0; i < 256; ++i)
    {
        table[i] = (uchar)(depth * (i / depth));
    }

    uchar* p;
    for(int i = 0; i < nRows; ++i)
    {
        p = qimg.ptr<uchar>(i);
        
        for (int j = 0; j < nCols; ++j)
        {
            p[j] = table[p[j]];
        }
    }

    return qimg;
}



void set_cv_8uc3_mat_to(Mat& mat, uchar c1, uchar c2, uchar c3)
{
    CV_Assert(mat.depth() == CV_8U);
    CV_Assert(mat.channels() == 3);
    MatIterator_<Vec3b> it, end;
    for( it = mat.begin<Vec3b>(), end = mat.end<Vec3b>(); it != end; ++it)
    {
        (*it)[0] = c1;
        (*it)[1] = c2;
        (*it)[2] = c3;
    }
}






Mat threshold_color_hsv(Mat& img, uchar hue_b, uchar sat_b, uchar val_b, uchar hue_u, uchar sat_u, uchar val_u)
{
    // CV_Assert(img.channels() == 3);
    Mat threshold_u(img.size(), img.type()), threshold_b(img.size(), img.type());
    set_cv_8uc3_mat_to(threshold_b, hue_b, sat_b, val_b);
    set_cv_8uc3_mat_to(threshold_u, hue_u, sat_u, val_u);

    Mat is_in_bounds;
    inRange(img, threshold_b, threshold_u, is_in_bounds);

    return is_in_bounds;
}






Mat limit_channels(Mat& img, uchar l1, uchar l2, uchar l3)
{
    CV_Assert(img.channels() == 3);
    Mat limg;
    img.convertTo(limg, CV_8UC3);
    MatIterator_<Vec3b> it, end;
    for( it = limg.begin<Vec3b>(), end = limg.end<Vec3b>(); it != end; ++it)
    {
        (*it)[0] -= ((*it)[0] - l1) * ((*it)[0] > l1);
        (*it)[1] -= ((*it)[1] - l2) * ((*it)[1] > l2);
        (*it)[2] -= ((*it)[2] - l3) * ((*it)[2] > l3);
    }

    return limg;
}





Mat remove_lights_hsv(Mat& img)
{
    CV_Assert(img.channels() == 3);
    Mat unlimg;
    img.copyTo(unlimg);

    MatIterator_<Vec3b> it, end;
    for( it = unlimg.begin<Vec3b>(), end = unlimg.end<Vec3b>(); it != end; ++it)
    {
        if (
            (*it)[0] >= 0 && (*it)[0] <= 77 &&
            (*it)[1] >= 0 && (*it)[1] <= 13 &&
            (*it)[2] >= 228 && (*it)[2] <= 255
        )
        {
            (*it)[0] = 0;
            (*it)[1] = 0;
            (*it)[2] = 0;
        } 
    }

    return unlimg;
}




std::vector<Moments> find_moments(Mat& canny_output)
{
    RNG rng(12345);
    std::vector<std::vector<Point>> contours;
    findContours(canny_output, contours, RETR_TREE, CHAIN_APPROX_SIMPLE);

    std::vector<Moments> mu(contours.size());
    for( size_t i = 0; i < contours.size(); i++ )
    {
        mu[i] = moments( contours[i] );
    }

    std::vector<Point2f> mc( contours.size() );
    for( size_t i = 0; i < contours.size(); i++ )
    {
        //add 1e-5 to avoid division by zero
        mc[i] = Point2f( static_cast<float>(mu[i].m10 / (mu[i].m00 + 1e-5)),
        static_cast<float>(mu[i].m01 / (mu[i].m00 + 1e-5)) );
        // std::cout << "mc[" << i << "]=" << mc[i] << std::endl;
    }


    Mat drawing = Mat::zeros( canny_output.size(), CV_8UC3 );
    for( size_t i = 0; i < contours.size(); i++ )
    {
        Scalar color = Scalar( rng.uniform(0, 256), rng.uniform(0,256), rng.uniform(0,256) );
        drawContours( drawing, contours, (int)i, color, 2 );
        circle( drawing, mc[i], 4, color, -1 );
    }

    imshow( "Contours", drawing );

    return mu;
}





Moments find_moment_largest_area(std::vector<Moments> ms, Moments& out_moments)
{
    if (!ms.empty())
    {
        int16_t largest_area = 0;
        int idx;
        for (int i = 0; i < ms.size(); ++i)
        {
            if (ms.at(i).m00 > largest_area) 
            {
                largest_area = ms.at(i).m00;
                idx = i;
            }
        }

        out_moments = ms.at(idx);
    }
}




Mat find_color(Mat& img, ColorRange color_rng, std::vector<Moments>& moments)
{
    CV_Assert(img.depth() == CV_8U);
    CV_Assert(img.channels() == 3);

    Mat img_hsv;
    cvtColor(img, img_hsv, COLOR_BGR2HSV);
    img_hsv = remove_lights_hsv(img_hsv);

    Mat color_img = threshold_color_hsv(img_hsv, color_rng.lower_bound.x, color_rng.lower_bound.y, color_rng.lower_bound.z, color_rng.upper_bound.x, color_rng.upper_bound.y, color_rng.upper_bound.z);

    dilate_img(color_img, 2, 3);
    erode_img(color_img, 3, 3);
    Mat color_img_countours = contours_canny(color_img);

    moments = find_moments(color_img_countours);


    // drawMarker(img_hsv, rect_pos, Scalar(255, 0, 0), 4, size, 2);

    // cvtColor(img_hsv, frame, COLOR_HSV2BGR);
    // cvtColor(red_countours, red_countours, COLOR_GRAY2BGR);
    return color_img_countours;
}



Point find_center_of_moment(Moments moments)
{
    
    // find_moment_largest_area(moments, moments);

    double x_cntr = (double)moments.m10 / moments.m00;
    double y_cntr = (double)moments.m01 / moments.m00;

    double size = sqrt(moments.m00);

    Point rect_pos(x_cntr, y_cntr);
    // object_centers.push_back(rect_pos);
    return rect_pos;
}


