﻿#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include "header.h"

std::string TMPLT_STR = "pos_start\npos_length var\npos_number Номер позиции --> var\npos_firma Фирма-изготовитель --> var\npos_datchiki Датчики --> var\n\tpos_video_camera Видеокамера --> var\n\tpos_lidar Лидар --> var\n\tpos_accelerometer Акселерометр --> var\n\tpos_hyroscope Гироскоп --> var\n\tpos_ultrasonic Ультразвуковой дальномер --> var\npos_massa Масса --> var кг\npos_data Дата изготовления --> var\n\tpos_day День --> var\n\tpos_month Месяц --> var\n\tpos_year Год --> var\npos_end\n";

int length(std::string str); // Считает и возвращает длину передаваемой строки


struct position_meta
{
    int start_pos;
    int end_pos;
    int seq_number;
};

// ==================================================================================================================
// =============================================   CLASS   ==========================================================
// ==================================================================================================================

class position_handler{
    private:
    int start_position;
    int end_position;
    
    int position_number;
    std::string manufacturer;
    int sensors_present;
    std::string video_camera;
    std::string lidar;
    std::string accelerometer;
    std::string hyroscope;
    std::string ultrasonic;
    float mass;
    int date;
    int day, month, year;

    public:
    position_handler()
    {
        start_position = 0;
        end_position = start_position + TMPLT_STR.size();

        position_number = 1;
        manufacturer = "-";
        sensors_present = 0;
        video_camera = "-";
        lidar = "-";
        accelerometer = "-";
        hyroscope = "-";
        ultrasonic = "-";
        mass = 0;
        date = 0;
        day = 0;
        month = 0;
        year = 0;
    }


    void ask_for_data() // Эта функция запрашивает у пользоватея данные для заполнения позиции
    {
        std::cout << "Введите название компании-производителя:" << std::endl;
        std::cin >> manufacturer;
        std::cout << "Введите, присутствуют ли сенсоры(1 - да, 0 - нет):" << std::endl;
        std::cin >> sensors_present;    
        while (sensors_present != 1 && sensors_present != 0)
        {
            std::cout << "Введен неверный ответ. Повторите попытку:" << std::endl;
            std::cin >> sensors_present;
        }
        if(sensors_present == 1)
        {
            std::cout << "Введите название камеры(\"-\", если отсутсвует):" << std::endl;
            std::cin >> video_camera;
            std::cout << "Введите название лидара(\"-\", если отсутсвует):" << std::endl;
            std::cin >> lidar;
            std::cout << "Введите название акселерометр(\"-\", если отсутсвует):" << std::endl;
            std::cin >> accelerometer;
            std::cout << "Введите название гироскопа(\"-\", если отсутсвует):" << std::endl;
            std::cin >> hyroscope;
            std::cout << "Введите название ультразвукового дальномера(\"-\", если отсутсвует):" << std::endl;
            std::cin >> ultrasonic;
        }
        std::cout << "Введите массу(-1, если неизвестна):" << std::endl;
        std::cin >> mass;
        std::cout << "Введите, есть ли дата производства(1 - есть, 0 - нет):" << std::endl;
        std::cin >> date;
        while(date != 0 && date != 1)
        {
            std::cout << "Неверный ввод. Введите 1, если дата производства есть, 2Ю если ее нет" << std::endl;
        }
        if(date == 1)
        {
            std::cout << "Введите день производства, затем месяц производства.затем год производства:" << std::endl;
            std::cin >> day >> month >> year;
        }
        std::cout << "Ввод данных завершен." << std::endl;
    }
};

// ==================================================================================================================
// =============================================   CLASS   ==========================================================
// ==================================================================================================================

class bd: public position_handler
{
    private:
    std::string filename;
    std::string position_template;
    int template_size; //IDEA Реализовать проверку номера позиции на основе размера строки шаблона и поиска номера позиции
    std::vector<position_meta> meta;
    std::fstream file_obj;


    public:
    bd() // Пустой контсруктор
    {
        position_template = TMPLT_STR;
        template_size = position_template.size();
    }


    bd(std::string filename_input) // Конструктор, меняющий имя файла, с которым мы работаем
    {
        position_template = TMPLT_STR;
        template_size = position_template.size();
        filename = filename_input;
        open_file();
    }


    void changeFilename(std::string& filename) // Меняет имя файла, с которым мы работаем
    {
        std::cout << "Введите название файла --> " << std::endl;
        std::cin >> filename; //IDEA Добавить проверку ввода
    }


    void initPosition() // Инициализирует новую позицию в конце файла
    {
        std::cout << "Вы хотите инициализировать новую позицию? y или n\n";
        char a = 'n';
        std::cin >> a;
        if(a == 'y' || a == 'Y')
        {
            if(file_obj.is_open()) 
            {
                file_obj.close();
                file_obj.open(filename, std::ios_base::app);
            }
            else file_obj.open(filename, std::ios_base::app);
            file_obj << position_template;
            file_obj.close();
        }
        else
        {
            std::cout << "Инициализация отменена." << std::endl;
        }
    }


    void get_position_numbers() // Получает данные расположения и порядка существующих позиций 
    {
        meta.resize(0);
        open_file();
        std::vector<std::string> buffer;
        int n = 0; 
        buffer.resize(2);
        buffer.at(0) == "";
        buffer.at(1) == "";
        do
        {
            buffer.at(1) = buffer.at(0);
            file_obj >> buffer.at(0);
            if(buffer.at(0) == "pos_start" && buffer.at(1) == "")
            {
                n = 1;
                meta.resize(n);
                meta.at(n - 1).start_pos = file_obj.tellg() - length("pos_start");
                meta.at(n - 1).end_pos = meta.at(n - 1).start_pos + template_size/* - length("pos_end\n")*/;
                meta.at(n - 1).seq_number = n;
            }
            else if(buffer.at(0) == "pos_start" && buffer.at(1) == "pos_end")
            {
                n += 1;
                meta.push_back(meta.at(n - 2));
                meta.at(n - 1).start_pos = file_obj.tellg() - length("pos_start");
                meta.at(n - 1).end_pos = meta.at(n - 1).start_pos + template_size/* - length("pos_end\n")*/;
                meta.at(n - 1).seq_number = n;
            }
            std::cout << "get cycle complete!" << std::endl;
            std::cout << "buffer0 is " << buffer.at(0) << std::endl << "buffer1 is " << buffer.at(1) << std::endl;
        }
        while((buffer.at(0) != "" && buffer.at(1) != "pos_end") || (buffer.at(0) != "pos_end" && buffer.at(1) != "pos_end") || (buffer.at(0) == "pos_start"));
        close_file();
        std::cout << "Получение информации о позициях завершено." << std::endl;
    }


    int search_for_string_poscode(std::string search_request)
    {
        int search_request_place;
        open_file();
        file_obj.seekp(0);
        file_obj.seekg(0);
        
        int continue_search = 1;
        std::vector<std::string> buffer;
        buffer.resize(2);
        buffer.at(0) = "";
        buffer.at(1) = "";

        while (continue_search > 0)
        {
            buffer.at(1) = buffer.at(0);
            file_obj >> buffer.at(0);

            if(buffer.at(1) == search_request)
            {
                file_obj.seekp(file_obj.tellp() - length(buffer.at(0)));
                file_obj.seekg(file_obj.tellg() - length(buffer.at(0)));

                continue_search = 0;
                search_request_place = file_obj.tellp() * (file_obj.tellg() == file_obj.tellp()) - 1 * (!(file_obj.tellg() == file_obj.tellp()));
                std::cout << "POSCODE STRING SEARCH: Строка найдена!" << std::endl << "Строка: " << buffer.at(0) << std::endl << "Положение её начала:" << search_request_place << std::endl;
            }

            else if((buffer.at(0) == "" && buffer.at(1) == "pos_end") || (buffer.at(0) == "pos_end" && buffer.at(1) == "pos_end") || (buffer.at(0) == "" && buffer.at(1) == ""))
            {
                std::cout << "Строка не найдена. Завершаю..." << std::endl;
                continue_search = 0;
            }
        }
        close_file();
    }

    int search_for_string_poscode3(std::string search_request, std::vector<std::string> prespecification)
    {
        int search_request_place = 0;
        open_file();
        file_obj.seekp(0);
        file_obj.seekg(0);
        
        int continue_search = 1;
        std::vector<std::string> buffer;
        buffer.resize(5);
        buffer.at(0) = "";
        buffer.at(1) = "";
        buffer.at(2) = "";
        buffer.at(3) = "";
        buffer.at(4) = "";

        while (continue_search > 0)
        {
            buffer.at(4) = buffer.at(3);
            buffer.at(3) = buffer.at(2);
            buffer.at(2) = buffer.at(1);
            buffer.at(1) = buffer.at(0);
            file_obj >> buffer.at(0);

            if(buffer.at(1) == search_request && buffer.at(2) == prespecification.at(0) && buffer.at(3) == prespecification.at(1) && buffer.at(4) == prespecification.at(2))
            {
                file_obj.seekp(file_obj.tellp() - length(buffer.at(0)));
                file_obj.seekg(file_obj.tellg() - length(buffer.at(0)));

                continue_search = 0;
                search_request_place = file_obj.tellp() * (file_obj.tellg() == file_obj.tellp()) - 1 * (!(file_obj.tellg() == file_obj.tellp()));
                std::cout << "POSCODE STRING SEARCH: Строка найдена!" << std::endl << "Строка: " << buffer.at(0) << std::endl << "Положение её начала:" << search_request_place << std::endl;
                return search_request_place;
            }

            else if((buffer.at(0) == "" && buffer.at(1) == "pos_end") || (buffer.at(0) == "pos_end" && buffer.at(1) == "pos_end") || (buffer.at(0) == "" && buffer.at(1) == ""))
            {
                std::cout << "Строка не найдена. Завершаю..." << std::endl;
                continue_search = 0;
                return 0;
            }
        }
        file_obj.seekp(0);
        file_obj.seekg(0);
        close_file();
    }


    void replace_string(std::string input_string, std::string str_to_replace, std::vector<std::string> position_code)
    {
        std::cout << std::endl << std::endl << "REPLACE ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
        open_file();
        file_obj.seekp(search_for_string_poscode3(str_to_replace, position_code));
        file_obj >> input_string;

        close_file();
    }


    void print_template_size() // Выводит в консоли размер шаблона
    {
        std::cout << "Размер шаблона равен --> " << template_size << std::endl; 
    }


    void print_template() // Выводит в консоли текст шаблона
    {
        std::cout << std::endl << position_template << std::endl;
    }


    void open_file()
    {
        if(!file_obj.is_open())
        {
            file_obj.open(filename);
        }
    }


    void close_file()
    {
        if(file_obj.is_open())
        {
            file_obj.close();
        }
    }
};

// ==================================================================================================================
// ==================================================================================================================


int main()
{
    setlocale(LC_ALL, "rus");

    bd file("file.txt");
    file.print_template_size();
    file.print_template();
    file.initPosition();
    file.get_position_numbers();
    file.search_for_string_poscode("pos_number");
    
    std::vector<std::string> prespec;
    prespec.resize(3);
    prespec.at(0) = "-->";
    prespec.at(1) = "позиции";
    prespec.at(2) = "Номер";

    file.replace_string("1", "var", prespec);

    return 0;
}

int length(std::string str) // Считает и возвращает длину передаваемой строки
{
    return str.length();
    }



// ==================================================================================================================
// ==================================================================================================================
// ==================================================================================================================
// ==================================================================================================================
// ==================================================================================================================
// ==================================================================================================================
// ==================================================================================================================
// ==================================================================================================================
// ==================================================================================================================
// ==================================================================================================================
// ==================================================================================================================

    // void meta_resize_plusone(std::vector<position_meta>& meta, int n) // Увеличивает размер вектора метаданных позиций на 1
    // {
    //     std::vector<position_meta> meta_new;
    //     meta_new.resize(meta.capacity() + 1);
    //     int DEBUG = meta.capacity();
    //     for(int i = 0; i < meta.capacity(); i++)
    //     {
    //         meta_new.at(i).seq_number = meta.at(i).seq_number;
    //         meta_new.at(i).start_pos = meta.at(i).start_pos;
    //         meta_new.at(i).end_pos = meta.at(i).end_pos;
    //     }
    //     meta_new.at(meta.capacity()).seq_number = meta.capacity() + 1;
    //     meta_new.at(meta.capacity()).end_pos = 0;
    //     meta_new.at(meta.capacity()).start_pos = 0;
    //     meta.resize(meta.size() + 1); // Раньше было meta_new.capacity() или meta.capacity(), но они на третьей итерации выдавали на этой строчке значение 4 :/
    //     DEBUG = meta.capacity();
    //     int DEBUG_NEW = meta_new.capacity();
        
    //     if (n == 3)
    //     {
    //         meta.resize(meta.capacity() - 1);
    //     }

    //     for(int i = 0; i < meta.capacity(); i++)
    //     {
    //         meta.at(i).seq_number = meta_new.at(i).seq_number;
    //         meta.at(i).start_pos = meta_new.at(i).start_pos;
    //         meta.at(i).end_pos = meta_new.at(i).end_pos;
    //     }
    // }
