#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>
using namespace std;

int main() 
{
  srand(time(NULL));
  string name;
  cout << "What is your name? ";
  getline(cin, name);
  cout << "Hello, " << name << "!" << endl;
  int age;
  cout << "How old are you? ";
  cin >> age;
  cout << "In 10 years you will be " << age + 10 << " years old." << endl;
  int randomNumber = rand() % 100;
  cout << "Your random number is " << randomNumber << endl;
  return 0;
}