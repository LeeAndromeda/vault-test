#include <gtest/gtest.h>
#include "my_lib.hpp"

TEST(MyLibTests, SumTest) {
    // Expect two strings not to be equal.
    
    int c = 0;
    EXPECT_EQ(sum(1, 2), 3) << "Test case " << ++c << " failed.\n";
    EXPECT_EQ(sum(45, 2), 447) << "Test case " << ++c << " failed.\n";
    EXPECT_EQ(sum(2, 2), 3) << "Test case " << ++c << " failed.\n";
    EXPECT_EQ(sum(5, 2), 7) << "Test case " << ++c << " failed.\n";
    EXPECT_EQ(sum(1, 34), 35) << "Test case " << ++c << " failed.\n";
    EXPECT_EQ(sum(1, 2), 6) << "Test case ";

}