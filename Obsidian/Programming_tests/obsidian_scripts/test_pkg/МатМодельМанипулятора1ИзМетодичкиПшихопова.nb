(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 13.1' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     20284,        503]
NotebookOptionsPosition[     18463,        461]
NotebookOutlinePosition[     18928,        479]
CellTagsIndexPosition[     18885,        476]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["\:041c\:0430\:043d\:0438\:043f\:0443\:043b\:044f\:0442\:043e\:0440\:044b\
", "Title",
 CellChangeTimes->{{3.889544311821495*^9, 3.88954431556785*^9}, {
   3.8895694467205*^9, 3.8895694503648167`*^9}, 
   3.8896740187255793`*^9},ExpressionUUID->"cf96a1e4-08ad-46fc-90b6-\
2bd8b1c6d792"],

Cell[CellGroupData[{

Cell["\:041c\:0430\:0442\:0440\:0438\:0446\:044b \:043f\:043e\:0432\:043e\
\:0440\:043e\:0442\:0430 \:0414\:0435\:043d\:0430\:0432\:0438\:0442\:0430-\
\:0425\:0430\:0440\:0442\:0435\:043d\:0431\:0435\:0440\:0433\:0430", \
"Subsubsection",
 CellChangeTimes->{{3.8895695726771584`*^9, 3.889569576672587*^9}, {
  3.889570394506775*^9, 
  3.8895704106778717`*^9}},ExpressionUUID->"ec0812b4-ffec-4cab-9870-\
3b05dc5bf023"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"RotateVecY", "[", 
   RowBox[{"vec_", ",", "\[Phi]_"}], "]"}], ":=", 
  RowBox[{"Module", "[", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"{", "q2", "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"q2", "=", 
      RowBox[{
       RowBox[{"(", GridBox[{
          {
           RowBox[{"Cos", "[", "\[Phi]", "]"}], "0", 
           RowBox[{"Sin", "[", "\[Phi]", "]"}], "0"},
          {"0", "1", "0", "0"},
          {
           RowBox[{"-", 
            RowBox[{"Sin", "[", "\[Phi]", "]"}]}], "0", 
           RowBox[{"Cos", "[", "\[Phi]", "]"}], "0"},
          {"0", "0", "0", "1"}
         }], ")"}], ".", "vec"}]}], ";", "\[IndentingNewLine]", "q2"}]}], 
   "\[IndentingNewLine]", "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"RotateVecX", "[", 
   RowBox[{"vec_", ",", "\[Phi]_"}], "]"}], ":=", 
  RowBox[{"Module", "[", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"{", "q2", "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"q2", "=", 
      RowBox[{
       RowBox[{"(", GridBox[{
          {"1", "0", "0", "0"},
          {"0", 
           RowBox[{"Cos", "[", "\[Phi]", "]"}], 
           RowBox[{"-", 
            RowBox[{"Sin", "[", "\[Phi]", "]"}]}], "0"},
          {"0", 
           RowBox[{"Sin", "[", "\[Phi]", "]"}], 
           RowBox[{"Cos", "[", "\[Phi]", "]"}], "0"},
          {"0", "0", "0", "1"}
         }], ")"}], ".", "vec"}]}], ";", "\[IndentingNewLine]", "q2"}]}], 
   "\[IndentingNewLine]", "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"RotateVecZ", "[", 
   RowBox[{"vec_", ",", "\[Phi]_"}], "]"}], ":=", 
  RowBox[{"Module", "[", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"{", "q2", "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"q2", "=", 
      RowBox[{
       RowBox[{"(", GridBox[{
          {
           RowBox[{"Cos", "[", "\[Phi]", "]"}], 
           RowBox[{"-", 
            RowBox[{"Sin", "[", "\[Phi]", "]"}]}], "0", "0"},
          {
           RowBox[{"Sin", "[", "\[Phi]", "]"}], 
           RowBox[{"Cos", "[", "\[Phi]", "]"}], "0", "0"},
          {"0", "0", "1", "0"},
          {"0", "0", "0", "1"}
         }], ")"}], ".", "vec"}]}], ";", "\[IndentingNewLine]", "q2"}]}], 
   "\[IndentingNewLine]", "]"}]}]}], "Input",
 CellChangeTimes->{{3.889537593931925*^9, 3.8895376257418528`*^9}, {
  3.889538324052432*^9, 3.889538380076071*^9}, {3.8895384711277*^9, 
  3.8895385277400427`*^9}, {3.889538631141878*^9, 3.8895386558073893`*^9}, {
  3.8895392760262613`*^9, 3.88953938129533*^9}, {3.889540659007934*^9, 
  3.889540673925828*^9}, {3.889540872927286*^9, 3.8895408734148827`*^9}, {
  3.889569481246451*^9, 3.889569500947926*^9}, {3.889570366939414*^9, 
  3.889570435672653*^9}},
 CellLabel->
  "In[405]:=",ExpressionUUID->"b3938530-45b4-4ddf-abe9-b5a04ea10e0f"]
}, Open  ]],

Cell[CellGroupData[{

Cell["\:041c\:0430\:0442\:0440\:0438\:0446\:0430 \:0414\:0435\:043d\:0430\
\:0432\:0438\:0442\:0430-\:0425\:0430\:0440\:0442\:0435\:043d\:0431\:0435\
\:0440\:0433\:0430", "Subsubsection",
 CellChangeTimes->{{3.889569507797653*^9, 
  3.8895695291866302`*^9}},ExpressionUUID->"7adcb6cd-344a-499a-8b7e-\
ae9a83da50c4"],

Cell[BoxData[
 RowBox[{
  RowBox[{"DHMatrix", "[", "tmx_", "]"}], ":=", 
  RowBox[{"Module", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{
     "d", ",", "a", ",", "theta", ",", "alpha", ",", "costheta", ",", 
      "sintheta", ",", "cosalpha", ",", "sinalpha"}], "}"}], ",", 
    "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"d", "=", 
      RowBox[{"tmx", "[", 
       RowBox[{"[", 
        RowBox[{"1", ",", "1"}], "]"}], "]"}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"a", " ", "=", " ", 
      RowBox[{"tmx", "[", 
       RowBox[{"[", 
        RowBox[{"1", ",", "2"}], "]"}], "]"}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"\[Theta]", " ", "=", " ", 
      RowBox[{"tmx", "[", 
       RowBox[{"[", 
        RowBox[{"1", ",", "3"}], "]"}], "]"}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"\[Alpha]", " ", "=", " ", 
      RowBox[{"tmx", "[", 
       RowBox[{"[", 
        RowBox[{"1", ",", "4"}], "]"}], "]"}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"costheta", "=", 
      RowBox[{"Cos", "[", "\[Theta]", "]"}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"sintheta", "=", 
      RowBox[{"Sin", "[", "\[Theta]", "]"}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"cosalpha", "=", 
      RowBox[{"Cos", "[", "\[Alpha]", "]"}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"sinalpha", "=", 
      RowBox[{"Sin", "[", "\[Alpha]", "]"}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"A", "=", 
      RowBox[{"(", GridBox[{
         {"costheta", 
          RowBox[{
           RowBox[{"-", "cosalpha"}], "*", "sintheta"}], 
          RowBox[{"sinalpha", "*", "sintheta"}], 
          RowBox[{"a", "*", "costheta"}]},
         {"sintheta", 
          RowBox[{"cosalpha", "*", "costheta"}], 
          RowBox[{
           RowBox[{"-", "sinalpha"}], "*", "costheta"}], 
          RowBox[{"a", "*", "sintheta"}]},
         {"0", "sinalpha", "cosalpha", "d"},
         {"0", "0", "0", "1"}
        }], ")"}]}], ";", "\[IndentingNewLine]", "A"}]}], 
   "\[IndentingNewLine]", "]"}]}]], "Input",
 CellChangeTimes->{{3.889537593931925*^9, 3.8895376257418528`*^9}, {
   3.889538324052432*^9, 3.889538380076071*^9}, {3.8895384711277*^9, 
   3.8895385277400427`*^9}, {3.889538631141878*^9, 3.8895386558073893`*^9}, {
   3.8895392760262613`*^9, 3.88953938129533*^9}, {3.889540659007934*^9, 
   3.889540673925828*^9}, {3.889540872927286*^9, 3.8895408734148827`*^9}, {
   3.889569481246451*^9, 3.889569500947926*^9}, 3.889570468204865*^9, {
   3.889581521303581*^9, 3.889581528030072*^9}, {3.889674033553377*^9, 
   3.889674069868245*^9}, {3.889683024689513*^9, 3.889683024836269*^9}},
 CellLabel->
  "In[408]:=",ExpressionUUID->"b30e68ba-08ae-4eab-8ea5-afa636ebf531"]
}, Open  ]],

Cell[CellGroupData[{

Cell["\:0424\:0443\:043d\:043a\:0446\:0438\:044f \:043f\:043e\:043b\:0443\
\:0447\:0435\:043d\:0438\:044f \:0432\:0435\:043a\:0442\:043e\:0440\:0430 \
\:043a\:043e\:043e\:0440\:0434\:0438\:043d\:0430\:0442", "Subsubsection",
 CellChangeTimes->{{3.889570575410907*^9, 
  3.8895705989152517`*^9}},ExpressionUUID->"30381ec9-dc14-43c6-b0dd-\
e564eccadfc1"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"GetCoordsFromDH", "[", "mx_", "]"}], ":=", 
  RowBox[{"Module", "[", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"{", "q", "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{"q", "=", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"mx", "[", 
         RowBox[{"[", 
          RowBox[{"1", ",", "4"}], "]"}], "]"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"mx", "[", 
         RowBox[{"[", 
          RowBox[{"2", ",", "4"}], "]"}], "]"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"mx", "[", 
         RowBox[{"[", 
          RowBox[{"3", ",", "4"}], "]"}], "]"}], "}"}]}], "}"}]}]}], 
   "\[IndentingNewLine]", "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"GetPointFromDH", "[", "mx_", "]"}], ":=", 
  RowBox[{"Module", "[", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"{", "q", "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{"q", "=", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"mx", "[", 
        RowBox[{"[", 
         RowBox[{"1", ",", "4"}], "]"}], "]"}], ",", " ", 
       RowBox[{"mx", "[", 
        RowBox[{"[", 
         RowBox[{"2", ",", "4"}], "]"}], "]"}], ",", " ", 
       RowBox[{"mx", "[", 
        RowBox[{"[", 
         RowBox[{"3", ",", "4"}], "]"}], "]"}]}], "}"}]}]}], 
   "\[IndentingNewLine]", "]"}]}]}], "Input",
 CellChangeTimes->{{3.889570604068956*^9, 3.88957074176528*^9}, {
  3.8895707983598833`*^9, 3.8895708176679287`*^9}, {3.889696208973885*^9, 
  3.8896962738777637`*^9}},
 CellLabel->
  "In[409]:=",ExpressionUUID->"7bd82504-9a8f-48dc-b286-7679cbeed8ca"]
}, Open  ]],

Cell[CellGroupData[{

Cell["\:0424\:0443\:043d\:043a\:0446\:0438\:044f \:0440\:0438\:0441\:043e\
\:0432\:0430\:043d\:0438\:044f \:043b\:0438\:043d\:0438\:0439", \
"Subsubsection",
 CellChangeTimes->{{3.889682852119169*^9, 
  3.889682860310645*^9}},ExpressionUUID->"cb6c3282-1489-4159-ab6d-\
7e78e23c9155"],

Cell[BoxData[
 RowBox[{
  RowBox[{"linePlot", "[", 
   RowBox[{"point1_", ",", " ", "point2_"}], "]"}], ":=", 
  RowBox[{"Module", "[", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"{", "line", "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"line", " ", "=", 
      RowBox[{"Graphics3D", "[", "\[IndentingNewLine]", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"Thickness", "->", " ", "0.01"}], ",", 
         RowBox[{"Line", "[", 
          RowBox[{"{", 
           RowBox[{"point1", ",", "point2"}], "}"}], "]"}]}], "}"}], 
       "\[IndentingNewLine]", "]"}]}], ";", "\[IndentingNewLine]", "line"}]}],
    "\[IndentingNewLine]", "]"}]}]], "Input",
 CellChangeTimes->{{3.889682882340343*^9, 3.889682996744247*^9}, {
  3.8896833022653513`*^9, 3.8896833324783487`*^9}},
 CellLabel->
  "In[411]:=",ExpressionUUID->"d7c66840-8e70-4a0f-bd52-dd73d16d8f89"]
}, Open  ]],

Cell[CellGroupData[{

Cell["\:0420\:0430\:0441\:0441\:0447\:0435\:0442", "Subsubsection",
 CellChangeTimes->{{3.889569656951521*^9, 3.889569668256423*^9}, {
  3.889696502701075*^9, 
  3.889696506172984*^9}},ExpressionUUID->"5630edec-b513-4b39-bb2d-\
e1911566bc04"],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"Clear", "[", 
   RowBox[{
   "l1", ",", " ", "q1", ",", "k1", ",", "q2", ",", "k2", ",", "q3", ",", 
    "k3"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.8896978623773623`*^9, 3.889697879942338*^9}},
 CellLabel->
  "In[412]:=",ExpressionUUID->"22bf01a9-4a50-48d7-a0a2-b8643f6f9514"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"Manipulate", "[", "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"l1", "=", "10"}], ";", "\[IndentingNewLine]", 
    RowBox[{"k1", "=", "2.5"}], ";", "\[IndentingNewLine]", 
    RowBox[{"k2", "=", "2.5"}], ";", "\[IndentingNewLine]", 
    RowBox[{"k3", "=", "2.5"}], ";", "\[IndentingNewLine]", "\n", 
    RowBox[{"link1", "=", 
     RowBox[{"(", GridBox[{
        {"q1", "k1", 
         FractionBox["\[Pi]", "2"], "0"}
       }], ")"}]}], ";", "\[IndentingNewLine]", 
    RowBox[{"link2", "=", 
     RowBox[{"(", GridBox[{
        {"0", "0", 
         RowBox[{
          FractionBox[
           RowBox[{"-", "\[Pi]"}], "2"], "+", "q2"}], 
         FractionBox["\[Pi]", "2"]}
       }], ")"}]}], ";", "\[IndentingNewLine]", 
    RowBox[{"link3", "=", 
     RowBox[{"(", GridBox[{
        {"k2", "0", "\[Pi]", 
         FractionBox["\[Pi]", "2"]}
       }], ")"}]}], ";", "\[IndentingNewLine]", 
    RowBox[{"endEffector", "=", 
     RowBox[{"(", GridBox[{
        {"0", 
         RowBox[{"-", "k3"}], 
         RowBox[{
          FractionBox["\[Pi]", "2"], "+", "q3"}], "0"}
       }], ")"}]}], ";", "\n", "\n", 
    RowBox[{"A01", " ", "=", " ", 
     RowBox[{"DHMatrix", "[", "link1", "]"}]}], ";", "\[IndentingNewLine]", 
    RowBox[{"A12", " ", "=", " ", 
     RowBox[{"DHMatrix", "[", "link2", "]"}]}], ";", "\[IndentingNewLine]", 
    RowBox[{"A23", " ", "=", " ", 
     RowBox[{"DHMatrix", "[", "link3", "]"}]}], ";", "\[IndentingNewLine]", 
    RowBox[{"A34", "=", 
     RowBox[{"DHMatrix", "[", "endEffector", "]"}]}], ";", 
    "\[IndentingNewLine]", 
    RowBox[{"A02", "=", 
     RowBox[{"A01", ".", "A12"}]}], ";", "\[IndentingNewLine]", 
    RowBox[{"A03", "=", 
     RowBox[{"A02", ".", "A23"}]}], ";", "\[IndentingNewLine]", 
    RowBox[{"A04", "=", 
     RowBox[{"A03", ".", "A34"}]}], ";", "\[IndentingNewLine]", "\n", "\n", 
    RowBox[{"linkPl1", "=", 
     RowBox[{"linePlot", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"0", ",", "0", ",", "0"}], "}"}], ",", 
       RowBox[{"GetPointFromDH", "[", "A01", "]"}]}], "]"}]}], ";", "\n", 
    RowBox[{"linkPl2", "=", 
     RowBox[{"linePlot", "[", 
      RowBox[{
       RowBox[{"GetPointFromDH", "[", "A01", "]"}], ",", 
       RowBox[{"GetPointFromDH", "[", "A02", "]"}]}], "]"}]}], ";", "\n", 
    RowBox[{"linkPl3", "=", 
     RowBox[{"linePlot", "[", 
      RowBox[{
       RowBox[{"GetPointFromDH", "[", "A02", "]"}], ",", 
       RowBox[{"GetPointFromDH", "[", "A03", "]"}]}], "]"}]}], ";", "\n", 
    RowBox[{"linkPl4", "=", 
     RowBox[{"linePlot", "[", 
      RowBox[{
       RowBox[{"GetPointFromDH", "[", "A03", "]"}], ",", 
       RowBox[{"GetPointFromDH", "[", "A04", "]"}]}], "]"}]}], ";", "\n", 
    RowBox[{"Show", "[", 
     RowBox[{"linkPl1", ",", "linkPl2", ",", "linkPl3", ",", " ", "linkPl4"}],
      "]"}]}], ",", "\[IndentingNewLine]", 
   RowBox[{"{", 
    RowBox[{"q1", ",", "0", ",", "5"}], "}"}], ",", "\[IndentingNewLine]", 
   RowBox[{"{", 
    RowBox[{"q2", ",", "0", ",", 
     RowBox[{"2", "\[Pi]"}]}], "}"}], ",", "\[IndentingNewLine]", 
   RowBox[{"{", 
    RowBox[{"q3", ",", "0", ",", 
     RowBox[{"2", "\[Pi]"}]}], "}"}]}], "\[IndentingNewLine]", "\n", 
  "]"}], "\n", 
 RowBox[{"                     "}]}], "Input",
 CellChangeTimes->{{3.889695886320216*^9, 3.88969595559579*^9}, {
  3.889696508478047*^9, 3.889696518037202*^9}, {3.889697042469467*^9, 
  3.889697044461837*^9}, {3.8896978191195927`*^9, 3.88969785701936*^9}, {
  3.8896978877245092`*^9, 3.889697893673485*^9}, {3.889697926679007*^9, 
  3.889697991186853*^9}, {3.889698088322912*^9, 3.889698114191427*^9}, {
  3.8896981853181047`*^9, 3.889698189042541*^9}, {3.889698231643403*^9, 
  3.889698246374916*^9}},
 CellLabel->
  "In[413]:=",ExpressionUUID->"ad3abe99-f356-42ba-a79e-b9f285a7cb0f"],

Cell[BoxData[
 TagBox[
  StyleBox[
   DynamicModuleBox[{$CellContext`q1$$ = 5., $CellContext`q2$$ = 
    2.6138050877867083`, $CellContext`q3$$ = 2.27451308119901, 
    Typeset`show$$ = True, Typeset`bookmarkList$$ = {}, 
    Typeset`bookmarkMode$$ = "Menu", Typeset`animator$$, Typeset`animvar$$ = 
    1, Typeset`name$$ = "\"untitled\"", Typeset`specs$$ = {{
      Hold[$CellContext`q1$$], 0, 5}, {
      Hold[$CellContext`q2$$], 0, 2 Pi}, {
      Hold[$CellContext`q3$$], 0, 2 Pi}}, Typeset`size$$ = {
    288., {93., 96.34641069481648}}, Typeset`update$$ = 0, Typeset`initDone$$,
     Typeset`skipInitDone$$ = True}, 
    DynamicBox[Manipulate`ManipulateBoxes[
     1, StandardForm, 
      "Variables" :> {$CellContext`q1$$ = 0, $CellContext`q2$$ = 
        0, $CellContext`q3$$ = 0}, "ControllerVariables" :> {}, 
      "OtherVariables" :> {
       Typeset`show$$, Typeset`bookmarkList$$, Typeset`bookmarkMode$$, 
        Typeset`animator$$, Typeset`animvar$$, Typeset`name$$, 
        Typeset`specs$$, Typeset`size$$, Typeset`update$$, Typeset`initDone$$,
         Typeset`skipInitDone$$}, 
      "Body" :> ($CellContext`l1 = 10; $CellContext`k1 = 2.5; $CellContext`k2 = 
        2.5; $CellContext`k3 = 
        2.5; $CellContext`link1 = {{$CellContext`q1$$, $CellContext`k1, Pi/2, 
           0}}; $CellContext`link2 = {{
          0, 0, -Pi/2 + $CellContext`q2$$, Pi/
           2}}; $CellContext`link3 = {{$CellContext`k2, 0, Pi, Pi/
           2}}; $CellContext`endEffector = {{
          0, -$CellContext`k3, Pi/2 + $CellContext`q3$$, 
           0}}; $CellContext`A01 = $CellContext`DHMatrix[$CellContext`link1]; \
$CellContext`A12 = $CellContext`DHMatrix[$CellContext`link2]; \
$CellContext`A23 = $CellContext`DHMatrix[$CellContext`link3]; \
$CellContext`A34 = $CellContext`DHMatrix[$CellContext`endEffector]; \
$CellContext`A02 = Dot[$CellContext`A01, $CellContext`A12]; $CellContext`A03 = 
        Dot[$CellContext`A02, $CellContext`A23]; $CellContext`A04 = 
        Dot[$CellContext`A03, $CellContext`A34]; $CellContext`linkPl1 = \
$CellContext`linePlot[{0, 0, 0}, 
          $CellContext`GetPointFromDH[$CellContext`A01]]; \
$CellContext`linkPl2 = $CellContext`linePlot[
          $CellContext`GetPointFromDH[$CellContext`A01], 
          $CellContext`GetPointFromDH[$CellContext`A02]]; \
$CellContext`linkPl3 = $CellContext`linePlot[
          $CellContext`GetPointFromDH[$CellContext`A02], 
          $CellContext`GetPointFromDH[$CellContext`A03]]; \
$CellContext`linkPl4 = $CellContext`linePlot[
          $CellContext`GetPointFromDH[$CellContext`A03], 
          $CellContext`GetPointFromDH[$CellContext`A04]]; 
       Show[$CellContext`linkPl1, $CellContext`linkPl2, $CellContext`linkPl3, \
$CellContext`linkPl4]), 
      "Specifications" :> {{$CellContext`q1$$, 0, 5}, {$CellContext`q2$$, 0, 
         2 Pi}, {$CellContext`q3$$, 0, 2 Pi}}, "Options" :> {}, 
      "DefaultOptions" :> {}],
     ImageSizeCache->{325., {148.849609375, 153.150390625}},
     SingleEvaluation->True],
    Deinitialization:>None,
    DynamicModuleValues:>{},
    SynchronousInitialization->True,
    UndoTrackedVariables:>{Typeset`show$$, Typeset`bookmarkMode$$},
    UnsavedVariables:>{Typeset`initDone$$},
    UntrackedVariables:>{Typeset`size$$}], "Manipulate",
   Deployed->True,
   StripOnInput->False],
  Manipulate`InterpretManipulate[1]]], "Output",
 CellChangeTimes->{
  3.889697882317564*^9, 3.8896979925068913`*^9, {3.889698094032984*^9, 
   3.889698114925816*^9}, 3.889698189918157*^9, 3.8896982474592237`*^9},
 CellLabel->
  "Out[413]=",ExpressionUUID->"c5dc79b1-8c60-4e59-bbc9-225664843bc1"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1440., 768.75},
WindowMargins->{{0, Automatic}, {0, Automatic}},
TaggingRules-><|"TryRealOnly" -> False|>,
Magnification:>0.8 Inherited,
FrontEndVersion->"13.1 for Linux x86 (64-bit) (June 16, 2022)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"3a8bd03b-5552-469b-b037-34b30c7f688e"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 291, 5, 78, "Title",ExpressionUUID->"cf96a1e4-08ad-46fc-90b6-2bd8b1c6d792"],
Cell[CellGroupData[{
Cell[896, 31, 417, 7, 36, "Subsubsection",ExpressionUUID->"ec0812b4-ffec-4cab-9870-3b05dc5bf023"],
Cell[1316, 40, 2820, 72, 409, "Input",ExpressionUUID->"b3938530-45b4-4ddf-abe9-b5a04ea10e0f"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4173, 117, 315, 5, 36, "Subsubsection",ExpressionUUID->"7adcb6cd-344a-499a-8b7e-ae9a83da50c4"],
Cell[4491, 124, 2677, 60, 254, "Input",ExpressionUUID->"b30e68ba-08ae-4eab-8ea5-afa636ebf531"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7205, 189, 352, 5, 36, "Subsubsection",ExpressionUUID->"30381ec9-dc14-43c6-b0dd-e564eccadfc1"],
Cell[7560, 196, 1589, 44, 139, "Input",ExpressionUUID->"7bd82504-9a8f-48dc-b286-7679cbeed8ca"]
}, Open  ]],
Cell[CellGroupData[{
Cell[9186, 245, 283, 5, 36, "Subsubsection",ExpressionUUID->"cb6c3282-1489-4159-ab6d-7e78e23c9155"],
Cell[9472, 252, 881, 21, 128, "Input",ExpressionUUID->"d7c66840-8e70-4a0f-bd52-dd73d16d8f89"]
}, Open  ]],
Cell[CellGroupData[{
Cell[10390, 278, 242, 4, 36, "Subsubsection",ExpressionUUID->"5630edec-b513-4b39-bb2d-e1911566bc04"],
Cell[10635, 284, 340, 8, 41, "Input",ExpressionUUID->"22bf01a9-4a50-48d7-a0a2-b8643f6f9514"],
Cell[CellGroupData[{
Cell[11000, 296, 3814, 90, 522, "Input",ExpressionUUID->"ad3abe99-f356-42ba-a79e-b9f285a7cb0f"],
Cell[14817, 388, 3606, 68, 328, "Output",ExpressionUUID->"c5dc79b1-8c60-4e59-bbc9-225664843bc1"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
}
]
*)

