import os
import re
import shutil

def extract_filenames_from_markdown(file_path):
    with open(file_path, 'r') as markdown_file:
        content = markdown_file.read()
    
    # Define regex patterns to match markdown links
    pattern = r'\[\[([\w\s.]+?)(?:\|[\w\s.]+)?\]\]|\!\[\[([\w\s.]+?)(?:\|[\w\s.]+)?\]\]'

    # Extract filenames from markdown content
    filenames = re.findall(pattern, content)
    filenames = [file[0] or file[1] for file in filenames if file[0] or file[1]]

    return filenames

def copy_files(filenames, source_directory, target_directory):
    for filename in filenames:
        source_file_path = os.path.join(source_directory, filename)
        target_file_path = os.path.join(target_directory, filename)
        if os.path.exists(source_file_path):
            shutil.copy(source_file_path, target_file_path)




##############################
# WORKING ZONE


# Specify the paths and directories
markdown_file_path = '/home/leevroko/Vaults/Полезные ресурсы.md'
source_directory = '/home/leevroko/Vaults'
target_directory = '/home/leevroko/Vaults/Obsidian/Programming_tests/obsidian_scripts/test_pkg'

# Extract filenames from the markdown file
filenames = extract_filenames_from_markdown(markdown_file_path)

print(filenames)

# Copy files to the target directory
copy_files(filenames, source_directory, target_directory)

# Print the list of copied file paths
copied_file_paths = [os.path.join(target_directory, filename) for filename in filenames]
print('Copied file paths:', copied_file_paths)
