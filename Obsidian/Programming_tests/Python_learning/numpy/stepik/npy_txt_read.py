import numpy as np
from urllib.request import urlopen

f = urlopen('https://stepik.org/media/attachments/lesson/16462/boston_houses.csv')

input_txt = np.loadtxt(f, usecols=(0,1,2,3), skiprows=1, delimiter=",")

result = input_txt.mean(axis=1)
print(result)