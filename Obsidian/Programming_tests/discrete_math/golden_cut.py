import numpy as np
import math as m
import matplotlib.pyplot as plt

#  Функция поиска производной в точке
def derivative(func, arg: float) -> float:
    arg1 = arg - 0.0000001
    arg2 = arg + 0.0000001

    der = (func(arg2) - func(arg1)) / (arg2 - arg1)
    
    return der


# Функция поиска экстремума функции методом золотого сечения
def golden_cut(func, lim_a: float, lim_b: float, threshold: float, sign) -> float:
    counter = 1
    lim_a_init = lim_a
    lim_b_init = lim_b
    while True:
        x1 = lim_a + (lim_b - lim_a) * (3 - 5**(1/2)) / 2
        x2 = lim_a + (lim_b - lim_a) * (5**(1/2) - 1) / 2
        y1 = sign * func(x1)
        y2 = sign * func(x2)
        if (y1 < y2):
            lim_b = x2
            der = derivative(func, x1)
            if abs(der) < threshold:
                X = np.arange(lim_a_init, lim_b_init, 0.001)
                Y = func(X)
                fig, ax = plt.subplots()
                ax.scatter(X, Y)
                ax.scatter(x1, sign * y1, c="r", s=100)
                
                plt.show()
                print(f"Finished after {counter} times")
                return (x1, sign * y1, der)
        else:
            lim_a = x1
            der = derivative(func, x2)
            if abs(der) < threshold:
                X = np.arange(lim_a_init, lim_b_init, 0.001)
                Y = func(X)
                fig, ax = plt.subplots()
                ax.scatter(X, Y)
                ax.scatter(x2, sign * y2, c="r", s=100)
                plt.show()
                print(f"Finished after {counter} times")
                return (x2, sign * y2, der)
        counter += 1
    

# Функция 6 варианта
def ln_x_plus_3_cos_x(arg):
    return np.log(arg) + 3 * np.cos(arg)
    

# Функция 7 варианта
def ln_x_squared(arg):
    return np.log(arg)**2

# Функция 8 вариана
def ln_x_minus_1_x_sin_plus_088(arg):
    return (np.log(arg) - 1) * np.sin(arg) + 0.88

# Поиск максимума для функции 6 варианта
print(golden_cut(ln_x_plus_3_cos_x, 0.1, 1.0, 0.00001, -1))

# Поиск минимума функции для 7 варианта
print(golden_cut(ln_x_squared, 0.6, 1.35, 0.00001, 1))

# Поиск минмума функции для 8 варианта
print(golden_cut(ln_x_minus_1_x_sin_plus_088, 0.5, 1.5, 0.000001, 1))

# Поиск максимума функции для 8 варианта
print(golden_cut(ln_x_minus_1_x_sin_plus_088, 2.5, 3.5, 0.000001, -1))
