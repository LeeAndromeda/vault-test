
import torch
from torch import nn # nn contains all of PyTorch's building blocks for neural networks
import matplotlib.pyplot as plt
import random

device = "cuda" if torch.cuda.is_available() else "cpu"

# Check PyTorch version
print(torch.__version__)

# # So here I had an idea to shuffle the tensor and return 
# # it to randomly choose tensor elements to include them 
# # into the training and testing datasets
# def pop_from_tensor(index: int, tensor_in: torch.Tensor):
#     tensor = tensor_in
#     val_to_pop = tensor[index]
#     tensor = torch.cat((tensor[0:index], tensor[index + 1:]))
#     return (val_to_pop, tensor)


# def shuffle(vec_in: torch.Tensor):
#     """
#     @brief Returns a shuffled vector comprised of the elements of vec_in
#     @param vec_in - the vector to be shuffled
#     @returns shuffled_vec - the shuffled vector
#     """
#     vec = vec_in
    
#     shuffled_vec = torch.tensor([])
    
#     while (len(vec) != 0):
#         i = random.randint(0, len(vec) - 1)
#         add_val, vec = pop_from_tensor(i, vec)
#         shuffled_vec.add(add_val)
#         print(shuffled_vec)
        
#     return shuffled_vec





# Lets create some data for the straight line
# Input some "known" data
bias = -2.3 # b from kx + b
weight = 3.7 # k from kx + b

# Create the data
start = 0
end = 1
step = 0.02

X = torch.arange(start, end, step).unsqueeze(dim=1).to(device) # Make a nx1 shaped matrix
y = (X ** weight + bias).to(device)


# Now we gotta split the data into training and testing sets
train_split = int(0.8 * len(X)) # 80% of data used for training set, 20% for testing 
X_train, y_train = X[:train_split].to(device), y[:train_split].to(device)
X_test, y_test = X[train_split:].to(device), y[train_split:len(X)].to(device)

# print(f"{len(X_train)} {len(y_train)} {len(X_test)} {len(y_test)}")



def plot_predictions(train_data=X_train, train_labels=y_train, test_data=X_test, test_labels=y_test, predictions: torch.Tensor=None):
    """
    Plots training data, test data and compares predictions.
    """
    plt.figure(figsize=(10, 7))

    # Plot training data in blue
    plt.scatter(train_data.cpu(), train_labels.cpu(), c="b", s=4, label="Training data")
    
    # Plot test data in green
    plt.scatter(test_data.cpu(), test_labels.cpu(), c="r", s=16, label="Testing data")

    if predictions is not None:
        # Plot the predictions in red (predictions were made on the test data)
        plt.scatter(test_data.cpu(), predictions.cpu(), c="g", s=10, label="Predictions")

    # Show the legend
    plt.legend(prop={"size": 14});
    plt.show()
plot_predictions()

# So now we need a model. Let's build one!
# Let's replicate a standard linear regression model using pure Pytorch
# Create a Linear Regression model class
class LinearRegressionModel(nn.Module): # <- almost everything in PyTorch is a nn.Module (think of this as neural network lego blocks)
    def __init__(self):
        super().__init__() 
        self.weights = nn.Parameter(torch.randn(1, # <- start with random weights (this will get adjusted as the model learns)
                                    dtype=torch.float,
                                    device=device), # <- PyTorch loves float32 by default
                                    requires_grad=True) # <- can we update this value with gradient descent?)

        self.bias = nn.Parameter(torch.randn(1, # <- start with random bias (this will get adjusted as the model learns)
                                dtype=torch.float,
                                device=device), # <- PyTorch loves float32 by default
                                requires_grad=True) # <- can we update this value with gradient descent?))

    # Forward defines the computation in the model
    def forward(self, x: torch.Tensor) -> torch.Tensor: # <- "x" is the input data (e.g. training/testing features)
        return x ** self.weights + self.bias # <- this is the linear regression formula (y = m*x + b)
    
    
# So we've made a model. Now let's create an instance of this model and check it's parameters
# Set manual seed since nn.Parameter are randomly initialzied
torch.manual_seed(42)

# Create an instance of the model (this is a subclass of nn.Module that contains nn.Parameter(s))
model_0 = LinearRegressionModel()
model_0.to(device=device)

# Check the nn.Parameter(s) within the nn.Module subclass we created
print(list(model_0.parameters()))

# We can also get the state of the model using the `.state_dict()`
print(model_0.state_dict())

# Make predictions with model
with torch.inference_mode(): 
    y_preds = model_0(X_test)
    # Check the predictions
    print(f"Number of testing samples: {len(X_test)}") 
    print(f"Number of predictions made: {len(y_preds)}")
    print(f"Predicted values:\n{y_preds}")
    plot_predictions(predictions=y_preds)

# Note: in older PyTorch code you might also see torch.no_grad()
# with torch.no_grad():
#   y_preds = model_0(X_test)

# Create the loss function
loss_fn = nn.L1Loss() # Mean Absolute Error loss is same as L1Loss

# Create the optimizer
optimizer = torch.optim.SGD(params=model_0.parameters(), # parameters of target model to optimize
                            lr=0.01) # learning rate (how much the optimizer should change parameters at each step, higher=more (less stable), lower=less (might take a long time))
scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer=optimizer)

torch.manual_seed(42)

# Set the number of epochs (how many times the model will pass over the training data)
epochs = 5000

# Create empty loss lists to track values
train_loss_values = []
test_loss_values = []
epoch_count = []

for epoch in range(epochs):
    ### Training

    # Put model in training mode (this is the default state of a model)
    model_0.train()

    # 1. Forward pass on train data using the forward() method inside 
    y_pred = model_0(X_train)
    # print(y_pred)

    # 2. Calculate the loss (how different are our models predictions to the ground truth)
    loss = loss_fn(y_pred, y_train)

    # 3. Zero grad of the optimizer
    optimizer.zero_grad()

    # 4. Loss backwards
    loss.backward()

    # 5. Progress the optimizer
    optimizer.step()

    scheduler.step(loss)
    ### Testing

    # Put the model in evaluation mode
    model_0.eval()

    with torch.inference_mode():
        # 1. Forward pass on test data
        test_pred = model_0(X_test)

        # 2. Caculate loss on test data
        test_loss = loss_fn(test_pred, y_test.type(torch.float)) # predictions come in torch.float datatype, so comparisons need to be done with tensors of the same type

        # Print out what's happening
        if epoch % 10 == 0:
            epoch_count.append(epoch)
            # train_loss_values.append(loss.detach().numpy())
            # test_loss_values.append(test_loss.detach().numpy())
            print(f"Epoch: {epoch} | MAE Train Loss: {loss} | MAE Test Loss: {test_loss} ")
            # plot_predictions(predictions=test_pred)
            
# Plot the loss curves
# plt.plot(epoch_count, train_loss_values, label="Train loss")
# plt.plot(epoch_count, test_loss_values, label="Test loss")
# plt.title("Training and test loss curves")
# plt.ylabel("Loss")
# plt.xlabel("Epochs")
# plt.legend();
# plt.show()

# Find our model's learned parameters
print("The model learned the following values for weights and bias:")
print(model_0.state_dict())
print("\nAnd the original values for weights and bias are:")
print(f"weights: {weight}, bias: {bias}")

# 1. Set the model in evaluation mode
model_0.eval()

# 2. Setup the inference mode context manager
with torch.inference_mode():
  # 3. Make sure the calculations are done with the model and data on the same device
  # in our case, we haven't setup device-agnostic code yet so our data and model are
  # on the CPU by default.
  # model_0.to(device)
  # X_test = X_test.to(device)
  y_preds = model_0(X_test)
y_preds

plot_predictions(predictions=y_preds)