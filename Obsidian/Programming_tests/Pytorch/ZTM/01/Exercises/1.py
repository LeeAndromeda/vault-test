##########################
# 1 ######################
# Create a straight line dataset using the linear regression formula (weight * X + bias).
# - Set weight=0.3 and bias=0.9 there should be at least 100 datapoints total.
# - Split the data into 80% training, 20% testing.
# - Plot the training and testing data so it becomes visual.

import torch
import torch.nn as nn
import matplotlib.pyplot as plt
import os
import random


# Create data
weight = 0.3
bias = 0.9
X_data = torch.arange(0, 100, 0.1)
Y_data = weight * X_data + bias

# Split the data into 80% training and 20% testing
test_size = int(len(X_data) * 0.8)
X_train, X_test = X_data[:test_size], X_data[test_size:]
Y_train, Y_test = Y_data[:test_size], Y_data[test_size:]

print(f"x test length: {len(X_test)}\ny test length: {len(Y_test)}")
print(f"x train length: {len(X_train)}\ny train length: {len(Y_train)}")
print(f"x data: \n{X_data[:10]}\ny data: {Y_data[:10]}")
input("Waiting for any key...")

def plot_predictions(train_data=X_train, train_labels=Y_train, test_data=X_test, test_labels=Y_test, predictions=None, real_data=None):
    """
    Plots training data, test data and compares predictions.
    """
    plt.figure(figsize=(10, 7))

    # Plot training data in blue
    plt.scatter(train_data, train_labels, c="b", s=4, label="Training data")
    
    # Plot test data in green
    plt.scatter(test_data, test_labels, c="r", s=16, label="Testing data")

    if predictions is not None:
        # Plot the predictions in red (predictions were made on the test data)
        plt.scatter(real_data, predictions, c="g", s=25, label="Predictions")

    # Show the legend
    plt.legend(prop={"size": 14});
    plt.show()



def save_checkpoint(state, filename='./checkpoint.pth.tar'):
    if os.path.isfile(filename):
        os.remove(filename)
    torch.save(state, filename)

# plot_predictions()


##########################
# 2 ######################
# Build a PyTorch model by subclassing nn.Module.
#
# Inside should be a randomly initialized nn.Parameter() with requires_grad=True, one for weights and one for bias.
# Implement the forward() method to compute the linear regression function you used to create the dataset in 1
# Once you've constructed the model, make an instance of it and check its state_dict().
# Note: If you'd like to use nn.Linear() instead of nn.Parameter() you can.

class LinearRegressionModel(torch.nn.Module):
    def __init__(self):
        super().__init__()
        
        # Initialize model parameters
        self.weights = nn.Parameter(torch.randn(1, requires_grad=True, dtype=torch.float))
        
        self.bias = nn.Parameter(torch.randn(1, requires_grad=True, dtype=torch.float))
        
    def forward(self, x: torch.Tensor) -> torch.Tensor:
        return self.weights * x + self.bias
    

model = LinearRegressionModel()
print(model.state_dict())

##########################
# 3 ######################
# Create a loss function and optimizer using nn.L1Loss() and torch.optim.SGD(params, lr) respectively.
# 
# Set the learning rate of the optimizer to be 0.01 and the parameters to optimize should be the model parameters from the model you created in 2.
# Write a training loop to perform the appropriate training steps for 300 epochs.
# The training loop should test the model on the test dataset every 20 epochs.

loss_fn = nn.L1Loss()
<<<<<<< HEAD
optimizer = torch.optim.SGD(params=model.parameters(), lr=0.01)
=======
optimizer = torch.optim.SGD(params=model.parameters(), lr=0.0001)
>>>>>>> f35ee1dca5de0501755324c8c6552ebbc21fd54d

def train():
    model.train()
    
    Y_pred = model(X_train)
    
    loss = loss_fn(Y_pred, Y_train)
    
    optimizer.zero_grad()
    
    loss.backward()
    
    optimizer.step()
    

def test(cur_ep: int):
    model.eval()
    
    with torch.inference_mode():
        test_pred = model(X_test)
        test_loss = loss_fn(test_pred, Y_test)
        print(f"######################\nEpoch: {cur_ep}\nLoss: {test_loss}")
    return test_loss
<<<<<<< HEAD
=======

slow_learn = False
test_period = 5
>>>>>>> f35ee1dca5de0501755324c8c6552ebbc21fd54d

epochs = 50000
for i in range(epochs):
    train()
    
<<<<<<< HEAD
    if i % 10 == 0:
        loss = test(i)
        if loss < 0.5:
            opt_st = optimizer.state_dict()
            print("################################\n################################\n################################\n################################\n################################\n################################\n################################\n################################\n################################\n################################\n################################\n################################\n")
            optimizer = torch.optim.SGD(params=model.parameters(), lr=1e-3)
            optimizer.load_state_dict(opt_st)
=======
    if i % test_period == 0:
        # test(i)
        loss_val = test(i)
        if loss_val < 0.3 and slow_learn == False:
            print("$$$$$$\n$$$$$$\n$$$$$$\n$$$$$$\n$$$$$$\n$$$$$$\n$$$$$$\n$$$$$$\n$$$$$$\n$$$$$$\n")
            opt_st = optimizer.state_dict()
            optimizer = torch.optim.SGD(params=model.parameters(), lr=0.00000001)
            optimizer.load_state_dict(opt_st)
            slow_learn == True
            test_period = 30
        

save_model_file = {
    'model_state' : model.state_dict(),
    'optimizer_state' : optimizer.state_dict()
}

save_name = './LinearRegression.mdl'
# save_checkpoint(save_model_file, save_name)

checkpoint = torch.load(save_name)
model.load_state_dict(checkpoint['model_state'])
with torch.inference_mode():
    model.eval()
    a = random.randint(0, 10)
    b = random.randint(1000, 9999)
    print(f'Model forward pass: {model(X_data[a:b])}\nY data: {Y_data[a:b]}')
    plot_predictions(predictions=model(X_data[a:b], ), real_data=X_data[a:b])
>>>>>>> f35ee1dca5de0501755324c8c6552ebbc21fd54d
