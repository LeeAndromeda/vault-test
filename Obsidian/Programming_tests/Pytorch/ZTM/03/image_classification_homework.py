
# Задание:
# Выполнить обучение сверточной нейросети с архитекутрой TinyVGG для классификации изображений цифр
# от 0 до 9 на датасете MNIST. 
# 
# Выполнил: студент группы РТбо3-41, Лещев-Романенко Андрей Васильевич, les@sfedu.ru

import torch
import pandas as pd
import os
from torchvision.datasets import MNIST
from torch import nn
from torchvision import transforms
from torch.utils.data import DataLoader
from matplotlib import pyplot as plt
from torchmetrics import ConfusionMatrix
from mlxtend.plotting import plot_confusion_matrix


EPOCHS = 6 # Количество эпох обучения сети
load_model = True # Загрузить созраненную модель из path_to_model_checkpoint?
save_model = False # Сохранить модель по результатам прогона в path_to_model_checkpoint?
train = False # Обучать модель?

path_to_model_checkpoint = "/home/leevroko/Vaults/Obsidian/Programming_tests/Pytorch/ZTM/image_classifier_v0.tar"


device = "cuda" if torch.cuda.is_available() else "cpu"
print(f"device: {device}")

### Загружаем датасе и создаем DataLoader'ы
mnist_train = MNIST('~/mist_data', train=True, download=True, transform=transforms.ToTensor())
mnist_test = MNIST('~/mist_data', train=False, download=True, transform=transforms.ToTensor())

train_dl = DataLoader(mnist_train, shuffle=True, batch_size=32)
test_dl = DataLoader(mnist_test, shuffle=False, batch_size=32)

class_names = mnist_test.classes

### Загружаем датасет в pandas.dataframe, чтобы узнать о нем больше

train_dict = {
    "labels" : []
}

test_dict = {
    "labels" : []
}

for batch, (img, labels) in enumerate(train_dl):
    for i in range(len(labels)):
        train_dict["labels"].append(labels.numpy()[i])
        
for batch, (img, labels) in enumerate(test_dl):
    for i in range(len(labels)):
        test_dict["labels"].append(labels.numpy()[i])

train_df = pd.DataFrame.from_dict(train_dict)
test_df = pd.DataFrame.from_dict(test_dict)

print(train_df.describe())

print(test_df.describe())

### Создаем модель - тут повторяется архитектура TinyVGG
class model_V0(nn.Module):
    def __init__(self, input_shape: int, hidden_units: int, num_classes: int):
        super().__init__()
        
        self.relu = nn.ReLU()
        
        
        # Block 1
        self.conv_1_1 = nn.Conv2d(in_channels=input_shape, out_channels=hidden_units, kernel_size=3, padding=1)
        # ReLU
        self.conv_1_2 = nn.Conv2d(in_channels=hidden_units, out_channels=hidden_units, kernel_size=3, padding=1)
        # ReLU
        self.max_pool_1 = nn.MaxPool2d(kernel_size=2, stride=2)
        
        
        # Block 2
        self.conv_2_1 = nn.Conv2d(in_channels=hidden_units, out_channels=hidden_units, kernel_size=3, padding=1)
        # ReLU
        self.conv_2_2 = nn.Conv2d(in_channels=hidden_units, out_channels=hidden_units, kernel_size=3, padding=1)
        # ReLU
        self.max_pool_2 = nn.MaxPool2d(kernel_size=2, stride=2)
        
        
        # Classificator
        self.flatten = nn.Flatten()
        self.linear = nn.Linear(in_features=hidden_units*7*7, out_features=num_classes)


    def forward(self, x):
        
        # Block 1
        x = self.conv_1_1(x)
        x = self.relu(x)
        x = self.conv_1_2(x)
        x = self.relu(x)
        x = self.max_pool_1(x)
        
        # Block 2
        x = self.conv_2_1(x)
        x = self.relu(x)
        x = self.conv_2_2(x)
        x = self.relu(x)
        x = self.max_pool_2(x)
        
        # Classificator
        x = self.flatten(x)
        x = self.linear(x)
        
        return x



### Тестируем модель
model = model_V0(1, 10, 10).to(device)

with torch.inference_mode():
    for batch, (imgs, labels) in enumerate(train_dl):
        imgs = imgs.to(device)
        labels = labels.to(device)
        
        # print(f"imgs shape = {imgs.shape}")
        # print(f"labels shape = {labels.shape}")
        
        logits = model(imgs).to(device)
        soft_preds = torch.softmax(logits, dim=1)
        preds = torch.argmax(soft_preds, dim=1)
        print(f"logits shape = {logits.shape}")
        print(f"soft_preds shape = {soft_preds.shape}")
        print(f"preds shape = {preds.shape}")
        
        
        # for i in range(len(labels)):
        print(f"Prediction: {preds}")
        print(f"Label: {labels}")
        break


### Функция для сохранения состояния модели
def save_checkpoint(state, filename='./checkpoint.pth.tar'):
    if os.path.isfile(filename):
        os.remove(filename)
    torch.save(state, filename)


### Функции train и test step
def train_step(model, train_dl: DataLoader, optimizer, loss_fn):
    model.train()
    loss_var = 0
    acc = 0 
    num_elements = len(train_dl) * 32
        
    for batch, (imgs, labels) in enumerate(train_dl):
        optimizer.zero_grad()
        imgs = imgs.to(device)
        labels = labels.to(device)
        
        logits = model(imgs).to(device)
        
        # print(f"logits device: {logits.device}\nlabels device: {labels.device}")
        
        loss = loss_fn(logits, labels)
        loss_var += loss
        loss.backward()
        optimizer.step()
        train_soft_preds = torch.softmax(logits, dim=1)
        train_preds = torch.argmax(train_soft_preds, dim=1)
            
        for i in range(len(train_preds)):
            acc += train_preds[i] == labels[i]
        print(f"Train step progress: {(batch + 1) / len(train_dl)}")
        print(f"loss: {loss}\naccuracy: {acc / (32 * (batch + 1))}")
    return loss_var / num_elements, acc / num_elements


def test_step(model, test_dl, loss_fn):
    model.eval()
    
    loss = 0
    acc = 0    
    num_elements = len(test_dl) * 32
    
    with torch.inference_mode():
        for batch, (imgs, labels) in enumerate(test_dl):
            imgs = imgs.to(device)
            labels = labels.to(device)
            
            logits = model(imgs).to(device)
            
            test_soft_preds = torch.softmax(logits, dim=1)
            test_preds = torch.argmax(test_soft_preds, dim=1)
            
            
            
            loss += loss_fn(logits, labels)
            
            for i in range(len(test_preds)):
                acc += test_preds[i] == labels[i]
            print(f"Test step progress: {(batch + 1) / len(test_dl)}")
            print(f"loss: {loss / (32 * (batch + 1))}\naccuracy: {acc / (32 * (batch + 1))}")
            
            
            
        return loss.to("cpu") / num_elements, acc / num_elements


### Train-test loop

model = model_V0(1, 10, 10).to(device)
optimizer = torch.optim.SGD(params=model.parameters(), lr=0.01)
loss_fn = torch.nn.CrossEntropyLoss()

training_epochs = 0
if (load_model):
    checkpoint = torch.load(path_to_model_checkpoint)
    model.load_state_dict(checkpoint["state_dict"])
    optimizer.load_state_dict(checkpoint["optimizer_state_dict"])
    training_epochs = checkpoint["training_epochs"]
    

if (train):
    for epoch in range(EPOCHS):
        train_loss, train_accuracy = train_step(model, train_dl, optimizer, loss_fn)
        test_loss, test_accuracy = test_step(model, test_dl, loss_fn)
        print("\n\n")
        print(f"Train loss: {train_loss}\nTrain accuracy: {train_accuracy}")
        print(f"Test loss: {test_loss}\nTest accuracy: {test_accuracy}")
        print("\n\n")
    training_epochs += EPOCHS

if (save_model):
    save_checkpoint(
                    {
                        "state_dict": model.to("cpu").state_dict(),
                        "optimizer_state_dict" : optimizer.state_dict(),
                        "training_epochs": training_epochs,
                    },
                    path_to_model_checkpoint)

### Функция выполнения предсказания при помощи готовой модели
def predict(model_in, input_img):
    model_in.eval()
    if (len(input_img.shape) == 3):
        input_img = input_img.unsqueeze(dim=0)
    
    with torch.inference_mode():
        logits = model_in(input_img)
        soft_preds = torch.softmax(logits, dim=1)
        preds = torch.argmax(soft_preds, dim=1)
        
    return preds

### Вывод confusion matrix для определения слабых мест модели
test_img = None
test_label = None

y_pred_tensor = []
targets = []

acc = 0
for batch, (imgs, labels) in enumerate(train_dl):
    imgs = imgs.to(device)
    labels = labels.to(device)
    sum_acc = 0
    prediction = predict(model, imgs)
    
    for i in range(len(prediction)):
        y_pred_tensor.append(prediction[i])
        targets.append(labels[i])
    
    for i in range(len(labels)):
        sum_acc += prediction[i] == labels[i]
        if (batch == 2 and i == 3):
            test_img = imgs[i]
            test_label = labels[i]
            break
        # print(f"prediction: {prediction[i]}\nlabel: {labels[i]}")
    acc = sum_acc / len(labels)

y_pred_tensor = torch.tensor(y_pred_tensor)
targets = torch.tensor(targets)


confmat = ConfusionMatrix(num_classes=len(class_names), task='multiclass')
confmat_tensor = confmat(preds=y_pred_tensor,
                         target=targets)

fig, ax = plot_confusion_matrix(
    conf_mat=confmat_tensor.numpy(),
    class_names=class_names, 
    figsize=(10, 7)
);

plt.show()