import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import torch
import torchmetrics as tm
from sklearn.datasets import make_moons
from sklearn.model_selection import train_test_split
from torch import nn
from torch.utils.data import DataLoader, Dataset
from torchmetrics import Accuracy

# Set device variable
device = "cuda" if torch.cuda.is_available() else "cpu"


#################################################
#############   DEFINITIONS  ##################
#################################################

NUM_CLASSES = 3



def plot_decision_boundary(model, X, y):
  
    # Put everything to CPU (works better with NumPy + Matplotlib)
    model.to("cpu")
    X, y = X.to("cpu"), y.to("cpu")

    # Source - https://madewithml.com/courses/foundations/neural-networks/ 
    # (with modifications)
    x_min, x_max = X[:, 0].min() - 0.1, X[:, 0].max() + 0.1
    y_min, y_max = X[:, 1].min() - 0.1, X[:, 1].max() + 0.1
    xx, yy = np.meshgrid(np.linspace(x_min, x_max, 101), 
                         np.linspace(y_min, y_max, 101))

    # Make features
    X_to_pred_on = torch.from_numpy(np.column_stack((xx.ravel(), yy.ravel()))).float()

    # Make predictions
    model.eval()
    with torch.inference_mode():
        y_logits = model(X_to_pred_on)

    # Test for multi-class or binary and adjust logits to prediction labels
    if len(torch.unique(y)) > 2:
        y_pred = torch.softmax(y_logits, dim=1).argmax(dim=1) # mutli-class
    else: 
        y_pred = torch.round(torch.sigmoid(y_logits)) # binary
    
    # Reshape preds and plot
    y_pred = y_pred.reshape(xx.shape).detach().numpy()
    plt.contourf(xx, yy, y_pred, cmap=plt.cm.RdYlBu, alpha=0.7)
    plt.scatter(X[:, 0], X[:, 1], c=y, s=40, cmap=plt.cm.RdYlBu)
    plt.xlim(xx.min(), xx.max())
    plt.ylim(yy.min(), yy.max())





class SpiralsDataset(Dataset):
  def __init__(self, datax, datay):
    self.data = []
    for i in range(len(datax)):
      self.data.append([datax[i], datay[i]])

  def __len__(self):
    return len(self.data)
  
  def __getitem__(self, i):
    return self.data[i]




<<<<<<< HEAD
# Create model by subclassing nn.Module
class MulticlassClassifier(nn.Module):
  def __init__(self):
    super().__init__()
    self.layer_1 = torch.nn.Linear(in_features=2, out_features=30, device=device)
    self.layer_2 = torch.nn.Linear(in_features=30, out_features=30, device=device)
    self.layer_3 = torch.nn.Linear(in_features=30, out_features=30, device=device)
    self.layer_4 = torch.nn.Linear(in_features=30, out_features=NUM_CLASSES, device=device)
    self.relu = torch.nn.ReLU()
=======
# Inherit from nn.Module to make a model capable of fitting the mooon data
class MoonModelV0(nn.Module):
    ## Your code here ##
    def __init__(self):
      super().__init__()
      self.layer_1 = nn.Linear(2, 1000)
      self.layer_2 = nn.Linear(1000, 1000)
      self.layer_3 = nn.Linear(1000, 1)
      self.relu = nn.ELU()
      # self.sigmoid = nn.Sigmoid()
>>>>>>> tmp

  def forward(self, x):
    return self.layer_4(self.relu(self.layer_3(self.relu(self.layer_2(self.relu(self.layer_1(x)))))))







def train(model: MulticlassClassifier, dloader: DataLoader, lossfn: torch.nn.CrossEntropyLoss, optim: torch.optim.SGD):
  model.train()
  sum_loss = 0.0
  acc = 0.0
  
  for idx, (trainx, trainy) in enumerate(dloader):
    trainx = trainx.to(device)
    trainy = trainy.to(device)
    
    logits = model(trainx)
    soft_preds = torch.softmax(logits, dim=1).argmax(dim=1)
    loss = lossfn(logits, trainy)
    # loss = lossfn(soft_preds.unsqueeze(dim=0).float(), trainy.float())
    optim.zero_grad()
    loss.backward()
    optim.step()
    sum_loss += loss.item()
    acc += soft_preds == trainy

  
  sum_loss /= len(dloader)
  acc /= len(dloader)
  print("Train metrics: ")
  print(f"\tLoss: {sum_loss}") 
  print(f"\tAccuracy: {acc*100}%")






def test(model: MulticlassClassifier, dloader: DataLoader, lossfn, epoch=None):
  model.eval()
  
  with torch.inference_mode():
    test_loss = 0.0
    accuracy = 0.0
    
    for idx, (testx, testy) in enumerate(dloader):
      testx = testx.to(device)
      testy = testy.to(device)
      logits = model(testx)
      soft_preds = torch.softmax(logits, dim=1).argmax(dim=1) 
      test_loss += lossfn(logits, testy)
      accuracy += soft_preds == testy

<<<<<<< HEAD
    if epoch is not None and epoch % 10 == 0:
      test_loss /= len(dloader)
      accuracy /= len(dloader)
      
      print(f"\n\n\n")
      print(f"Test metrics: ")
      print(f"\tEpoch: {epoch}")
      print(f"\tLoss: {test_loss}") 
      print(f"\tAccuracy: {accuracy*100}%")
      print(f"\n\n\n")
=======
epochs = 20
>>>>>>> tmp





def create_spirals():
  """
  Code for creating a spiral dataset from CS231n
  
  Returns:
    X, y - input(X) and output(y) data
  """ 
  RANDOM_SEED = 42
  np.random.seed(RANDOM_SEED)
  N = 100 # number of points per class
  D = 2 # dimensionality
  K = NUM_CLASSES # number of classes
  X = np.zeros((N*K,D)) # data matrix (each row = single example)
  y = np.zeros(N*K, dtype='uint8') # class labels
  for j in range(K):
    ix = range(N*j,N*(j+1))
    r = np.linspace(0.0,1,N) # radius
    t = np.linspace(j*4,(j+1)*4,N) + np.random.randn(N)*0.2 # theta
    X[ix] = np.c_[r*np.sin(t), r*np.cos(t)]
    y[ix] = j
  # lets visualize the data
  plt.scatter(X[:, 0], X[:, 1], c=y, s=40, cmap=plt.cm.RdYlBu)
  plt.show()


  # # Turn data into tensors
  # import torch
  X = torch.from_numpy(X).type(torch.float) # features as float32
  y = torch.from_numpy(y).type(torch.LongTensor) # labels need to be of type long
  
  return X, y








X, y = create_spirals()

print(f"X: {X[:5]}, Y: {y[:5]}")

# Create train and test splits
train_size = int(len(X) * 0.8)
test_size = int(len(X) * 0.2)

spirals_dset = SpiralsDataset(X, y)

train_dset, test_dset = torch.utils.data.random_split(spirals_dset, [train_size, test_size])
train_dloader = DataLoader(train_dset, shuffle=True)
test_dloader = DataLoader(test_dset, shuffle=True)

# print(f"train_dset is: {train_dset[:5]}\n\ntest_dset is: {test_dset[:5]}")






# Let's calculuate the accuracy for when we fit our model
# !pip -q install torchmetrics # colab doesn't come with torchmetrics

# # TODO: uncomment the two lines below to send the accuracy function to the device
acc_fn = Accuracy(task="multiclass", num_classes=NUM_CLASSES).to(device)

# # Instantiate model and send it to device

model_0 = MulticlassClassifier().to(device)
loss_fn = torch.nn.CrossEntropyLoss().to(device)
optimizer = torch.optim.Adam(params=model_0.parameters(), lr=0.0001)

# # Setup data to be device agnostic

epochs = 300

for epoch in range(epochs):
  train(model_0, train_dloader, loss_fn, optimizer)
  # train(model_0, X[:split_index], y[:split_index], loss_fn, optimizer)
  test(model_0, test_dloader, loss_fn, epoch)



plot_decision_boundary(model_0, X, y)
plt.show()

# preds = torch.softmax(model_0(X_test[34:40]), dim=1).argmax(dim=1)
# print(f"Preds: {preds}\nReal: {y_test[34:40]}")







