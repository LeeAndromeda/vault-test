from sklearn.datasets import make_circles
import pandas as pd
import matplotlib.pyplot as plt
import torch
from torch import nn
from torch.utils.data import DataLoader, Dataset

device = "cuda" if torch.cuda.is_available() else "cpu"
print(f"DEVICE IS {device}")

# Make 1000 samples 
n_samples = 1000
epochs = 1000

# Create circles
X, y = make_circles(n_samples,
                    noise=0.03, # a little bit of noise to the dots
                    random_state=42) # keep random state so we get the same values

print(f"First 5 X values:\n {X[:5]}\n")
print(f"First 5 y values:\n {y[:5]}\n")
print(f"\nX.shape = {X.shape}\ny.shape = {y.shape}")

# Put data into pandas dataframe
data_dict = {
    'X1' : X[:, 0],
    'X2' : X[:, 1],
    'label' : y
}

df2 = pd.DataFrame.from_dict(data=data_dict)

df = pd.DataFrame({
    'X1' : X[:, 0],
    'X2' : X[:, 1],
    "label" : y
})
print(df.tail(5))

print(df.X1.value_counts())
print(df.X2.value_counts())
print(df.label.value_counts())

plt.scatter(x=X[:, 0],
            y=X[:, 1],
            c=y,
            cmap=plt.cm.RdYlBu);
plt.show()

# Now I need to explore the data to understand how to work with it
print(f"shapes: x: {X.shape}, y: {y.shape}\n")
# shapes: x: (1000, 2), y: (1000,)

# Now I wanna see what a singular piece of the dataset looks like
print(f"The X values of the piece are {X[0]} and the y values are {y[0]}")
# The X values of the piece are [0.75424625 0.23148074] and the y values are 1
# This tells me that I should have 2 inputs and 1 output

# Split data into train and test sets
from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X, 
                                                    y, 
                                                    test_size=0.2, # 20% test, 80% train
                                                    random_state=42) # make the random split reproducible

X_train = torch.tensor(X_train, dtype=torch.float).to(device)
X_test = torch.tensor(X_test, dtype=torch.float).to(device)
y_train = torch.tensor(y_train, dtype=torch.float).to(device)
y_test = torch.tensor(y_test, dtype=torch.float).to(device)

print(f"{len(X_train)})"), print(f"{len(X_test)})"), print(f"{len(y_train)})"), print(f"{len(y_test)}")
print(f"{type(X_train)}"), print(f"{type(X_test)}"), print(f"{type(y_train)}"), print(f"{type(y_test)}")


# Dataset definition for another paradigm of working with datasets in pytorch
class CirclesDataset(Dataset):
    def __init__(self, data=None):
        # Make 1000 samples 
        n_samples = 1000

        # Create circles
        X_loc, y_loc = make_circles(n_samples,
                            noise=0.03, # a little bit of noise to the dots
                            random_state=42) # keep random state so we get the same values
        
        if data is None:
            data=[]
            for i in range(len(X_loc)):
                data.append([X_loc[i], y_loc[i]])
        
        self.data = data
        print(self.data[:5])
        
    def __len__(self):
        return len(self.data)
    
    def __getitem__(self, index):
        return self.data[index]
    
# dataset = CirclesDataset()


class CircleClassifierV0(nn.Module):
    def __init__(self):
        super().__init__()
        
        self.layer_1 = torch.nn.Linear(2, 5)
        self.layer_2 = torch.nn.Linear(5, 1)
        
    def forward(self, x):
        return self.layer_2(self.layer_1(x))


class CircleClassifierV1(nn.Module):
    def __init__(self):
        super().__init__()
        
        self.layer_1 = torch.nn.Linear(2, 10)
        self.layer_2 = torch.nn.Linear(10, 10)
        self.layer_3 = torch.nn.Linear(10, 1)
        
    def forward(self, x):
        # Creating a model like this is the same as below, though below
        # generally benefits from speedups where possible.
        # z = self.layer_1(x)
        # z = self.layer_2(z)
        # z = self.layer_3(z)
        # return z
        return self.layer_3(self.layer_2(self.layer_1(x)))


class CircleClassifierV2(nn.Module):
    def __init__(self):
        super().__init__()
        
        self.layer_1 = torch.nn.Linear(2, 100)
        self.layer_2 = torch.nn.Linear(100, 100)
        self.layer_3 = torch.nn.Linear(100, 1)
        self.relu = torch.nn.ReLU()
        
    def forward(self, x):
        # Creating a model like this is the same as below, though below
        # generally benefits from speedups where possible.
        # z = self.layer_1(x)
        # z = self.layer_2(z)
        # z = self.layer_3(z)
        # return z
        return self.layer_3(self.relu(self.layer_2(self.relu(self.layer_1(x)))))
    


model_0 = CircleClassifierV0()
model_0.to(device)

model_1 = CircleClassifierV1()
model_1.to(device)

model_2 = CircleClassifierV2()
model_2.to(device)

# loss_fn = nn.BCEWithLogitsLoss().to(device)
# optimizer = torch.optim.SGD(params=model_0.parameters(), lr=0.1)

loss_fn = nn.BCEWithLogitsLoss().to(device)
optimizer = torch.optim.SGD(params=model_2.parameters(), lr=0.1)
from torch.optim import lr_scheduler as lrs
scheduler = lrs.CosineAnnealingLR(optimizer, epochs)

# Calculate accuracy (a classification metric)
def accuracy_fn(y_true, y_pred):
    correct = torch.eq(y_true, y_pred).sum().item() # torch.eq() calculates where two tensors are equal
    acc = (correct / len(y_pred)) * 100 
    return acc


def train(model, input_train, output_train, lossfn, optim):
    """Training function for my classification model

    Args:
        model (torch.nn.Module): model
        input_train: x training data
        output_train: y training data
        lossfn: loss function
        optim: optimizer

    Returns:
        float: accuracy
    """
    ### Training
    model.train()

    # 1. Forward pass (model outputs raw logits)
    output_logits = model(input_train).squeeze() # squeeze to remove extra `1` dimensions, this won't work unless model and data are on same device 
    output_train = torch.round(torch.sigmoid(output_logits)) # turn logits -> pred probs -> pred labls

    # 2. Calculate loss/accuracy
    # loss = loss_fn(torch.sigmoid(output_logits), # Using nn.BCELoss you need torch.sigmoid()
    #                y_train) 
    loss = lossfn(output_logits, # Using nn.BCEWithLogitsLoss works with raw logits
                   y_train)
    acc = accuracy_fn(y_true=y_train, 
                      y_pred=output_train) 

    # 3. Optimizer zero grad
    optim.zero_grad()

    # 4. Loss backwards
    loss.backward()

    # 5. Optimizer step
    optim.step()
    return acc


def test(model, input_test, output_test, lossfn, epoch=None):
    """Testing function for my classification model

    Args:
        model (torch.nn.Model) : NN model
        input_test : input testing data
        output_test : output testing data
        lossfn : loss function
        
    Returns:
        test_loss (float) : loss function value
        test_acc (float) : accuracy val in percantage
    """
    ### Testing
    model.eval()
    with torch.inference_mode():
        # 1. Forward pass
        test_logits = model(input_test).squeeze() 
        test_pred = torch.round(torch.sigmoid(test_logits))
        # 2. Caculate loss/accuracy
        test_loss = lossfn(test_logits,
                            output_test)
        test_acc = accuracy_fn(y_true=output_test,
                               y_pred=test_pred)
    if epoch is not None and epoch % 10 == 0:
        print(f"\nEpoch: {epoch}\nTest loss: {test_loss}\nTest accuracy: {test_acc}%")
    elif epoch is None:
        print(f"\nTest loss: {test_loss}\nTest accuracy: {test_acc}%")
    return test_loss, test_acc


untrained_preds = model_0(X_test.to(device))
print(f"Length of predictions: {len(untrained_preds)}, Shape: {untrained_preds.shape}")
print(f"Length of test samples: {len(y_test)}, Shape: {y_test.shape}")
print(f"\nFirst 10 predictions:\n{untrained_preds[:10]}")
print(f"\nFirst 10 test labels:\n{y_test[:10]}")



# View the frist 5 outputs of the forward pass on the test data
y_logits = model_0(X_test.to(device))[:5]
print(f"y_logits: {y_logits}")

# Use sigmoid on model logits
y_pred_probs = torch.sigmoid(y_logits)
print(f"y_pred_probs: {y_pred_probs}")

# Find the predicted labels (round the prediction probabilities)
y_preds = torch.round(y_pred_probs)

# In full
y_pred_labels = torch.round(torch.sigmoid(model_0(X_test.to(device))[:5]))

# Check for equality
print(torch.eq(y_preds.squeeze(), y_pred_labels.squeeze()))

# Get rid of extra dimension
print(y_preds.squeeze())


for epoch in range(epochs):
    train(model_2, X_train, y_train, loss_fn, optimizer)
    loss_val, accuracy = test(model_2, X_test, y_test, loss_fn, epoch)
    scheduler.step()

import requests
from pathlib import Path 

# Download helper functions from Learn PyTorch repo (if not already downloaded)
if Path("helper_functions.py").is_file():
  print("helper_functions.py already exists, skipping download")
else:
  print("Downloading helper_functions.py")
  request = requests.get("https://raw.githubusercontent.com/mrdbourke/pytorch-deep-learning/main/helper_functions.py")
  with open("helper_functions.py", "wb") as f:
    f.write(request.content)

from helper_functions import plot_predictions, plot_decision_boundary

# Plot decision boundaries for training and test sets
plt.figure(figsize=(12, 6))
plt.subplot(1, 2, 1)
plt.title("Train")
plot_decision_boundary(model_2, X_train, y_train)
plt.subplot(1, 2, 2)
plt.title("Test")
plot_decision_boundary(model_2, X_test, y_test)
plt.show()
