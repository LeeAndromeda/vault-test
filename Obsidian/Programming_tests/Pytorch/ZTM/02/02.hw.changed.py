import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import torch
# import torchmetrics as tm
from sklearn.datasets import make_moons
from sklearn.model_selection import train_test_split
from torch import nn
from torch.utils.data import DataLoader, Dataset
# from torchmetrics import Accuracy

# Set device variable
device = "cuda" if torch.cuda.is_available() else "cpu"
    
    

# # Code for creating a spiral dataset from CS231n
# import numpy as np
# import matplotlib.pyplot as plt
RANDOM_SEED = 42
np.random.seed(RANDOM_SEED)
N = 100 # number of points per class
D = 2 # dimensionality
K = 3 # number of classes
X = np.zeros((N*K,D)) # data matrix (each row = single example)
y = np.zeros(N*K, dtype='uint8') # class labels
for j in range(K):
  ix = range(N*j,N*(j+1))
  r = np.linspace(0.0,1,N) # radius
  t = np.linspace(j*4,(j+1)*4,N) + np.random.randn(N)*0.2 # theta
  X[ix] = np.c_[r*np.sin(t), r*np.cos(t)]
  y[ix] = j
# lets visualize the data
plt.scatter(X[:, 0], X[:, 1], c=y, s=40, cmap=plt.cm.RdYlBu)
# plt.show()


# # Turn data into tensors
# import torch
X = torch.from_numpy(X).type(torch.float) # features as float32
y = torch.from_numpy(y).type(torch.LongTensor) # labels need to be of type long

print(f"X: {X[:5]}, Y: {y[:5]}")

# Create train and test splits
# split_index = int(len(X) * 0.8)
train_size = int(len(X) * 0.8)
test_size = int(len(X) * 0.2)
print(f"X is: {X[:5]}\n\ny is: {y[:5]}")

class SpiralsDataset(Dataset):
  def __init__(self, datax, datay):
    self.data = []
    for i in range(len(datax)):
      self.data.append([datax[i], datay[i]])

  def __len__(self):
    return len(self.data)
  
  def __getitem__(self, i):
    return self.data[i]


# spirals = SpiralsDataset(X[:split_index], y[:split_index])
spirals = SpiralsDataset(X, y)
train_dset, test_dset = torch.utils.data.random_split(spirals, [train_size, test_size])
dloader = DataLoader(train_dset, shuffle=True)

# Let's calculuate the accuracy for when we fit our model
# !pip -q install torchmetrics # colab doesn't come with torchmetrics

# # TODO: uncomment the two lines below to send the accuracy function to the device
# acc_fn = Accuracy(task="multiclass", num_classes=K).to(device)

# Create model by subclassing nn.Module
class MulticlassClassifier(nn.Module):
  def __init__(self):
    super().__init__()
    self.layer_1 = torch.nn.Linear(in_features=2, out_features=50, device=device)
    self.layer_2 = torch.nn.Linear(in_features=50, out_features=50, device=device)
    # self.layer_3 = torch.nn.Linear(in_features=50, out_features=50, device=device)
    self.layer_4 = torch.nn.Linear(in_features=50, out_features=K, device=device)
    self.relu = torch.nn.ReLU()

  def forward(self, x):
    # return self.layer_3(self.layer_2(self.layer_1(x)))
    # return self.layer_4(self.relu(self.layer_3(self.relu(self.layer_2(self.relu(self.layer_1(x)))))))
    return self.layer_4(self.relu(self.layer_2(self.relu(self.layer_1(x)))))



def train(model: MulticlassClassifier, dataset, lossfn: torch.nn.CrossEntropyLoss, optim: torch.optim.SGD, epoch: int):
  model.train()
  sum_loss = 0.0
  acc = 0.0
  for idx, (trainx, trainy) in enumerate(dataset):
    logits = model(trainx)
    # print(f"Logits shape {logits.shape}, target shape: {trainy.shape}")
    loss = lossfn(logits, trainy)
    # soft_preds = torch.softmax(logits, dim=0).argmax()
    # loss = lossfn(soft_preds.unsqueeze(dim=0).float(), trainy.float())
    optim.zero_grad()
    loss.backward()
    optim.step()
    sum_loss += loss.item()

    soft_preds = torch.softmax(logits, dim=1).argmax(dim=1)
    if soft_preds == trainy:
        acc += 1
  if epoch % 10 == 0:
    sum_loss /= len(dataset)
    acc /= len(dataset)
    print(f'Epoch {epoch}, train_loss {sum_loss}, train_acc {acc}')
    
    
# def test(model: MulticlassClassifier, testx, testy, lossfn, epoch=None, accuracy_fn=None):
def test(model: MulticlassClassifier, test_dset: Dataset, lossfn, epoch=None, accuracy_fn=None):
  model.eval()
  
  with torch.inference_mode():
    sum_loss = 0.0
    acc = 0.0
    for idx, (test_x, test_y) in enumerate(test_dset):
        logits = model(test_x)
        # torch.argmax(logits, dim=1)
        # soft_preds = torch.softmax(logits, dim=1).argmax(dim=1) 

        
        # print(f"Logits shape {logits.shape}, target shape: {testy.shape}")
        test_loss = lossfn(logits, test_y)
        sum_loss += test_loss

        soft_preds = torch.softmax(logits, dim=0).argmax(dim=0)
        if soft_preds == test_y:
          acc += 1
        # accuracy = None
    if epoch is not None and epoch % 10 == 0:
        test_loss /= len(test_dset)
        acc /= len(test_dset)
        print(f"Epoch: {epoch}")
        print(f"Loss: {test_loss}")
        print(f'Accuracy: {acc}')

        print('Softmax')
        for _ in range(4):
            test_x, test_y = test_dset[torch.randint(0, len(test_dset), (1,))]
            logits = model(test_x)
            soft_preds = torch.softmax(logits, dim=0).argmax(dim=0) 
            print(soft_preds, test_y)
        # if accuracy_fn is not None:
        #     accuracy = accuracy_fn(soft_preds, testy.to(device))
        #     print(f"Accuracy: {accuracy*100}%")


# # Instantiate model and send it to device

model_0 = MulticlassClassifier().to(device)
# loss_fn = torch.nn.CrossEntropyLoss(
#   torch.tensor([1.0, 1.0, 2.5], dtype=torch.float32, device=device)).to(device)
loss_fn = torch.nn.CrossEntropyLoss().to(device)
optimizer = torch.optim.Adam(params=model_0.parameters(), lr=0.001, weight_decay=0.01)

# # Setup data to be device agnostic

epochs = 200

# X_test = X[:split_index]
# y_test = y[:split_index]

for epoch in range(epochs):
  train(model_0, dloader, loss_fn, optimizer, epoch)
  # train(model_0, X[:split_index], y[:split_index], loss_fn, optimizer)
#   test(model_0, X_test, y_test, loss_fn, epoch, acc_fn)
  test(model_0, test_dset, loss_fn, epoch)

# plot_decision_boundary(model_0, X_test, y_test)

# preds = torch.softmax(model_0(X_test[34:40]), dim=1).argmax(dim=1)
# print(f"Preds: {preds}\nReal: {y_test[34:40]}")