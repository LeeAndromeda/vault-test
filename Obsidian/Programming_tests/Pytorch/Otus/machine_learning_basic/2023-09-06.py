from sys import getsizeof
from copy import copy, deepcopy

name = 'Vitaly'
print(id(name))

fruit = 'banana'
print (id(name) == id(fruit))
print(getsizeof(name), getsizeof(fruit))

a = complex(5, 2)
b = a / 2

from copy import copy, deepcopy

a = copy(b)
a = deepcopy(b)