import torch
from torch import tensor


m1 = torch.rand(size=(5, 8))
m2 = torch.rand(size=(8, 15))

m3 = m1.mm(m2)

print('M1')
print(m1)
print(m1.shape)
print(m1.size)

print('M2')
print(m2)
print(m2.shape)
print(m2.size)

print('M3')
print(m3) 
print(m3.shape)
print(m3.size)

m4 = m3 * 2
print ('M4')
print(m4)
print(m4.shape)
print(m4.size)