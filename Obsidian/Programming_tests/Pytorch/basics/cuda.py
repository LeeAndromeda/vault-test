import torch

device = "cuda" if torch.cuda.is_available() else "cpu"

print(device)

A = torch.arange(0, 100, 10).to(device)
print(A)