---

Tags: 
aliases: [ssh]
regex-to-search: "((.*)[Ss][Ss][Hh].*)"
regex-to-be-found: "NoRgxTBFKey"

Тип: конспект
Описание: -

День: 03 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Monday 3rd October 2022 22:06:34

---
# [[Работа с SSH]]

**Безопасный шелл (SSH)** — это сетевой протокол, обеспечивающий функции шелла на удалённой машине через безопасный канал. SSH несёт в себе различные улучшения безопасности, среди них аутентификация пользователя/хоста, шифрование данных и целостность данных, благодаря чему невозможны популярные атаки вроде подслушивания (eavesdropping), DNS/IP spoofing, подделка данных (data forgery), перехват соединения (connection hijacking) и т. д. Пользователям ftp, telnet или rlogin, которые используют протокол, передающий данные в виде открытого текста, крайне рекомендуется переключиться на SSH.

# OpenSSH

## Теория

- [[Как установить OpenSSH]]
- [[Настройка sshd_config]]
- [[Соединение SSH]]
- [[Как создать ssh ключ для GitHub]]

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

## Статьи

- [# Урок по SSH: азы использования и продвинутые трюки (часть первая)](https://codeby.net/threads/urok-po-ssh-azy-ispolzovanija-i-prodvinutye-trjuki-chast-pervaja.67437/)
- [Азы SSH](https://gitjournal.tech/komanda-ssh-linux-ispolzovanie-podkljuchenie-i-nastrojka/)

#SSH #СетевойПротокол