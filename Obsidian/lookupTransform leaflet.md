[[lookupTransform.svg]]

```leaflet
id: lookupTransform
image: [[lookupTransform.svg]]
height: 500px 
lat: 5
long: 5
minZoom: 1
maxZoom: 3
defaultZoom: 5
unit: meters
scale: 1
darkMode: false 
```
