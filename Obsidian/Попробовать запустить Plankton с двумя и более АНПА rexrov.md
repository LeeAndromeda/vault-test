---
Тип: Задача
Описание: -
Исполнители: 
Проект: Рубин

Дисциплина: Работа
Tags: Задача 
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

День: 09 # Не трогать!
Месяц: 11 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Wednesday 9th November 2022 22:07:36

день_завершения: 10
месяц_завершения: 11
год_завершения:: 2022

Выполнено: да
Результат: нет
---

```dataview
TABLE день_завершения as "День истечения", месяц_завершения as "Месяц истечения", Проект, Выполнено, Результат, Количество_выходящих_задач as "Количество выходящих задач"
WHERE contains(file.name, "Попробовать запустить Plankton с двумя и более АНПА rexrov")
limit 1
```
# [[Попробовать запустить Plankton с двумя и более АНПА rexrov]]

Ребятам необходимо тестировать групповое управление

%%

## Результат: 



## Следующая задача:

#Входящие 

%%

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```


---

#Работа 