# Уравнение p=nkT

Как уже можно было убедиться, все макропараметры связаны между собой некой зависимостью. То же самое можно сказать и о связи температуры и давления.

Давайте представим ситуацию - у нас имеется некоторый постоянный объем газа. Если мы начнем нагревать данное вещество, то его молекулы станут двигаться еще быстрее, что приведет к частым ударам о стенку сосудов. А те, в свою очередь, приведут к увеличению давления на стенку. То есть температура напрямую влияет на давление.

А теперь представьте, что объем и температура фиксированы, но увеличивается количество вещества. Чем больше будет заполняться объем, тем чаще молекулы будут касаться стенки, что также приведет к увеличению давления. То есть количество вещества также влияет на давление.

Отсюда можно сделать вывод, что давление пропорционально концентрации молекул вещества в некотором объеме и его температуре:

$$p~~{v\over V}={N\over N_аV}={n\over N_а}~~n$$

$$p=nkT$$

[КК Физика ЕГЭ](КК%20Физика%20ЕГЭ.md)