---

Tags: 
aliases: 
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

Тип: конспект
Описание: -

День: 23 # Не трогать!
Месяц: 11 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Wednesday 23rd November 2022 17:05:49

---
# [[Что делать, если Obsidian не находит файлы с расширениями кроме md]]

1. Зайди в Files and Links
2. Включи Detect all file extensions

%%## Приложения%%

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

---
#PKM/obsidian #туториал 