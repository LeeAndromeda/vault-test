---
Тип: Задача
Описание: -
Исполнители: 
Проект: ВнедрениеЗнаний

Дисциплина: Программирование
Tags: Задача
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: 06 # Не трогать!
Месяц: 12 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Tuesday 6th December 2022 18:09:17

день_завершения: нет
месяц_завершения: 12
год_завершения: 2022
дата_завершения: 2022-12-нет

Выполнено: нет
Результат: 
---

```dataview
TABLE день_завершения as "День истечения", месяц_завершения as "Месяц истечения", Проект, Выполнено, Результат, Количество_выходящих_задач as "Количество выходящих задач"
WHERE contains(file.name, "cmake target_incldue_directories()")
limit 1
```
# [[cmake target_include_directories()]]

- Получение источников и ссылок
	- [x] Получение одной ссылки
	- [ ] Получение ссылок на разные источники
- Конспектирование
	- [ ] Достаточное для использования конспектирование
	- [ ] Полное конспектирование
- Создание справок
	- [ ] Справки практического применения
	- [x] Список терминов
	- [ ] Определение терминов из списка терминов
	- [ ] Список связанных тем(если нет внутри текста)
- [ ] Внедрение в практику

%%
## Результат:



## Следующая задача:

==#Входящие ==
%%

**Содержание потом вырезается, все содержимое конспекта удаляется и содержание вставляется по шаблону входящего конспекта.**

# Содержание 

```cmake
target_include_directories(<target> [SYSTEM] [AFTER|BEFORE]
  <INTERFACE|PUBLIC|PRIVATE> [items1...]
  [<INTERFACE|PUBLIC|PRIVATE> [items2...] ...])
```

Указывает директории, которые нужно включить для компиляции данного `<target>`. [[cmake target_include_directories()#^445f8b|Указанный]] `<target>` должен быть создан командой типа [[add_executable cmake|add_executable]] или [[add_library cmake|add_library]] и не должен быть [[ALIAS target cmake|ALIAS target]].

Аргументы к `target_incldue_directories` могут использовать "generator expressions(выражения генератора)" с синтаксисом `$<...>`. Для больших подробностей смотри [[cmake-generator-expressions]]. 

Указанные [[Инклюды]] могут быть абсолютными или относительными. В случае, если инклюд указывается относительно, он будет рассматриваться относительно переменной [[CMAKE_CURRENT_SOURCE_DIR]]. Выражения генератора [[cmake BUILD_INTERFACE genex]] и [[cmake INSTALL_INTERFACE genex]] описывают требования, основываясь на пути использования. 

- [[cmake BUILD_INTERFACE genex]] - принимает только абсолютный путь
- [[cmake INSTALL_INTERFACE genex]] - принимает любой путь. Относительные пути воспринимает относительно [[cmake installation prefix]]

#Входящие 

## Справки

> [!example]- Список терминов
> - [ ] SYSTEM
> - [ ] AFTER|BEFORE
> - [ ] INTERFACE|PUBLIC|PRIVATE
> - [[cmake BUILD_INTERFACE genex]]
> - [[cmake INSTALL_INTERFACE genex]]


> [!question]- Соурслист
> 1. https://cmake.org/cmake/help/latest/command/target_include_directories.html

%%
## Приложения
%%

## Смотрите также:

- [[cmake target - что это]] ^445f8b

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

---
#ВнедрениеЗнаний/НеобходимоеЗнание #Программирование/cmake/команда 