---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 24 # Не трогать!
Месяц: 04 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-04-24

---
# [[git blame - история файла]]

Git blame is a command in Git that allows you to see who last modified each line of a file and when. It shows the commit hash, author name, and date for each line of the file. This can be useful for identifying who made a particular change to a file, or for understanding the history of a file and how it has evolved over time. Git blame can be run from the command line using the "git blame" command followed by the file name.


> [!question]- Соурслист
> 1. Chat gpt

%%
## Приложения
%%

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

---
#git 

[^def]: термин
[^que]: вопрос