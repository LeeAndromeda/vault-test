---
Тип: Задача
Описание: -
Исполнители: 
Проект: 

Дисциплина: Саморазвитие
Tags: Задача
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

День: 16 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Sunday 16th October 2022 10:56:18

день_завершения: 16
месяц_завершения: 10
год_завершения: 2022

Выполнено: да
Результат: 
---

```dataview
TABLE день_завершения as "День истечения", месяц_завершения as "Месяц истечения", Проект, Выполнено, Результат, Количество_выходящих_задач as "Количество выходящих задач"
WHERE contains(file.name, "Написать Номерчуку список людей и подать заявку на участие 21 числа")
limit 1
```
# [[Написать Номерчуку список людей и подать заявку на участие 21 числа]]

#Входящие 

## Результат:



## Следующая задача:

#Входящие 

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

---

#Входящие 