# iomanip - Библиотека C++

## Функции

### setprecision(i);

Данная функция указывает точность вывода вещественных чисел при помощи оператора **cout**. **i - количество знаков после запятой.**

Пример использования:

```cpp
// setprecision example
#include <iostream>     // std::cout, std::fixed
#include <iomanip>      // std::setprecision
using namespace std;

int main () {
  double f =3.14159;
  cout << std::setprecision(5) << f << '\n';
  cout << std::setprecision(9) << f << '\n';
  cout << std::fixed;
  cout << std::setprecision(5) << f << '\n';
  cout << std::setprecision(9) << f << '\n';
  return 0;
}

/* Вывод
3.1416
3.14159
3.14159
3.141590000
*/
```
#cpp 