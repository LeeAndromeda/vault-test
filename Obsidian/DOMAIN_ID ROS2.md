---

Tags: 
aliases: 
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

Тип: конспект
Описание: -

День: 12 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Wednesday 12th October 2022 17:08:10

---
# [[DOMAIN_ID ROS2]]

**DOMAIN_ID** - иднтификатор подсети, который позволяет разграничивать вазимодействие между устройствами,использующими ROS2 в одной сети. Устройства с разными ID не увидят друг друга в ROS2.

> [!note]- Linux
> By default, the Linux kernel uses ports 32768-60999 for ephemeral ports. This means that domain IDs 0-101 and 215-232 can be safely used without colliding with ephemeral ports. The ephemeral port range is configurable in Linux by setting custom values in /proc/sys/net/ipv4/ip_local_port_range. If a custom ephemeral port range is used, the above numbers may have to be adjusted accordingly.

> [!note]- Windows
> By default, the ephemeral port range on Windows is 49152-65535. This means that domain IDs 0-166 can be safely used without colliding with ephemeral ports. The ephemeral port range is configurable in Windows by [using netsh](https://docs.microsoft.com/en-us/troubleshoot/windows-server/networking/default-dynamic-port-range-tcpip-chang). If a custom ephemeral port range is used, the above numbers may have to be adjusted accordingly.

> [!error]- Лимит участников
> For each ROS 2 process running on a computer, one DDS “participant” is created. Since each DDS participant takes up two ports on the computer, running more than 120 ROS 2 processes on one computer may spill over into other domain IDs or the ephemeral ports.


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

---
#Робототехника/ROS2 