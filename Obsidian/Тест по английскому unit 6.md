---
Тип: ДЗ
Дисциплина: #Входящие 
Описание: -
aliases: 
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

День: 14 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Friday 14th October 2022 15:27:26
день_завершения: 13
месяц_завершения: 10
год_завершения: 2022

Выполнено: да
Результат: 

Tags: ДомашнееЗадание
---

```dataview
TABLE день_завершения as "День истечения", месяц_завершения as "Месяц истечения", Выполнено, Результат
WHERE contains(file.name, "Тест по английскому unit 6")
limit 1
```

# [[Тест по английскому unit 6]]


1. Task 1
	1. lived
	2. 's worked
	3. didn't learn
	4. Has he been
	5. we've started
	6. retired
	7. 's been
	8. Did you see
	9. did you go
	10. Have you
	11. 've lived
2. Task 2
	1. since
	2. since
	3. for
	4. for
	5. for
	6. since
3. Task 3
	1. Have, been
	2. have broken
	3. did you stay
	4. was
	5. Have, studied
	6. have never learned
	7. Have, read
	8. enjoyed
	9. did, move
	10. arrived, have been living
4. Task 4
	1. before
	2. ago
	3. never
	4. already
	5. gone
	6. ever
	7. recently
	8. since
	9. yet
	10. been
	11. last
5. Task 5
	1. has died
	2. was taken
	3. was awarded
	4. surprised
	5. decided
	6. was won
6. Task 6
	1. 've been riding
	2. has been raining
	3. have been working
	4. 've been baking
	5. Has he been cycling
	6. haven't waited very long
7. Task 7
	1. have been sold
	2. 've written
	3. were adapted
	4. 've played
	5. have seen
	6. Have, been
	7. have been played
	8. have, been doing
	9. have fallen
	10. haven't been
	11. have, been waiting
	12. have, been 
8. Task 8
	1. peacefully
	2. suddenly
	3. deeply
	4. fluently
	5. bravely
	6. smartly
9. Task 9
	1. wrong
	2. late
	3. hard
	4. loud
	5. single-handedly
	6. close
	7. fine
	8. regularly
	9. suddenly
	10. clearly
	11. badly
10. Task 10
	1. g
	2. k
	3. f
	4. i
	5. e
	6. a
	7. d
	8. h
	9. b
	10. c
	11. j
11. Task 11
	1. been staying
	2. have just
	3. dress smartly
	4. came
	5. Mee too
	6. fought fiercely
	7. has been nominated
	8. been eating
	9. finished
	10. wrong
	11. Have you ever


## Смотрите также:

- [[HW5e_Int_Test Unit 6A.pdf]]

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

---
#ГумманитарныеНауки/Английский 