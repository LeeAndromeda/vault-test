---
Тип: Задача
Описание: -
Исполнители: 
Проект: ВнедрениеЗнаний

Tags: Задача
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: 29 # Не трогать!
Месяц: 05 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-05-29

дата_завершения: "2023-нет-нет-"

Выполнено: нет
Результат: 
---

# [[C++ condition_variable]]

> [!danger] Обязательный чеклист
> - [ ] Получение одной ссылки
> - [ ] Расставить теги 


> [!example]+  Чеклист знания
> - Получение источников и ссылок
> 	- [ ] Получение ссылок на разные источники
> - Конспектирование
> 	- [ ] Достаточное для использования конспектирование
> 	- [ ] Полное конспектирование
> - Создание справок
> 	- [ ] Справки практического применения
> 	- [ ] Список терминов
> 	- [ ] Определение терминов из списка терминов
> 	- [ ] Список связанных тем(если нет внутри текста)
> - Внедрение в базу знаний
> 	- [ ] Создание связей с оригинальной страницей (этой)
> 	- [ ] Разбиение на подтемы
> 	- [ ] Атомизация - (по необходимости) разбиение конспекта по подтемам на страницы
> - [ ] Внедрение в практику
> 

<mark style="background: #FF5582A6;">**Содержание потом вырезается, все содержимое конспекта удаляется и содержание вставляется по шаблону входящего конспекта.**</mark> 

# Содержание 

```leaflet
id: leaflet-idmnbvcdrtsaddbvkjsbjvhis8gy9w8y498tgupuevne
image: Pasted image 20230529145206.png
height: 500px
lat: 50
long: 50
minZoom: 1
maxZoom: 10
defaultZoom: 8
unit: meters
scale: 1
darkMode: false
```


**When a condition_variable is destroyed, all waiting threads (if any) must be notified (i.e., told to  wake up) or they may wait forever.** 

The ‘‘plain’’ wait(lck) is a low-level operation that should be used with extra care and usually in  
the implementation of some higher-level abstraction. <mark style="background: #FF5582A6;">It can wake up ‘‘spuriously.’’</mark> **That is, the  
system may decide to resume wait()’s thread even though no other thread has notified it!**

Apparently, allowing spurious wake-up simplifies implementation of condition_variables on some  
systems. Always use ‘‘plain’’ wait() in a loop. For example:  
```cpp
while (queue.empty()) wait(queue_lck);  
```
An additional reason for this loop is that some thread may have ‘‘snuck up’’ and invalidated the  
condition (here, queue.empty()) before the thread calling the unconditional wait() got to run. Such a  loop basically is the implementation of a wait with a condition, so prefer those over the unconditional wait().

The status returned by wait_until() and wait_for() is defined as:  
```cpp
enum class cv_status { no_timeout, timeout };
```



## Смотрите также:

> [!question]- Соурслист
> 1. [[The_C++_Programming_Language_4th_Edition_Bjarne_Stroustrup#page=1246]]

> [!quote]- Контекст
> ```dataview
> LIST 
> FROM !outgoing([[]])
> WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
> ```

---
#ВнедрениеЗнаний 
#Входящие 

---

[^def]: термин
[^que]: вопрос