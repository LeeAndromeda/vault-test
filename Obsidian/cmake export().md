---
Тип: Задача
Описание: -
Исполнители: 
Проект: ВнедрениеЗнаний

Дисциплина: #Входящие 
Tags: Задача
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: 06 # Не трогать!
Месяц: 12 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Tuesday 6th December 2022 19:20:39

день_завершения: нет
месяц_завершения: нет
год_завершения: 2022
дата_завершения: 2022-нет-нет

Выполнено: нет
Результат: 
---

```dataview
TABLE день_завершения as "День истечения", месяц_завершения as "Месяц истечения", Проект, Выполнено, Результат, Количество_выходящих_задач as "Количество выходящих задач"
WHERE Дата_создания = this.Дата_создания
limit 1
```
# [[cmake export()]]

- Получение источников и ссылок
	- [x] Получение одной ссылки
	- [ ] Получение ссылок на разные источники
- Конспектирование
	- [x] Достаточное для использования конспектирование
	- [ ] Полное конспектирование
- Создание справок
	- [ ] Справки практического применения
	- [ ] Список терминов
	- [ ] Определение терминов из списка терминов
	- [ ] Список связанных тем(если нет внутри текста)
- [ ] Внедрение в практику

%%
## Результат:



## Следующая задача:

==#Входящие ==
%%

**Содержание потом вырезается, все содержимое конспекта удаляется и содержание вставляется по шаблону входящего конспекта.**

# Содержание 
%%
> [!tldr]- Практическая справка
> 
%%

> [!seealso] [[Синопсис]]
> ```cmake
> export(TARGETS <target>... [...])
export(EXPORT<export-name> [...])
>export(PACKAGE <PackageName>)
>```

> [!note] Использование
> ```cmake
> export(TARGETS <target>... [NAMESPACE <namespace>]
>       [APPEND] FILE <filename> [EXPORT_LINK_INTERFACE_LIBRARIES]
>       [CXX_MODULES_DIRECTORY <directory>])
>```

Export targets or packages for outside projects to use them ==directly(???)== from the current [[cmake project build tree|project's build tree]], without installation.

See the [[cmake install()#^dd98c7]][^install_export] command to export targets from an install tree.

Creates a file `<filename>` that may be included by outside projects to import targets named by `<target>...` from the current project's build tree. This is useful during cross-compiling to build utility executables that can run on the host platform in one project and then import them into another project being compiled for the target platform.


> [!danger]
> The file created by this command is specific to the build tree and should never be installed. See the [[cmake install()#^dd98c7|install(EXPORT)]] command to export targets from an install tree.


#Входящие/EnglishUsed 

%%
> [!example]- Список терминов
> - [ ] Пример
%%

> [!question]- Соурслист
> 1. https://cmake.org/cmake/help/latest/command/export.html#command:export

%%
## Приложения
%%

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

[^install_export]: [`install(EXPORT)`](https://cmake.org/cmake/help/latest/command/install.html#command:install "install")

---
#ВнедрениеЗнаний #Программирование/cmake/команда 