---
Тип: Задача
Описание: -
Исполнители: 
Проект: Гентра

Дисциплина: #Входящие 
Tags: Задача
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: 31 # Не трогать!
Месяц: 01 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-01-31

дата_завершения: "2023-02-20"

Выполнено: да
Результат: отмена
---

```dataview
TABLE день_завершения as "День истечения", месяц_завершения as "Месяц истечения", Проект, Выполнено, Результат, Количество_выходящих_задач as "Количество выходящих задач"
WHERE Дата_создания = this.Дата_создания
limit 1
```
# [[Разработка планировщика трехмерной траектории на основе кривых Reeds-Shepp]]

- Получение источников и ссылок
	- [ ] Получение одной ссылки
	- [ ] Получение ссылок на разные источники
- Конспектирование
	- [ ] Достаточное для использования конспектирование
	- [ ] Полное конспектирование
- Создание справок
	- [ ] Справки практического применения
	- [ ] Список терминов
	- [ ] Определение терминов из списка терминов
	- [ ] Список связанных тем(если нет внутри текста)
- Внедрение в базу знаний
	- [ ] Создание связей с оригинальной страницей (этой)
	- [ ] Разбиение на подтемы
	- [ ] Атомизация - (по необходимости) разбиение конспекта по подтемам на страницы
- [ ] Внедрение в практику

%%
## Результат:



## Следующая задача:

==#Входящие ==
%%

**Содержание потом вырезается, все содержимое конспекта удаляется и содержание вставляется по шаблону входящего конспекта.**

# Содержание 

## ChatGPT

The Reeds-Shepp algorithm is a path planning algorithm for a car-like robot, which can be implemented in robotic systems to generate a smooth path between two points while avoiding obstacles. The algorithm generates a path by concatenating a set of basic curvatures called Reeds-Shepp primitives. The algorithm is particularly useful in robotics due to its ability to generate paths that consider the kinematic constraints of a car-like robot, such as maximum turning radius and forward speed.

Mathematically, the Reeds-Shepp algorithm can be formulated as follows:

Given two points $p_{start}$ and $p_{goal}$ in a 2D plane, the algorithm generates a path $\Gamma(t) = [x(t), y(t)]^T$ between them by concatenating a set of Reeds-Shepp primitives $\gamma_i(t)$. Each primitive is a smooth curve with a fixed length and describes a basic maneuver of the robot, such as a straight line, a circle with a certain radius, or a combination of the two.

The algorithm starts by finding the set of possible Reeds-Shepp primitives that connect $p_{start}$ and $p_{goal}$. This can be done by searching the configuration space of the robot and checking which primitives can connect the two points while avoiding obstacles. Once the set of primitives is found, the algorithm concatenates them to generate the final path $\Gamma(t)$.

In order to generate the path, the Reeds-Shepp primitives need to be parameterized in terms of a parameter $t \in [0, 1]$. This parameter represents the progression of the path from $p_{start}$ to $p_{goal}$ and can be used to interpolate between the two points. For example, for a straight line primitive, the parameterization would be as follows:

$\gamma_{straight}(t) = p_{start} + t(p_{goal} - p_{start})$

For a circle primitive, the parameterization would be as follows:

$\gamma_{circle}(t) = p_{center} + R\left(\cos(\omega t), \sin(\omega t)\right)^T$

where $R$ is the radius of the circle and $\omega$ is the angular velocity.

Once the primitives are parameterized, the Reeds-Shepp algorithm can concatenate them to generate the final path $\Gamma(t)$. This can be done by combining the parameterizations of each primitive and ensuring that the resulting path is smooth and continuous.

In summary, the Reeds-Shepp algorithm generates a path for a car-like robot by concatenating a set of basic curvatures called Reeds-Shepp primitives. The algorithm starts by finding the set of possible primitives that connect the start and goal points, and then concatenates them to generate the final path. The Reeds-Shepp algorithm takes into account the kinematic constraints of the robot and generates paths that are smooth and continuous.

%%
### Справки и термины

> [!tldr]- Практическая справка
> 
%%

> [!example]- Список терминов [^def]
> - [ ] Пример

> [! example] Список вопросов [^que]
> 

#Входящие 


> [!question]- Соурслист
> 1. https://chat.openai.com/chat/7c879012-22f2-4ca2-9d6f-7f837be32fd0

%%
## Приложения
%%

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

---
#ВнедрениеЗнаний 
#Входящие 

---

[^def]: термин
[^que]: вопрос