### Quick Note about logger levels

Logging levels are prioritized in the following order:

Fatal
Error
Warn
Info
Debug

Fatal has the highest priority and Debug has the lowest. By setting the logger level, you will get all messages of that priority level or higher. For example, by setting the level to Warn, you will get all Warn, Error, and Fatal logging messages.