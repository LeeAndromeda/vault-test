# srv файлы

srv файлы похожи на сообщения, но они содержат две части: запрос и ответ. Части разделяются "\---". Пример:
```
int64 A
int64 B
---
int64 Sum
```
A и B это запрос, а Sum ответ.

## Настройка srv файлов

### 1.  Unless you have done so already, open package.xml, and make sure these two lines are in it and uncommented:

```
  <build_depend>message_generation</build_depend>
  <exec_depend>message_runtime</exec_depend>
```

### 2. Unless you have done so already for messages in the previous step, add the message_generation dependency to generate messages in CMakeLists.txt:
```
# Do not just add this line to your CMakeLists.txt, modify the existing line
find_package(catkin REQUIRED COMPONENTS
  roscpp
  rospy
  std_msgs
  message_generation
)
```
(Despite its name, message_generation works for both msg and srv.)

### 3. Remove # to uncomment the following lines:
```
# add_service_files(
#   FILES
#   Service1.srv
#   Service2.srv
# )
```
### 4. And replace the placeholder Service*.srv files for your service files:
```
add_service_files(
  FILES
  AddTwoInts.srv
)
```

## Общий шаг для msg и srv

[[Общий шаг для msg и srv]]

#Робототехника/ROS 