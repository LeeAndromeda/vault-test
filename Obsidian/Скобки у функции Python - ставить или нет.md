---
Тип: Задача
Описание: -
Исполнители: 
Проект: ВнедрениеЗнаний

Tags: Задача
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: 27 # Не трогать!
Месяц: 06 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-06-27

дата_завершения: "2023-нет-нет"

Выполнено: нет
Результат: 
---

# [[Скобки у функции Python - ставить или нет]]

> [!danger] Обязательный чеклист
> - [ ] Получение одной ссылки
> - [ ] Расставить теги 


> [!example]+  Чеклист знания
> - Получение источников и ссылок
> 	- [ ] Получение ссылок на разные источники
> - Конспектирование
> 	- [ ] Достаточное для использования конспектирование
> 	- [ ] Полное конспектирование
> - Создание справок
> 	- [ ] Справки практического применения
> 	- [ ] Список терминов
> 	- [ ] Определение терминов из списка терминов
> 	- [ ] Список связанных тем(если нет внутри текста)
> - Внедрение в базу знаний
> 	- [ ] Создание связей с оригинальной страницей (этой)
> 	- [ ] Разбиение на подтемы
> 	- [ ] Атомизация - (по необходимости) разбиение конспекта по подтемам на страницы
> - [ ] Внедрение в практику
> 

<mark style="background: #FF5582A6;">**Содержание потом вырезается, все содержимое конспекта удаляется и содержание вставляется по шаблону входящего конспекта.**</mark> 

# Содержание 

6/27/2023, 8:55:44 PM
Here I provide you with a code that converts numpy arrays into torch tensors. Please, provide a detailed explanation on what is happening:

```python
import torch

x_train, y_train, x_valid, y_valid = map(
    torch.tensor, (x_train, y_train, x_valid, y_valid)
)
n, c = x_train.shape
print(x_train, y_train)
print(x_train.shape)
print(y_train.min(), y_train.max())
```

---

6/27/2023, 8:56:00 PM

The code snippet demonstrates the conversion of NumPy arrays into PyTorch tensors. The conversions are performed using the torch.tensor function.

First, the code imports the PyTorch library with the statement import torch.

Next, the code uses the map() function to apply the torch.tensor function to each of the four NumPy arrays x_train, y_train, x_valid, and y_valid. This creates four corresponding PyTorch tensor objects: x_train, y_train, x_valid, and y_valid.

After the conversion, the code prints out the shape of the x_train tensor using the .shape attribute. The x_train tensor has a shape of (n, c), where n is the number of samples in the training set, and c is the number of features. It also prints out the min and max values of y_train tensor using the .min() and .max() methods, respectively.

Overall, this code is useful when working with machine learning models that require data to be in PyTorch tensor format for efficient computation of gradients during backpropagation.

---

6/27/2023, 8:56:43 PM
What does the torch.tensor function do? Why is it written without the brackets?

---

6/27/2023, 8:56:52 PM

torch.tensor is a function in PyTorch that creates a new tensor from an existing array-like object, such as a NumPy array or a Python list. The torch.tensor function returns a new PyTorch tensor object with the same data type and shape as the input array-like object.

In the code snippet provided, the torch.tensor function is used to convert each of the four NumPy arrays x_train, y_train, x_valid, and y_valid into PyTorch tensors. This is achieved by mapping the torch.tensor function over these arrays using the map() function, which applies the function to each element of the iterable and returns an iterator that yields the results.

Regarding the use of the torch.tensor function without brackets, this is because torch.tensor is a function object and can be passed around like any other object in Python. In the code snippet, the torch.tensor function is being passed as an argument to the map() function, which takes a function and an iterable as arguments. Therefore, it is not necessary to call the function with parentheses when passing it as an argument to another function.

---

6/27/2023, 8:59:01 PM
So what I can dervive from your statement is that the brackets are used to explicitly call the function object, and they are not used when I'm passing the function object to somewhere else, where I want to be used further?

---

6/27/2023, 8:59:08 PM

Yes, that's correct!

The parentheses () are used to explicitly call a function object and execute it. For example, in the code snippet provided, torch.tensor is called with parentheses to create new PyTorch tensor objects from the NumPy arrays.

On the other hand, when passing a function object as an argument to another function, we don't need to call it using parentheses. Instead, we only pass the function object itself. In the code snippet provided, the map() function takes two arguments - a function (torch.tensor) and an iterable (the tuple of NumPy arrays). By passing torch.tensor without parentheses, we are passing the function object itself rather than calling it.

So, in summary, you use parentheses to call a function and execute it, and you omit them when passing a function object to another function or method.

---

6/27/2023, 9:00:54 PM
Let's assume I have a function (func1) that accepts another function's (func2) object as an argument. How would I call func2 inside func1?

---

6/27/2023, 9:01:04 PM

To call func2 inside func1, you can simply pass the arguments to func2 as you normally would when calling a function.

Here's an example:
```python

def func1(func2, arg1, arg2):
    """
    Example function that accepts another function as an argument.
    """
    result = func2(arg1, arg2)
    return result

def func2(x, y):
    """
    Example function that is passed as an argument to func1.
    """
    return x + y
# Call func1 with func2 as an argument
result = func1(func2, 2, 3)

print(result)  # Output: 5
```

In this example, func1 is defined to take three arguments: func2, arg1, and arg2. Inside func1, we call func2 by passing the arguments arg1 and arg2 to it using parentheses. We then assign the result of the func2 call to the variable result and return it.

When we call func1 in the last line of the code, we pass func2 as the first argument. This effectively passes the func2 function object itself as an argument to func1.

I hope this helps!

## Смотрите также:

> [!question]- Соурслист
> 1. ChatGPT
> 2. https://chatbot.theb.ai/#/chat/1687888506866

> [!quote]- Контекст
> ```dataview
> LIST 
> FROM !outgoing([[]])
> WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
> ```

---
#ВнедрениеЗнаний 
#chatgpt 
#Python 

---

[^def]: термин
[^que]: вопрос