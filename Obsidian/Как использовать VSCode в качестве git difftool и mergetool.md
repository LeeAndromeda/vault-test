---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 18 # Не трогать!
Месяц: 05 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-05-18

---
# [[Как использовать VSCode в качестве git difftool и mergetool]]

## Configuration

To update your git configuration, run the following command:
```shell
git config --global core.editor 'code --wait'
```
If you prefer that a new window opens each time, add the `--new-window` code flag:
```shell
git config --global core.editor 'code --wait --new-window'
```
If you only want to change it for your current project, run the same command without the _–global_ git flag.

Unhappy and want to go back?
```shell
git config --global --unset core.editor
```
## Difftool

To configure it from the command-line:

```shell
git config --global diff.tool vscode   
git config --global difftool.vscode.cmd 'code --wait --diff $LOCAL $REMOTE'
```
This adds the following settings to your global Git config:

```plaintext
[diff]
	tool = vscode
[difftool "vscode"]
	cmd = code --wait --diff $LOCAL $REMOTE
```

## Mergetool

To do it from the command-line:
```shell
git config --global merge.tool vscode
git config --global mergetool.vscode.cmd 'code --wait $MERGED'
```

This adds the following settings to your global Git config:
```txt
[merge]
	tool = vscode
[mergetool "vscode"]
	cmd = code --wait $MERGED
```
You can paste this in yourself if you prefer.

If you’re not feeling VS Code as your merge tool, you run the command `git mergetool --tool-help` to see more options.

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://www.roboleary.net/vscode/2020/09/15/vscode-git.html#configuration


---
#git 
#vscode 

---

[^def]: термин
[^que]: вопрос