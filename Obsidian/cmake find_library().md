---
Тип: Задача
Описание: -
Исполнители: 
Проект: ВнедрениеЗнаний

Дисциплина: #Входящие 
Tags: Задача
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: 06 # Не трогать!
Месяц: 12 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Tuesday 6th December 2022 18:54:38

день_завершения: нет
месяц_завершения: нет
год_завершения: 2022
дата_завершения: 2022-нет-нет

Выполнено: нет
Результат: 
---

```dataview
TABLE день_завершения as "День истечения", месяц_завершения as "Месяц истечения", Проект, Выполнено, Результат, Количество_выходящих_задач as "Количество выходящих задач"
WHERE contains(file.name, "cmake find_library()")
limit 1
```
# [[cmake find_library()]]

- Получение источников и ссылок
	- [x] Получение одной ссылки
	- [ ] Получение ссылок на разные источники
- Конспектирование
	- [ ] Достаточное для использования конспектирование
	- [ ] Полное конспектирование
- Создание справок
	- [ ] Справки практического применения
	- [ ] Список терминов
	- [ ] Список связанных тем(если нет внутри текста)
- [ ] Внедрение в практику

%%
## Результат:



## Следующая задача:

==#Входящие ==
%%

**Содержание потом вырезается, все содержимое конспекта удаляется и содержание вставляется по шаблону входящего конспекта.**

# Содержание 

Эта команда используется, чтобы находить библиотеки.

Из источника 1:

```cmake
find_library (
          <VAR>
          name | NAMES name1 [name2 ...] [NAMES_PER_DIR]
          [HINTS [path | ENV var]... ]
          [PATHS [path | ENV var]... ]
          [REGISTRY_VIEW (64|32|64_32|32_64|HOST|TARGET|BOTH)]
          [PATH_SUFFIXES suffix1 [suffix2 ...]]
          [VALIDATOR function]
          [DOC "cache documentation string"]
          [NO_CACHE]
          [REQUIRED]
          [NO_DEFAULT_PATH]
          [NO_PACKAGE_ROOT_PATH]
          [NO_CMAKE_PATH]
          [NO_CMAKE_ENVIRONMENT_PATH]
          [NO_SYSTEM_ENVIRONMENT_PATH]
          [NO_CMAKE_SYSTEM_PATH]
          [NO_CMAKE_INSTALL_PREFIX]
          [CMAKE_FIND_ROOT_PATH_BOTH |
           ONLY_CMAKE_FIND_ROOT_PATH |
           NO_CMAKE_FIND_ROOT_PATH]
         )
```

- \<VAR\> - переменная, в которой будет храниться <mark style="background: #FF5582A6;">результат</mark> работы команды

#Входящие 

%%## Приложения%%

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

> [!question]- Соурслист
> 1. https://cmake.org/cmake/help/latest/command/find_library.html


---
#Программирование/cmake/команда 