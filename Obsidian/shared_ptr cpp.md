
---
Тип: Задача
Описание: -
Исполнители: 
Проект: ВнедрениеЗнаний

Дисциплина:  Программирование
Tags: Задача
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

День: 02 # Не трогать!
Месяц: 12 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Friday 2nd December 2022 02:45:27

день_завершения: 7
месяц_завершения: 12
год_завершения: 2022
дата_завершения: "2022-12-07"

Выполнено: нет
Результат: 
---

```dataview
TABLE день_завершения as "День истечения", месяц_завершения as "Месяц истечения", Проект, Выполнено, Результат, Количество_выходящих_задач as "Количество выходящих задач"
WHERE contains(file.name, "shared_ptr cpp")
limit 1
```
# [[shared_ptr cpp]]

- [x] Получение источников и ссылок
- [x] Конспектирование
- [ ] Создание справок
- [ ] Внедрение в практику

%%
## Результат:



## Следующая задача:

#Входящие 
%%

**Содержание потом вырезается, все содержимое конспекта удаляется и содержание вставляется по шаблону входящего конспекта.**

# Содержание 

## Инициализация

Когда ресурс в памяти создается впервые, по возможности используйте функцию [[Pre: std make_shared cpp]] [^1] для создания указателя `shared_ptr`. Функция `make_shared` безопасна в отношении исключений. Для выделения памяти под блок управления и ресурс используется один вызов, что снижает накладные расходы. Если вы не используете функцию `make_shared`, то придется использовать явное выражение **`new`** для создания объекта, прежде чем передавать его в конструктор `shared_ptr`. В приведенном ниже примере представлены различные способы объявления и инициализации указателя `shared_ptr` вместе с новым объектом:

```cpp
// Use make_shared function when possible.
auto sp1 = make_shared<Song>(L"The Beatles", L"Im Happy Just to Dance With You");

// Ok, but slightly less efficient. 
// Note: Using new expression as constructor argument
// creates no named variable for other code to access.
shared_ptr<Song> sp2(new Song(L"Lady Gaga", L"Just Dance"));

// When initialization must be separate from declaration, e.g. class members, 
// initialize with nullptr to make your programming intent explicit.
shared_ptr<Song> sp5(nullptr);
//Equivalent to: shared_ptr<Song> sp5;
//...
sp5 = make_shared<Song>(L"Elton John", L"I'm Still Standing");
```

## Объявление shared_ptr для совместного владения указателем на объект

В приведенном ниже примере показано, как объявить и инициализировать экземпляры `shared_ptr`, которые будут совместно владеть объектом, память для которого уже выделена с помощью другого экземпляра `shared_ptr`. Предполагается, что `sp2` — это инициализированный указатель `shared_ptr`.

```cpp
//Initialize with copy constructor. Increments ref count.
auto sp3(sp2);

//Initialize via assignment. Increments ref count.
auto sp4 = sp2;

//Initialize with nullptr. sp7 is empty.
shared_ptr<Song> sp7(nullptr);

// Initialize with another shared_ptr. sp1 and sp2
// swap pointers as well as ref counts.
sp1.swap(sp2);
```

## Копирование объектов в другие контейнеры STL

Указатели `shared_ptr` также полезны при использовании алгоритмов копирования элементов в контейнеры стандартной библиотеки C++. Элемент можно заключить в указатель `shared_ptr`, а затем копировать его в другие контейнеры, учитывая при этом, что выделенная область памяти доступна только до тех пор, пока она требуется. В приведенном ниже примере показано, как использовать алгоритм `remove_copy_if` применительно к экземплярам `shared_ptr` в векторе.

```cpp
vector<shared_ptr<Song>> v {
  make_shared<Song>(L"Bob Dylan", L"The Times They Are A Changing"),
  make_shared<Song>(L"Aretha Franklin", L"Bridge Over Troubled Water"),
  make_shared<Song>(L"Thalía", L"Entre El Mar y Una Estrella")
};

vector<shared_ptr<Song>> v2;
remove_copy_if(v.begin(), v.end(), back_inserter(v2), [] (shared_ptr<Song> s) 
{
    return s->artist.compare(L"Bob Dylan") == 0;
});

for (const auto& s : v2)
{
    wcout << s->artist << L":" << s->title << endl;
}
```

## [[Pre: dynamic_pointer_cast]]

#Входящие 


> [!question]- Соурслист
> 1. https://learn.microsoft.com/ru-ru/cpp/cpp/how-to-create-and-use-shared-ptr-instances?view=msvc-170 - из этой статьи неудачно рассмотрен пример 4 в связи с непониманием некоторых конструкций, а так же примеры 5 и 6
> 2. https://habr.com/ru/post/191018/ - проблемы при использовании shared_ptr

## Смотрите также:

- [[Преобразование типов указателей|pointer_type_conversion.pdf]]

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

---
#ВнедрениеЗнаний #cpp 

[^1]:  [make_shared](https://learn.microsoft.com/ru-ru/cpp/standard-library/memory-functions?view=msvc-170#make_shared)