---
Тип: Задача
Описание: -
Дисциплина: Работа
Проект: АРМ 
Tags: Задача 
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

День: 05 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Wednesday 5th October 2022 17:45:58

день_завершения: нет
месяц_завершения: 0
год_завершения: 2022

Выполнено: да
Результат: 
---

```dataview
TABLE день_завершения as "День истечения", месяц_завершения as "Месяц истечения", Проект, Выполнено, Результат
WHERE contains(file.name, "Ознакомиться с информацией, которую предоставил Владимир MoCap")
limit 1
```
# [[Ознакомиться с информацией, которую предоставил Владимир MoCap]]


## Результат: 



## Следующая задача:


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

