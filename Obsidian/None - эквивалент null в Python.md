---

Tags: 
aliases: 
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

Тип: конспект
Описание: -

День: 20 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Thursday 20th October 2022 16:47:05

---
# [[None - эквивалент null в Python]]

## Эквивалент null в Python: None

Он был разработан таким образом, по двум причинам:

Многие утверждают, что слово _null_ несколько эзотерично. Это не наиболее дружелюбное слово для новичков. Кроме того, **None** относится именно к требуемой функциональности - это ничего, и не имеет поведения.

Присвоить переменной значение None очень просто:
```Python
my_none_variable = None
```
Существует много случаев, когда следует использовать None.

Часто вы хотите выполнить действие, которое может работать либо завершиться неудачно. Используя None, вы можете проверить успех действия. Вот пример:

# Мы хотели бы подключиться к базе данных. Мы не знаем, верны ли логин и пароль
# Если соединение с базой будет неуспешно, то
# Он бросит исключение. Обратите внимание, что MyDatabase и DatabaseException
# НЕ являются реальными классами, мы просто используем их в качестве примеров.
```Python
try:
    database = MyDatabase(db_host, db_user, db_password, db_database)
    database_connection = database.connect()
except DatabaseException:
    pass

if database_connection is None:
    print('The database could not connect')
else:
    print('The database could connect')
```


Python является объектно-ориентированным, и поэтому None - тоже объект, и имеет свой тип.
```Python
>>>

>>> type(None)
<class 'NoneType'>
```

## Проверка на None

Есть (формально) два способа проверить, на равенство None.

Один из способов - с помощью [ключевого слова](https://pythonworld.ru/osnovy/klyuchevye-slova-modul-keyword.html) **is**.

```Python
null_variable = None
not_null_variable = 'Hello There!'

# The is keyword
if null_variable is None:
    print('null_variable is None')
else:
    print('null_variable is not None')

if not_null_variable is None:
    print('not_null_variable is None')
else:
    print('not_null_variable is not None')

# The == operator
if null_variable == None:
    print('null_variable is None')
else:
    print('null_variable is not None')

if not_null_variable == None:
    print('not_null_variable is None')
else:
    print('not_null_variable is not None')
```

Второй - с помощью **\=\=** (но никогда не пользуйтесь вторым способом, потому что с пользовательскими типами он может работать некорректно).

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

---
#Входящие 