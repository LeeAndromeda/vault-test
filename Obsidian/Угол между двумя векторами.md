---
Тип: конспект
Описание: -

День: 26
Месяц: 08
Год: 2022
Дата_последнего_изменения: Friday 26th August 2022 10:21:32
Tags:
---
# [[Угол между двумя векторами]]

Рассчет делается по формуле скалярного произведения векторов:
![[skalar_dt_product.jpg]]

## Пример на С++


```cpp
angles.yaw = acos((tmpvec_x * 1 + tmpvec_y * 0) / (sqrt(tmpvec_x * tmpvec_x + tmpvec_y * tmpvec_y) * sqrt(1 * 1 + 0 * 0)));

angles.yaw = (angles.yaw) * (tmpvec_y > 0) + (-angles.yaw) * (!(tmpvec_y > 0));
```
## Смотрите также:

#Математика #Программирование 