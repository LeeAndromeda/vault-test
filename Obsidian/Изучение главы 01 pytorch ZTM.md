---
Тип: Задача
Описание: -
Исполнители: 
Проект: pytorch ztm

Дисциплина: #Входящие 
Tags: Задача
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: 22 # Не трогать!
Месяц: 06 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-06-22

дата_завершения: "2023-06-25"

Выполнено: да
Результат: 
---

# [[Изучение главы 01 pytorch ZTM]]

> [!note]+ Pytorch workflow
> ![[01_a_pytorch_workflow.png]]

## How to split the data to train a neural network

| Split           | Purpose                                                                                                                       | Amount of total data  | How often is it used? |
|-----------------|-------------------------------------------------------------------------------------------------------------------------------|-----------------------|-----------------------|
| Training set    | The model learns from this data (like the course materials you study during the semester).                                    | ~60-80%               | Always                |
| Validation set  | The model gets tuned on this data (like the practice exam you take before the final exam).                                    | ~10-20%               | Often but not always  |
| Testing set     | The model gets evaluated on this data to test what it has learned (like the final exam you take at the end of the semester).  | ~10-20%               | Always                |

> [!attention]
> **Note:** When dealing with real-world data, this step is typically done right at the start of a project (the test set should always be kept separate from all other data). We want our model to learn on training data and then evaluate it on test data to get an indication of how well it **generalizes** to unseen examples.

> [!important]
> 
> **Note:** Now's a good time to introduce you to the data explorer's motto... ***"visualize, visualize, visualize!"***
> 
> Think of this whenever you're working with data and turning it into numbers, if you can visualize something, it can do wonders for understanding.
> 
> Machines love numbers and we humans like numbers too but we also like to look at things.

## Python model building essentials

PyTorch has four (give or take) essential modules you can use to create almost any kind of neural network you can imagine.

They are [`torch.nn`](https://pytorch.org/docs/stable/nn.html), [`torch.optim`](https://pytorch.org/docs/stable/optim.html), [`torch.utils.data.Dataset`](https://pytorch.org/docs/stable/data.html#torch.utils.data.Dataset) and [`torch.utils.data.DataLoader`](https://pytorch.org/docs/stable/data.html). For now, we'll focus on the first two and get to the other two later (though you may be able to guess what they do).

| PyTorch module      | What does it do?                                                                                                                                                                                                                              |
|---------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| torch.nn            | Contains all of the building blocks for computational graphs (essentially a series of computations executed in a particular way).                                                                                                             |
| torch.nn.Parameter  | Stores tensors that can be used with nn.Module. If requires_grad=True gradients (used for updating model parameters via gradient descent) are calculated automatically, this is often referred to as "autograd".                              |
| torch.nn.Module     | The base class for all neural network modules, all the building blocks for neural networks are subclasses. If you're building a neural network in PyTorch, your models should subclass nn.Module. Requires a forward() method be implemented. |
| torch.optim         | Contains various optimization algorithms (these tell the model parameters stored in nn.Parameter how to best change to improve gradient descent and in turn reduce the loss).                                                                 |
| def forward()       | All nn.Module subclasses require a forward() method, this defines the computation that will take place on the data passed to the particular nn.Module (e.g. the linear regression formula above).                                             |

If the above sounds complex, think of like this, almost everything in a PyTorch neural network comes from `torch.nn`,

- `nn.Module` contains the larger building blocks (layers)
- `nn.Parameter` contains the smaller parameters like weights and biases (put these together to make `nn.Module`(s))
- `forward()` tells the larger blocks how to make calculations on inputs (tensors full of data) within `nn.Module`(s)
- `torch.optim` contains optimization methods on how to improve the parameters within `nn.Parameter` to better represent input data

[[Pytorch Hierarchy.canvas|Pytorch Hierarchy]] --> ![[Pytorch Hierarchy.png]]

```leaflet
id: leaflet-idmnbdrtyuiko987trdfghju765redcgh
image: Pasted image 20230627062727.png
height: 500px
lat: 50
long: 50
minZoom: 1
maxZoom: 10
defaultZoom: 7
unit: meters
scale: 1
darkMode: false
```



## Making predictions with `torch.inference_mode()`

To check this we can pass it the test data `X_test` to see how closely it predicts `y_test`.

When we pass data to our model, it'll go through the model's `forward()` method and produce a result using the computation we've defined.

Let's make some predictions.
```python
# Make predictions with model
with torch.inference_mode(): 
    y_preds = model_0(X_test)

# Note: in older PyTorch code you might also see torch.no_grad()
# with torch.no_grad():
#   y_preds = model_0(X_test)
```
Hmm?

You probably noticed we used [`torch.inference_mode()`](https://pytorch.org/docs/stable/generated/torch.inference_mode.html) as a [context manager](https://realpython.com/python-with-statement/) (that's what the `with torch.inference_mode():` is) to make the predictions.

As the name suggests, `torch.inference_mode()` is used when using a model for inference (making predictions).

`torch.inference_mode()` turns off a bunch of things (like gradient tracking, which is necessary for training but not for inference) to make **forward-passes** (data going through the `forward()` method) faster.

> [!info]
> **Note:** In older PyTorch code, you may also see `torch.no_grad()` being used for inference. While `torch.inference_mode()` and `torch.no_grad()` do similar things,

`torch.inference_mode()` is newer, potentially faster and preferred. See this [Tweet from PyTorch](https://twitter.com/PyTorch/status/1437838231505096708?s=20) for more.

## Train a model

### Creating a loss function and an optimizer in Pytorch

| Function       | What does it do?                                                                                                             | Where does it live in PyTorch?                                              | Common values                                                                                                                                        |
|----------------|------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------|
| Loss function  | Measures how wrong your models predictions (e.g. y_preds) are compared to the truth labels (e.g. y_test). Lower the better.  | PyTorch has plenty of built-in loss functions in torch.nn.                  | Mean absolute error (MAE) for regression problems (torch.nn.L1Loss()). Binary cross entropy for binary classification problems (torch.nn.BCELoss()). |
| Optimizer      | Tells your model how to update its internal parameters to best lower the loss.                                               | You can find various optimization function implementations in torch.optim.  | Stochastic gradient descent (torch.optim.SGD()). Adam optimizer (torch.optim.Adam()).                                                                |

For our problem, since we're predicting a number, let's use MAE (which is under `torch.nn.L1Loss()`) in PyTorch as our loss function.

![[Pasted image 20230627065141.png]]

_Mean absolute error (MAE, in PyTorch: `torch.nn.L1Loss`) measures the absolute difference between two points (predictions and labels) and then takes the mean across all examples._

And we'll use SGD, `torch.optim.SGD(params, lr)` where:

- `params` is the target model parameters you'd like to optimize (e.g. the `weights` and `bias` values we randomly set before).
- `lr` is the **learning rate** you'd like the optimizer to update the parameters at, higher means the optimizer will try larger updates (these can sometimes be too large and the optimizer will fail to work), lower means the optimizer will try smaller updates (these can sometimes be too small and the optimizer will take too long to find the ideal values). The learning rate is considered a **hyperparameter** (because it's set by a machine learning engineer). Common starting values for the learning rate are `0.01`, `0.001`, `0.0001`, however, these can also be adjusted over time (this is called [learning rate scheduling](https://pytorch.org/docs/stable/optim.html#how-to-adjust-learning-rate)). - [[Настройка скорости обучения в процессе обучения нейронной сети - Pytorch]]

### Creating an optimization loop

To create these we're going to write a Python `for` loop in the theme of the [unofficial PyTorch optimization loop song](https://twitter.com/mrdbourke/status/1450977868406673410?s=20) (there's a [video version too](https://youtu.be/Nutpusq_AFw)).

![[Pasted image 20230627070317.png]]

#### Pytorch training loop

| Number  | Step name                                | What does it do?                                                                                                                                                                       | Code example                    |
|---------|------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------|
| 1       | Forward pass                             | The model goes through all of the training data once, performing its forward() function calculations.                                                                                  | model(x_train)                  |
| 2       | Calculate the loss                       | The model's outputs (predictions) are compared to the ground truth and evaluated to see how wrong they are.                                                                            | loss = loss_fn(y_pred, y_train) |
| 3       | Zero gradients                           | The optimizers gradients are set to zero (they are accumulated by default) so they can be recalculated for the specific training step.                                                 | optimizer.zero_grad()           |
| 4       | Perform backpropagation on the loss      | Computes the gradient of the loss with respect for every model parameter to be updated (each parameter with requires_grad=True). This is known as backpropagation, hence "backwards".  | loss.backward()                 |
| 5       | Update the optimizer (gradient descent)  | Update the parameters with requires_grad=True with respect to the loss gradients in order to improve them.                                                                             | optimizer.step()                |

```leaflet
id: leaflet-idlkjuhytredszxcvbnjkloiuytrfdxcvbnmloi7trdcvbn
image: Pasted image 20230627070430.png
height: 500px
lat: 50
long: 50
minZoom: 1
maxZoom: 10
defaultZoom: 7
unit: meters
scale: 1
darkMode: false
```

> [!attention]
> **Note:** The above is just one example of how the steps could be ordered or described. With experience you'll find making PyTorch training loops can be quite flexible.
> 
> And on the ordering of things, the above is a good default order but you may see slightly different orders. Some rules of thumb:
> 
> - Calculate the loss (`loss = ...`) _before_ performing backpropagation on it (`loss.backward()`).
> - Zero gradients (`optimizer.zero_grad()`) _before_ stepping them (`optimizer.step()`).
> - Step the optimizer (`optimizer.step()`) _after_ performing backpropagation on the loss (`loss.backward()`).

#### Pytorch testing loop

| Number  | Step name                               | What does it do?                                                                                               | Code example                   |
|---------|-----------------------------------------|----------------------------------------------------------------------------------------------------------------|--------------------------------|
| 1       | Forward pass                            | The model goes through all of the training data once, performing its forward() function calculations.          | model(x_test)                  |
| 2       | Calculate the loss                      | The model's outputs (predictions) are compared to the ground truth and evaluated to see how wrong they are.    | loss = loss_fn(y_pred, y_test) |
| 3       | Calulate evaluation metrics (optional)  | Alongisde the loss value you may want to calculate other evaluation metrics such as accuracy on the test set.  | Custom functions               |

Notice the testing loop doesn't contain performing backpropagation (`loss.backward()`) or stepping the optimizer (`optimizer.step()`), this is because no parameters in the model are being changed during testing, they've already been calculated. For testing, we're only interested in the output of the forward pass through the model.

```leaflet
id: leaflet-idkjhgfdrthjkyfdertghu76rgtyu76ui876re3456yhgfdcvghytrertyuiokjhgfdsaqwedsq
image: Pasted image 20230627070826.png
height: 500px
lat: 50
long: 50
minZoom: 1
maxZoom: 10
defaultZoom: 7
unit: meters
scale: 1
darkMode: false
```


## Making predictions with a Pytorch model

Once you've trained a model, you'll likely want to make predictions with it.

We've already seen a glimpse of this in the training and testing code above, the steps to do it outside of the training/testing loop are similar.

There are three things to remember when making predictions (also called performing inference) with a PyTorch model:

1. Set the model in evaluation mode (`model.eval()`).
2. Make the predictions using the inference mode context manager (`with torch.inference_mode(): ...`).
3. All predictions should be made with objects on the same device (e.g. data and model on GPU only or data and model on CPU only).

The first two items make sure all helpful calculations and settings PyTorch uses behind the scenes during training but aren't necessary for inference are turned off (this results in faster computation). And the third ensures that you won't run into cross-device errors.



## Смотрите также:
- https://www.learnpytorch.io/01_pytorch_workflow/
- # <mark style="background: #FF5582A6;">[[01.py|Код к конспекту]]</mark> 

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```


---
#pytorch 

---