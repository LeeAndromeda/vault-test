---

Tags: 
aliases: 
regex-to-search: "((.*)[Cc][Mm]ake(.*))"
regex-to-be-found: "((.*)[Cc][Mm]ake(.*))"

Тип: конспект
Описание: -

День: 28 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Friday 28th October 2022 13:18:27

---
# [[CMake]]

## Основные задачи в проекте

- [[Cmake изначальные настройки проекта]]
- [[Добавление исполняемого файла в проект CMake]]

## Команды для CMakeLists.txt

- [[cmake find_library()]]
- [[cmake target_include_directories()]]
- [[cmake export()]]
- [[cmake install()]]

### Old

- `project("project cmake name")`
- `cmake_minimum_required(VERSION 3.16)` - указывает минимальную версию (указывай свою версию cmake)
- `add_executable(exec_name [source1] [source2])` - exec_name для каждой программы в проекте должен быть уникальным
- Для включения пользовательских библиотек нужнр использовать:
	- `include_directories(include)` - включение директории с заголовочными файлами
	- `add_library(lib src/header.cpp include/header.h)` - добавление заголовочного файла, как библиотеки, с добавлением реализации
	- для каждого исполняемого cpp файла записать:
		- `add_executable(main src/main.cpp)` - добавляет в список на билд
		- `target_link_libraries(main lib)` - добавляет в main путь к lib

> [!todo]- Пример кода
> ```cmake
> project("header_test")
>cmake_minimum_required(VERSION 3.16)
>
>include_directories(include)
>
>add_library(lib src/header.cpp include/header.h)
>
>add_executable(main src/main.cpp)
>target_link_libraries(main lib)
>```



## Переменные

- [[cmake target - что это]]
- [[cmake installation prefix]]
- [[CMAKE_CURRENT_SOURCE_DIR]]

## [[cmake-generator-expressions|Выражения генератора]]

- [[cmake BUILD_INTERFACE genex]]
- [[cmake INSTALL_INTERFACE genex]]

## Смотрите также:

- [[Использование заголовочных файлов с классами]]
- [[cmake создание shared library cpp]]

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```



> [!question]- Соурслист
> 1. [add_executable](https://cmake.org/cmake/help/latest/command/add_executable.html)


---
#Программирование/cmake 