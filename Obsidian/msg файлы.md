# msg файлы

**msg** - простые текстовые файлы с типом поля и именем поля на каждой линии.

> *Типы полей, которые вы можете использовать:*
> -   int8, int16, int32, int64 (plus uint*)
> -   float32, float64
> -   string
> -   time, duration
> -   other msg files
> -   variable-length array[] and fixed-length array[C]
> В РОС так же есть особый тип данных - Header. Он содержит временную метку и информацию координатной сетки, которые часто используются в РОС.

Вот пример msg файла:


```
  Header header
  string child_frame_id
  geometry_msgs/PoseWithCovariance pose
  geometry_msgs/TwistWithCovariance twist
```

Давайте определим новый msg в пакете, который был создан в предыдущем уроке.
```
$ roscd beginner_tutorials
$ mkdir msg
$ echo "int64 num" > msg/Num.msg
```
Приведенный выше пример файла .msg содержит только 1 строку. Конечно, вы можете создать более сложный файл, добавив несколько элементов, по одному на строку, как показано ниже:
```cpp
string first_name
string last_name
uint8 age
uint32 score
```
## Настройка пакета для работы с сообщениями

Нам нужно убедиться, что наши msg файлы превращаются в код для C++, питона и других языков:

### 1.

Откройте package.xml и убедитесь, что там есть две строки и они незакомментированы:

```
  <build_depend>message_generation</build_depend>
  <exec_depend>message_runtime</exec_depend>
```

Заметь, что во время компиляции нам нужен "message_generation", а во время запуска нам нужен только "message_runtime".

### 2.

Open **CMakeLists.txt** in your favorite text editor ([rosed](http://wiki.ros.org/ROS/Tutorials/UsingRosEd) from the previous tutorial is a good option).

Добавь зависимость **message_generation** в**find_package** call, который уже существует в вашем CMakeLists.txt так, что вы сможете генерировать сообщения. ВЫ можете сделать это просто добавив **message_generation** в **list of COMPONENTS** так, что это будет выглядеть так:

```
# НЕ ВСТАВЛЯТЬ БЕЗДУМНО, ОТРЕДАКТИРОВАТЬ УЖЕ СУЩЕСТВУЮЩЕЕ
find_package(catkin REQUIRED COMPONENTS
   roscpp
   rospy
   std_msgs
   message_generation
)
```

> You may notice that sometimes your project builds fine even if you did not call find_package with all dependencies. This is because catkin combines all your projects into one, so if an earlier project calls find_package, yours is configured with the same values. But forgetting the call means your project can easily break when built in isolation.

### 3.

Убедитесь, что вы экспортируете message runtime dependency.

```
catkin_package(
  ...
  CATKIN_DEPENDS message_runtime ...
  ...)
```

### 4.

Науди следующий блок кода:

```
# add_message_files(
#   FILES
#   Message1.msg
#   Message2.msg
# )
```

Раскомментируй и затем замени Message*.msg своими:

```
add_message_files(
  FILES
  Num.msg
)
```

Добавляя .msg файлы вручную, мы убеждаемся, что CMake знает, когда ему нужно перенастроить проект, после того, как вы добавили новые .msg файлы.

### 5.

Теперь нам нужно убедиться, что generate_messages() вызывается.

_Для ROS Hydro и позже_ вам нужно раскомментировать следующие строки:

```
# generate_messages(
#   DEPENDENCIES
#   std_msgs
# )
```

-   так что это выглядит так:

```
    generate_messages(
      DEPENDENCIES
      std_msgs
    )
```

## Общий шаг для msg и srv

[[Общий шаг для msg и srv]]

# Связано:

- ## Использование [[rosmsg]]

#Робототехника/ROS