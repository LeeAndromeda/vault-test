---
Тип: ДЗ
Дисциплина: Английский
Описание: -

День: 17
Месяц: 09
Год: 2022
Дата_последнего_изменения: Saturday 17th September 2022 16:25:21
день_завершения: 30
месяц_завершения: 09
год_завершения: 2022

Выполнено: да

Tags: ДомашнееЗадание
---
# [[Выполнить дз по инглишу на пятницу и подготовить факты об актере, который занимался благотворительностью]]

Дз в файлах в тимс в группе.
> [!hint]- Актер, который занимается благотворительностью - Киану Ривз
> # The Most Expensive Charities Made By Keanu Reeves
>
Keanu Reeves' generosity goes beyond his media presence and the Matrix star often makes hefty donations for various causes.
>
Keanu Reeves is one of the most loved celebrities in Hollywood, and .
>
Keanu Reeves has been working in the film industry since the late 1980s. [He gained popularity](https://www.therichest.com/rich-powerful/a-peek-inside-the-million-dollar-fortune-of-keanu-reeves/) for starring in the romance drama _Dangerous Liaisons_ and the comedy _Bill And Ted’s Excellent Adventures_. With his mastery in martial arts, he has been a part of some of the biggest action movies that have hiked his [net worth to a whopping $380 million.](https://www.therichest.com/celebnetworth/celeb/keanu-reeves-net-worth/) Known for his rugged looks and mind-blowing stunts, Reeves is also one of the most wholesome figures in Hollywood. The actor has a generous personality and tends to help people and organizations that work for notable causes.
>
From helping cancer research centers to donating large portions of his salaries to fund the stunt team, Keanu Reeves has always gone above and beyond to help people and anonymously create a difference in the world. One of the most generous people in the industry.
>
>## Cancer Research Donations
>
When _The Matrix_ was released in 1999, the science-fiction movie was an instant hit, earning Reeves a whopping $45 million from the salary and overall box office sales. With his sister Kim suffering from leukemia, cancer research has always been a big part of Reeves’ donations. According to [Vogue](https://www.vogue.com/article/keanu-reeves-donation-cancer-research), he donated $31.5 million of his salary to the noble cause.
>
In 2020, Keanu stepped up during the pandemic to help various organizations. One of the organizations was Camp Rainbow Gold, a children’s cancer charity from Idaho. In June, Reeves auctioned 15-minutes of virtual date with him through Zoom. The bidding began at $10,000 and landed at $16,000 until an anonymous bidder bagged the date for a whopping $75,200
>
>## Gifting Customized Rolex
>
>Keanu Reeves is the nicest man in Hollywood for a reason. The actor has always financially supported his staff and [stunt team on movies](https://www.therichest.com/expensive-lifestyle/10-actors-who-do-their-own-stunts/) that take a lot of effort to shoot the action-packed sequences. After _John Wick 4_ wrapped in October 2021, the actor took his stunt team that consists of Bruce Concepcion, Jeremy Marinas, Li Qiang, and Dave Camarillo to a dinner in Paris. He gifted them $10,000 worth of customized Rolex Submariner watches with the personal inscription John Wick 5 engraved on them, as stated by [Vanity Fair](https://www.vanityfair.com/style/2021/10/keanu-reeves-john-wick-stunt-crew-wrap-gift-personalized-rolexes). There was a more personal message alongside the inscription that read “Jeremy Thank you Keanu JW4 2021”, which was personalized for every stuntman.
>
>## $75 Million _The Matrix_ Earnings To The Crew
>
Keanu loves his work and always has a lot of respect for the people who work behind the scenes with him. Along with earning $45 million for _The Matrix_, he made a hefty amount from the collection of _The Matrix Revolutions,_ and _The Matrix Reloaded_ was released in 2003. He donated an estimated $75 million from his earnings so the crew could receive one million in bonus.
>
>## Secret Foundation For Cancer Research
>
> >Keanu Reeves leads secret cancer foundation which funds children's hospitals.[https://t.co/jyxBWzUmsa](https://t.co/jyxBWzUmsa) [pic.twitter.com/vhizOoWpRv](https://t.co/vhizOoWpRv)
> >
> >— LADbible (@ladbible) [January 18, 2018](https://twitter.com/ladbible/status/953900910568263681?ref_src=twsrc%5Etfw)
>
The actor, known to make large impacts in minor instances, was once spotted sharing food and listening to stories of a homeless man on the streets of Los Angeles. He has also made large donations to cancer research charities to uplift the lives of the needy. In September 2020, when the artist was forced to share his contributions, he admitted that he has been running a secret organization for cancer research for many years and doesn’t like to attach his name to it. As reported by [Mirror](https://www.mirror.co.uk/3am/celebrity-news/keanu-reeves-helps-fund-children-22710264), the organization has helped make valuable efforts in research and provide funding to children’s hospitals in the USA.
>
>## SickKids Foundation
>
He supported numerous causes through his secret charity and donated millions to various foundations. One of the most prominent foundations actively changing lives, SickKids Foundation, is looking for donations to build a state-of-the-art hospital that will research new cures and offer optimum care to the patients. Intending to raise $1.5 billion in grants, Reeves has donated millions to support their cause.
>
>## Being Generous To Stunt Crew
>
Keanu is known to give even [more expensive gifts](https://www.therichest.com/world-entertainment/nicki-minaj-paid-for-her-fans-university-tuitions-and-19-other-expensive-gifts-celebs-gave-their-fans/) to the stunt team of _The Matrix Reloaded_. For the Neo v Smith scene in the movie, the production hired 12 stuntmen to work as clones. The actor purchased 12 Harley Davidson bikes for all the doubles in the film, which was upwards of $15,000 per bike. After the trilogy wrapped in 2003, he gifted each person on the 800-crew movie set an expensive bottle of champagne that was an additional $50,000, as reported by [Yahoo Finance](https://finance.yahoo.com/finance/news/keanu-reeves-gave-stunt-crew-201927577.html).
>
>## PETA Donations
>
An international nonprofit that advocates for animal rights and safety, Keanu Reeves has always been vocal about his love for animals and how they deserve kindness. The actor has donated over a million dollars to help the organization complete its goals. He has also actively supported their causes on the internet.
>
From lending a hand to study cancer research to be generous to his film crew by offering expensive giveaways, Keanu Reeves is the most generous man in the industry who appreciates the people around him.
>
>## Смотрите также:

![Иллюстрации к дз в рабочей тетради](IMG_20220929_222721.md)

#ГумманитарныеНауки/Английский 