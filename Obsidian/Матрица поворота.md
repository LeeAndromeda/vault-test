$$\begin{bmatrix} \cos \theta & \pm \sin \theta \\ \pm \sin \theta & \cos \theta\end{bmatrix}$$

## Особенности

Конкретные знаки в формулах зависят от того, является ли система координат правосторонней или левосторонней, и выполняется ли вращение по или против часовой стрелки. Верхний знак указан для обычного соглашения: [[Правосторонняя и левосторонняя системы координат|правосторонняя система координат]] и положительное направление вращения против часовой стрелки (тот же знак верен для [[Правосторонняя и левосторонняя системы координат|левосторонней координатной системы]] при выборе положительного направления вращения по часовой стрелке; в оставшихся двух комбинациях — нижний знак).

## Поворот вектора матрицей поворота
 ![[Pasted image 20220704155854.png]]

 $$\begin{bmatrix} x ^{'}\\ y ^{ '}\end{bmatrix} = \begin{bmatrix} \cos \theta & \pm \sin \theta \\ \pm \sin \theta & cos \theta\end{bmatrix} \begin{bmatrix} x\\ y\end{bmatrix}$$

> [!hint]+ Пример на C++
>
> В [[Правосторонняя и левосторонняя системы координат#Правосторонняя система координат|правосторонней системе координат]]:
>
> ```cpp
> finvec_x = tmpvec_x * cos(angles.yaw) - tmpvec_y * sin(angles.yaw);
> finvec_y = tmpvec_x * sin(angles.yaw) + tmpvec_y * cos(angles.yaw);
> ```


## Смотрите также

- [[Умножение матриц]]

#Робототехника #Математика #вектор 