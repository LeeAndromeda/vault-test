---
Тип: Задача
Описание: -
Исполнители: 
Проект: ВнедрениеЗнаний

Дисциплина: Программирование
Tags: Задача
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

День: 02 # Не трогать!
Месяц: 12 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Friday 2nd December 2022 02:25:50

день_завершения: 10
месяц_завершения: 1
год_завершения: 2023
дата_завершения: "2023-01-10"

Выполнено: нет
Результат: 
---

```dataview
TABLE день_завершения as "День истечения", месяц_завершения as "Месяц истечения", Проект, Выполнено, Результат, Количество_выходящих_задач as "Количество выходящих задач"
WHERE contains(file.name, Рубин)
limit 1
```
# [[std thread cpp]]

- Получение источников и ссылок
	- [ ] Получение одной ссылки
	- [ ] Получение ссылок на разные источники
- Конспектирование
	- [ ] Достаточное для использования конспектирование
	- [ ] Полное конспектирование
- Создание справок
	- [ ] Справки практического применения
	- [ ] Список терминов
	- [ ] Список связанных тем(если нет внутри текста)
- [ ] Внедрение в практику

%%
## Результат:



## Следующая задача:

==#Входящие ==
%%

**Содержание потом вырезается, все содержимое конспекта удаляется и содержание вставляется по шаблону входящего конспекта.**

# Содержание 

> [!note]- Справка
> - Снанчала следует инициализировать поток:
> 	- `std::thread thr_name(func_obj, args, args...)` - `func_obj` - функция, которая будет выполняться в этом потоке
> - За этим нужно запуситить поток:
> 	- `thr_name.join()`
> - Если вы хотите поставить поток на паузу, используйте эту функцию:
> 	- `std::this_thread::sleep_for(std::chrono::milliseconds(10);` - вместо миллисекунд можно использовать другое время из библиотеки chrono


Изначально выбранная для потоков библиотека называется [[pthread cpp|pthread]]. Однако пока я решил отказаться от ее изучения в пользу std::thread по причине достаточного срока существоваия этой библиотеки в C++ и уверенности в её дальнейшем существовании и эффективности.

## std::thread объект класса

### Конструктор
```cpp
thread() noexcept; //  not a thread
thread( thread&& other ) noexcept; // move the other thread to this one. Deletes the other thread
template< class Function, class... Args > // clear declaration. pass function and its arguments
explicit thread( Function&& f, Args&&... args ); // clear declaration. pass function and its arguments
thread( const thread& ) = delete; // ??
```

### Функция потока

Функцией, выполняемой в потоке может быть любая обычная функция. Объект выполняемой функции является первым аргументом, передаваемым в конструктор std::thread, остальеые аргументы являются аргументами самой функции.

```cpp
std::thread thread2(reg_bridge, &anpa_ctx, csp_p);
```

Далее, чтобы запустить поток, нужно вызвать функцию `std::thread::join`:

```cpp
thread2.join();
```

Чтобы запустить несколько потоков, нужно сначала инициализировать объекты потоков, а после использовать функцию `std::thread::join` для каждого из них:

```cpp
std::thread thread1(nav_and_sensors_bridge, &anpa_ctx, anpa_parser);
std::thread thread2(reg_bridge, &anpa_ctx, csp_p);
std::thread thread3(spin_subscribers, csp_p);
std::thread thread4(reg_publisher_thread, &reg_pub_ctx, csp_p);

thread1.join();
thread2.join();
thread3.join();
thread4.join();
```

## Встреченные проблемы

- [[error static assertion failed std thread arguments must be invocable after conversion to rvalues]]

> [!question]- Соурслист
> 1. https://en.cppreference.com/w/cpp/thread/thread
> 2. https://habr.com/ru/post/326138/ pthread
> 3. https://eax.me/pthreads/ pthread
> 4. https://www.cs.cmu.edu/afs/cs/academic/class/15492-f07/www/pthreads.html pthread
> 5. https://www.geeksforgeeks.org/thread-functions-in-c-c/ pthread

## Смотрите также:

- [[Нити процесса или потоки, что это и как это понять]]
```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

---
#ВнедрениеЗнаний/НеобходимоеЗнание  #cpp #Рубин 