---

Tags: 
aliases: 
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

Тип: конспект
Описание: -

День: 20 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Thursday 20th October 2022 16:31:19

---
# [[Ввод-вывод в Python]]

## Вывод

```Python
print('Hello World!') # Hello World!
```

```python
print(4.5, "hello") # 4.5 hello
```

```Python
print(4.5, "hello", end = '|') # 4.5 hello|
```

`end = 'input'` заменяет конец строки на 'input'

## Ввод

> [!caution]- Непонятная информация о функции input()
> 
>```Python
>x = int(raw_input ("Введи число:"))
>print "Квадрат этого числа составляет ", x * x
>```
>
>Для ввода-вывода в Python следует использовать `raw_input(invitation)`, которая выведет на экран строку `invitation`, а затем будет ожидать >пользовательского ввода.
>
>> [!caution]+ Угроза безопасности - функция input()
> >Внимание! Несмотря на существование функции input() схожего действия, использовать ее в программах не рекомендуется, так как интерпретатор пытается выполнить вводимые с ее помощью синтаксические выражения, что является серьезной дырой в безопасности программы.

### Множественный ввод

The developer often wants a user to enter multiple values or inputs in one line. In C++/C user can take multiple inputs in one line using `scanf` but in Python user can take multiple values or inputs in one line by two methods. 

- Using `split()` method
- Using List comprehension

#### Using `split()` method : 

This function helps in getting multiple inputs from users. It breaks the given input by the specified separator. If a separator is not provided then any white space is a separator. Generally, users use a `split()` method to split a Python string but one can use it in taking multiple inputs.

Syntax : 

```python
input().split(separator, maxsplit)
```

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

---
#Python 