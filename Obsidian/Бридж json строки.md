---
Тип: Задача
Описание: -
Исполнители: 
Проект: бридж

Дисциплина: #Входящие 
Tags: Задача
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: 10 # Не трогать!
Месяц: 05 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-05-10

дата_завершения: "2023-05-11"

Выполнено: да
Результат: 
---



# [[Бридж json строки]]

Кастомное сообщение СГРУ(строка): 8100
[Топик](https://github.com/AndrewKLoyd/protoDroch): <mark style="background: #FF5582A6;">?</mark> 

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```


---

---