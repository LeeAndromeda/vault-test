# rosrun 

```
$ rosrun [package_name] [node_name]
```

Можно при запуске переименовать нод:

```
rosrun turtlesim turtlesim_node __name:=my_turtle
```

#Робототехника/ROS