---

Tags: 
aliases: 
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

Тип: конспект
Описание: -

День: 02 # Не трогать!
Месяц: 11 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Wednesday 2nd November 2022 12:00:59

---
# [[Hello world сервер-клиент на ZMQ на C++]]

![[Pasted image 20221102141318.png]]

REQ-REP пара сокетов закреплена в алгоритме действий относительно друг друга. Клиент сначала выполняет [zmq_send()](http://api.zeromq.org/master:zmq_send), а затем [zmq_recv()](http://api.zeromq.org/master:zmq_recv), в цикле (или единожды, если это все, что ему нужно). Выполнение любой другой последовательности  (например, отправление двух соодщений подряд) приведт к return code -1 от send или recv call. Подобным образом, сервис выолняет сначала [zmq_recv()](http://api.zeromq.org/master:zmq_recv) и затем [zmq_send()](http://api.zeromq.org/master:zmq_send) в этом порядке так часто, как ему нужно.

## Приложения 

### Приложение 1. Hello world сервер на C++

```cpp
//
//  Hello World server in C++
//  Binds REP socket to tcp://*:5555
//  Expects "Hello" from client, replies with "World"
//
#include <zmq.hpp>
#include <string>
#include <iostream>
#ifndef _WIN32
#include <unistd.h>
#else
#include <windows.h>

#define sleep(n)	Sleep(n)
#endif

int main () {
    //  Prepare our context and socket
    zmq::context_t context (2);
    zmq::socket_t socket (context, zmq::socket_type::rep);
    socket.bind ("tcp://*:5555");

    while (true) {
        zmq::message_t request;

        //  Wait for next request from client
        socket.recv (request, zmq::recv_flags::none);
        std::cout << "Received Hello" << std::endl;

        //  Do some 'work'
        sleep(1);

        //  Send reply back to client
        zmq::message_t reply (5);
        memcpy (reply.data (), "World", 5);
        socket.send (reply, zmq::send_flags::none);
    }
    return 0;
}
```


> [!cite]- Мои выводы
> ```cpp
> zmq::context_t context (2);
> zmq::socket_t socket (context, zmq::socket_type::rep);
> socket.bind ("tcp://*:5555");
>```
> Этот элемент в коде встречается везде - создается контекст, создается сокет, сокет привязывается к адресу
> 
> - Обрати внимание, что сообщение `zmq::message_t` инициализируется в исполняемом цикле. Этот тип данных небходими перебилдить каждый раз, когда ты его используешь.
> - `zmq::socket_t::recv(zmq::message_t, zmq::recv_flags)` - функция, которая ожидает прихода сообщения и не дает программе исполняться дальше, пока не получит сообщение. [[Pre: zmq::socket_t::recv()]] 

### Приложение 2. Hellow world клиент на C++
```cpp
//
// Hello World client in C++
// Connects REQ socket to tcp://localhost:5555
// Sends "Hello" to server, expects "World" back
//
#include <zmq.hpp>
#include <string>
#include <iostream>
  
int main ()
{
	// Prepare our context and socket
	zmq::context_t context (1);
	zmq::socket_t socket (context, zmq::socket_type::req);
  
	std::cout << "Connecting to hello world server..." << std::endl;
	socket.connect ("tcp://localhost:5555");
  
	// Do 10 requests, waiting each time for a response
	for (int request_nbr = 0; request_nbr != 10; request_nbr++) {
		zmq::message_t request (5);
		memcpy (request.data (), "Hello", 5);
		std::cout << "Sending Hello " << request_nbr << "..." << std::endl;
		socket.send (request, zmq::send_flags::none);
  
		// Get the reply.
		zmq::message_t reply;
		socket.recv (reply, zmq::recv_flags::none);
		std::cout << "Received World " << request_nbr << std::endl;
	}
	return 0;
}
```
## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

> [!question]- Соурслист
> 1. 


---
#ZMQ #cpp 