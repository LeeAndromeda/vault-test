---
Тип: Задача
Описание: -
Исполнители: 
Проект: ВнедрениеЗнаний

Tags: Задача
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: 23 # Не трогать!
Месяц: 05 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-05-23

дата_завершения: "2023-05-23"

Выполнено: нет
Результат: 
---

# [[JSON для современного C++]]

> [!danger] Обязательный чеклист
> - [ ] Получение одной ссылки
> - [ ] Расставить теги 


> [!example]+  Чеклист знания
> - Получение источников и ссылок
> 	- [ ] Получение ссылок на разные источники
> - Конспектирование
> 	- [ ] Достаточное для использования конспектирование
> 	- [ ] Полное конспектирование
> - Создание справок
> 	- [ ] Справки практического применения
> 	- [ ] Список терминов
> 	- [ ] Определение терминов из списка терминов
> 	- [ ] Список связанных тем(если нет внутри текста)
> - Внедрение в базу знаний
> 	- [ ] Создание связей с оригинальной страницей (этой)
> 	- [ ] Разбиение на подтемы
> 	- [ ] Атомизация - (по необходимости) разбиение конспекта по подтемам на страницы
> - [ ] Внедрение в практику
> 

<mark style="background: #FF5582A6;">**Содержание потом вырезается, все содержимое конспекта удаляется и содержание вставляется по шаблону входящего конспекта.**</mark> 

# Содержание 

## JSON в качестве класса 

Here are some examples to give you an idea how to use the class.

Assume you want to create the JSON object

```json
{
  "pi": 3.141,
  "happy": true,
  "name": "Niels",
  "nothing": null,
  "answer": {
    "everything": 42
  },
  "list": [1, 0, 2],
  "object": {
    "currency": "USD",
    "value": 42.99
  }
}
```

With this library, you could write:
```cpp
// create an empty structure (null)
json j;

// add a number that is stored as double (note the implicit conversion of j to an object)
j["pi"] = 3.141;

// add a Boolean that is stored as bool
j["happy"] = true;

// add a string that is stored as std::string
j["name"] = "Niels";

// add another null object by passing nullptr
j["nothing"] = nullptr;

// add an object inside the object
j["answer"]["everything"] = 42;

// add an array that is stored as std::vector (using an initializer list)
j["list"] = { 1, 0, 2 };

// add another object (using an initializer list of pairs)
j["object"] = { {"currency", "USD"}, {"value", 42.99} };

// instead, you could also write (which looks very similar to the JSON above)
json j2 = {
  {"pi", 3.141},
  {"happy", true},
  {"name", "Niels"},
  {"nothing", nullptr},
  {"answer", {
    {"everything", 42}
  }},
  {"list", {1, 0, 2}},
  {"object", {
    {"currency", "USD"},
    {"value", 42.99}
  }}
};
```

## Сериализация / Десериализация

### В / из строки

В объект json строка может быть преобразована с помощью [`json::parse()`](https://json.nlohmann.me/api/basic_json/parse/):

```cpp
// parse explicitly
auto j3 = json::parse(R"({"happy": true, "pi": 3.141})");
```

###### JSON dump()

[`.dump()`](https://json.nlohmann.me/api/basic_json/dump/) возвращает <mark style="background: #FF5582A6;">изначально хранимую строку</mark> ?????.

В строк объект json может быть преобразован через его функцию `dump()`: 

```cpp
// explicit conversion to string
std::string s = j.dump();    // {"happy":true,"pi":3.141}
```

Можно указать количество пробелов для индентации:

```cpp
// serialization with pretty printing
// pass in the amount of spaces to indent
std::cout << j.dump(4) << std::endl;
// {
//     "happy": true,
//     "pi": 3.141
// }
```

###### Разница между сериализацией и присваиванием

```cpp
// store a string in a JSON value
json j_string = "this is a string";

// retrieve the string value
auto cpp_string = j_string.get<std::string>();
// retrieve the string value (alternative when a variable already exists)
std::string cpp_string2;
j_string.get_to(cpp_string2);

// retrieve the serialized value (explicit JSON serialization)
std::string serialized_string = j_string.dump();

// output of original string
std::cout << cpp_string << " == " << cpp_string2 << " == " << j_string.get<std::string>() << '\n';
// output of serialized value
std::cout << j_string << " == " << serialized_string << std::endl;
```

### Другие варианты серилазиции / десериализации

- https://github.com/nlohmann/json#tofrom-streams-eg-files-string-streams
- https://github.com/nlohmann/json#read-from-iterator-range
- https://github.com/nlohmann/json#custom-data-source

## STL подобный доступ к объектам

### Создание массива методом `push_back()` или `emplace_back()`

```cpp
// create an array using push_back
json j;
j.push_back("foo");
j.push_back(1);
j.push_back(true);

// also use emplace_back
j.emplace_back(1.78);
```

### Итерация через массив в JSON

```cpp
// iterate the array
for (json::iterator it = j.begin(); it != j.end(); ++it) {
  std::cout << *it << '\n';
}

// range-based for
for (auto& element : j) {
  std::cout << element << '\n';
}
```

### Get / set методы

```cpp
// getter/setter
const auto tmp = j[0].get<std::string>();
j[1] = 42;
bool foo = j.at(2);
```

### Сравнение

```cpp
// comparison
j == R"(["foo", 1, true, 1.78])"_json;  // true
```

### Вспомогательные функции
```cpp
// other stuff
j.size();     // 4 entries
j.empty();    // false
j.type();     // json::value_t::array
j.clear();    // the array is empty again
```

### Проверка типов
```cpp
// convenience type checkers
j.is_null();
j.is_boolean();
j.is_number();
j.is_object();
j.is_array();
j.is_string();
```

### Создание объекта
```cpp
// create an object
json o;
o["foo"] = 23;
o["bar"] = false;
o["baz"] = 3.141;

// also use emplace
o.emplace("weather", "sunny");
```

### Обращение к ключу или значению
```cpp
// special iterator member functions for objects
for (json::iterator it = o.begin(); it != o.end(); ++it) {
  std::cout << it.key() << " : " << it.value() << "\n";
}

// the same code as range for
for (auto& el : o.items()) {
  std::cout << el.key() << " : " << el.value() << "\n";
}

// even easier with structured bindings (C++17)
for (auto& [key, value] : o.items()) {
  std::cout << key << " : " << value << "\n";
}

```

## Краткое руководство по преобразованию из контейнеров STL с помощью библиотеки json для C++

Библиотека json для C++ позволяет пользователям преобразовывать последовательные и ассоциативные контейнеры в массивы и объекты JSON соответственно. Можно использовать следующие контейнеры:

- Контейнеры последовательностей: std::array, std::vector, std::deque, std::forward_list, std::list.
- Ассоциативные контейнеры: std::map, std::multimap, std::unordered_map, std::unordered_multimap, std::set, std::multiset, std::unordered_set, std::unordered_multiset.

### Создание массивов JSON

Чтобы создать JSON массив из контейнера последовательности STL, такого как std::vector, std::deque, std::forward_list, std::list или std::array, просто передайте контейнер конструктору json:


```cpp
std::vector<int> c_vector{1, 2, 3, 4};
json j_vec(c_vector);
```

В этом примере `c_vector` передается в конструктор json, в результате чего создается объект массива JSON `j_vec`, содержащий `[1, 2, 3, 4]`.

### Создание JSON-объектов

Чтобы создать объект JSON из ассоциативного контейнера STL, такого как std::map, std::multimap, std::unordered_map, std::unordered_multimap, std::set, std::multiset, std::unordered_set, std::unordered_multiset, передайте контейнер конструктору json:

```cpp
std::map<std::string, int> c_map{{"один", 1}, {"два", 2}, {"три", 3}};
json j_map(c_map);
```

В этом примере `c_map` передается конструктору json, в результате чего создается JSON-объект `j_map`, содержащий `{"one": 1, "3": 3, "2": 2 }`.

Обратите внимание, что в случае мультимапов в объекте JSON используется только один ключ, а значение зависит от внутреннего порядка контейнера STL. Аналогично, в случае ассоциативных контейнеров, где присутствуют дублирующиеся ключи, в объекте JSON используется только одна запись для ключа.

## JSON C++ Library Type Conversion Documentation

The JSON C++ library supports implicit conversions of certain types to JSON values. However, it is not recommended to use implicit conversions from a JSON value. To switch off implicit conversions, define `JSON_USE_IMPLICIT_CONVERSIONS` to 0 before including the `json.hpp` header or set the option `JSON_ImplicitConversions` to `OFF` in CMake.

### Implicit Conversions

#### Strings

Strings can be implicitly converted to JSON values as shown below.

```cpp
std::string s1 = "Hello, world!";
json js = s1;
auto s2 = js.get<std::string>();
```

It is not recommended to use implicit conversions from a JSON value.
```cpp
std::string s3 = js;
std::string s4;
s4 = js;
```

#### Booleans

Booleans can be implicitly converted to JSON values as shown below.

```cpp
bool b1 = true;
json jb = b1;
auto b2 = jb.get<bool>();
```


It is not recommended to use implicit conversions from a JSON value.

```cpp
bool b3 = jb;
bool b4;
b4 = jb;
```


#### Numbers

Numbers can be implicitly converted to JSON values as shown below.

cppCopy Code

```cpp
int i = 42;
json jn = i;
auto f = jn.get<double>();
```

It is not recommended to use implicit conversions from a JSON value.
```cpp
double f2 = jb;
double f3;
f3 = jb;
```

### Arbitrary Types Conversions

Every type can be serialized in JSON, not just STL containers and scalar types. To serialize a custom type, you need to define two functions: `to_json` and `from_json`.
```cpp
namespace ns {
    struct person {
        std::string name;
        std::string address;
        int age;
    };
}

ns::person p = {"Ned Flanders", "744 Evergreen Terrace", 60};

// serialize to JSON
json j;
j["name"] = p.name;
j["address"] = p.address;
j["age"] = p.age;

// deserialize from JSON
ns::person p {
    j["name"].get<std::string>(),
    j["address"].get<std::string>(),
    j["age"].get<int>()
};
```


To simplify the serialization/deserialization process, two macros are provided:

-   `NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(name, member1, member2, ...)` - to be defined inside the namespace of the class/struct to create code for.
-   `NLOHMANN_DEFINE_TYPE_INTRUSIVE(name, member1, member2, ...)` - to be defined inside the class/struct to create code for. This macro can also access private members.
```cpp
namespace ns {
    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(person, name, address, age)
}
```


### Converting Third-Party Types

To convert third-party types, you need to add a specialization of `adl_serializer` to the nlohmann namespace.

```cpp
namespace nlohmann {
    template <typename T>
    struct adl_serializer<boost::optional<T>> {
        static void to_json(json& j, const boost::optional<T>& opt) {
            if (opt == boost::none) {
                j = nullptr;
            } else {
              j = *opt; // this will call adl_serializer<T>::to_json which will
                        // find the free function to_json in T's namespace!
            }
        }

        static void from_json(const json& j, boost::optional<T>& opt) {
            if (j.is_null()) {
                opt = boost::none;
            } else {
                opt = j.get<T>(); // same as above, but with
                                  // adl_serializer<T>::from_json
            }
        }
    };
}
```

## Смотрите также:

> [!question]- Соурслист
> 1. https://github.com/nlohmann/json#read-json-from-a-file

> [!quote]- Контекст
> ```dataview
> LIST 
> FROM !outgoing([[]])
> WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
> ```

---
#ВнедрениеЗнаний 
#JSON 
#cpp

---

[^def]: термин
[^que]: вопрос