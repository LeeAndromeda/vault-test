# Общий шаг для msg и srv

Если вы этого не делали до этого, совершите следующие изменения в CMakeLists.txt. :
```
# generate_messages(
#   DEPENDENCIES
# #  std_msgs  # Or other packages containing msgs
# )
```

Раскомментируй и добавь все пакеты, от которых вы зависите, которые содержат \*.msg файлы, которые используют ваши сообщения так, что это будет выглядеть примерно так:
```
generate_messages(
  DEPENDENCIES
  std_msgs
)
```
#Робототехника/ROS 