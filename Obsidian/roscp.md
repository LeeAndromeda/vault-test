# roscp

Позволяет копировать файлы из одного пакета в другой.

```
$ roscp [package_name] [file_to_copy_path] [copy_path]
```

#Робототехника/ROS 