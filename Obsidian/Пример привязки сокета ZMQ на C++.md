---

Tags: 
aliases: 
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

Тип: конспект
Описание: -

День: 31 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Monday 31st October 2022 12:50:47

---
# [[Пример привязки сокета ZMQ на C++]]


```cpp
#include <string>
#include <zmq.hpp>
int main()
{
   zmq::context_t ctx;
   zmq::socket_t sock(ctx, zmq::socket_type::push);
   sock.bind("inproc://test");
   const std::string_view m = "Hello, world";
   sock.send(zmq::buffer(m), zmq::send_flags::dontwait);
}
```

> [!cite]- Мои выводы
> - `zmq::context_t` - класс контекста(?)
> 	- `zmq::socket_t` - класс для работы с сокетом. 
> 	- Конструктор принимает:
> 		1. Контекст
> 		2. Тип сокета
> 	- `zmq::socket_t::bind("protocol://address")` - привязывает сокет к определенному адресу
> 	- `zmq::socket_t::send()` - функция, отвечающая за отправку


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

> [!question]- Соурслист
> 1. 


---
#ZMQ 