# Обсидиан - информация по использованию


## Продвинутые концепты использования обсидиан

```dataview
TABLE FROM #НовоеПоколениеЗнания 
SORT file.name ASC
```
### Использование обсидиан для тайм менеджмента

- ![[Структурная схема видов записей для тайм менеджмента и их отношений]]


## Базовые туториалы обсидиан

```dataview
TABLE FROM #PKM/obsidian or #туториал and !#НовоеПоколениеЗнания and !#kanban
SORT file.name ASC
```

## Недоделанные туториалы

```dataview
TABLE FROM #PKM/obsidian or #туториал and #недо
```

# Связано:

- [[Гайд по использованию основных функций Obsidian для новичков]]

#PKM/obsidian #туториал 