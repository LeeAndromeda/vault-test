# Уроки Linux(для работы с SQL) - Online Programming Courses

## Список видео

- [Уроки Linux - 1. Базовые команды Linux](https://www.youtube.com/watch?v=NTqB5ged4Rw&list=PLOFNiVk8RnFeV9pwLHpf_Nc72M0HgzSn2)
- [02 - Уроки Linux. Создание своих команд](https://www.youtube.com/watch?v=svs1zTRlXfE&list=PLOFNiVk8RnFeV9pwLHpf_Nc72M0HgzSn2&index=2)
- [03 - Уроки Linux. Копирование файлов на сервер](https://www.youtube.com/watch?v=NhSF7AcTNTU&list=PLOFNiVk8RnFeV9pwLHpf_Nc72M0HgzSn2&index=3)
- [04 - Уроки Linux. Установка nginx, php, mysql](https://www.youtube.com/watch?v=lzSz6IiXAck&list=PLOFNiVk8RnFeV9pwLHpf_Nc72M0HgzSn2&index=4)
- [05 - Уроки Linux. Настройка nginx](https://www.youtube.com/watch?v=s27NwdrVDhw&list=PLOFNiVk8RnFeV9pwLHpf_Nc72M0HgzSn2&index=5)
- [06 - Уроки Linux. Установка yii2 на ubuntu](https://www.youtube.com/watch?v=khpMJiZDvkE&list=PLOFNiVk8RnFeV9pwLHpf_Nc72M0HgzSn2&index=6)
- [07 - Уроки Linux. Установка PHPstorm](https://www.youtube.com/watch?v=nLWK5pHxihY&list=PLOFNiVk8RnFeV9pwLHpf_Nc72M0HgzSn2&index=7)
- [08 - Уроки Linux. yii2 и консоль](https://www.youtube.com/watch?v=qeJ-Yx9daII&list=PLOFNiVk8RnFeV9pwLHpf_Nc72M0HgzSn2&index=8)
- [09 - Уроки Linux. Настройка БД yii2, задачи в cron](https://www.youtube.com/watch?v=PKzx4NsBbK8&list=PLOFNiVk8RnFeV9pwLHpf_Nc72M0HgzSn2&index=9)
- [10 - Уроки Linux. Раширения для yii2](https://www.youtube.com/watch?v=9VCgJG5bJFE&list=PLOFNiVk8RnFeV9pwLHpf_Nc72M0HgzSn2&index=10)
- [11 - Уроки Linux. Установка Redis на Linux](https://www.youtube.com/watch?v=yfq4hRDlzXE&list=PLOFNiVk8RnFeV9pwLHpf_Nc72M0HgzSn2&index=11)
- [12 - Уроки Linux. Знакомство с git](https://www.youtube.com/watch?v=kvJUs2guruQ&list=PLOFNiVk8RnFeV9pwLHpf_Nc72M0HgzSn2&index=12)

#Linux 