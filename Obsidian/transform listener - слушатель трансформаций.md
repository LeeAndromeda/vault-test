# transform listener - слушатель трансформаций

Слушатель трансформаций нужен, чтобы находить смещение одного фрейма относительно другого.

## Особенности

## Пояснение

```cpp
/*
Все, что касается черепашки и действий с ней, не связанных с tf2, будет помечаться меткой "черепашка".
*/

#include <ros/ros.h>
#include <tf2_ros/tansform_listener.h>
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/Twist.h>
#include <turtlesim/Spawn.h>
  
int main(int argc, char** argv)
{
	ros::init(argc, argv, "my_tf2_listener");
	  
	ros::NodeHandle node;
	  
	ros::service::waitForService("spawn"); // черепашка
	ros::ServiceClient spawner = // черепашка
	node.serviceClient<turtlesim::Spawn>("spawn");// черепашка
	turtlesim::Spawn turtle;// черепашка
	turtle.request.x = 4;// черепашка
	turtle.request.y = 2;// черепашка
	turtle.request.theta = 0;// черепашка
	turtle.request.name = "turtle2";// черепашка
	spawner.call(turtle);// черепашка
	  
	ros::Publisher turtle_vel =
	node.advertise<geometry_msgs::Twist>("turtle2/cmd_vel", 10); // черепашка скорость
	  
	tf2_ros::Buffer tfBuffer;
	tf2_ros::TransformListener tfListener(tfBuffer);
	  
	ros::Rate rate(10.0);
	while (node.ok())
	{
		geometry_msgs::TransformStamped transformStamped;
		try
		{
			transformStamped = tfBuffer.lookupTransform("turtle2", "turtle1",
	ros::Time(0));
		}
		catch (tf2::TransformException &ex) 
		{
			ROS_WARN("%s",ex.what());
			ros::Duration(1.0).sleep();
			continue;
		}
	  
		geometry_msgs::Twist vel_msg;
		  
		vel_msg.angular.z = 4.0 * atan2(transformStamped.transform.translation.y,
		transformStamped.transform.translation.x);
		vel_msg.linear.x = 0.5 * sqrt(pow(transformStamped.transform.translation.x, 2) +
		pow(transformStamped.transform.translation.y, 2));
		turtle_vel.publish(vel_msg);
		  
		rate.sleep();
	}
	return 0;
};
```

![[Untitled Diagram.svg]]
## Теория - как работает получение трансформаций

![[Kak_rabotaet_poluchenie_transformacij.svg.svg]]
Допустим у нас есть фрейм world и фрейм some_frame. 

Получая трансформацию some_frame в world, мы получаем координаты some_frame относительно world, с этим ясно.

Трансформацию по вращению мы получаем в формате [[Кватернионы|кватерниона]]. [[Конвертация кватерниона в Эйлеровы углы|Его можно перевести в Эйлеровы углы]]. Если мы это сделаем, мы в итоге получим значения трех углов: [[Эйлеровы углы|крена, тонгажа и рыскания]]. Эти значения являются углами, на которые оси some_frame повернуты относительно осей world. В данном примере мы будем иметь угол рыскания с положительным значением.

### Смотрите также:

- [[tf2 и время - как получить трансформации из прошлого]]

#tf2 #Робототехника/ROS #Робототехника 