---

Tags: 
aliases: 
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

Тип: конспект
Описание: -

День: 29 # Не трогать!
Месяц: 11 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Tuesday 29th November 2022 10:53:10

---
# [[Добавление исполняемого файла в проект CMake]]

Создание исполняемого файла происходит при помощи команды `add_executable(exec_name source_list)`, где `exec_name` - это имя итогового исполняемого файла, `source_list` - список всех файлов, из которых исполняемый файл будет собран. Например:

```cmake
add_executable(Hello_world hello.cpp world.cpp)
```

%%## Приложения%%

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

> [!question]- Соурслист
> 1. 


---
#Программирование/cmake 