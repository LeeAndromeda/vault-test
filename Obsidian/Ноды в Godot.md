---
Тип: конспект
Описание: Краткое описание нодов в Godot
День: 23
Месяц: 07
Год: 2022
---
# Ноды в Godot

Каждый год в годот по своей сути - класс. Ключевое слово [[Программирование на GDscript|extends]] означает, что этот над наследует свойства указанного после к.с. нода, а так же всех нодов, у которых наследует указанный.

> [!info] Автоматическая документация
> In GDScript, if you omit the line with the extends keyword, your class will implicitly extend Reference, which Godot uses to manage your application's memory.

## Класс Node

### Методы класса

`_process()` - виртуальная функция класса Node. Если определить ее в любом классе, который наследует класс Node

#Godot 