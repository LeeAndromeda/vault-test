---
Тип: конспект
Описание: -
День: 25
Месяц: 07
Год: 2022
---
# Шаблон конфиг файла для сервера динамической реконфигурации ROS

```python
#!/usr/bin/env python
PACKAGE = "package_name" # Указывает пакет
from dynamic_reconfigure.parameter_generator_catkin import * gen = ParameterGenerator()

gen.add("double_param", double_t, 0, "A double parameter",    .5, 0,   1)
gen.add("int_param", int_t, 0, "An int parameter", 1, 0, 3, edit_method=size_enum)
exit(gen.generate(PACKAGE, "package_name", "Name")) 
```

> [!tip]- Все доступные типы данных
> ![[Расширенный пример кода конфиг файла сервера динамической реконфигурации со всеми доступными типами данных]]


```ad-caution
title: Внимание!
collapse: open
Третий аргумент в функции `exit` должен быть таким же, как и название файла-конфига без расширения.
```

## Аргументы функции `ParameterGenerator::add()`

По порядку:

-   **Имя** - указывает имя параметра. Так к параметру нужно обращаться в **коде** клиента    
-   **тип параметра** - определяеь тип хранимого параметра и может быть одни из следующих: int_t, double_t, str_t или bool_t    
-   **уровень** - A bitmask which will later be passed to the dynamic reconfigure callback. When the callback is called all of the level values for parameters that have been changed are ORed together and the resulting value is passed to the callback. *==ничего не понятно, так что оставлю в оригинале==*    
-   **описание** - описание параметра, строковая переменная
-   **значение по-умолчанию** - указывает значение по-умолчанию    
-   **минимальное** - определяет минимальное значени численной переменной    
-   **максимальное** - определяет максимальное значени численной переменной    

## Подготовка конфиг файла к использованию

```shell
chmod a+x cfg/Name.cfg
```

#ROS #ДинамическаяРеконфигурация