# rosparam

**rosparam** позволяет хранить и манипулировать информацией на ROS **[Parameter Server](http://wiki.ros.org/Parameter%20Server)[^1]**. *Parameter Server* может хранить **integers**, **floats**, **boolean**, **dictionaries** и **lists**. rosparam использует YAML markup язык для синтаксиса. В простых случаях YAMLвыглядит естественно: 1 - целочисленное, 1.0 - с плавающей точкой, one это string, true это boolean, [1, 2, 3] список целочисленных и {a: b, c: d} это словарь. rosparam hимеет следующие команды:

```linux
rosparam set            set parameter
rosparam get            get parameter
rosparam load           load parameters from file
rosparam dump           dump parameters to file
rosparam delete         delete parameter
rosparam list           list parameter names
```

## rosparam set and rosparam get

### Синтаксис

```linux
rosparam set [param_name]
rosparam get [param_name]
```

>Вы так же можете использовать ```rosparam get /```, чтобы вывести содержимое всего Сервера Параметров.
>
>```
>$ rosparam get /
>
>-   rosdistro: 'noetic
>      '
>    roslaunch:
>      uris:
>        host_nxt__43407: http://nxt:43407/
>    rosversion: '1.15.5
>      '
>    run_id: 7ef687d8-9ab7-11ea-b692-fcaa1494dbf9
>    turtlesim:
>      background_b: 255
>      background_g: 86
>      background_r: 69
>```

## rosparam dump and rosparam load

```
rosparam dump [file_name] [namespace]
rosparam load [file_name] [namespace]
```

# Статьи:

[rosparam](http://wiki.ros.org/rosparam)

#Робототехника/ROS

[^1]: [[ROS сервер параметров]]