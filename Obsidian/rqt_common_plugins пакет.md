# rqt common плагины

**rqt_common_plugins** метапакет снабжает рос бэкэнд-графическим инструментарием, который может быть использован с/без работой робота.

## rqt_console

Вызывает консоль rqt.

## rqt_logger_level

Вызывает rqt_logger_level. [^1]


# Статьи:

- [Источник](http://wiki.ros.org/rqt_common_plugins?distro=noetic)

_[rqt_action](http://wiki.ros.org/rqt_action?distro=noetic) | [rqt_bag](http://wiki.ros.org/rqt_bag?distro=noetic) | [rqt_bag_plugins](http://wiki.ros.org/rqt_bag_plugins?distro=noetic) | [rqt_console](http://wiki.ros.org/rqt_console?distro=noetic) | [rqt_dep](http://wiki.ros.org/rqt_dep?distro=noetic) | [rqt_graph](http://wiki.ros.org/rqt_graph?distro=noetic) | [rqt_image_view](http://wiki.ros.org/rqt_image_view?distro=noetic) | [rqt_launch](http://wiki.ros.org/rqt_launch?distro=noetic) | [rqt_logger_level](http://wiki.ros.org/rqt_logger_level?distro=noetic) | [rqt_msg](http://wiki.ros.org/rqt_msg?distro=noetic) | [rqt_plot](http://wiki.ros.org/rqt_plot?distro=noetic) | [rqt_publisher](http://wiki.ros.org/rqt_publisher?distro=noetic) | [rqt_py_common](http://wiki.ros.org/rqt_py_common?distro=noetic) | [rqt_py_console](http://wiki.ros.org/rqt_py_console?distro=noetic) | [rqt_reconfigure](http://wiki.ros.org/rqt_reconfigure?distro=noetic) | [rqt_service_caller](http://wiki.ros.org/rqt_service_caller?distro=noetic) | [rqt_shell](http://wiki.ros.org/rqt_shell?distro=noetic) | [rqt_srv](http://wiki.ros.org/rqt_srv?distro=noetic) | [rqt_top](http://wiki.ros.org/rqt_top?distro=noetic) | [rqt_topic](http://wiki.ros.org/rqt_topic?distro=noetic) | [rqt_web](http://wiki.ros.org/rqt_web?distro=noetic)_

#Робототехника/ROS 

[^1]: [[Quick note about logger levels]]