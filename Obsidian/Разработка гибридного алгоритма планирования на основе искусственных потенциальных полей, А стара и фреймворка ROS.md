---

kanban-plugin: basic

---

## Идеи



## Запланированные

- [ ] Выяснить, почему total arrow не работает в px4<br><br>#Debug #Робототехника/Аэробот
- [ ] При изначальном запуске, на 3 режиме регулятора, если не задать целевую точку, все равно существует передаваемая скорость, которая большем, чем лимит на регуляторе<br><br>#Debug
- [ ] Найти или разработать концепт регулятора для потенциальных полей, а затем реализовать его<br>#Робототехника <br>#Конференция #SAUM


## В процессе реализации

- [ ] Сделать минимальную угловую скорость и добавить ее в сервер динамической рекофигурации<br>#Робототехника #SAUM


## Готовые

**Complete**
- [x] Принимать одометрию ориентации по рысканию квадрокоптера для поворота вектора скорости в глобальной системе координат<br><br>*У Антона в коде итоговая скорость квадрокоптера публикуется в map(или какой-то такой, уточню), что приводит к неправильной передаче скорости на квадрокоптер при отсутствии поворота вектора*<br><br>/mavros/local_position/pose<br>одометрия квадрокоптера - использовать для поворота вектора в глобальную систему координат матрицей поворота <br><br>#Робототехника #ТекущиеЗадачи<br>#Робототехника/Аэробот
- [x] Разработка модуля контроля выдачи точек из траектории астар в потенциальнные поля - [[Проект Astar Point Stack]]<br>#Робототехника #SAUM
- [x] Интегрировать потенциальные поля и A* в SLAM<br><br>#Робототехника #Конференция #SAUM #ТекущиеЗадачи
- [x] Переделать MarkerHandler, чтобы передавать ему Nodehandle из main<br><br>#Робототехника/ROS #Робототехника
- [x] Добавить комментарии к классам <br><br>#Робототехника #Робототехника/Аэробот
- [x] Переименовать константы в КАПС #Робототехника #Робототехника/Аэробот <br>
- [x] Проверить использование goal_is_new
- [x] Сделать сервер динамической реконфигурации для настройки параметров dynamic reconfigure в rqt<br><br>> [!NOTE] [[Создание сервера динамической реконфигурации ROS]]<br><br>#Робототехника #ТекущиеЗадачи<br>#Робототехника/Аэробот
- [x] ### Синхронизация ПП отталкивания и притягивания<br><br>Попробовать перенести расчет притяжения в колбэк скана. Таким образом синхронизировать отталкивание и притягивание. Это должно сработать, потому что будет использоваться последняя доступная одометрия, в том числе постоянно обновляющаяся<br>#SAUM #Робототехника
- [x] [[Введение задаваемой через 2d nav goal в rviz целевой точки в качестве поля притяжения]]
- [x] Перевод существующей программы в идеологию ООП


## Отмененные



## Информация по проекту

- [ ] [[Структурная схема взимодействия подсистем проекта]]
- [ ] #Проект




%% kanban:settings
```
{"kanban-plugin":"basic"}
```
%%