---
# obsidian info
Тип: человек
Tags: Человек
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

# prof info
Место_работы: "НИИ РиПУ" 

Проект: "(.*)Рубин(.*)Демонстратор(.*)" # "a, b, c, n" # (RegEx)
Компетенции: неиз # "a, b, c, n" # (RegEx)

# social info
Особенности: - 

# bio
Фамилия: #  Фамилия" # (RegEx)
Имя: # "Имя" # (RegEx)
Отчество: # "Отчество" # (RegEx)
Число_рождения: #Входящие
Месяц_рождения: #Входящие 
Год_рождения: #Входящие 
Описание: -

День: 09 # Не трогать!
Месяц: 12 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Friday 9th December 2022 18:30:39
---
# [[Шпак Игорь]]

## Актуальное

```dataview
LIST 
FROM [[]]
WHERE Тип != Конспект and Тип != Лекция and Выполнено != да 
```

> [!example]- Черты
> ## Жизненные ценности
> 
> ## Черты характера
> 


> [!abstract]- Контакты
> 
>### Соцсети
> 
>### Другое 

> [!example]- Проекты
> ```dataview
> LIST
> FROM #Проект and !"Obsidian/Templates"
> WHERE regexmatch(this.Проект, название_проекта) or regexmatch(название_проекта, this.Проект)
> ```


## Связи с людьми

- [[Иванов Антон Сергеевич]]
```dataview
LIST
FROM #Человек and [[]] and !outgoing([[]]) and !"Obsidian/Templates"
```

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

---

#Робототехника 