---
Тип: Задача
Описание: -
Дисциплина: PKM
Tags: Задача
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

День: 04 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Tuesday 4th October 2022 12:37:22

день_завершения: нет
месяц_завершения: 0
год_завершения: 2022

Выполнено: да
Результат: Успех
---

```dataview
TABLE день_завершения as "День истечения", месяц_завершения as "Месяц истечения", Проект, Выполнено, Результат
WHERE contains(file.name, "Узнать, как задавать переменные среды в shell, чтобы standard aliases были одинаковые, но были разные конфиги путей")
limit 1
```
# [[Узнать, как задавать переменные среды в shell, чтобы standard aliases были одинаковые, но были разные конфиги путей]]



## Результат:



## Следующая задача:



## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

#PKM/obsidian #bash 