---

Tags: 
aliases: 
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

Тип: конспект
Описание: -

День: 28 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Friday 28th October 2022 14:06:15

---
# [[Создание собственных msg и srv файлов ROS2]]

Во многом они похожи на [[msg файлы|msg]] и [[srv файлы|srv]] из [[Работа с ROS|ROS]]. В форме самих файлов они идентичны. Все \*.msg и \*.srv файлы хранятся в pakcage/msg и package/srv соответственно. 

## После создания msg и srv файлов

```cmake
# В CMakeLists
# ...
find_package(rosidl_default_generators REQUIRED)
# ...
rosidl_generate_interfaces(${PROJECT_NAME}
  "msg/Num.msg"
  "msg/Sphere.msg"
  "srv/AddThreeInts.srv"
  DEPENDENCIES geometry_msgs # Add packages that above messages depend on, in this case geometry_msgs for Sphere.msg
)
```

```xml
<!-- В package.xml -->
<depend>geometry_msgs</depend>
<build_depend>rosidl_default_generators</build_depend>
<exec_depend>rosidl_default_runtime</exec_depend>
<member_of_group>rosidl_interface_packages</member_of_group>
```

> [!faq]- Как испольовать сообщения в пакете, где они объявлены
>  Добавь в cmake`rosidl_target_interfaces(sim_anpas_bridge_node ${PROJECT_NAME} "rosidl_typesupport_cpp")` - [Источник](https://discourse.ros.org/t/ros2-how-to-use-custom-message-in-project-where-its-declared/2071)

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

> [!question]- Соурслист
> 1. https://docs.ros.org/en/foxy/Tutorials/Beginner-Client-Libraries/Custom-ROS2-Interfaces.html


---
#Робототехника/ROS2 