---
Тип: Задача
Описание: -
Исполнители: 
Проект: бридж 

Дисциплина: Работа
Tags: Задача
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: 23 # Не трогать!
Месяц: 05 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-05-23

дата_завершения: "2023-05-23"

Выполнено: да
Результат: 
---


# [[Добавить передачу кастомного json с пульта на СГРУ]]


> [!caution] Это текущая задача
> Данная задача была помечена текущей. Если она таковой быть перестает, удалите полностью данный спойлер.
> Выберите тип задачи и удалите ненужный:
> 
> #Задача/РаботаИдет
> 
> ### Прогресс:: 0%

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```


---


---