---
aliases:
  - EulerAngles
---

# Работа с кватернионами

Кватернионы состоят из четырех переменых - $x, y, z, w$. В большинстве случае применения в РОС они расположены именно в этом порядке, однако иногда $w$ выносится на первое место.

## Вращение кватерниона

Чтобы повернуть кватернион, умножьте его на кватернион, который выражает это вращение(**порядок важен**):

**c++:**
```cpp
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
tf2::Quaternion q_orig, q_rot, q_new;
// Get the original orientation of 'commanded_pose'

tf2::convert(commanded_pose.pose.orientation , q_orig);
double r=3.14159, p=0, y=0;  // Rotate the previous pose by 180* about X

q_rot.setRPY(r, p, y);
q_new = q_rot*q_orig;  // Calculate the new orientation
q_new.normalize();

// Stuff the new rotation back into the pose. This requires conversion into a msg type
tf2::convert(q_new, commanded_pose.pose.orientation);
```

**Python:**

```python
from tf_transformations import *
q_orig = quaternion_from_euler(0, 0, 0)
q_rot = quaternion_from_euler(pi, 0, 0)
q_new = quaternion_multiply(q_rot, q_orig)
print q_new
```

## Противоположный кватернион

Простой способ обернуть кватернион($q _{1} ^{-1}$) - умножить на -1 его компоненту $w$:

```Python
q[3] = -q[3]
```

## Относительные вращения

Итак,  у вас есть кватернионы $q _{1}, q _{2}$ и вы хотите найти относительное вращение $q _{r}$ от $q _{1}$ до $q _{2}$:
 $$q _{2} = q _{r} * q _{1} $$
 Вы можете решить это уравнение для $q _{r}$ аналогично матричному уравнению(**порядок важен**):
  $$q _{r} = q _{2} * q _{1} ^{-1} $$

# Мой класс для работы с эйлеровыми углами

Для работы ему необходимы заголовочные файлы tf2:
```cpp
#include "tf2/LinearMath/Matrix3x3.h"
#include "tf2/LinearMath/Quaternion.h"
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
```

## Класс
```cpp
// Класс для работы с Эйлеровыми углами и кватернионами
class EulerAngles {
    public:
    EulerAngles()
    {
        roll_ = 0;
        pitch_ = 0;
        yaw_ = 0;
    }


    double roll()
    {
        return roll_;
        // return normalize_angle(roll_);
    }


    double pitch()
    {
        // return normalize_angle(pitch_);radians
        return pitch_;
    }


    double yaw()
    {
        return yaw_;
        // return normalize_angle(yaw_);
    }


    /**
     * @brief Returns 2-element vector with rotated x and y
     * 
     * @param x meters - coords.at(0)
     * @param y meters - coords.at(1)
     * @param angle radians 
     * @return std::vector<double>  
     */
    std::vector<double> rotate_vector_by_angle(double x, double y, double angle)
    {
        std::vector<double> coords;
        coords.resize(2);
        coords.at(0) = x * cos(angle) - y * sin(angle);
        coords.at(1) = x * sin(angle) + y * cos(angle);

        return coords;

        // finvec_x = tmpvec_x * cos(angles.yaw) - tmpvec_y * sin(angles.yaw);
        // finvec_y = tmpvec_x * sin(angles.yaw) + tmpvec_y * cos(angles.yaw);
    }


    geometry_msgs::Quaternion get_current_msg_quaternion()
    {
        geometry_msgs::Quaternion q;
        setRPY_of_quaternion(q);
        return q;
    }


    tf2::Quaternion get_current_tf2_quaternion()
    {
        tf2::Quaternion q;
        setRPY_of_quaternion(q);
        return q;
    }


    // Функция для установки Эйлеровых углов по переданным параметрам
    void setRPY(float new_roll, float new_pitch, float new_yaw)
    {
        roll_ = new_roll;
        pitch_ = new_pitch;
        yaw_ = new_yaw;
    }


    /**
    * @brief Функция для передачи Эйлеровых углов в вектор вращения.
    * 
    * Нормализация кватернионов есть
    */
    void setRPY_of_quaternion(tf2::Quaternion &q)
    {
        q.setRPY(roll_, pitch_, yaw_);
        q.normalize();
    }


    /**
    * @brief Функция для передачи Эйлеровых углов в вектор вращения.
    * 
    * Нормализация кватернионов есть
    */
    void setRPY_of_quaternion(geometry_msgs::Quaternion &q)
    {
        tf2::Quaternion q_tf;
        q_tf.setRPY(roll_, pitch_, yaw_);
        q_tf.normalize();

        q.w = q_tf.getW();
        q.x = q_tf.getX();
        q.y = q_tf.getY();
        q.z = q_tf.getZ();
    }


    // Функция для получения Эйлеровых углов из вектора вращения 
    void get_RPY_from_quaternion(tf2::Quaternion q)
    {
        tf2::Matrix3x3 m(q);
        m.getRPY(roll_, pitch_, yaw_);
    }


    /**
    * @brief Функция для получения Эйлеровых углов из вектора вращения заданного geometry_msgs::Quaternion.
    * 
    * Нормализация кватернионов есть
    */
    void get_RPY_from_msg_quaternion(geometry_msgs::Quaternion q)
    {
        tf2::Quaternion q_tf;
        tf2::convert(q, q_tf);

        q_tf.normalize();

        get_RPY_from_quaternion(q_tf);
    }


    /**
    * @brief Returns quaternion with applied rotation
    */
    geometry_msgs::Quaternion rotate_position_yaw_by_angle(geometry_msgs::Quaternion orientation, double angle)
    {
        get_RPY_from_msg_quaternion(orientation);
        yaw_ = yaw_ + angle;

        geometry_msgs::Quaternion q;
        setRPY_of_quaternion(q);
        return q;
    }


    /**
    * @brief Нормализует угол в пределах от -pi до pi
    * @warning НЕ РАБОТАЕТ
    */
    double normalize_angle(double angle)
    {
        double new_angle = angle;
        if (angle > M_PI)
        {
            new_angle = angle - 2 * M_PI;
        }
        else if (angle < - M_PI)
        {
            new_angle = angle + 2 * M_PI;
        }

        if (new_angle > M_PI)
        {
            ROS_ERROR_STREAM("EulerAngles: returning unnormalized angle");
        }

        return new_angle;
    }


    double quantize_angle_by(double radians, double angle)
    {
        double division_res = angle / radians;
        // ROS_INFO_STREAM("\nangle: " << angle << "\nradians: " << radians << "\ndivision_res * radians: " << round(division_res) * radians << "\ndivision_res raw: " << division_res << "\ndivision_res rounded: " << round(division_res));
        division_res = round(division_res);
        return division_res * radians;
    }


    geometry_msgs::Quaternion quantize_quaternion_yaw_by(double radians, geometry_msgs::Quaternion q)
    {
        get_RPY_from_msg_quaternion(q);
        yaw_ = quantize_angle_by(radians, yaw_);
        
        setRPY_of_quaternion(q);
        return q;
    }


    private:
    double roll_;
    double pitch_;
    double yaw_;
};


```

## Пайплайн конвертации

[[Пайплайн конвертации кватернионов.canvas]]

## Работа с кватернионами в Python

### Необходимо установить

```shell
sudo apt install ros-$DISTRO-tf-transformations
sudo pip3 install transforms3d
```

### Создание кватерниона

```python
from geometry_msgs.msg import Quaternion

# Create a list of floats, which is compatible with tf
# quaternion methods
quat_tf = [0, 1, 0, 0]

quat_msg = Quaternion(quat_tf[0], quat_tf[1], quat_tf[2], quat_tf[3])
```

### Получение эйлеровых углов из кватерниона

```python
#type(pose) = geometry_msgs.msg.Pose
quaternion = (
    pose.orientation.x,
    pose.orientation.y,
    pose.orientation.z,
    pose.orientation.w)
euler = tf.transformations.euler_from_quaternion(quaternion)
roll = euler[0]
pitch = euler[1]
yaw = euler[2]
```

### Перевод tf.quaternion в geometry_msgs.quaternion

```python
quaternion = tf.transformations.quaternion_from_euler(roll, pitch, yaw)
#type(pose) = geometry_msgs.msg.Pose
pose.orientation.x = quaternion[0]
pose.orientation.y = quaternion[1]
pose.orientation.z = quaternion[2]
pose.orientation.w = quaternion[3]
```

### Вращение кватерниона

Чтобы применить вращение к кватерниону текущей позиции, просто создайте кватернион, выражающий желаемое вращение и умножьте кватернион текущей ориентации на кватернион вращения:

```python
from tf.transformations import *

q_orig = quaternion_from_euler(0, 0, 0)
q_rot = quaternion_from_euler(pi, 0, 0)
q_new = quaternion_multiply(q_rot, q_orig)
print q_new
```

### Инверсия кватерниона

Чтобы инвертировать кватернион, умножьте `w`-часть на `-1`:

```python
q[3] = -q[3]
```

### Относительные варщения


Допустим, у вас есть два кватерниона из одной и той же системы координат, q_1 и q_2. Вы хотите найти относительное вращение, q_r, для перехода от q_1 к q_2:
```python
q_2 = q_r*q_1
```
Решить q_r можно аналогично решению матричного уравнения. Инвертируем q_1 и умножаем обе стороны вправо. Опять же, порядок умножения имеет значение:

```python
q_r = q_2*q_1_inverse
```

Вот пример получения относительного поворота от предыдущего положения робота к текущему:

```python
q1_inv[0] = prev_pose.pose.orientation.x
q1_inv[1] = prev_pose.pose.orientation.y
q1_inv[2] = prev_pose.pose.orientation.z
q1_inv[3] = -prev_pose.pose.orientation.w # Отрицание для инверсии

q2[0] = current_pose.pose.orientation.x
q2[1] = current_pose.pose.orientation.y
q2[2] = current_pose.pose.orientation.z
q2[3] = current_pose.pose.orientation.w

qr = tf.transformations.quaternion_multiply(q2, q1_inv)
```

- [tf2 tutroials](http://wiki.ros.org/tf2/Tutorials/Quaternions)
- [Потенциальные поля - моя реализация на гитхабе](https://github.com/Yyote/apf_la/blob/master/src/potential_fields.cpp)
- [Quaternion transformations in Python](https://answers.ros.org/question/69754/quaternion-transformations-in-python/)

#Кватернионы #Математика #Робототехника/ROS 