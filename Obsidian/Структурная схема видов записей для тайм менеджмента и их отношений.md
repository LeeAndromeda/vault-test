---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
Задачи ^Wyvxyg4E

Проекты ^h4l1o128

Ежедневная запись ^APcpptsG

Вид задач ^pYe9hYFd

Вид проектов ^QcHdEbSH

Вид работы с днем ^xaMMOrRb

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://excalidraw.com",
	"elements": [
		{
			"id": "4gfp_bbvDLR3Tagp8LFOd",
			"type": "rectangle",
			"x": 106.72562212942043,
			"y": -262.00849344938314,
			"width": 116,
			"height": 52.727264404296875,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"seed": 2124520580,
			"version": 379,
			"versionNonce": 470387204,
			"isDeleted": false,
			"boundElements": [
				{
					"type": "text",
					"id": "Wyvxyg4E"
				},
				{
					"id": "k4iLf6wQllfm-q2Hm1xQo",
					"type": "arrow"
				},
				{
					"id": "AfV_LCkIiJQFnVEca0rTh",
					"type": "arrow"
				},
				{
					"id": "zkCDhhJ7SPcWuldUJ5teZ",
					"type": "arrow"
				}
			],
			"updated": 1667748115343,
			"link": null,
			"locked": false
		},
		{
			"id": "Wyvxyg4E",
			"type": "text",
			"x": 111.72562212942043,
			"y": -248.1448612472347,
			"width": 106,
			"height": 25,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"seed": 135729412,
			"version": 341,
			"versionNonce": 352052484,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1667748074890,
			"link": null,
			"locked": false,
			"text": "Задачи",
			"rawText": "Задачи",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 17,
			"containerId": "4gfp_bbvDLR3Tagp8LFOd",
			"originalText": "Задачи"
		},
		{
			"id": "0WYXb1YBlh4UvpCrbVEYi",
			"type": "rectangle",
			"x": 111.04534424460022,
			"y": -402.9894534201223,
			"width": 122.18182373046875,
			"height": 58.181854248046875,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"seed": 1350532612,
			"version": 247,
			"versionNonce": 404569732,
			"isDeleted": false,
			"boundElements": [
				{
					"type": "text",
					"id": "h4l1o128"
				},
				{
					"id": "JpMg5eZ06QjOa8Sn3xyPa",
					"type": "arrow"
				},
				{
					"id": "UluBKIVruOQVS0CFdRBl6",
					"type": "arrow"
				},
				{
					"id": "zkCDhhJ7SPcWuldUJ5teZ",
					"type": "arrow"
				}
			],
			"updated": 1667748113982,
			"link": null,
			"locked": false
		},
		{
			"id": "h4l1o128",
			"type": "text",
			"x": 116.04534424460022,
			"y": -386.39852629609885,
			"width": 112.18182373046875,
			"height": 25,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"seed": 413008132,
			"version": 206,
			"versionNonce": 1535077380,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1667748070390,
			"link": null,
			"locked": false,
			"text": "Проекты",
			"rawText": "Проекты",
			"fontSize": 20.03246852329799,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 17,
			"containerId": "0WYXb1YBlh4UvpCrbVEYi",
			"originalText": "Проекты"
		},
		{
			"id": "EbCMkj7PElrvYdfWV_gsB",
			"type": "rectangle",
			"x": -155.2973895204494,
			"y": -357.1692055526991,
			"width": 186,
			"height": 69,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"seed": 1555463356,
			"version": 325,
			"versionNonce": 1979516092,
			"isDeleted": false,
			"boundElements": [
				{
					"type": "text",
					"id": "APcpptsG"
				},
				{
					"id": "a2MGFIEbvpZ6LVFadjkpB",
					"type": "arrow"
				},
				{
					"id": "UluBKIVruOQVS0CFdRBl6",
					"type": "arrow"
				},
				{
					"id": "AfV_LCkIiJQFnVEca0rTh",
					"type": "arrow"
				}
			],
			"updated": 1667748101280,
			"link": null,
			"locked": false
		},
		{
			"id": "APcpptsG",
			"type": "text",
			"x": -150.2973895204494,
			"y": -347.6692055526991,
			"width": 176,
			"height": 50,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"seed": 1470022660,
			"version": 325,
			"versionNonce": 936359172,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1667748101281,
			"link": null,
			"locked": false,
			"text": "Ежедневная \nзапись",
			"rawText": "Ежедневная запись",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 42,
			"containerId": "EbCMkj7PElrvYdfWV_gsB",
			"originalText": "Ежедневная запись"
		},
		{
			"id": "3GYkVz9sfQgLgKlh6fVQJ",
			"type": "rectangle",
			"x": 96.95421930321461,
			"y": -109.93195975991364,
			"width": 133.28511820773778,
			"height": 72.70094835584706,
			"angle": 0,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"seed": 1854675132,
			"version": 492,
			"versionNonce": 854022716,
			"isDeleted": false,
			"boundElements": [
				{
					"type": "text",
					"id": "pYe9hYFd"
				},
				{
					"id": "k4iLf6wQllfm-q2Hm1xQo",
					"type": "arrow"
				}
			],
			"updated": 1667748143679,
			"link": null,
			"locked": false
		},
		{
			"id": "pYe9hYFd",
			"type": "text",
			"x": 101.95421930321461,
			"y": -86.08148558199014,
			"width": 123.28511820773778,
			"height": 25,
			"angle": 0,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"seed": 1739145404,
			"version": 432,
			"versionNonce": 1434417028,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1667748143679,
			"link": null,
			"locked": false,
			"text": "Вид задач",
			"rawText": "Вид задач",
			"fontSize": 20.046360684185007,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 17,
			"containerId": "3GYkVz9sfQgLgKlh6fVQJ",
			"originalText": "Вид задач"
		},
		{
			"id": "k4iLf6wQllfm-q2Hm1xQo",
			"type": "arrow",
			"x": 157.7846340563491,
			"y": -119.85399461884369,
			"width": 1.9200777307620456,
			"height": 84.73765339084615,
			"angle": 0,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 160635324,
			"version": 973,
			"versionNonce": 556656388,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1667748143680,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					1.9200777307620456,
					-84.73765339084615
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "3GYkVz9sfQgLgKlh6fVQJ",
				"gap": 9.922034858930061,
				"focus": -0.10160690017247231
			},
			"endBinding": {
				"elementId": "4gfp_bbvDLR3Tagp8LFOd",
				"gap": 4.689581035396429,
				"focus": 0.07367689885120708
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "elvb_yXgiLRb_n6DCBHPS",
			"type": "rectangle",
			"x": 95.49924928428308,
			"y": -537.0106955239862,
			"width": 138,
			"height": 60.630120890685134,
			"angle": 0,
			"strokeColor": "#364fc7",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"seed": 996447932,
			"version": 285,
			"versionNonce": 445751172,
			"isDeleted": false,
			"boundElements": [
				{
					"type": "text",
					"id": "QcHdEbSH"
				},
				{
					"id": "JpMg5eZ06QjOa8Sn3xyPa",
					"type": "arrow"
				}
			],
			"updated": 1667748065481,
			"link": null,
			"locked": false
		},
		{
			"id": "QcHdEbSH",
			"type": "text",
			"x": 100.49924928428308,
			"y": -531.6956350786436,
			"width": 128,
			"height": 50,
			"angle": 0,
			"strokeColor": "#364fc7",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"seed": 1770956476,
			"version": 239,
			"versionNonce": 1216154300,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1667748065481,
			"link": null,
			"locked": false,
			"text": "Вид \nпроектов",
			"rawText": "Вид проектов",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 42,
			"containerId": "elvb_yXgiLRb_n6DCBHPS",
			"originalText": "Вид проектов"
		},
		{
			"id": "JpMg5eZ06QjOa8Sn3xyPa",
			"type": "arrow",
			"x": 161.81922708650876,
			"y": -472.29325610311236,
			"width": 5.566232826125201,
			"height": 60.97074982543103,
			"angle": 0,
			"strokeColor": "#364fc7",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 864499716,
			"version": 372,
			"versionNonce": 1010518916,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1667748070390,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					5.566232826125201,
					60.97074982543103
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "elvb_yXgiLRb_n6DCBHPS",
				"gap": 4.087318530188725,
				"focus": 0.0780731482046861
			},
			"endBinding": {
				"elementId": "0WYXb1YBlh4UvpCrbVEYi",
				"gap": 8.33305285755904,
				"focus": -0.020930177893863457
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "BZb4kJQFY9CO4eWGTDZQU",
			"type": "rectangle",
			"x": -174.27059857877867,
			"y": -519.9798205035936,
			"width": 187.34020408768941,
			"height": 79.70474093370842,
			"angle": 0,
			"strokeColor": "#2b8a3e",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"seed": 1555812228,
			"version": 97,
			"versionNonce": 522347780,
			"isDeleted": false,
			"boundElements": [
				{
					"id": "a2MGFIEbvpZ6LVFadjkpB",
					"type": "arrow"
				},
				{
					"type": "text",
					"id": "xaMMOrRb"
				}
			],
			"updated": 1667748131099,
			"link": null,
			"locked": false
		},
		{
			"id": "xaMMOrRb",
			"type": "text",
			"x": -169.27059857877867,
			"y": -505.12745003673945,
			"width": 177.34020408768941,
			"height": 50,
			"angle": 0,
			"strokeColor": "#2b8a3e",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"seed": 1020431420,
			"version": 45,
			"versionNonce": 644177212,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1667748131099,
			"link": null,
			"locked": false,
			"text": "Вид работы с \nднем",
			"rawText": "Вид работы с днем",
			"fontSize": 20.038441139851912,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 42,
			"containerId": "BZb4kJQFY9CO4eWGTDZQU",
			"originalText": "Вид работы с днем"
		},
		{
			"id": "a2MGFIEbvpZ6LVFadjkpB",
			"type": "arrow",
			"x": -98.64382462546479,
			"y": -435.8756665619801,
			"width": 8.56250933509827,
			"height": 74.84601868491052,
			"angle": 0,
			"strokeColor": "#2b8a3e",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 442162564,
			"version": 524,
			"versionNonce": 1488868796,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1667748131101,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					8.56250933509827,
					74.84601868491052
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "BZb4kJQFY9CO4eWGTDZQU",
				"gap": 4.399413007905182,
				"focus": 0.23617369990132206
			},
			"endBinding": {
				"elementId": "EbCMkj7PElrvYdfWV_gsB",
				"gap": 3.860442324370474,
				"focus": -0.24132218210521356
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "UluBKIVruOQVS0CFdRBl6",
			"type": "arrow",
			"x": 36.097837768565164,
			"y": -323.2191581414231,
			"width": 72.41709502701895,
			"height": 35.74692866364262,
			"angle": 0,
			"strokeColor": "#2b8a3e",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 1444181508,
			"version": 271,
			"versionNonce": 871785988,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1667748101278,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					72.41709502701895,
					-35.74692866364262
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "EbCMkj7PElrvYdfWV_gsB",
				"gap": 5.395227289014562,
				"focus": 0.5790224638428256
			},
			"endBinding": {
				"elementId": "0WYXb1YBlh4UvpCrbVEYi",
				"gap": 2.530411449016104,
				"focus": 0.278034026282672
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "AfV_LCkIiJQFnVEca0rTh",
			"type": "arrow",
			"x": 46.82326113948425,
			"y": -293.43460590923945,
			"width": 56.03254861661821,
			"height": 52.172654729377996,
			"angle": 0,
			"strokeColor": "#2b8a3e",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 1721163524,
			"version": 357,
			"versionNonce": 2034445572,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1667748104392,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					56.03254861661821,
					52.172654729377996
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "EbCMkj7PElrvYdfWV_gsB",
				"focus": -0.5976296891489191,
				"gap": 16.12065065993366
			},
			"endBinding": {
				"elementId": "4gfp_bbvDLR3Tagp8LFOd",
				"focus": -0.6469065428674077,
				"gap": 3.86981237331797
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "zkCDhhJ7SPcWuldUJ5teZ",
			"type": "arrow",
			"x": 157.25853458235613,
			"y": -334.69749779309166,
			"width": 0.46063608115392185,
			"height": 62.03581398858444,
			"angle": 0,
			"strokeColor": "#364fc7",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 668384260,
			"version": 70,
			"versionNonce": 716113980,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1667748119352,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-0.46063608115392185,
					62.03581398858444
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "0WYXb1YBlh4UvpCrbVEYi",
				"focus": 0.23792812955136233,
				"gap": 10.110101378983757
			},
			"endBinding": {
				"elementId": "4gfp_bbvDLR3Tagp8LFOd",
				"focus": -0.14094816826327652,
				"gap": 10.653190355124082
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "wKiXhqDg7_by04ctag_dQ",
			"type": "text",
			"x": 108.72294471213473,
			"y": -162.13957674837005,
			"width": 123,
			"height": 25,
			"angle": 0,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"seed": 1389268228,
			"version": 6,
			"versionNonce": 1669505340,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1667747967202,
			"link": null,
			"locked": false,
			"text": "",
			"rawText": "",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 17,
			"containerId": "3GYkVz9sfQgLgKlh6fVQJ",
			"originalText": ""
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#364fc7",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "hachure",
		"currentItemStrokeWidth": 1,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStrokeSharpness": "sharp",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"currentItemLinearStrokeSharpness": "round",
		"gridSize": null,
		"colorPalette": {}
	},
	"files": {}
}
```
%%