---
Тип: Задача
Описание: -
Исполнители: 
Проект: ВнедрениеЗнаний

Дисциплина: #Входящие 
Tags: Задача
regex-to-search: "(.*)[Cc]make(.*)[Gg]en[Ee]x(.*)[(INSTALL_INTERFACE)(install_interface)](.*)" # "что-то" regex
regex-to-be-found: "(.*)[Cc]make(.*)[Gg]en[Ee]x(.*)[(INSTALL_INTERFACE)(install_interface)](.*)" # "что-то" regex

День: 06 # Не трогать!
Месяц: 12 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Tuesday 6th December 2022 19:06:53

день_завершения: нет
месяц_завершения: нет
год_завершения: 2022
дата_завершения: 2022-нет-нет

Выполнено: нет
Результат: 
---

```dataview
TABLE день_завершения as "День истечения", месяц_завершения as "Месяц истечения", Проект, Выполнено, Результат, Количество_выходящих_задач as "Количество выходящих задач"
WHERE contains(file.name, "cmake INSTALL_INTERFACE genex")
limit 1
```
# [[cmake INSTALL_INTERFACE genex]]

- Получение источников и ссылок
	- [x] Получение одной ссылки
	- [ ] Получение ссылок на разные источники
- Конспектирование
	- [ ] Достаточное для использования конспектирование
	- [ ] Полное конспектирование
- Создание справок
	- [ ] Справки практического применения
	- [ ] Список терминов
	- [ ] Определение терминов из списка терминов
	- [ ] Список связанных тем(если нет внутри текста)
- [ ] Внедрение в практику

%%
## Результат:



## Следующая задача:

==#Входящие ==
%%

**Содержание потом вырезается, все содержимое конспекта удаляется и содержание вставляется по шаблону входящего конспекта.**

# Содержание 
%%
> [!tldr]- Практическая справка
> 
%%

#Входящие 

%%
> [!example]- Список терминов
> - [ ] Пример
%%

> [!question]- Соурслист
> 1. https://cmake.org/cmake/help/latest/manual/cmake-generator-expressions.7.html#genex:INSTALL_INTERFACE

%%
## Приложения
%%

## Смотрите также:

- [[cmake install()]]

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

---
#ВнедрениеЗнаний #Программирование/cmake/genex