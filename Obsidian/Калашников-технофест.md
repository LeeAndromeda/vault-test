


---
Тип: Проект
Проект: Аэробот
название_проекта: "(.*)[Кк]алашников(.*)"
Описание: -
Исполнители: 

Дисциплина: Работа
Tags:
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: 17 # Не трогать!
Месяц: 01 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-01-17

дата_завершения: "2023-02-02"

Выполнено: да
Результат: "Успешно просран"
---


```dataview
TABLE название_проекта as "Название проекты", день_завершения as "День истечения", месяц_завершения as "Месяц истечения", Выполнено, Результат
WHERE Дата_создания = this.Дата_создания
limit 1
```
# [[Калашников-технофест]]

## Цели проекта

> [!info]- Пример
> 1. Конечная цель 1
>	1. Подцель 1.1
>	2. Подцель 1.2
>	3. ~~Выполненная подцель 1.3~~
>2. Конечная цель 2
>	1. Подцель 2.1
>	2. Подцель 2.2

## Задачи

```dataview
TABLE Описание, дата_завершения as "Дата завершения", Исполнители, Выполнено
FROM #Задача  and !"Templates"
WHERE Тип = "Задача" and regexmatch(this.название_проекта, Проект) and Выполнено = "нет"
SORT дата_завершения ASC
```

> [!success]- Выполненные
> ```dataview
TABLE Описание, дата_завершения as "Дата завершения", Исполнители, Выполнено
FROM #Задача  and !"Templates"
WHERE Тип = "Задача" and regexmatch(this.название_проекта, Проект) and Выполнено = "да"
SORT дата_завершения ASC
>```

## Подпроекты

```dataview
TABLE Описание, день_завершения as "День завершения", месяц_завершения as "Месяц завершения", Исполнители, Выполнено
FROM #Проект and !"Templates"
WHERE Тип = "Проект" and regexmatch(this.название_проекта, Проект) and Выполнено = "нет"
SORT год_завершения ASC, месяц_завершения ASC, день_завершения ASC
```

## Примерный план

> [!attention] Caution
> 


## Участники проекта

```dataview
LIST
FROM !"Obsidian/Templates" and !"Templates" and #Человек 
WHERE regexmatch(this.название_проекта, Проект) or regexmatch(Проект, this.название_проекта)
```

## Смотрите также

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
---

#Проект/Текущий 



