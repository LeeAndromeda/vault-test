# Cout Новая строка

Объект cout не добавляет окончание строки в конце вывода.

Одним из способов напечатать две строки является использование манипулятора **endl**, который означает окончание строки.

    #include <iostream>
    using namespace std;
    
    int main()
    {
        cout << "Hello world!" << endl;
        cout << "I love programming!";
        return 0;
    }

**Символ новой строки \n** может быть использован как альтернатива **endl**.

Использование одного выражения cout с тем количеством **\n**, которое необходимо в вашей программе, выведет множество строк текста.

Например:

    #include <iostream>
    using namespace std;
    
    int main()
    {
        cout << "Hello world! \n";
        cout << "I love programming!";
        return 0;
    }

# Связано:

- [[КК Программирование на C++]]
- [[Вывод чисел с плавающей точкой]]

#ЕГЭ #Информатика #Программирование