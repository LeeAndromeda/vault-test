---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 30 # Не трогать!
Месяц: 07 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-07-30

---
# [[Простой сервер-клиент (C++)]]


# Простой сервер-клиент

## Сервер

Создайте файл src/add_two_ints_server.cpp в пакете beginner_tutorials и вставьте в него следующее:

```cpp
#include "ros/ros.h"
#include "beginner_tutorials/AddTwoInts.h"
bool add(beginner_tutorials::AddTwoInts::Request  &req,
         beginner_tutorials::AddTwoInts::Response &res)
{
  res.sum = req.a + req.b;
  ROS_INFO("request: x=%ld, y=%ld", (long int)req.a, (long int)req.b);
  ROS_INFO("sending back response: [%ld]", (long int)res.sum);
  return true;
}
int main(int argc, char **argv)
{
  ros::init(argc, argv, "add_two_ints_server");
  ros::NodeHandle n;
  ros::ServiceServer service = n.advertiseService("add_two_ints", add);
  ROS_INFO("Ready to add two ints.");
  ros::spin();
  return 0;
}
```

### Пояснение

Теперь давайте разложим код на части.

```cpp
#include "ros/ros.h"
#include "beginner_tutorials/AddTwoInts.h"
```

beginner_tutorials/AddTwoInts.h - это заголовочный файл, автоматически сгенерированный из файла srv, который мы создали ранее.

```cpp
bool add(beginner_tutorials::AddTwoInts::Request &req,
         beginner_tutorials::AddTwoInts::Response &res)
```

Эта функция предоставляет сервис для сложения двух int, она принимает тип запроса и ответа, определенный в файле srv, и возвращает булево значение.

```cpp
{
  res.sum = req.a + req.b;
  ROS_INFO("запрос: x=%ld, y=%ld", (long int)req.a, (long int)req.b);
  ROS_INFO("отправка ответа: [%ld]", (long int)res.sum);
  return true;
}
```

Здесь два int добавляются и сохраняются в ответе. Затем записывается некоторая информация о запросе и ответе. Наконец, сервис возвращает true, когда он завершен.

```cpp
  ros::ServiceServer service = n.advertiseService("add_two_ints", add);
```

Здесь сервис создается и рекламируется через ROS.

## Клиент

Создайте файл src/add_two_ints_client.cpp в пакете beginner_tutorials и вставьте в него следующее:

```cpp
#include "ros/ros.h"
#include "beginner_tutorials/AddTwoInts.h"
#include <cstdlib>
int main(int argc, char **argv)
{
  ros::init(argc, argv, "add_two_ints_client");
  if (argc != 3)
  {
    ROS_INFO("usage: add_two_ints_client X Y");
    return 1;
  }
  ros::NodeHandle n;
  ros::ServiceClient client = n.serviceClient<beginner_tutorials::AddTwoInts>("add_two_ints");
  beginner_tutorials::AddTwoInts srv;
  srv.request.a = atoll(argv[1]);
  srv.request.b = atoll(argv[2]);
  if (client.call(srv))
  {
    ROS_INFO("Sum: %ld", (long int)srv.response.sum);
  }
  else
  {
    ROS_ERROR("Failed to call service add_two_ints");
    return 1;
  }
  return 0;
}
```

### Пояснение

```cpp
  ros::ServiceClient client = n.serviceClient<beginner_tutorials::AddTwoInts>("add_two_ints");
```

Это создает клиента для службы add_two_ints. Объект ros::ServiceClient используется для последующего вызова сервиса.

```cpp
  beginner_tutorials::AddTwoInts srv;
  srv.request.a = atoll(argv[1]);
  srv.request.b = atoll(argv[2]);
```

Здесь мы создаем автогенерируемый класс сервиса и присваиваем значения его члену request. Класс сервиса содержит два члена: запрос и ответ. Он также содержит два определения класса, Request и Response.

```cpp
  if (client.call(srv))
```

Это фактически вызывает сервис. Поскольку вызовы служб блокируются, функция вернется, как только вызов будет завершен. Если вызов службы прошел успешно, call() вернет true и значение в srv.response будет действительным. Если вызов не удался, call() вернет false, а значение в srv.response будет недействительным. 

## Настройка CMakeLists.txt для сервера и клиента

> ### В общем виде:

```cpp
add_executable(<<< executive_name >>> src/<executive_name>.cpp)
target_link_libraries(<<< executive_name >>> ${catkin_LIBRARIES})
add_dependencies(<<< executive_name >>> <package_name>_gencpp)
```

Снова отредактируйте файл beginner_tutorials CMakeLists.txt, расположенный по адресу ~/catkin_ws/src/beginner_tutorials/CMakeLists.txt, и добавьте следующее в конце: 

```cpp
add_executable(add_two_ints_server src/add_two_ints_server.cpp)
target_link_libraries(add_two_ints_server ${catkin_LIBRARIES})
add_dependencies(add_two_ints_server beginner_tutorials_gencpp)

add_executable(add_two_ints_client src/add_two_ints_client.cpp)
target_link_libraries(add_two_ints_client ${catkin_LIBRARIES})
add_dependencies(add_two_ints_client beginner_tutorials_gencpp)
```

Это создаст два исполняемых файла, add_two_ints_server и add_two_ints_client, которые по умолчанию будут помещены в каталог package вашего пространства devel, расположенный по умолчанию по адресу ~/catkin_ws/devel/lib/<имя пакета>. Вы можете вызывать исполняемые файлы напрямую или использовать rosrun для их вызова. 


## Смотрите также:
- [[srv файлы]]
```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Робототехника/ROS 

---

[^def]: термин
[^que]: вопрос