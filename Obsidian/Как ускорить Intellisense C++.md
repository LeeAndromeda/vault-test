---

Tags: 
aliases: [ускорение intellisense для c++, ускорение интеллисенс для с++, intellisence C++]
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 24 # Не трогать!
Месяц: 05 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-05-24

---
# [[Как ускорить Intellisense C++]]


My answer's for c++ but still kinda related.

I'm using the C/C++ extension from Microsoft, and when I switched its `Intelli Sense Engine` setting from `Default`, with "context-aware results", to `Tag Parser`, with "'fuzzy' results that are not context-aware", it immediately started showing IntelliSense options instead of delaying for 5+ seconds.

So maybe check the particular options of your language's or environment's extension(s).

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://stackoverflow.com/questions/51874486/visual-studio-code-intellisense-is-very-slow-is-there-anything-i-can-do


---
#cpp

---

[^def]: термин
[^que]: вопрос