---
version: 1
workflow: Default Workflow
drafts:
  - name: Draft 1
    folder: Draft 1
    scenes:
      - Список полезной литературы
      - Аннотация
      - I. Введение
      - II. Родственные работы
      - III. Устройство алгоритма
      - IV. Моделирование и эксперименты
      - V. Выводы
      - Приложение Локации
  - name: Вспомогательный
    folder: Вспомогательный
    scenes:
      - Совместить картинки локаций и нарисовать оси
      - Перспективы развития алгоритма
      - Перенос формул в word
      - Описать алгоритм проверки на застревание
      - Наложить на каждое видео картинку локации
      - Кратко описать работу SLAM gmapping
      - Как писать статьи
      - Идеи
      - Вычисление частоты срабатывания колбэка в РОС
      - Вопросы, которые мне могут задать
      - Варианты тем для статьи
      - Важная информация по конференциям
      - Аннотация Планирование движения колесного робота в плоской
        неопределенной среде
      - O Khatib - Real-Time Obstacle Avoidance for Manipulators and Mobile
        Robots
      - Min Gyu Park - A New Technique to Escape Local Minimum in Artificial
        Potential  Field Based Path Planning
      - Fethi Matoui - Local minimum solution for the potential ﬁeld method in
        multiple robot motion planning task
      - Benchmarking Motion Planning Algorithms - Kavraki Lab
---


This file is managed by Longform. Please avoid editing it directly; doing so will almost certainly confuse the plugin, and may cause a loss of data.

Longform uses this file to organize your folders and notes into a project. For more details, please see [The Index File](https://github.com/kevboh/longform#the-index-file) section of the plugin’s README.

