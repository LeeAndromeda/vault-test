---
Тип: Проект 
название_проекта: "(.*)SAUM(.*)"                                                                                                                                                             
Описание: -
Дисциплина: Робототехника
Tags: Проект
regex-to-search: "((.*)[Ss][Aa][Uu][Mm](.*))"
regex-to-be-found: "NoRgxTBFKey"

День: 11 # Не трогать!
Месяц: 11 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Friday 11th November 2022 00:23:37

день_завершения: 5
месяц_завершения: 1
год_завершения: 2022

Выполнено: да
Результат: 
---


```dataview
TABLE название_проекта as "Название проекты", день_завершения as "День истечения", месяц_завершения as "Месяц истечения", Выполнено, Результат
WHERE contains(file.name, "SAUM") and !contains(file.name, "екст")
limit 1
```
# [[SAUM]]

## Цели проекта

 

> [!info]- Пример
> 1. Конечная цель 1
>	1. Подцель 1.1
>	2. Подцель 1.2
>	3. ~~Выполненная подцель 1.3~~
>2. Конечная цель 2
>	1. Подцель 2.1
>	2. Подцель 2.2

## Задачи

```dataview
TABLE Описание, день_завершения as "День завершения", месяц_завершения as "Месяц завершения", Исполнители, Выполнено
FROM #Задача  and !"Templates"
WHERE Тип = "Задача" and regexmatch(this.название_проекта, Проект) and Выполнено = "нет"
SORT год_завершения ASC, месяц_завершения ASC, день_завершения ASC
```

> [!success]- Выполненные
> ```dataview
TABLE Описание, дата_завершения as "Дата завершения", Исполнители, Выполнено
FROM #Задача  and !"Templates"
WHERE Тип = "Задача" and regexmatch(this.название_проекта, Проект) and Выполнено = "да"
SORT дата_завершения ASC
>```

## Примерный план

> [!attention] Caution
> 


## Участники проекта

```dataview
LIST
FROM !"Obsidian/Templates" and !"Templates" and #Человек 
WHERE regexmatch(this.название_проекта, Проект) or regexmatch(Проект, this.название_проекта)
```


## Смотрите также

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

---

#Входящие 

