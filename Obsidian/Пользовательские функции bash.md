---
Тип: конспект
Описание: -
День: 20
Месяц: 07
Год: 2022
---
# Пользовательские функции bash

Sometimes you may need to create an alias that accepts one or more arguments. That’s where bash functions come in handy.

The syntax for creating a [bash function](https://linuxize.com/post/bash-functions/) is very easy. They can be declared in two different formats:

```sh
function_name () {
  [commands]
}
```

or

```sh
function function_name {
  [commands]
}
```

To pass any number of arguments to the bash function simply, put them right after the function’s name, separated by a space. The passed parameters are `$1`, `$2`, `$3`, etc., corresponding to the position of the parameter after the function’s name. The `$0` variable is reserved for the function name.

Let’s create a simple bash function which will [create a directory](https://linuxize.com/post/how-to-create-directories-in-linux-with-the-mkdir-command/) and then navigate into it:

~/.bashrc

```sh
mkcd ()
{
  mkdir -p -- "$1" && cd -P -- "$1"
}
```

Same as with aliases, add the function to your `~/.bashrc` file and run `source ~/.bash_profile` to reload the file.

Now instead of using `mkdir` to create a new directory and then `cd` to [move into that directory](https://linuxize.com/post/linux-cd-command/) , you can simply type:

```
mkcd new_directory
```

If you wonder what are `--` and `&&` here is a short explanation.

-   `--` - makes sure you’re not accidentally passing an extra argument to the command. For example, if you try to create a directory that starts with `-` (dash) without using `--` the directory name will be interpreted as a command argument.
-   `&&` - ensures that the second command runs only if the first command is successful.

## Источник

- https://linuxize.com/post/how-to-create-bash-aliases/

#bash #Linux #Программирование 