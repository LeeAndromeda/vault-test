---

Tags: 
aliases: 
regex-to-search: "((.*)[Cc]make(.*))|((.*)[Нн]астрой(.*))"
regex-to-be-found: "((.*)[Cc]make(.*))|((.*)[Нн]астрой(.*))"

Тип: конспект
Описание: -

День: 29 # Не трогать!
Месяц: 11 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Tuesday 29th November 2022 06:49:12

---
# [[Cmake изначальные настройки проекта]]

Все начинается с `cmake_minimum_required(VERSION <num>)`. Так мы указываем минимальную требуемую версию cmake для билда проекта. Узнать свою версию можно через консоль `cmake --version` или через [переменные](https://stackoverflow.com/questions/43697154/detect-current-cmake-version-using-cmake). Например:

```cmake
cmake_minimum_required(VERSION 3.25.0)
```

Далее настраивается `project()`. Туда обязательно нужно записать имя проекта и можно дополнительно написать другие [данные](https://cmake.org/cmake/help/latest/command/project.html). Например:

```cmake
project(porject_name VERSION 1.0.0.0)
```

%%## Приложения%%

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

> [!question]- Соурслист
> 1. mastering cmake pdf


---
#Программирование/cmake 