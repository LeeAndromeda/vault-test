---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 10 # Не трогать!
Месяц: 05 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-05-10

---
# [[Переменные среды в Python]]

To set and get environment variables in Python you can just use the `os` module:

```python
import os

# Set environment variables
os.environ['API_USER'] = 'username'
os.environ['API_PASSWORD'] = 'secret'

# Get environment variables
USER = os.getenv('API_USER')
PASSWORD = os.environ.get('API_PASSWORD')

# Getting non-existent keys
FOO = os.getenv('FOO') # None
BAR = os.environ.get('BAR') # None
BAZ = os.environ['BAZ'] # KeyError: key does not exist.
```

Note that using `getenv()` or the `get()` method on a dictionary key will return `None` if the key does not exist. However, in the example with `BAZ`, if you reference a key in a dictionary that does not exist it will raise a `KeyError`.


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://able.bio/rhett/how-to-set-and-get-environment-variables-in-python--274rgt5


---
#Python 
#ПеременныеСреды

---

[^def]: термин
[^que]: вопрос