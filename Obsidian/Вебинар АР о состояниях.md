# Про продуктивность

## **Аксиомы**

### 1. Ваше состояние измеримо

То есть ты можешь отличать одно от другого.

### 2. Ваше состояние влияет на ваше поведение

То, как ты себя чувствуешь, влияет на то, **что ты делаешь**. Очевидно, что и на продуктивность тоже.

### 3. Ваше состояние влияет на ваше восприятие

То, как ты видишь мир, это твоя субъективная картина мира, которая создаётся твоим мозгом.

### 4. Твои действия влияют на твоё состояние

**Состояние аттрактор** - это набор состояний и действий, из которого очень сложно выбраться. То есть в этом состоянии, действия, которые вы делаете, приводят в то же состояние, и, чтобы выбраться из этого состояния, нужно будет приложить огромное усилие.

### 5. Внешняя среда влияет на твое состояние

### 6. Можно планировать не только действия, но и состояния


![image](Sostojanije.png)


## **Две личности: планировщик и исполнитель**

**Планировщик** это ты, который планирует. Это мозг твоей работы, личность, которая строит безумные планы и чего-то пытается добиться.

**Исполнитель** это твой тупой подчинённый, но это тоже ты. Представь, что у тебя есть в подчинении совсем тупой рабочий, который действует только под стимулом. Вспомни себя на станции ЮНАТО и поймёшь чуть лучше.

И вот тот ты, который хочет встать в пять утра и сделать все дела из всех списков - это *ты-планировщик*, а тот ты, который встает в пять утра, говорит всем "досвидания, я ложусь обратно спать" - это *ты-исполнитель*.

**Состояние исполнителя очень важно для выполнения задач.**

Для исполнителя необходимо создать систему наград-наказаний, которая и будет контроллировать его поведение. **И не забывай хвалить его, когда он что-то делает правильно.**

Например:
- Отметь, какой ты молодец, когда идешь на тренировку
- Отметь, какой ты молодец, когда занимаешься допами
- Отметь, какой ты молодец, когда делаешь конспекты чего-то полезного в обсидиане
и т.д.

Твоя задача, то есть задача планировщика, окружить исполнителя такой средой, в которой он будет хотеть выполнять твои задачи. **Идея в том, чтобы сэкономить ресурсы исполнителя.**

>Задачи в ГТД надо переформулировать, делать их понятными, потому что *это забота о состоянии исполнителя*. Так он вероятнее их выполнит.

## Что делать, если ты оказался в плохом состоянии?

Плохое состяние можно представить, как очень большой(точнее даже параболический) холм, по которому тебе нужно перебраться на другую сторону. И в начале пути ты лезешь по крутой стене, в некоторые моменты тебе даже кажется, что она отвесная и никогда не перестанет быть такой. *Каждый шаг в этом состоянии дается тебе очень тяжело, и даже мысль о будущем пути заставляет твои руки опускаться.*

**Но чем дальше ты будешь двигаться, чем лучше ты будешь ладить с собой-исполнителем, тем проще будет становиться твой путь.**

Даже маленькие шаги будут двигать тебя вперед и облгечать твою жизнь. Именно на них в тяжелые времена и нужно сосредоточиться, ведь на большой шаг у тебя сил нет, а стояние на месте никак тебя не продвинет.

![image](KrivayaProduktivnosti.png)

> ## Любой ценой избегай сайтов с мемами, картинками, смешными видосами и т.д., потому что они ОЧЕНЬ эффективны в том, чтобы двигать нас по склону состояния обратно. 

### Эксперимент с крысами и «лентой»

Однажды был проведен эксперимент. Изначально нормально живущей, питающейся, размножающейся крысе к центру мозга, отвечающему за получение удовольствия, подключили электрод и поставили кнопку, которая запускала разряд по этому электроду. То есть крыска нажала на кнопку и тут же кайфанула.

Тогда крыса начала нажимать на эту кнопку постоянно и перестала есть пить, размножаться и вообще делать что-либо, кроме нажимания этой кнопки. 

Вот вроде и результат, но весь сок заключается в том, что при препарировании крысы выяснилось, что электрод случайно был подключен *не к центру, отвечающему за удовольствие, а к ценру, отвечающему за ожидание удовольствия.* То есть крыса нажимала на кнопку и думала: "Вот ща будет реально кайф, ща кайфану так кайфану". После этого эксперимент повторялся не раз и результаты были те же. Так же и человек сидит, например, листает ленту, ожидая, что сейчас ему попадется что-то очень крутое, от чего ему будет очень хорошо. Но этого не происходит и человек попадает в более тяжелое состояние.
 
# О привычках в ГТД
>В ГТД не должно быть *отдельного действия выработать привычку*. В ГТД должен быть **ПРОЕКТ выработать привычку**

---
 # Упражнения
### Упражнение 1.
1. Поставь будильники на каждые 15 минут своего бодрствования(да, даже на время работы/учебы/тренировки/т.д., можно оставить вибро режим, главное, чтобы ты его замечал)
2. Каждый раз, когда звенит будильник, мысленно отмечай своё состояние
3. Продолжай так на протяжении 4-х дней
Это упражнение нужно для того, чтобы ты хорошо узнал свои состояния и ввёл в привычку за ними следить. **Это упражнение обязательно, если ты его не выполнишь, ты краб >(~_~)< **

### Упражнение 2.
1. Выполнить упражнение 1. >(~_~)<
2. Составить два списка: список для вещей, которые хорошо влияют на твоё состояние и список для вещей, которые плохо влияют на твоё состояние.

### Упражнение 3.
1. Выполнить упражнения 1. и 2.
2. Упорядочить списки по легкости выполнения

### Упражнение 4.
**При следуюшем планировании задуматься не только о результате, но и о затрачиваемых ресурсах.** То есть учитывать [Р/РС баланс](Р%20РС%20баланс.md).

### Упражнение 5. Съешь лягушку на завтрак

Выработать привычку каждое утро делать одно самое большое, сложное, гадостное дело. Причём планировать лягушку надо предыдущим вечером.
Это нужно потому, что утром у каждого человека самая высокая продуктивность(точнее после пробуждения). Планировать лягушку на завтра нужно сегодня вечером, потому что так ты готовишься к её выполнению.А съев лягушку, ты станешь продуктивнее на весь оставшийся день, будешь удовлетворен собой, а может даже на подъеме съешь еще пару лягушек.

Ещё один плюс в том, что *за месяц ты выполнишь 30 больших важных дел*. Короче привычка полезная, ты и сам уже понял.

### Упражнение 6.
Задумайся, как влияют на твою жизнь изменения в этих пунктах и сделай выводы:
- Сон
- Спорт
- Еда

### Упражнение 7. "Deep work"
Организуй себе 1-1,5 часа возможности глубоко уйти в максимально сосредоточенную работу. Если они у тебя уже есть, то попытайся расширить это время. Попытайся работать так, чтобы ты увлекся работой, погрузился в неё с головой, а после того, как встал от неё, был собой максимально доволен. Причем, чем больше у тебя глубокой работы в течение дня, тем легче и приятнее тебе в неё погружаться.

#продуктивность #АР #состояние