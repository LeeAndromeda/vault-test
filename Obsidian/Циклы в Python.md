---

Tags: 
aliases: 
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

Тип: конспект
Описание: -

День: 20 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Thursday 20th October 2022 11:51:01

---
# [[Циклы в Python]]

## while

```python
while x <= 10:
	do_something()
	x += 1
``` 

## for

```Python
start_n = 0
stop_n = 10
step_n = 2
for i in range(start_n, stop_n, step_n)
# Перечисление от заданного до заданного числа с заданным шагом
```

```Python
x = [1, 2, 4, 767]
for i in range(len(x))
	print(x[i])
# Перечисление в пределах списка
```

```python
fruits = ["apple", "banana", "cherry"]  
for x in fruits:  
  print(x)
# Перечисление из заданного списка поэлементно
```

```python
for x in "banana":  
  print(x)
# Перебор символов в строке
```

```Python
fruits = ["apple", "banana", "cherry"]  
for x in fruits:  
  print(x)  
  if x == "banana":  
    break
# Перечисление, пока не будет выполнено условие прерывания
```

```Python
fruits = ["apple", "banana", "cherry"]  
for x in fruits:  
  if x == "banana":  
    continue  # Останавливает текущую итерацию и запускает следующую
  print(x)
# Перечисление, но banana пропускается
```

```Python
for x in range(6):
  if x == 3:
  	print(x) 
  	#break # Если цикл прерывается, else не выполняется
else:
  print("Finally finished!")

#If the loop breaks, the else block is not executed.

```

```Python
for x in [0, 1, 2]:  
  pass
# Цикл for не может быть пустым, но если так нужно, то напиши pass, чтобы не получить ошибку
```

### Функция range()

> [!seealso]+ Перегрузки
> `range(to)` - составляет последовательность целых чисел от 0 до `to`
> `range(from, to)` - составляет последовательность целых чисел от `from` до `to`
> `range(from, to, step)` - составляет последовательность целых чисел от  `from` до `to` с шагом `step`



## Смотрите также:

- [Источник ...](https://www.w3schools.com/python/python_for_loops.asp)

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

---
#Python 