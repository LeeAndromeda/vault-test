---
Тип: Проект 
название_проекта: "(.*)[Pp]ytorch(.*)[Ee]xtra(.*)curricul(.*)"
Проект: pytorch
Описание: -
Дисциплина: #Входящие 
Tags: Проект
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: 20 # Не трогать!
Месяц: 06 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-06-20
Дата_создания: 2023-06-23, 06-08

дата_завершения: "2023-06-20"

Выполнено: да
Результат: 
---

# [[Доделать extra curriculum по ztm и переходить на 01 главу]]

> [!abstract]- Материалы
> 
> ```dataview
> LIST
> FROM (#ВнедрениеЗнаний/РасширяющееЗнание or #ВнедрениеЗнаний/РасширяющееЗнание/Тропинка) and #pytorch
> 
> ```

> [!example]+ Подпроекты
> ```dataview
> TABLE Описание, день_завершения as "День завершения", месяц_завершения as "Месяц завершения", Исполнители, Выполнено
> FROM #Проект and !"Templates"
> WHERE Тип = "Проект" and regexmatch(this.название_проекта, Проект) and Выполнено = "нет"
> SORT год_завершения ASC, месяц_завершения ASC, день_завершения ASC
> ```

> [!hint]+ Задачи
> 
> > [!hint]+ Текущие по проекту
> > ```dataview
> > TABLE Описание, дата_завершения as "Дата завершения", Исполнители, Выполнено
> > FROM #Задача/РаботаИдет and !"Templates"
> > WHERE regexmatch(this.название_проекта, Проект) and Выполнено = "нет"
> > SORT дата_завершения ASC
> >```
>
> > [!example]- В очереди по проекту
> > ```dataview
> > TABLE Описание, дата_завершения as "Дата завершения", Исполнители, Выполнено
> > FROM #Задача/вОчереди and !"Templates"
> > WHERE regexmatch(this.название_проекта, Проект) and Выполнено = "нет"
> > SORT дата_завершения ASC
> >```
> 
> > [!faq]- Ансорт
> > ```dataview
> > TABLE Описание, дата_завершения as "Дата завершения", Исполнители, Выполнено
> > FROM #Задача and !"Templates"
> > WHERE regexmatch(this.название_проекта, Проект) and Выполнено = "нет"
> > SORT дата_завершения ASC
> >```
> 
> > [!success]- Выполненные
> > ```dataview
> > TABLE Описание, дата_завершения as "Дата завершения", Исполнители, Выполнено
> > FROM #Задача  and !"Templates"
> > WHERE regexmatch(this.название_проекта, Проект) and Выполнено = "да"
> > SORT дата_завершения ASC
> >```



## Участники проекта

```dataview
LIST
FROM !"Obsidian/Templates" and !"Templates" and #Человек 
WHERE regexmatch(this.название_проекта, Проект) or regexmatch(Проект, this.название_проекта)
```

## Смотрите также

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
---

#Входящие 

---


