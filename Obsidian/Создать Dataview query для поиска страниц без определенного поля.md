---
Тип: Задача
Описание: -
Исполнители: 
Проект: 

Дисциплина: PKM
Tags: Задача
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: 05 # Не трогать!
Месяц: 12 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Monday 5th December 2022 20:14:41

день_завершения: 1
месяц_завершения: 12
год_завершения: 2022
дата_завершения: "нет-нет-нет"

Выполнено: нет
Результат: 
---

```dataview
TABLE день_завершения as "День истечения", месяц_завершения as "Месяц истечения", Проект, Выполнено, Результат, Количество_выходящих_задач as "Количество выходящих задач"
WHERE contains(file.name, "Создать Dataview query для поиска страниц без определенного поля")
limit 1
```
# [[Создать Dataview query для поиска страниц без определенного поля]]

#Входящие 

%%

## Результат:

%%

## Следующая задача:

#Входящие 

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

---
#PKM/obsidian #Dataview 