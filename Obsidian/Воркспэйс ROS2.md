---

Tags: 
aliases: 
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

Тип: конспект
Описание: -

День: 12 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Wednesday 12th October 2022 11:35:23

---
# [[Воркспэйс ROS2]]

То же самое, что и воркспэйсы в [[Рабочее пространство catkin|ROS]].

Colcon создает следующие директории, как соседи директории `src`:
- `build` - здесь будут храниться исполняемые файлы
- `install` - сюда будут устанавливаться пакеты, а так же здесь хранятся `setup.bash` и `local_setup.bash`
- `log`

#### Создание воркспэйса

```shell
mkdir -p ~/ros2_ws/src
cd ~/ros2_ws
```

#### Сборка воркспэйса

В корневой директории воркспэйса:

```shell
colcon build --symlink-install
```

После сборки нужно засоурсить install/setup.bash или install/local_setup.bash:

```shell
. install/setup.bash
```

## Терминология

- Основной воркспэйс называется **подложкой(underlay)**, а те, которые в него включаются - **надложкой(overlay)**.

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

---
#Робототехника/ROS2 