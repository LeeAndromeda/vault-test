---

Tags: 
aliases: [ZMQ, sockets lib, библиотека для работы с сокетами] 
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

Тип: конспект
Описание: -

День: 31 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Monday 31st October 2022 12:22:41

---
# [[ZeroMQ]]
Весь следующий код написан по библиотеке cppzmq для C++
---

%%> [!abstract]- Проходимый туториал
> https://zguide.zeromq.org/docs/chapter1/%%


## Теория 

 - [[Пример привязки сокета ZMQ на C++]]
 - [[Hello world сервер-клиент на ZMQ на C++]]
- [[Пример паблишера-сабскрайбера ZMQ на C++]]
- [[Pre: Про строки в ZMQ]]
- [[ZMQ socket poll или сабскрайбер с таймаутом]]

### Шаблоны

- [[zmq шаблоны Паблишер и Сабскрайбер]]

### Элементы кода

- [[Pre: zmq::context_t]]
- [[Pre: zmq::socket_t]]
	- [[Pre: zmq::socket_t::bind()]]
	- [[Pre: zmq::socket_t::send()]]

### Встреченные проблемы

- [[ZMQ сабскрайбер получает пустое сообщение]]

## Материалы

- [ZMQ guide](https://zguide.zeromq.org/docs/chapter1/)
- [A tour of ZMQ](https://brettviren.github.io/cppzmq-tour/index.html)

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

> [!question]- Соурслист
> 1. 


---
#ZMQ