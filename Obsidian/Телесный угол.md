---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 18 # Не трогать!
Месяц: 09 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-09-18

---
# [[Телесный угол]]

Телесный угол равен количеству секторов сфер с площадью равной квадрату радиуса сферы, необходимых, чтобы покрыть заданную часть поверхности сферы. $\eta = \frac{S _{К \cup Ш} }{R ^{2}  }$

> # Илл. 5
[[Лекция по матмоделям роботов 2023-09-18.xopp]]

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Входящие 

---

[^def]: термин
[^que]: вопрос