---

Tags: #ЕжедневныйКонспект
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: ежедневный_конспект
Описание: -

День: 07 # Не трогать!
Месяц: 02 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-02-07

---
# Цели

```dataview
TABLE дата_завершения as "Дата завершения", Проект
FROM #ДолгосрочнаяЦель
WHERE Выполнено = "нет"
SORT месяц_завершения ASC, день_завершения ASC
```
# Дела на сегодня

```dataview
TABLE дата_завершения as "Дата завершения", Проект
FROM #ДомашнееЗадание or #Задача 
WHERE Выполнено = "нет" and дата_завершения = this.дата_создания
SORT месяц_завершения ASC, день_завершения ASC
```

> **Принцип рычага** - Хэл Элрод
> Выдели сегодня 7 самых важных дел и сделай 3 из них. И все, ты выполнил план на день.

## План на сегодня

[[Все задачи полотно.canvas]]

> [!tip]- Фроги
> > [!danger]+ Просроченные
> > > ```dataview
> TABLE дата_завершения as "Дата завершения"
FROM (#Задача or #ДомашнееЗадание) and (#frog or #frog/лягушка) and !#tags and !"Templates" and !"Obsidian/Templates"
WHERE Выполнено = "нет" and (дата_завершения - this.дата_создания <= dur(0 days))
SORT год_завершения ASC, месяц_завершения ASC, день_завершения ASC
>>>```
>
> > [!warning]- В течение недели
> > > ```dataview
> TABLE дата_завершения as "Дата завершения"
FROM (#Задача or #ДомашнееЗадание) and (#frog or #frog/лягушка) and !#tags and !"Templates" and !"Obsidian/Templates"
WHERE Выполнено = "нет" and (дата_завершения - this.дата_создания > dur(0 days) and дата_завершения - this.дата_создания <= dur(7 days))
SORT год_завершения ASC, месяц_завершения ASC, день_завершения ASC
>>>```
> 
>> [!info]- Еще впереди
> > > ```dataview
> TABLE дата_завершения as "Дата завершения"
FROM (#Задача or #ДомашнееЗадание) and (#frog or #frog/лягушка) and !#tags and !"Templates" and !"Obsidian/Templates"
WHERE Выполнено = "нет" and (дата_завершения - this.дата_создания > dur(7 days))
SORT год_завершения ASC, месяц_завершения ASC, день_завершения ASC
>>>```


> [!example]- Дела на неделю
> ```dataview
TABLE дата_завершения as "Дата завершения", Проект
FROM #ДомашнееЗадание or #Задача 
WHERE Выполнено = "нет" and дата_завершения > this.дата_создания and дата_завершения < this.дата_создания + dur(1 week)
SORT дата_завершения ASC
>```

> [!info]- Выполненные
> ```dataview
TABLE дата_завершения as "Дата завершения"
FROM #ДомашнееЗадание or #Задача 
WHERE Выполнено = "да" and день_завершения = this.День and месяц_завершения = this.Месяц and год_завершения = this.Год 
SORT дата_завершения ASC
>```

> [!example]+ Над чем я работаю сейчас
> ```dataview
> LIST FROM #Задача/РаботаИдет 
> WHERE Выполнено = "нет"
> SORT file.name ASC
> ```
> 

> [!example]+ Текущие проекты
> ```dataview
> LIST FROM #Проект/Текущий 
> WHERE Выполнено = "нет"
> SORT file.name ASC
> ```
> 

# Просроченные дела

> [!warning]- Просроченные актуальные
> 
>```dataview
TABLE дата_завершения as "Дата завершения", Проект
FROM #ДомашнееЗадание or #Задача and !"Templates" and !"Obsidian/Templates"  and !#ВнедрениеЗнаний 
WHERE  this.дата_создания - dur(2 weeks) < дата_завершения and this.дата_создания >= дата_завершения and !contains(дата_завершения, "нет") and Выполнено = "нет"
SORT дата_завершения DESC
>```

> [!error]- Эти все ещё релевантны?
> 
>```dataview
TABLE дата_завершения as "Дата завершения", Проект
FROM #ДомашнееЗадание or #Задача and !"Templates" and !"Obsidian/Templates" and !#ВнедрениеЗнаний 
WHERE Выполнено = "нет" and this.дата_создания - дата_завершения > dur(1 week)
SORT месяц_завершения DESC, день_завершения DESC
>```

> [!warning]- Просроченные фроги
> ```dataview
> TABLE дата_завершения as "Дата завершения"
FROM (#Задача or #ДомашнееЗадание) and (#frog or #frog/лягушка) and !#tags and !"Templates" and !"Obsidian/Templates"
WHERE Выполнено = "нет" and дата_завершения - this.дата_создания < dur(0 days)
SORT дата_завершения ASC
> ```

> [!bug]- Все просроченные дела
> 
>```dataview
TABLE дата_завершения as "Дата завершения", Проект
FROM #ДомашнееЗадание or #Задача and !"Templates" and !"Obsidian/Templates" and !#ВнедрениеЗнаний 
WHERE Выполнено = "нет" and ((день_завершения < this.День and месяц_завершения = this.Месяц and год_завершения = this.Год) or (месяц_завершения < this.Месяц and год_завершения = this.Год) or год_завершения < this.Год )
SORT дата_завершения DESC
>```

# Будущие дела


> [!important]- Будущие дела
> ```dataview
TABLE дата_завершения as "Дата завершения", Проект
FROM #ДомашнееЗадание or #Задача and !"Templates" and !"Obsidian/Templates"
WHERE Выполнено = "нет" and дата_завершения - this.дата_создания > dur(1 day)
SORT дата_завершения ASC
>```

## Сегодняшний день

- [[Andrey/ЛИМ/Дневник 2023-02-07]]
- [[Доска приоритетов 2023-02-07]]


# Связано:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

---

---