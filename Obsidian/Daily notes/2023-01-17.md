---

Tags: #ЕжедневныйКонспект
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: ежедневный_конспект
Описание: -

День: 17 # Не трогать!
Месяц: 01 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-01-17

---
# Дела на сегодня

```dataview
TABLE дата_завершения as "Дата завершения", Проект
FROM #ДомашнееЗадание or #Задача 
WHERE Выполнено = "нет" and день_завершения = this.День and месяц_завершения = this.Месяц and год_завершения = this.Год 
SORT месяц_завершения ASC, день_завершения ASC
```

## План на сегодня

[[Все задачи полотно.canvas]]

> [!example]- Дела на неделю
> ```dataview
TABLE дата_завершения as "Дата завершения", Проект
FROM #ДомашнееЗадание or #Задача 
WHERE Выполнено = "нет" and дата_завершения > this.дата_создания and дата_завершения < this.дата_создания + dur(1 week)
SORT дата_завершения ASC
>```

> [!info]- Выполненные
> ```dataview
TABLE дата_завершения as "Дата завершения"
FROM #ДомашнееЗадание or #Задача 
WHERE Выполнено = "да" and день_завершения = this.День and месяц_завершения = this.Месяц and год_завершения = this.Год 
SORT дата_завершения ASC
>```

> [!help] Календарные не назначенные
> ```dataview
> TABLE
> FROM #Задача or #ДомашнееЗадание and #Reclaimai/Назначить 
> ```

# Просроченные дела

> [!warning]- Просроченные актуальные
> 
>```dataview
TABLE дата_завершения as "Дата завершения", Проект
FROM #ДомашнееЗадание or #Задача and !"Templates" and !"Obsidian/Templates" 
WHERE  this.дата_создания - dur(2 weeks) < дата_завершения and !contains(дата_завершения, "нет")
SORT дата_завершения DESC
>```

> [!error]- Эти все ещё релевантны?
> 
>```dataview
TABLE дата_завершения as "Дата завершения", Проект
FROM #ДомашнееЗадание or #Задача and !"Templates" and !"Obsidian/Templates" and !#ВнедрениеЗнаний 
WHERE Выполнено = "нет" and this.дата_создания - дата_завершения > dur(1 week
SORT месяц_завершения DESC, день_завершения DESC
>```

> [!bug]- Все просроченные дела
> 
>```dataview
TABLE дата_завершения as "Дата завершения", Проект
FROM #ДомашнееЗадание or #Задача and !"Templates" and !"Obsidian/Templates"
WHERE Выполнено = "нет" and ((день_завершения < this.День and месяц_завершения = this.Месяц and год_завершения = this.Год) or (месяц_завершения < this.Месяц and год_завершения = this.Год) or год_завершения < this.Год )
SORT дата_завершения DESC
>```

# Будущие дела


> [!important]- Будущие дела
> ```dataview
TABLE дата_завершения as "Дата завершения", Проект
FROM #ДомашнееЗадание or #Задача and !"Templates" and !"Obsidian/Templates"
WHERE Выполнено = "нет" and дата_завершения - this.дата_создания > dur(1 day)
SORT дата_завершения ASC
>```


# Связано:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

---
