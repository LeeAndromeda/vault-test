---

Tags: Входящие

Тип: конспект
Описание: -

День: 19
Месяц: 09
Год: 2022
Дата_последнего_изменения: Monday 19th September 2022 16:50:26
---
# [[Regex]]


## Сайты

- [Обучающий сайт для Regex](https://regexone.com/lesson/letters_and_digits?)
- [Regexr.com](https://regexr.com/)
- [Basic regex syntax](https://www3.ntu.edu.sg/home/ehchua/programming/howto/Regexe.html)

## Смотрите также:

#Программирование/Regex