


---
Тип: Задача
Описание: -
Исполнители: 
Проект: 

Дисциплина: #Входящие 
Tags: Задача
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: 15 # Не трогать!
Месяц: 05 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-05-15

дата_завершения: "2023-нет-нет"

Выполнено: нет
Результат: 
---


# [[Лабораторные по OpenCV]]

[[Лабораторные по OpenCV.pdf]]

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```


---

---