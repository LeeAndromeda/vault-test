---

Tags: 
aliases: ["Wolfram Mathematica", "Установка mathematica на линукс", "Установка mathematica на linux"]
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 03 # Не трогать!
Месяц: 04 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-04-03

---
# [[Установка Wolfram Mathematica на линукс]]

![[Wolfram_Math_13.1.0_lin.torrent]]

Процедура установки и лечения:

1) Разрешить выполнение файла установки: chmod +x Mathematica_13.1.0_LINUX.sh;

2) Во избежание некорректной работы инсталлятора запускаем установку с параметром "target": $ ./Mathematica_13.1.0_LINUX.sh --target ~/wf_math_tmp

3) После распаковки архива инсталлятор запросит целевую директорию установки, вводим по своему выбору (/home/"user"/wf_math_13 например) или соглашаемся на дефолтную

4) Также инсталлятор спросит где создать файлы запуска программы, вводим по своему выбору (/home/user/wf_math_13/bin например) или соглашаемся на дефолтную

5) После завершения установки запускаем программу, в окне регистрации выбираем "Other ways to activate", а затем "Manual activation"

6) Перейти на [Keygen](https://032304.game4you.top/go.php?url=https://ibug.io/blog/2019/05/mathematica-keygen/) - https://ibug.io/blog/2019/05/mathematica-keygen/

7) Ввести MathID в поле ввода. MathID написан в окне активации mathematica.

8) Полученные activation key и password ввести в окно активации.

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]+ Соурслист
> 1. https://032304.game4you.top/forum/viewtopic.php?t=279704&view=newest


---
#Mathematica 

---

[^def]: термин
[^que]: вопрос