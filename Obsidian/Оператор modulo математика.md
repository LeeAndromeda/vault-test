---
Тип: Задача
Описание: -
Исполнители: 
Проект: ВнедрениеЗнаний

Tags: Задача
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: 27 # Не трогать!
Месяц: 02 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-02-27

дата_завершения: "2023-нет-нет"

Выполнено: нет
Результат: 
---

# [[Оператор modulo математика]]

> [!example]- Чеклист знания
> - Получение источников и ссылок
> 	- [ ] Получение одной ссылки
> 	- [ ] Получение ссылок на разные источники
> - Конспектирование
> 	- [ ] Достаточное для использования конспектирование
> 	- [ ] Полное конспектирование
> - Создание справок
> 	- [ ] Справки практического применения
> 	- [ ] Список терминов
> 	- [ ] Определение терминов из списка терминов
> 	- [ ] Список связанных тем(если нет внутри текста)
> - Внедрение в базу знаний
> 	- [ ] Создание связей с оригинальной страницей (этой)
> 	- [ ] Разбиение на подтемы
> 	- [ ] Атомизация - (по необходимости) разбиение конспекта по подтемам на страницы
> - [ ] Внедрение в практику
> 

<mark style="background: #FF5582A6;">**Содержание потом вырезается, все содержимое конспекта удаляется и содержание вставляется по шаблону входящего конспекта.**</mark> 


# Содержание 

## [Wikipedia](https://en.wikipedia.org/wiki/Modulo_(mathematics))

Modulo is a mathematical jargon that was introduced into mathematics in the book Disquisitiones Arithmeticae by Carl Friedrich Gauss in 1801. Given the integers a, b and n, the expression "a ≡ b (mod n)", pronounced "a is [[Конгруэнтность|congruent]] to b modulo n", means that a − b is an integer multiple of n, or equivalently, a and b both share the same remainder when divided by n. It is the Latin ablative of modulus, which itself means "a small measure."

### Переведу на программистском

$a \equiv b \mod n$

То же самое, что и

```cpp
(a - b) % n == 0
// или
a % n == 0 && b % n == 0
```

## ChatGPT

The math formula $\phi \equiv \theta \mod 2 \pi$ means that the angles $\phi$ and $\theta$ are congruent (i.e., equal) modulo $2\pi$.

To understand this, first consider the concept of congruence. Two numbers $a$ and $b$ are said to be congruent modulo $m$ if their difference $a - b$ is divisible by $m$, i.e., if $a - b$ is equal to some integer multiple of $m$. We write this as $a \equiv b \mod m$. For example, $7 \equiv 1 \mod 3$ because $7 - 1$ is divisible by 3 (specifically, $7 - 1 = 6 = 2 \cdot 3$).

In the formula $\phi \equiv \theta \mod 2 \pi$, we are applying this concept of congruence to angles, rather than to integers. Specifically, we are saying that the difference between $\phi$ and $\theta$ is a multiple of $2\pi$, the circumference of a circle. This means that $\phi$ and $\theta$ represent the same angle, but possibly with different revolutions around the circle.

For example, suppose we have two angles $\phi = \frac{\pi}{4}$ and $\theta = \frac{9\pi}{4}$, measured in radians. These angles differ by two full revolutions around the circle, because $\theta - \phi = \frac{8\pi}{4} = 2\pi$. Thus, we can say that $\phi \equiv \theta \mod 2\pi$.

In general, the formula $\phi \equiv \theta \mod 2\pi$ is useful in situations where we are interested in angles that differ by a multiple of $2\pi$, such as in periodic functions like sine and cosine. By using the concept of congruence, we can simplify calculations and avoid redundancy in our analysis.

## Смотрите также:

> [!question]- Соурслист
> 1. 



> [!quote]- Контекст
> ```dataview
> LIST 
> FROM !outgoing([[]])
> WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
> ```

---
#ВнедрениеЗнаний 
#Математика/Оператор

---

[^def]: термин
[^que]: вопрос