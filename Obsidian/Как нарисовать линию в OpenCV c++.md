---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 25 # Не трогать!
Месяц: 07 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-07-25

---
# [[Как нарисовать линию в OpenCV c++]]


In this article, we will discuss how to draw a line using [OpenCV](https://www.geeksforgeeks.org/opencv-c-program-face-detection/) in [C++](https://www.geeksforgeeks.org/c-plus-plus/). The idea is to use the **line() function** from OpenCV C++ library.

**Syntax:**

```cpp
line(img, pt1, pt2, color, thickness, lineType, shift)
```

**Parameters:**

- **img:** This is the image file.
- **start:** Start point of the line segment. The first point out of two ends of a line segment. It is a tuple of two coordinates (x-coordinate, y-coordinate)).
- **end:** Endpoint of the line segment. The second point out of two ends of a line segment. It is a tuple of two coordinates (x-coordinate, y-coordinate)).
- **color:** Color of the line to be drawn. It is a tuple representing 3 colors (B, G, R) i.e.. (Blue, Green, Red).
- **thickness:** Thickness of the line drawn.
- **lineType:** Type of the line. There are 3 types of line:
    - **LINE_4:** Line was drawn using 4 connected [Bresenham algorithm](https://www.geeksforgeeks.org/bresenhams-line-generation-algorithm/).
    - **LINE_8:** Line drawn using 8 connected Bresenham algorithm.
    - **LINE_AA:** It draws Antialiased lines formed by using the [Gaussian filter](https://www.geeksforgeeks.org/gaussian-filter-generation-c/).
- **nshift:** It is the Number of fractional bits in the point coordinates.

**Return Value:** It returns an image.

**Program 1:**

Below is the program shows how to draw all types of lines over a self-formed background image:

```cpp
// C++ program for the above approach
#include <iostream>
#include <opencv2/core/core.hpp>

// Library to include for
// drawing shapes
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
using namespace cv;
using namespace std;

// Driver Code
int main(int argc, char** argv)
{
	// Create a blank image of size
	// (500 x 500) with black
	// background (B, G, R) : (0, 0, 0)
	Mat image(500, 500, CV_8UC3,
			Scalar(0, 0, 0));

	// Check if the image is created
	// successfully
	if (!image.data) {
		cout << "Could not open or find"
			<< " the image";

		return 0;
	}

	Point p1(0, 0), p2(100, 0);
	Point p3(200, 0), p4(500, 500);
	int thickness = 2;

	// Line drawn using 8 connected
	// Bresenham algorithm
	line(image, p1, p4, Scalar(255, 0, 0),
		thickness, LINE_8);

	// Line drawn using 4 connected
	// Bresenham algorithm
	line(image, p2, p4, Scalar(0, 255, 0),
		thickness, LINE_4);

	// Antialiased line
	line(image, p3, p4, Scalar(0, 0, 255),
		thickness, LINE_AA);

	// Show our image inside window
	imshow("Output", image);
	waitKey(0);

	return 0;
}
```

**Output:**

[![](https://media.geeksforgeeks.org/wp-content/uploads/20210115151355/out1.jpg)](https://media.geeksforgeeks.org/wp-content/uploads/20210115151355/out1.jpg)

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#КомпьютерноеЗрение/opencv 
#cpp

---

[^def]: термин
[^que]: вопрос