---
Тип: конспект
Описание: "Краткий гайд по плагину Hover editor, позволяющему открывать конспекты в качестве окон"

День: 10  
Месяц: 07
Год: 2022
---
# Как использовать Hover editor

Этот плагин позволяет открыть множество конспектов в оконном виде.

```ad-warning
title: Внимание

В оконном режиме навигация по файлам затруднена.

```


#PKM/obsidian #Плагины #НовоеПоколениеЗнания 