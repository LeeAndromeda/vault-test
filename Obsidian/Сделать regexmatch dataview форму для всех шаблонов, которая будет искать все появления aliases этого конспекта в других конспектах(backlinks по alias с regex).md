---
Тип: Задача
regex-to-search: "((.*)[зЗ]акон.*|(.*)[Фф]изик.*)|([Сс]тать.*)"
Описание: -
Дисциплина: PKM
Tags: Задача

День: 01 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Saturday 1st October 2022 18:46:51

день_завершения: нет
месяц_завершения: 0
год_завершения: 2022

Выполнено: да
Результат: Успех
---

```dataview
TABLE день_завершения as "День истечения", месяц_завершения as "Месяц истечения", Проект, Выполнено
WHERE contains(file.name, "Сделать regexmatch dataview форму для всех шаблонов, которая будет искать все появления aliases этого конспекта в других конспектах(backlinks по alias с regex)")
limit 1
```
# [[Сделать regexmatch dataview форму для всех шаблонов, которая будет искать все появления aliases этого конспекта в других конспектах(backlinks по alias с regex)]]

> [!note] Пример
> ```dataview
LIST
WHERE regexmatch(this.regex-to-search, file.name) or regexmatch(this.regex-to-search, file.aliases)
>```
>

> [!note]+ Другой пример
> - [[Физика]]

## Следующая задача:

- [[Попробовать разобраться, как парсить содержание конспекта в dataview js с использованием dv.io.read()]]

## Смотрите также:

- [Источник](https://willcodefor.beer/posts/dataviewio)
- [[Функции Dataview]]

#PKM/obsidian 