---

Tags: Входящие

Тип: конспект
Описание: -

День: 08
Месяц: 09
Год: 2022
Дата_последнего_изменения: Thursday 8th September 2022 15:06:14
---
# [[Электротехника]]

## Лекции

```dataview
LIST FROM #Лекция and #Электротехника and !#Электротехника/solid and !([[]])
SORT дата_создания ASC
```

- [[Анализ простых цепей в частотной области - Лекция по электротехнике 2022-10-13]]
- [[Методы анализа простых цепей на переменном токе - Лекция по электротехнике 2022-09-29, 2022-10-06]]
- [[Метод комплексных амплитуд. Укравнение баланса мощностей - Лекция по электротехнике 2022-09-22]]
- [[Анализ линейных цепей на постоянном токе]]
- [[Базовые понятия электротехники - Лекция по электротехнике 2022_09_08]]
- [[Электротехника 2023-05-11.xopp]]

> [!abstract]- Содержание курса (1 семестр)
 > 
 > - [[Базовые понятия электротехники - Лекция по электротехнике 2022_09_08]]
> 	- Определение электротехники
> 	- Основные понятия
> 	- Идеальные элементы
> 	- Топология электрических цепей - основные понятия
> 	- Первый и Второй законы Кирхгофа
> 	- Пример составления законов Кирхгофа
> - [[Анализ линейных цепей на постоянном токе]]
> 	- Анализ линейных цепей на постоянном токе
> 	- Характеристика идеальных элементов вна постоянном токе
> 	- Первый и второй законы Кирхгофа на постоянном токе
> - [[Метод комплексных амплитуд. Укравнение баланса мощностей - Лекция по электротехнике 2022-09-22]]
> 	- Анализ электрических цепей на синусоидальном токе
> 	- Метод комплексных амплитуд
> 	- Характеристика идеальных элементов на синусоидальном токе
> 	- Закон Ома в комплексной форме
> 	- Законы Кирхгофа в комплексной форме
> 	- Алгоритм формирования уравнений по законам Кирхгофа в цепи с синусоидальным источником тока
> 	- Уравнение баланса мощностей
> - [[Методы анализа простых цепей на переменном токе - Лекция по электротехнике 2022-09-29, 2022-10-06]]
> 	- Анализ простых цепей на переменном токе
> 	- Методы анализа
> 		- Уравнения Кирхгофа
> 		- Метод пропорционального пересчета
> 		- Метод Эквивалентных преобразований
> 		- Графо-аналитический метод
> - [[Анализ простых цепей в частотной области - Лекция по электротехнике 2022-10-13]]
> 	- Анализ простых цепей в частотной области
> 	- Алгоритм формирования комплексного коэффициента передачи
> - [[Резонанс напряжения в последовательном колебательном контуре - Лекция по электротехнике 2022-10-20]]
> 	- Резонанс напряжения, возникающий в последовательном колебательном контуре
> 	- Определение комплексных коэффициентов передачи по напряжению на элементах RLC и построение АЧХ и ФЧХ
> - [[Лекция по электротехнике 2022-10-27]]
> 	- Резонанс токов
> 	- Свойство дуальности цепей
> 	- Построение дуальной схемы
> - [[Методы рассчета сложных цепей. Метод контурных токов. Метод узловых напряжений. Лекция по электротехнике 2022-11-10]]
> 	- Список методов рассчета сложных цепей
> 	- Метод контурных токов
> 	- Алгоритм формирования уравнений по методу контурныз токов
> 	- Методу узловых напряжений
> 	- Алгоритм формирования уравнений по методу узловых напряжений
> - [[Метод эквивалентного генератора - Лекция по электротехнике 2022-11-24]]
> 	- Метод эквивалентного генератора
> 	- Метод эквивалентных преобразований
> - [[Метод эквивалентных преобразований - Лекция по электротехнике 2022-12-01]]
> 	- Метод эквивалентных преобразований (полный)
> 	- Алгоритм метода эквивалентных преобразований сложной цепи
 > 



## Теория 

- [[Глоссарий электротехники]]
- [[Анализ простых цепей на переменном токе]]
- [[Комплексные числа]]

## Домашнее задание

```dataview
TABLE дата_завершения as "Дата завершения"
FROM #ДомашнееЗадание and #Электротехника 
WHERE Выполнено = "нет"
SORT дата_завершения DESC
```

> [!abstract]- Выполненные
> ```dataview
> TABLE день_завершения as "Число сдачи", месяц_завершения as "Месяц сдачи" 
> FROM #ДомашнееЗадание and #Электротехника 
> WHERE Выполнено = "да"
> SORT день_завершения DESC
>```
## Материалы, которые необходимо структурировать

```dataview
LIST
FROM #Электротехника and #Входящие 
```

## Смотрите также:



#Электротехника 
