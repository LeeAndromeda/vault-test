---
Тип: Задача
Описание: -
Дисциплина: PKM
Tags: Задача
regex-to-search: "((.*)[Jj]ava[Ss]cript(.*))|((.*)[Dd]ataview(.*))|((.*)[Сс]истематиз(.*)[Зз]нан(.*))"
regex-to-be-found: "NoRgxTBFKey"

День: 04 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Tuesday 4th October 2022 12:17:28

день_завершения: нет
месяц_завершения: 0
год_завершения: 2022

Выполнено: да
Результат: Неудача
---

```dataview
TABLE день_завершения as "День истечения", месяц_завершения as "Месяц истечения", Проект, Выполнено, Результат
WHERE contains(file.name, "Попробовать разобраться, как провернуть подобную штуку в dataview js с использованием dv.io.read()")
limit 1
```
# [[Попробовать разобраться, как парсить содержание конспекта в dataview js с использованием dv.io.read()]]



## Результат:



## Следующая задача:

- [[Проверить необходимость использования языка Rant для obsidian]]

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

#PKM/obsidian #JavaScript