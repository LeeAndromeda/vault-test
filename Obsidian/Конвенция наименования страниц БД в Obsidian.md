---

Tags: 
aliases: 
regex-to-search: "(.*)[Oo]bsidian(.*)[Тт]утор(.*)" # "что-то" regex
regex-to-be-found: "(.*)[Oo]bsidian(.*)[Тт]утор(.*)|(.*)[Нн]аимен(.*)" # "что-то" regex

Тип: конспект
Описание: -

День: 06 # Не трогать!
Месяц: 12 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Tuesday 6th December 2022 19:02:08

---
# [[Конвенция наименования страниц БД в Obsidian]]

Имена страниц в обсидиан должны быть образованы по следующему принципу:

```cpp
(префикс_глобальной_темы || префикс_верхней_темы) + тело_имени + постфикс_подтемы
```

%%## Приложения%%

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#PKM/obsidian #туториал 