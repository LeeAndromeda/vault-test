---
Тип: ДЗ
Дисциплина: "Дискретная математика"
Описание: -
aliases: 
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

День: 08 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Saturday 8th October 2022 19:02:28
день_завершения: 22
месяц_завершения: 10
год_завершения: 2022

Выполнено: да
Результат: 

Tags: ДомашнееЗадание
---

```dataview
TABLE день_завершения as "День истечения", месяц_завершения as "Месяц истечения", Выполнено, Результат
WHERE contains(file.name, "Сделать задание 3 по дискретке")
limit 1
```

# [[Сделать задание 3 по дискретке]]

Где-то есть бумажка с одни решенным заданием

## Следующее задание

- [[Переписать 2 и 3 лабораторные по дискретной математике в ворд до 28 октября]]

## Смотрите также:

- [[Дискретная математика лаба 2]]

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

---
#ДискретнаяМатематика 