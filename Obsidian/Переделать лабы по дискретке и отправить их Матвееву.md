---
Тип: ДЗ
Дисциплина: Дискретка
Описание: -
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: 26 # Не трогать!
Месяц: 12 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Monday 26th December 2022 17:59:24

день_завершения: 27
месяц_завершения: 12
год_завершения:  2022
дата_завершения: 2022-12-27

# да или нет (!без ковычек!)
Выполнено: "да"
Результат: 

Tags: ДомашнееЗадание
---

# [[Переделать лабы по дискретке и отправить их Матвееву]]

#Входящие 

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
---
#frog #ДискретнаяМатематика 