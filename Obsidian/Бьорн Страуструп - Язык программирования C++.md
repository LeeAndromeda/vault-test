---

Tags: 
aliases: 
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

Тип: книга
Описание: -

День: 30 # Не трогать!
Месяц: 11 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Wednesday 30th November 2022 11:57:25

---
# [[Бьорн Страуструп - Язык программирования C++]]

Эта одна из самых подробных книг, излагающих принципы языка C++.

## Приложения

- [[The_C++_Programming_Language_4th_Edition_Bjarne_Stroustrup.pdf]] 
- [[Straustrup-Yazyk_programmirovaniya_c.pdf]]

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
%%
> [!question]- Соурслист
> 1. 

%%
---
#cpp #книга 