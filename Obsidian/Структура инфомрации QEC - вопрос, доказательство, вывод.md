---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 06 # Не трогать!
Месяц: 05 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-05-06

---
# [[Структура инфомрации QEC - вопрос, доказательство, вывод]]



В любой информации можно найти три вещи:
- Вопрос Q  - о чем рассказывает автор? 
- Доказательство E (помечается дефисом в конспектах) - информация, которая выражает и доказывает мнение автора. Это могут быть примеры, анекдоты, рассказы и т.д. - все, что приводится в пример для подтверждения позиции по поводу вопроса
- Вывод C (помечается точкой в конспектах) - ответ, который автор дает на вопрос с учетом доказательства


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://www.youtube.com/watch?v=5O46Rqh5zHE


---
#PKM 

---

[^def]: термин
[^que]: вопрос