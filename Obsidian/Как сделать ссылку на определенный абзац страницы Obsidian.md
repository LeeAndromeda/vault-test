---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "(.*)[Oo]bsidian(.*)[([Тт]утори)([Tt]utorial)](.*)" # "что-то" regex

Тип: конспект
Описание: -

День: 06 # Не трогать!
Месяц: 12 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Tuesday 6th December 2022 18:32:07

---
# [[Как сделать ссылку на определенный абзац страницы Obsidian]]

После имени файла поставьте `^` и обсидиан предложит вам абзацы.

%%## Приложения%%

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#PKM/obsidian #obsidian #туториал 