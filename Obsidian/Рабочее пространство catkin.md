# Рабочее пространство catkin

## Как создать

Создадим рабочее пространство:

```linux

$ mkdir -p ~/catkin_ws/src
$ cd ~/catkin_ws/
$ catkin_make

```

Дополнительную информацию по catkin_make смотри здесь --> http://wiki.ros.org/catkin/Tutorials/create_a_workspace.

---

**Типичное рабочее пространство**

```
workspace_folder/        -- WORKSPACE
  src/                   -- SOURCE SPACE
    CMakeLists.txt       -- 'Toplevel' CMake file, provided by catkin
    package_1/
      CMakeLists.txt     -- CMakeLists.txt file for package_1
      package.xml        -- Package manifest for package_1
    ...
    package_n/
      CMakeLists.txt     -- CMakeLists.txt file for package_n
      package.xml        -- Package manifest for package_n
```

---

## Как засоурсить

Сначала компилируешь пакеты(если необходимо):

```
*In a catkin workspace*

$ catkin_make [make_targets] [-DCMAKE_VARIABLES=...]
```

Приведенная команда скомпилирует все пакеты, лежащие в текущем рабочем пространстве.

Затем:

```
cd ~/your_ws
source devel/setup.bash
```

#Робототехника/ROS