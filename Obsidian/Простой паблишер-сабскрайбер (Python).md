---
Тип: конспект
Описание: -

День: 11
Месяц: 08
Год: 2022
Дата_последнего_изменения: Thursday 11th August 2022 14:53:47
---
# [[Простой паблишер-сабскрайбер (Python)]]

- [[Пример ROS нода в Python с использованием класса]]

## Основные отличия

### Строка для интерпретатора

Эта строка должна быть в начале каждого скрипта:

```python3
#!/usr/bin/env python3
```

### Настройка CMakeLists.txt после создания нодов паблишера и сабскрайбера

```CMake
catkin_install_python(
PROGRAMS
scripts/astar.py
DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)
```

## Шаблон паблишера

```Python
#!/usr/bin/env python
# license removed for brevity
import rospy
from std_msgs.msg import String
def talker():
    pub = rospy.Publisher('chatter', String, queue_size=10)
    rospy.init_node('talker', anonymous=True)
    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
        hello_str = "hello world %s" % rospy.get_time()
        rospy.loginfo(hello_str)
        pub.publish(hello_str)
        rate.sleep()
if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
```

## Шаблон сабскрайбера

```cpp
#!/usr/bin/env python
import rospy
from std_msgs.msg import String

def callback(data):
    rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.data)
    
def listener():
    rospy.init_node('listener', anonymous=True)

    rospy.Subscriber("chatter", String, callback)
    rospy.spin()

if __name__ == '__main__':
    listener()
```

## Смотрите также:


#ROS #Python 