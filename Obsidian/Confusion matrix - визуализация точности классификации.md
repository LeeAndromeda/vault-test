---
Тип: Задача
alias: [A confusion matrix is a table that is often used to describe the performance of a classification model (or "classifier") on a set of test data for which the true values are known]
Описание: -
Исполнители: 
Проект: ВнедрениеЗнаний

Tags: Задача
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: 11 # Не трогать!
Месяц: 08 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-08-11

дата_завершения: "2023-нет-нет"

Выполнено: нет
Результат: 
---

# [[Confusion matrix - визуализация точности классификации]]

> [!danger] Обязательный чеклист
> - [ ] Получение одной ссылки
> - [ ] Расставить теги 


> [!example]+  Чеклист знания
> - Получение источников и ссылок
> 	- [ ] Получение ссылок на разные источники
> - Конспектирование
> 	- [ ] Достаточное для использования конспектирование
> 	- [ ] Полное конспектирование
> - Создание справок
> 	- [ ] Справки практического применения
> 	- [ ] Список терминов
> 	- [ ] Определение терминов из списка терминов
> 	- [ ] Список связанных тем(если нет внутри текста)
> - Внедрение в базу знаний
> 	- [ ] Создание связей с оригинальной страницей (этой)
> 	- [ ] Разбиение на подтемы
> 	- [ ] Атомизация - (по необходимости) разбиение конспекта по подтемам на страницы
> - [ ] Внедрение в практику
> 

<mark style="background: #FF5582A6;">**Содержание потом вырезается, все содержимое конспекта удаляется и содержание вставляется по шаблону входящего конспекта.**</mark> 

# Содержание 

**Confusuion matrix** - shows you where your classification model got confused between predicitons and true labels.

Пример  из [[03_hw.ipynb]]:

```python
from torchmetrics import ConfusionMatrix
from mlxtend.plotting import plot_confusion_matrix

test_img = None
test_label = None

y_pred_tensor = []
targets = []

acc = 0
for batch, (imgs, labels) in enumerate(train_dl):
    imgs = imgs.to(device)
    labels = labels.to(device)
    sum_acc = 0
    prediction = predict(model, imgs)
    
    for i in range(len(prediction)):
        y_pred_tensor.append(prediction[i])
        targets.append(labels[i])
    
    for i in range(len(labels)):
        sum_acc += prediction[i] == labels[i]
        if (batch == 2 and i == 3):
            test_img = imgs[i]
            test_label = labels[i]
            break
        # print(f"prediction: {prediction[i]}\nlabel: {labels[i]}")
    acc = sum_acc / len(labels)

y_pred_tensor = torch.tensor(y_pred_tensor)
targets = torch.tensor(targets)


print(f"accuracy: {acc * 100}%")

# 2. Setup confusion matrix instance and compare predictions to targets
confmat = ConfusionMatrix(num_classes=len(class_names), task='multiclass')
confmat_tensor = confmat(preds=y_pred_tensor,
                         target=targets)

# 3. Plot the confusion matrix
fig, ax = plot_confusion_matrix(
    conf_mat=confmat_tensor.numpy(), # matplotlib likes working with NumPy 
    class_names=class_names, # turn the row and column labels into class names
    figsize=(10, 7)
);
```

![[ConfusionMatrix.png]]

## Смотрите также:

> [!question]- Соурслист
> 1. https://www.dataschool.io/simple-guide-to-confusion-matrix-terminology/

> [!quote]- Контекст
> ```dataview
> LIST 
> FROM !outgoing([[]])
> WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
> ```

---
#ВнедрениеЗнаний 
#Нейросети 
#Статистика 

---

[^def]: термин
[^que]: вопрос