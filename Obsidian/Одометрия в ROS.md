# Одометрия в ROS

- **nav_msgs/Odometry.msg**
	- std_sgs/Header header
		- uint32 seq
		- time stamp
		- string frame_id
	- string child_frame_id
	- geometry_msgs/PoseWithCovariance pose
		- geometry_msgs/Pose pose
			- geometry_msgs/Point position
				- float64 x
				- float64 y
				- float64 z
			- geometry_msgs/Quaternion orientation
				- float64 x
				- float64 y
				- float64 z
				- float64 w
		- float64[36] covariance
	- geometry_msgs/TwistWithCovariance twist
		- geometry_msgs/Twist twist
			- geometry_msgs/Vector3 linear
				- float64 x
				- float64 y
				- float64 z
			- geometry_msgs/Vector3 angular
			- float64 x
			- float64 y
			- float64 z
	- float64[36] covariance

## Смотрите также:

- [[Одометрия]]
- [[Кватернионы]]

- [Как рассчитать ковариантность](https://www.reddit.com/r/ROS/comments/nz0p2z/how_to_calculate_covariance_for_wheel_odometry/)

#Робототехника/ROS 