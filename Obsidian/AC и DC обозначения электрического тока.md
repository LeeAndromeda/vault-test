---

Tags: 
aliases: 
regex-to-search: "(.*)[Ээ]лектр(.*)[Тт]ок(.*)" # "что-то" regex
regex-to-be-found: "(.*)[Ээ]лектр(.*)[Тт]ок(.*)" # "что-то" regex

Тип: конспект
Описание: -

День: 26 # Не трогать!
Месяц: 02 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-02-26

---
# [[AC и DC обозначения электрического тока]]

- **AC (alternating current)** - переменный ток
- **DC (direct current)** - постоянный ток


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://www.lcard.ru/lexicon/ac_dc_term


---
#Физика

---

[^def]: термин
[^que]: вопрос