---
Тип: Задача
Описание: -
Исполнители: 
Проект: 

Дисциплина: Работа
Tags: Задача 
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

День: 28 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Friday 28th October 2022 16:59:36

день_завершения: 31
месяц_завершения: 10
год_завершения: 2022

Выполнено: да
Результат: 
---

```dataview
TABLE день_завершения as "День истечения", месяц_завершения as "Месяц истечения", Проект, Выполнено, Результат, Количество_выходящих_задач as "Количество выходящих задач"
WHERE contains(file.name, "Установить ROS2 на Lenovo")
limit 1
```
# [[Установить ROS2 на Lenovo]]


## Результат: 



## Следующая задача:


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```


---

#Робототехника/ROS2 