---

Tags: 
aliases: [ROS2]
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

Тип: конспект
Описание: -

День: 12 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Wednesday 12th October 2022 11:33:15

---
# [[Работа с ROS2]]

> [!tip]- Проходимый туториал
> https://docs.ros.org/en/foxy/Tutorials/Beginner-Client-Libraries/Writing-A-Simple-Cpp-Publisher-And-Subscriber.html


## Основные понятия

- [[Типы данных ROS2]]
- [[Воркспэйс ROS2]]
- [[Пакеты ROS2]]
- [[Простой паблишер-сабскрайбер ROS2(С++)]]
- [[Простой паблишер-сабскрайбер ROS2(Python)]]
- [[Создание собственных msg и srv файлов ROS2]]
- [[Передача в callback функцию ROS2 нескольких аргументов C++]]
- [[Launch-файлы с пользовательскими параметрами в ROS2]]
- [[Тотальный разбор launch файлов в ROS2]]
- [[Многопоточность в ROS2]]

- [[Таблица команд ROS и их аналогов в ROS2]]

---

- [[Как получить текущее время в ROS2]]

---

- [[ROS Industrial]]

---

- **[[Проблемы, встреченные при работе с ROS2]]** 



### Работа с нодами

- [[Перенастройка топика на другой объект ROS2]]
- [[Дебаг нодов в ROS2]]

### Настройка переменных среды ROS2

- [[ROS_LOCALHOST_ONLY ROS2]]
- [[DOMAIN_ID ROS2]]
- [[RWM_IMPLEMENTATION]]

## Команды

- **ros2 run pkg node**
- **ros2 node**
	- list - возвращает лист нодов
	- info - возвращает список подписчиков, паблишеров, сервисов, экшн серверов, экшн клиентов
- **ros2 topic**
	- list -t - возвращает спс иок топиков с типами топиков
	- info - 
- **ros2 interface**
	- show pkg/msg/msg_name - описывает поля сообщения
- **[ros2 topic pub](https://docs.ros.org/en/foxy/Tutorials/Beginner-CLI-Tools/Understanding-ROS2-Topics/Understanding-ROS2-Topics.html#ros2-topic-pub)**
- **ros2 service**
	- list
	- type
	- find - показывает, какие сервисы используют этот тип srv файла
	- show - показывает
- **[[ros2 param]]**
	- list
	- get
	- set
	- dump <node_name> - сохраняет конфигурацию параметров в yaml файл
- **ros2 bag**
	- record topic name

## Смотрите также:
- [[Список тем по ROS2 для начинающих]]
- [ROS1 Bridge](https://github.com/ros2/ros1_bridge/blob/master/README.md)
- [ROS2 Tutorials](https://docs.ros.org/en/foxy/Tutorials.html)

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

---
#Робототехника/ROS2  

---