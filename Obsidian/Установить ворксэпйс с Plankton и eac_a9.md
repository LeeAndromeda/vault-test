---
Тип: Задача
Описание: -
Исполнители: 
Проект: Рубин

Дисциплина: Работа
Tags: Задача 
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

День: 07 # Не трогать!
Месяц: 11 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Monday 7th November 2022 17:09:35

день_завершения: нет
месяц_завершения: 1
год_завершения: 2022

Выполнено: да
Результат: Неудача
---

```dataview
TABLE день_завершения as "День истечения", месяц_завершения as "Месяц истечения", Проект, Выполнено, Результат, Количество_выходящих_задач as "Количество выходящих задач"
WHERE contains(file.name, "Установить ворксэпйс с Plankton и eac_a9")
limit 1
```
# [[Установить ворксэпйс с Plankton и eac_a9]]

Установить на работе на рос2 убунте воркспэйс, в который загрузить Plankton uuv_simulator и eca_a9 АНПА.

## Результат: 

- Удалось поставить Plankton. Удалось запустить тренировочный лаунч
- Не удалось запостить ни один другой лаунч - жалобы на неправильный синтаксис даже после исправлений
- Не удалось запустить ни один лаунч eca_a9 кроме спауна АНПАшки, однако ни один нод управления не запустился 



## Следующая задача:

- [[Попробовать запустить Plankton с двумя и более АНПА rexrov]]

## Смотрите также:

- https://github.com/Liquid-ai/Plankton
- https://uuvsimulator.github.io/packages/eca_a9/intro/

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```


---

#Робототехника 