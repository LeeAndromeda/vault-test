---
Тип: Задача
Описание: -
Исполнители: 
Проект: 

Дисциплина: Саморазвитие
Tags: Задача
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

День: 24 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Monday 24th October 2022 17:04:15

день_завершения: нет
месяц_завершения: 1
год_завершения: 2023
дата_завершения: 2023-1-нет

Выполнено: нет
Результат: 
---

```dataview
TABLE день_завершения as "День истечения", месяц_завершения as "Месяц истечения", Проект, Выполнено, Результат, Количество_выходящих_задач as "Количество выходящих задач"
WHERE contains(file.name, "Посмотереть стандарты оформления кода по c++")
limit 1
```
# [[Посмотереть стандарты оформления кода по c++]]

#Входящие 

## Результат:



## Следующая задача:

#Входящие 

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```


---

#cpp 