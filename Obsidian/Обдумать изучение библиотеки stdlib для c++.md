---
Тип: Задача
Описание: -
Исполнители: 
Проект: 

Дисциплина: Саморазвитие
Tags: Задача
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

День: 21 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Friday 21st October 2022 15:25:52

день_завершения: 2
месяц_завершения: 1
год_завершения: 2023
дата_завершения: "нет-нет-нет"

Выполнено: нет
Результат: 
---

```dataview
TABLE день_завершения as "День истечения", месяц_завершения as "Месяц истечения", Проект, Выполнено, Результат, Количество_выходящих_задач as "Количество выходящих задач"
WHERE contains(file.name, "Обдумать изучение библиотеки stdlib для c++")
limit 1
```
# [[Обдумать изучение библиотеки stdlib для c++]]



## Результат:



## Следующая задача:



## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```


---

#cpp 