---
Тип: Задача
Описание: -
Исполнители: 
Проект: ВнедрениеЗнаний

Tags: Задача
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: 02 # Не трогать!
Месяц: 05 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-05-02

дата_завершения: "2023-нет-нет"

Выполнено: нет
Результат: 
---

# [[Чтение и запись видео OpenCV]]

> [!danger] Обязательный чеклист
> - [ ] Получение одной ссылки
> - [ ] Расставить теги 


> [!example]+  Чеклист знания
> - Получение источников и ссылок
> 	- [ ] Получение ссылок на разные источники
> - Конспектирование
> 	- [ ] Достаточное для использования конспектирование
> 	- [ ] Полное конспектирование
> - Создание справок
> 	- [ ] Справки практического применения
> 	- [ ] Список терминов
> 	- [ ] Определение терминов из списка терминов
> 	- [ ] Список связанных тем(если нет внутри текста)
> - Внедрение в базу знаний
> 	- [ ] Создание связей с оригинальной страницей (этой)
> 	- [ ] Разбиение на подтемы
> 	- [ ] Атомизация - (по необходимости) разбиение конспекта по подтемам на страницы
> - [ ] Внедрение в практику
> 

<mark style="background: #FF5582A6;">**Содержание потом вырезается, все содержимое конспекта удаляется и содержание вставляется по шаблону входящего конспекта.**</mark> 

# Содержание 


# Чтение и запись видео файлов в OpenCV

## Карта конспектирования

- [ ] [[Learning-OpenCV-4-Computer-Vision-with-Python-3-3rd-Ed#page=46]]
	- [ ] Классы VideoCapture, VideoWriter
		- [ ] VideoCapture
			- [ ] read()
			- [ ] images in BGR format
- [ ] [[Learning-OpenCV-4-Computer-Vision-with-Python-3-3rd-Ed#page=47]]
	- [ ] VideoWriter
		- [ ] write()
	- [ ] <mark style="background: #FFB86CA6;">Пример чтения и записи видео</mark> 
	- [ ] Аргументы конструктора VideoWriter
- [ ] [[Learning-OpenCV-4-Computer-Vision-with-Python-3-3rd-Ed#page=48]]
	- [ ] **Захват видео с камеры**
	- [ ] <mark style="background: #FFB86CA6;">Пример завхата видео с камеры</mark>
	- [ ] Замечание о получении размеров кадра
- [ ] [[Learning-OpenCV-4-Computer-Vision-with-Python-3-3rd-Ed#page=49]]
	- [ ] VideoCapture.get()
	- [ ] Создание правильного VideoWriter
	- [ ] Количество и порядок айди камер зависит от системы и [[OpenCV]] не предоставляет методов их поиска
		- [ ] Как с этим справиться - метод VideoCapture.isOpened()
- [ ] [[Learning-OpenCV-4-Computer-Vision-with-Python-3-3rd-Ed#page=50]]
	- [ ] захват видео с многоголовочных камер(таких, как стерео камеры) - не read(), а grab() и retrieve()
		- [ ] Пример использования методов
	- [ ] **Отображение картинки в окне**
- [ ] [[Learning-OpenCV-4-Computer-Vision-with-Python-3-3rd-Ed#page=51]]
	- [ ] **Отображение камеры в окне**
- [ ] [[Learning-OpenCV-4-Computer-Vision-with-Python-3-3rd-Ed#page=53]]
	- [ ] **Проект Камео - отслеживание лиц и манипуляции над изображениями**
- [ ] [[Learning-OpenCV-4-Computer-Vision-with-Python-3-3rd-Ed#page=54]]
	- [ ] Камео - объектно ориентированный дизайн
	- [ ] **Абстракция потока видео с managers.CaptureManager**
- [ ] [[Learning-OpenCV-4-Computer-Vision-with-Python-3-3rd-Ed#page=55]]
	- [ ] Пакет time для работы со временем в Python
- [ ] [[Learning-OpenCV-4-Computer-Vision-with-Python-3-3rd-Ed#page=57]]
	- [ ] Декоратор `@property` [[Python]] - https://www.tutorialsteacher.com/python/property-decorator #ВнедрениеЗнаний/СвязатьС [[ООП в Python 3]]
	- [ ] Использование [[Docstring - документация кода в Python]]
	- [ ] Ключевле слово assert в python и [функция assert() в C++]()
- [ ] [[Learning-OpenCV-4-Computer-Vision-with-Python-3-3rd-Ed#page=60]]
	- [ ] **Абстракция окна и клавиатуры с managers.WindowManager**
- [ ] [[Learning-OpenCV-4-Computer-Vision-with-Python-3-3rd-Ed#page=61]]
	- [ ] **Применение всего с cameo.Cameo**
- [ ] [[Learning-OpenCV-4-Computer-Vision-with-Python-3-3rd-Ed#page=62]]
	- [ ] Использование оператора -> в python - [[Аннотации и метаданные в коде python]]
- [ ] [[Learning-OpenCV-4-Computer-Vision-with-Python-3-3rd-Ed#page=63]]
	- [ ] Замечание - поток видео вживую является отраженным и причина указана на Странице
- [ ] [[Learning-OpenCV-4-Computer-Vision-with-Python-3-3rd-Ed#page=64]]
	- [ ] подведение итога

## Конспект

## Смотрите также:

> [!question]- Соурслист
> 1. 

> [!quote]- Контекст
> ```dataview
> LIST 
> FROM !outgoing([[]])
> WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
> ```

---
#ВнедрениеЗнаний/НеобходимоеЗнание 
#КомпьютерноеЗрение/opencv 
#Python 

---

[^def]: термин
[^que]: вопрос