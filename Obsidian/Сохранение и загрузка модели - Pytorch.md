---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 23 # Не трогать!
Месяц: 06 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-06-23

---
# [[Сохранение и загрузка модели - Pytorch]]

## Для дальнейшей тренировки

Для того, чтобы сохранить модель, минимально необходимымм явлется сохранение словаря состояния модели и оптимизатора:

```python
def save_checkpoint(state, filename='./checkpoint.pth.tar'):
    if os.path.isfile(filename):
        os.remove(filename)
    torch.save(state, filename)


save_checkpoint({
    'epoch': 1,
    'state_dict': model.state_dict(),
    'optimizer': optimizer.state_dict()
}, save_name)
```

Для того, чтобы загрузить сохраненную модель, вам необходимо выполнить действия в следующем порядке:
1. Загрузить словарь состояний модели
2. Переопределить оптимизатор (создать новый, ***"чистый"*** объект оптимизатора)
3. Загрузить словарь состяния оптимизатора

```python
checkpoint = torch.load(args.resume)
args.start_epoch = checkpoint['epoch']
best_acc = checkpoint['best_acc']
model.load_state_dict(checkpoint['state_dict'])
optimizer.load_state_dict(checkpoint['optimizer'])
```

## Для использования

Basically, it's the same as the previous method, but you can also use another method, that is stated in the following [tutorial](https://www.learnpytorch.io/01_pytorch_workflow/#5-saving-and-loading-a-pytorch-model).


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://stackoverflow.com/questions/49371556/pytorch-how-to-save-the-previous-parameters-and-gradient-temporally-for-use-the
> 2. https://discuss.pytorch.org/t/loaded-model-doesnt-train-anymore/120302/4


---
#pytorch

---

[^def]: термин
[^que]: вопрос