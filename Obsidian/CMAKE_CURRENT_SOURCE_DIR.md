---
Тип: Задача
Описание: -
Исполнители: 
Проект: ВнедрениеЗнаний

Дисциплина: Программирование
Tags: Задача
regex-to-search: "(.*)[Cc]make(.*)" # "что-то" regex
regex-to-be-found: "(.*)[Cc][Mm][Aa][Kk][Ee](.*)[(src)(SRC)(SOURCE)(source)]" # "что-то" regex

День: 06 # Не трогать!
Месяц: 12 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Tuesday 6th December 2022 17:49:22

день_завершения: нет
месяц_завершения: нет
год_завершения: 2022
дата_завершения: 2022-нет-нет

Выполнено: нет
Результат: 
---

```dataview
TABLE день_завершения as "День истечения", месяц_завершения as "Месяц истечения", Проект, Выполнено, Результат, Количество_выходящих_задач as "Количество выходящих задач"
WHERE contains(file.name, "CMAKE_CURRENT_SOURCE_DIR - что это и для чего используется")
limit 1
```
# [[CMAKE_CURRENT_SOURCE_DIR]]

- Получение источников и ссылок
	- [x] Получение одной ссылки
	- [x] Получение ссылок на разные источники
- Конспектирование
	- [x] Достаточное для использования конспектирование
	- [ ] Полное конспектирование
- Создание справок
	- [ ] Справки практического применения
	- [ ] Список терминов
	- [ ] Список связанных тем(если нет внутри текста)
- [ ] Внедрение в практику

%%
## Результат:



## Следующая задача:

==#Входящие ==
%%

**Содержание потом вырезается, все содержимое конспекта удаляется и содержание вставляется по шаблону входящего конспекта.**

# Содержание 

**CMAKE_CURRENT_SOURCE_DIR** - абсолютный путь к директории, которая сейчас обрабатывается cmake. 

Похожим образом работает [[CMAKE_CURRENT_LIST_DIR]], а в случае обработки subdir/CMakeLists.txt внутри одной из поддиректорий при ее присоединении командой [[add_subdirectory cmake|add_subdirectory]] они даже оба указывают на один и тот же subdir/CMakeLists.txt. Различие появляется, если включение было призведено следующим образом: `include(subdir/CMakeLists.txt)`. Тогда CMAKE_CURRENT_LIST_DIR указывает на subdir/CMakeLists.txt, а CMAKE_CURRENT_SOURCE_DIR указывает на директорию самого проекта.


> [!question]- Соурслист
> 1. https://cmake.org/cmake/help/latest/variable/CMAKE_CURRENT_SOURCE_DIR.html
> 2. https://stackoverflow.com/questions/15662497/difference-between-cmake-current-source-dir-and-cmake-current-list-dir

%%
## Приложения
%%

## Смотрите также:

- [[CMAKE_BINARY_DIR]]
- [[CMAKE_SOURCE_DIR]]
- [[CMAKE_CURRENT_BINARY_DIR]]

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

---
#ВнедрениеЗнаний #Программирование/cmake/переменная 