---

Tags: 
aliases: 
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

Тип: конспект
Описание: -

День: 13 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Thursday 13th October 2022 16:52:48

---
# [[Пакеты ROS2]]

## Команды для работы с пакетами

- `ros2 pkg create --build-type ament_cmake <package_name>` - создает пакет package_name, с шаблонным CMakeLists.txt
- `ros2 pkg create --build-type ament_python <package_name>` - создает пакет package_name, с шаблонным CMakeLists.txt
- `colcon build` - собирает все пакеты в директории воркспэйса
- `colcon build --packages-select my_package` - собирает конкретно пакет `my_package`

> [!seealso] CMake
> Вообще говоря, для сборки проекта на ROS2 используются [[CMake]] и [[Colcon]] и было бы полезно понимать, как они работают.

##  Пререквизиты

### Установка colcon

```shell
sudo apt install python3-colcon-common-extensions
```

## Основы

#### Создание пакета

В директории воркспэйса:

```shell
ros2 pkg create
```

 #### Сборка пакета

```shell
colcon build 
```

Собирает все пакеты воркспэйса.

### Tips

-   If you do not want to build a specific package place an empty file named `COLCON_IGNORE` in the directory and it will not be indexed.
-   If you want to avoid configuring and building tests in CMake packages you can pass: `--cmake-args -DBUILD_TESTING=0`.
-   If you want to run a single particular test from a package:
	```shell
	colcon test --packages-select YOUR_PKG_NAME --ctest-args -R YOUR_TEST_IN_PKG
	```
## Смотрите также:

- [Colcon и его настройка](https://docs.ros.org/en/foxy/Tutorials/Beginner-Client-Libraries/Colcon-Tutorial.html)

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

---
#Робототехника/ROS2/Пакеты 