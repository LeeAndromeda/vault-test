---

Tags: 
aliases: 
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

Тип: конспект
Описание: -

День: 12 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Wednesday 12th October 2022 17:25:50

---
# [[Перенастройка топика на другой объект ROS2]]

```shell
ros2 run turtlesim turtlesim_node --ros-args --remap __node:=my_turtle
```

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

---
#Робототехника/ROS2
