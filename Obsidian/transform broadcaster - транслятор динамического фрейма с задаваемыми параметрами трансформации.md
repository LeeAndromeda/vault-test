# transform broadcaster - транслятор динамического фрейма с задаваемыми параметрами трансформации

Транслятор динамических трансформаций нужен, чтобы создавать новые фреймы и привязывать их к другим фреймам, при этом то, **как** вы привязываете новый фрейм, вы можете описать, как угодно. Это позволяет создавать сложные связанные системы координат и переводить информацию из одной в другую без выполнения лишней сложной для понимания работы по переводу вручную.

## Особенности

В передаче трансформаций используется тип данных ROS [[transformStamped]]. 

## Пояснение

```cpp
/*
Все, что касается черепашки и действий с ней, не связанных с tf2, будет помечаться меткой "черепашка".
*/

#include <ros/ros.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_ros/transform_broadcaster.h>
#include <geometry_msgs/TransformStamped.h>
#include <turtlesim/Pose.h>

std::string turtle_name; // черепашка

void poseCallback(const turtlesim::PoseConstPtr& msg) // Колбэк получает из топика turtle_name+"/pose" сообщение позиции, определенное в библиотеке turtlesim. turtle_name - параметр имени конкретной черепашки, который позволяет вазимодействовать не только с первой созданной черепашкой, но и с последующими
{
  static tf2_ros::TransformBroadcaster br; // Создание объекта транслятора трансформаций
  geometry_msgs::TransformStamped transformStamped; // Создание объекта трансформации
  
  transformStamped.header.stamp = ros::Time::now();
  transformStamped.header.frame_id = "world"; // задается фрейм-отец
  transformStamped.child_frame_id = turtle_name; // задается фрейм-ребенок
  transformStamped.transform.translation.x = msg->x; // Задаются координаты смещения нового фрейма относительно фрейма-отца
  transformStamped.transform.translation.y = msg->y;
  transformStamped.transform.translation.z = 0.0;
  tf2::Quaternion q; // Задается поворот(в кватернионе) фрейма-ребенка относительно фрейма-отца
  q.setRPY(0, 0, msg->theta);
  transformStamped.transform.rotation.x = q.x();
  transformStamped.transform.rotation.y = q.y();
  transformStamped.transform.rotation.z = q.z();
  transformStamped.transform.rotation.w = q.w();
  br.sendTransform(transformStamped); // Трансформация отправляется
}
int main(int argc, char** argv){
  ros::init(argc, argv, "my_tf2_broadcaster");
  ros::NodeHandle private_node("~");// черепашка
  if (! private_node.hasParam("turtle"))// черепашка
  {
    if (argc != 2){ROS_ERROR("need turtle name as argument"); return -1;};// черепашка
    turtle_name = argv[1];// черепашка
  }
  else// черепашка
  {
    private_node.getParam("turtle", turtle_name);// черепашка
  }
    
  ros::NodeHandle node;
  ros::Subscriber sub = node.subscribe(turtle_name+"/pose", 10, &poseCallback); // Сабскрайбер на позицию черепашки
  ros::spin();
  return 0;
};
```

В итоге получается нод, котораый при получении нового сообщения о позиции черепашки перемещает(обновляет смещение) фрейм-ребенок относительно фрейма отца в положение черепашки.

## Теория - как работает трансформация

Трансформация  - система координат, заданная относительно другой системы координат с указанным смещеним. [[transformStamped|Сообщение трансформации]] содержит в себе поля, отвечающие за определение этого смещения.

1. Смещение по координатам указывается в формате x, y, z координат по осям родительской системы
2. Смещение по углам указывается в формате кватерниона x, y, z, w и определяет вращение относительно родительской системы координат.

## Инструкция - как создать свой транслятор динамических трансформаций

1. Определи для себя, как именно и к чему ты будешь привязывать свой фрейм-ребенок
	- Допустим, что необходим сабскрайбер и какие-то операции с пойманным сообщением
2. Создай сабскрайбер на необходимый топик и получай из него информацию(сохраняй её)
3. Пользуйся колбэком(или нет, но в моем описании рассматривается вариант пересчета смещения фрейма-ребенка по мере изменения данных из пойманного сообщения)
	1. Обрабатывай полученную информацию
	2. Создай объекты транслятора и сообщения трансформации
	3. Передай обработанную информацию в сообщение и отправляй её транслятором

#tf2  #Робототехника/ROS #Робототехника 