---
Тип: ДЗ
Дисциплина: Термех
Описание:
  - 
aliases: 
regex-to-search: 
regex-to-be-found: NoRgxTBFKey
День: 20
Месяц: 1
Год: 2023
дата_создания: 2023-01-20
дата_завершения: 2023-04-30
Выполнено: да
Результат: 
tags:
  - ДомашнееЗадание
---

# [[Подготовить презентацию по патенту для термеха]]

[[Патент по термеху]]
[[Троссовый привод на скрученных нитях]]
[[2.ElbowExo.pdf]]
## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
---
#Термех 

---