---
Тип: конспект
Описание: -
День: 21
Месяц: 07
Год: 2022
---
# Работа с Godot

```ad-info
title: Следующий туториал
collapse: closed

https://docs.godotengine.org/en/stable/getting_started/step_by_step/scripting_player_input.html

```


## Теория

- [Как стоит структурировать свою игру в Godot](https://docs.godotengine.org/en/stable/getting_started/step_by_step/instancing.html)
- [[Ноды в Godot]]

## Хоткеи

- **F5** - Запускает игру (открывает окно выбора main сцены при первом запуске)
- **F8** - Закрывает окно 
- **Ctrl + D** - создает новый инстанс выделенной сцены

## Встроенные типы нодов

- **Label** - выводит текст

## Подсферы деятельности


- [[Программирование на GDscript]]

#Godot #Программирование #РазработкаИндиИгр 