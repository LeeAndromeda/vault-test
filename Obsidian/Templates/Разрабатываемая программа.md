


---
Тип: Задача
Описание: "Разработка программы <% tp.file.title %>"
Исполнители: 
Проект: #Входящие 

Tags: Задача
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: <% tp.date.now("DD", 0) %> # Не трогать!
Месяц: <% tp.date.now("MM", 0) %> # Не трогать!
Год: <% tp.date.now("YYYY", 0) %> # Не трогать!
дата_создания: <% tp.file.last_modified_date("YYYY-MM-DD") %>

день_завершения: <% tp.system.prompt("День завершения --->") %>
месяц_завершения: <% tp.system.prompt("Месяц завершения --->") %>
год_завершения: <% tp.date.now("YYYY", 0) %>
дата_завершения: <% tp.frontmatter["год_завершения"] %>-<% tp.frontmatter["месяц_завершения"] %>-<% tp.frontmatter["день_завершения"] %>

Статус: Входящий # [Входящий, Очередь, Дизайн, Разработка, Выполнено, Проверка, Доработка] 
Результат: 
---

```dataview
TABLE день_завершения as "День истечения", месяц_завершения as "Месяц истечения", Выполнено, Результат, Количество_выходящих_задач as "Количество выходящих задач"
WHERE contains(file.name, "<% tp.file.title %>")
limit 1
```
# [[<% tp.file.title %>]]

#Входящие 

%%

## Результат:


## Следующая задача:

#Входящие 
%%

%%
## Приложения
%%


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```


---
#Входящие 

---