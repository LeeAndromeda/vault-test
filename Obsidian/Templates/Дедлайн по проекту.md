---
Тип: Задача
Описание: ДЕДЛАЙН
Исполнители: 
Проект: #Входящие 

Дисциплина: #Входящие 
Tags: Дедлайн, Задачи
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: <% tp.date.now("DD", 0) %> # Не трогать!
Месяц: <% tp.date.now("MM", 0) %> # Не трогать!
Год: <% tp.date.now("YYYY", 0) %> # Не трогать!
дата_создания: <% tp.date.now("YYYY", 0) %>-<% tp.date.now("MM", 0) %>-<% tp.date.now("DD", 0) %>

дата_завершения: "<% tp.date.now("YYYY", 0) %>-<% tp.system.prompt("Месяц завершения --->") %>-<% tp.system.prompt("День завершения --->") %>"

Выполнено: нет
---

```dataview
TABLE день_завершения as "День истечения", месяц_завершения as "Месяц истечения", Выполнено, Результат, Количество_выходящих_задач as "Количество выходящих задач"
WHERE Дата_создания = this.Дата_создания
limit 1
```
# [[<% tp.file.title %>]]
📅
#Входящие 

%%

## Результат:

%%

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```


---
#Входящие 

---