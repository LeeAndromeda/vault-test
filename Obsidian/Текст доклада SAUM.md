


---

Tags: 
aliases: 
regex-to-search: "((.*)[Тт]екст.*)|((.*)[Дд]оклад.*)|((.*)SAUM)"
regex-to-be-found: "NoRgxTBFKey"

Тип: конспект
Описание: -

День: 02 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Sunday 2nd October 2022 11:08:38

---

%%

# [[Текст доклада SAUM]]

## Чек лист хорошего доклада
- [ ] Научная проблема, цель и задачи исследования, методы;
- [ ] Этапы и ход исследования – как была доказана поставленная в начале гипотеза, удалось ли решить проблему;
- [ ] Научная новизна работы и ценность исследования, личный вклад докладчика и, конечно, итоги.

## Текст

%%

### ***Титульный слайд***

*Приветствие*

Авторы: A.V. Leshchev-Romanenko and S. *Kh.* Saidov 
Тема: **Motion planning of a wheeled robot in a flat dynamic uncertain environment using  hybrid motion planning algorithm** (based on A* and Artificial Potential Field method)

## NEXT

*Актуальность*
The problem of motion planning is a very relevant and complex problem, despite the fact that much attention has been paid to it in recent years. It finds plenty of practical implementations and gives various challenges for people to think about. 

## NEXT

There are various path planning algorithms that solve the problem of motion planning, for example, A\*, D\*, rapidly-exploring random trees(RRT), probabilistic roadmap (PRM), artificial potential fields, unstable modes and others. 

The listed algorithms are best suited for <mark style="background: #D2B3FFA6;">different</mark>(specialised???) problems. Some are effective in navigating through a known environment, others can cope with dynamically changing environment. This fact leads to a conclusion, that a combination of path plannning algorithms can solve different tasks more effectively. Thats why there exist hybrid path planning algorithms.

A hybrid algorithm is an algorithm that combines two or more different algorithms to improve the efficiency of solving the problem. The paper considers a combination of A* algorithm and Artificial Potential Fields method.

## NEXT

### ***Теоретическая часть***

%%*(*А стар - эвристический алгоритм для такой такой среды.)*%% A\* is a heuristic planning algorithm. It begins with the neighbours of the starting node, evaluating the cost ov moving to them and a heuristic, which in this case is a straight distance from the reviewed node to the end node. Then it continues evaluating nodes with the least function value until it gets to the final node. It is best suited for navigating in known environment and is not meant to cope with dynamic obstacles.%%???Его минусы и плюсы???%% %%*(Потенциальные поля - алгоритм для того, того - )*%% 

## NEXT

The method of artificial potential fields uses the distance to the obstacles to calculate the repulsive force and the goal point to calculate the attraction force. It sums the froces and the resultant force is then used to move safely towards the goal point. It is best suited for real-time obstacle avoidance and is not used for global navigation.  So, how are they combined?%%минусы и плюсы???%% %%**Формулы** *%%

## NEXT


### ***Практическая часть*** (пересказ разделов III и IV)

%%*Гибридизация*%%

In general, the algorithm is like shown on the diagram.

First, the robot doesn't now anything about the environment but uses senseors to obtain information about it. In the current work only lidar sensor is used. This process is continuous and is going through the whole process of navigation. Then, using the gathered information the robot uses SLAM method to map the environment and localise itself in it. It is also used to check if robot has hit the local minimum. Using the map, the A\* planner builds or rebuilds the trajectory to the goal point when it is called. The calculated trajectory is point by point sent to the artificial potencial fields as sub-goal points for attraction. Environmental information is also used in it for calculating repulsive force from the obstacles.

To use A\* in an unknown environment we have to make some presumptions about the environment. 

## NEXT

The map that is built by SLAM is called occupation grid and has three types of cells as shown in the picture. In this work it is supposed that unknown cells are free and trajectory can be build through them.

## NEXT

For understanding if the robot is in a local minimum, two types of criteria is used:

1. The total robot velocity at each moment of time
2. The position of the robot over a certain period of
time

![[Pasted image 20221111144931.png]]
![[Pasted image 20221111144944.png]]

So we can say that the robot will move, find itself in a local minimum and replan trajectory until it gets to the final goal point.

## NEXT

### Эксперименты

The implementation of the hybrid algorithm is based on the ROS framework and includes custom packages: **apf_la**(artificial potential fields), **astar_planner**(A*), **object_mover_gazebo**(package for moving obstacles in the gazebo simulator), **tb3_custom_worlds**(includes created scenes and object_mover_gazebo package); and the **slam_gmapping** package taken "as is". The robot model that is used is a model of Turtlebot 3 Waffle Pi robot for the Gazebo simulator.

~~Сюда так же еще записать,какую задачу я поставил роботу для прохождения~~ During the experiments the robot had to navigate from the starting point to the end point using the data from lidar. To conduct the experiments three scenes were constructed. The first scene aims to show the basic principle of the hybrid planning algorithm. The second one aims to show how a hybrid planning algorithm works in an environment that requires replanning and mapping along the way. On each scene the obstacles are presented with cylinders that have diameter of a meter. The moving obstacles are presented with cylinders with the diameter of a meter on the first two scenes and three meters on the third scene. The obstacles are moving no faster then the maximum speed of Turtlebot 3.

## NEXT

Ten experiments were conducted on each scene. The proposed algorithm showed its performance and efficiency in solving the problem. The standard deviation and average completion time for each scene are shown on the slide.

## NEXT

## Problems of the algorithm

The most impactful problem was the localisation error when SLAM had to cope with the moving obstacles. There were some tests where the robot got stuck because of this error but they were not included as it is a problem of the SLAM algorithm and should be considered separately.

The other problem was the blockage between a moving and a stationary obstacle. This situation is illustrated at the slide. An obstacle is moving towards the wall and the robot is situated between them. As the moving obstacle moves closer to the robot, the repulsive force of the potential field moves it away so the criteria for replanning the trajectory are not satisfied. And as the obstacles are much bigger than the robot, the repulsive force does not allow it to get out of the blockage.

To solve this problem in future, it is intended to use an improved potential-field algorithm or another algorithm capable of unblocking the robot in the corresponding situation.

## NEXT

## Conlusion

The algorithm solves the problem of motion planning in a flat dynamic uncertain environment with obstacles moving no faster than the mobile robot successfully. It is best suitable for navigating a mobile robot in buildings and other flat environments but applications can vary depending on the end goal.

%%

- ***проблемы алгоритма***
	- ***упомянуть слам***
	- ***блокирование***

***ВЫводы о решении задачи***

- *~~структурная схема алгоритма и общий способ работы~~*
- *~~презумпция пустоты~~*
- *~~критерии пересчета~~*
- *эксперименты*
	- *пакеты РОС*
	- *сцены и препятствия*
	- *задача прохождения*
	- *изучаемые параметры*

***Выводы***

- *выводы о решении задачи планирования движения в ... среде ...*
- *перспективы развития алгоритма*

***Финальный слайд***

## Смотрите также:

```dataview
LIST
FROM #ТехническиеНауки/АкадемическоеПисьмо
WHERE regexmatch(this.regex-to-search, file.name)
```
```dataview
LIST
FROM "Статья на конференцию/Drafts/Draft 1"
SORT file.name ASC
```

#SAUM

%%