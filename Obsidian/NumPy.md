---
Тип: Задача
Описание: -
Исполнители: 
Проект: ВнедрениеЗнаний

Tags: Задача
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: 11 # Не трогать!
Месяц: 03 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-03-11

дата_завершения: "2023-нет-нет"

Выполнено: нет
Результат: 
---

# [[NumPy]]

> [!danger] Обязательный чеклист
> - [x] Получение одной ссылки
> - [x] Расставить теги 


> [!example]+  Чеклист знания
> - Получение источников и ссылок
> 	- [ ] Получение ссылок на разные источники
> - Конспектирование
> 	- [ ] Достаточное для использования конспектирование
> 	- [ ] Полное конспектирование
> - Создание справок
> 	- [ ] Справки практического применения
> 	- [ ] Список терминов
> 	- [ ] Определение терминов из списка терминов
> 	- [ ] Список связанных тем(если нет внутри текста)
> - Внедрение в базу знаний
> 	- [ ] Создание связей с оригинальной страницей (этой)
> 	- [ ] Разбиение на подтемы
> 	- [ ] Атомизация - (по необходимости) разбиение конспекта по подтемам на страницы
> - [ ] Внедрение в практику
> 

<mark style="background: #FF5582A6;">**Содержание потом вырезается, все содержимое конспекта удаляется и содержание вставляется по шаблону входящего конспекта.**</mark> 

# Содержание 

## Матрицы

- [[ndarray NumPy - общая теория]]
- [[Как создать массив в numpy]]
- [[Выделение части массива в numpy - слайсы]]

- [[Типы данных в NumPy]]
- [[Чтение из файлов в NumPy]]

## Работа с формой массива


-   `a.flatten()` — превращает массив в одномерный.  
-   `a.T` или `a.transpose(*axes)` — транспонирование (или смена порядка осей в случае, когда размерность массива больше двух).  
-   `a.reshape(shape)` — смена формы массива. Массив "распрямляется" и построчно заполняется в новую форму.


- [[flatten() - numpy - превращение массива в вектор]]
- [[mat.T, mat.transpose() - numpy - транспонирование матриц]]
- [[reshape() - numpy - изменение формы массива]]

---

- [[mat.ndim - numpy - количество измерений массива]]
- [[mat.shape - numpy - форма массива]]

## Основные методы

- [[a.min(), a.max(), a.mean(), a.std(), a.argmin(), a.argmax() - numpy]]
- [[a.sum(), a.prod() - numpy]]
- [[a.cumsum(), a.cumprod() - numpy]]
- [[numpy.median() - медиана массива]]
- [[numpy.std() - станадартное отклонение]]
- [[Маски (выбор из массива по условию) в numpy]]

---

### Статистика

- [[mat.sort() - сортировка массива в numpy]]
- [[mat.fill() - заполнение  массива значением в numpy]]
- [[mat.copy() - копирование значений массива в другой массив в numpy]]
- [[np.precentile и np.nanpercentile, np.quantile и np.nanquantile]]
- [[numpy.var - дисперсия в массиве]]
- [[numpy.corrcoef - коэффициент линейной корреляции двух переменных]]
- [[numpy.correlation - коэффициент кросс-корреляции (линейной) между двумя временными рядами]]
- [[numpy.cov - ковариация двух рядов]]
- [[Коэффициент Спирмана - реализация в numpy]]
- [[Тау Кэнделла - коэффициент порядковой ассоциации - реализация в numpy]]

### Линейная алгебра

- [[Матричное произведение, степень - numpy]]
- [[linalg.norm(a, ord=None) - норма - numpy]]
- [[Обратная и псевдообратная матрицы - np.linalg.inv() и np.linalg.pinv()]]
- [[Поиск коэффициентов полинома методом наименьших квадратов - numpy.polyfit()]]

### Преобразование типов

- [[mat.tolist() - numpy array в list python]]

## Важные методы NumPy

### Работа с внешними типами данных

- [[NumPy fromiter]]

## Входящие

- [[Слайсинг массивов NumPy]]
- [[numpy.fromfunction()]]

## Литература

- [[numpy-user-guide]]

## Смотрите также:

- [[Машинное обучение]]

> [!question]- Соурслист
> 1. https://stepik.org/lesson/16462/step/1?unit=4283

> [!quote]- Контекст
> ```dataview
> LIST 
> FROM !outgoing([[]])
> WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
> ```

---
#ВнедрениеЗнаний 
#Python/NumPy

---

[^def]: термин
[^que]: вопрос