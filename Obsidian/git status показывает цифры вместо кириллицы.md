# git status показывает цифры вместо кириллицы

```bash
git config --global core.quotepath off
```

## Решение:

- [Решение](https://stackoverflow.com/questions/22827239/how-to-make-git-properly-display-utf-8-encoded-pathnames-in-the-console-window/22828826#22828826)

#git #Debug 