---

Tags: 
aliases: 
regex-to-search: "(.*)[Уу]слов(.*)[Вы]ыраж(.*)"
regex-to-be-found: "((.*)[Уу]слов(.*)[Вы]ыраж(.*))|((.*)[Pp]ython(.*))"

Тип: конспект
Описание: -

День: 20 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Thursday 20th October 2022 11:47:13

---
# [[Условные выражения в Python]]

## Операторы сравнения

```Python
\
== # проверка на равенство
!= # проверка на неравенство
<= # меньше или равно
>= # больше или равно
>  # больше
<  # меньше
```

## Логические операторы

```Python
not # 1
and # 2
or  # 3
```

## if elif

```Python
if (statement) or (statement) and (statement):
	do_something()
elif (statement) or (statement) and (statement):
	do_something()
else:
	do_something_else()
```

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

---
#Python 