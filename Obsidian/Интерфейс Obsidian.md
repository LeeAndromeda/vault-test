---

Tags: 
aliases: 
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

Тип: конспект
Описание: -

День: 03 # Не трогать!
Месяц: 12 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Saturday 3rd December 2022 02:46:18

---
# [[Интерфейс Obsidian]]

Интерфейс обсидиана довольно прост и особой письменной документации не требует. Хорошее его описание содержится в видео, указанном в соурслисте.

%%## Приложения%%
%%

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
%%
> [!question]- Соурслист
> 1. https://www.youtube.com/watch?v=ye9YMLQ8hY0


---
#туториал #obsidian 