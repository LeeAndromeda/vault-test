


---
Тип: Проект 
название_проекта: "(.*)[Pp]ytorch(.*)(.*)[Zz][Tt][Mm](.*)"
Проект: Pytorch
Описание: -
Дисциплина: #Входящие 
Tags: Проект
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

alias: [ZTM]

День: 22 # Не трогать!
Месяц: 06 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-06-22
Дата_создания: 2023-06-23, 21-55

дата_завершения: "2023-06-10"

Выполнено: нет
Результат: 
---

> [!caution] Это текущая задача
> Данная задача была помечена текущей. Если она таковой быть перестает, удалите полностью данный спойлер.
> Выберите тип задачи и удалите ненужный:
> 
> #Задача/РаботаИдет
> 
> ### Прогресс:: 0%


# [[Pytorch - Zero To Mastery course]]

> [!abstract]- Материалы
> 
> ```dataview
> LIST
> FROM (#ВнедрениеЗнаний/РасширяющееЗнание or #ВнедрениеЗнаний/РасширяющееЗнание/Тропинка) and #tag
> ```

> [!example]+ Подпроекты
> ```dataview
> TABLE Описание, день_завершения as "День завершения", месяц_завершения as "Месяц завершения", Исполнители, Выполнено
> FROM #Проект and !"Templates"
> WHERE Тип = "Проект" and regexmatch(this.название_проекта, Проект) and Выполнено = "нет"
> SORT год_завершения ASC, месяц_завершения ASC, день_завершения ASC
> ```

> [!hint]+ Задачи
> 
> > [!hint]+ Текущие по проекту
> > ```dataview
> > TABLE Описание, дата_завершения as "Дата завершения", Исполнители, Выполнено
> > FROM #Задача/РаботаИдет and !"Templates"
> > WHERE regexmatch(this.название_проекта, Проект) and Выполнено = "нет"
> > SORT дата_завершения ASC
> >```
>
> > [!example]- В очереди по проекту
> > ```dataview
> > TABLE Описание, дата_завершения as "Дата завершения", Исполнители, Выполнено
> > FROM #Задача/вОчереди and !"Templates"
> > WHERE regexmatch(this.название_проекта, Проект) and Выполнено = "нет"
> > SORT дата_завершения ASC
> >```
> 
> > [!faq]+ Ансорт
> > ```dataview
> > TABLE Описание, дата_завершения as "Дата завершения", Исполнители, Выполнено
> > FROM #Задача and !"Templates"
> > WHERE regexmatch(this.название_проекта, Проект) and Выполнено = "нет"
> > SORT дата_завершения ASC
> >```
> 
> > [!success]- Выполненные
> > ```dataview
> > TABLE Описание, дата_завершения as "Дата завершения", Исполнители, Выполнено
> > FROM #Задача  and !"Templates"
> > WHERE regexmatch(this.название_проекта, Проект) and Выполнено = "да"
> > SORT дата_завершения ASC
> >```



## Участники проекта

```dataview
LIST
FROM !"Obsidian/Templates" and !"Templates" and #Человек 
WHERE regexmatch(this.название_проекта, Проект) or regexmatch(Проект, this.название_проекта)
```

## Смотрите также

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
---

#pytorch #Задача 

---

