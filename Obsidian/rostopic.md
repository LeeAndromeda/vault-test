# rostopic
```
$ rostopic -h

```

```
rostopic bw     display bandwidth used by topic
rostopic echo   print messages to screen
rostopic hz     display publishing rate of topic    
rostopic list   print information about active topics
rostopic pub    publish data to topic
rostopic type <<< topic >>>  print topic type
```

## Разбор комманд

### list -v

Показывает список топиков с количеством паблишеров и сабскрайберов.

```
Published topics:
 * /turtle1/color_sensor [turtlesim/Color] 1 publisher
 * /turtle1/cmd_vel [geometry_msgs/Twist] 1 publisher
 * /rosout [rosgraph_msgs/Log] 2 publishers
 * /rosout_agg [rosgraph_msgs/Log] 1 publisher
 * /turtle1/pose [turtlesim/Pose] 1 publisher


Subscribed topics:
 * /turtle1/cmd_vel [geometry_msgs/Twist] 1 subscriber
 * /rosout [rosgraph_msgs/Log] 1 subscriber
```

### rostopic pub
Позволяет публиковать сообщения в топик:

```
$ rostopic pub [topic] [msg_type] [args]
```

Для ROS Hydro и более поздних:

Эта команда будет публиковать сообщения в указанный топик:

```rostopic pub```

Эта опция заставляет rostopic публиковать только одно сообщение и выходить:

 ```-1 ```
 
Это имя топика, в который идёт публикация:

```/turtle1/cmd_vel```

Это тип сообщений, публикуемый в топиу:

```geometry_msgs/Twist```

This option (double-dash) tells the option parser that none of the following arguments is an option. This is required in cases where your arguments have a leading dash -, like negative numbers. Эта опция говорит парсеру [^1], что ни один из следующих аргументов не является опцией. Это необходимо, если у ваших аргументов есть "\-", как у отрицательных чисел:

```--```

Как указано ранее, geometry_msgs/Twist сообщения имеют два вектора, состоящие из трёх переменных с плавающей точкой каждый: линейный и угловой. В этом случае '[2.0, 0.0, 0.0]' становится линейным, где x=2.0, y=0.0 и z=0.0, а '[0.0, 0.0, 1.8]' становится угловым, где y=0.0, и z=1.8. эти аргументы на самом деле принадлежат синтаксу YAML, описанному более в YAML документации командной строки.

```'[2.0, 0.0, 0.0]' '[0.0, 0.0, 1.8]' ```

#Робототехника/ROS

[^1]: **Парсер** - синтаксический анализатор. Другие формы: парсеру, парсера, парсере.