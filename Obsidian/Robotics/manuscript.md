# Оригинальный маркдаун инструкции или СОДЕРЖАНИЕ

![[Настройка ROS для запуска]]

# В новом воркспэйсе

## 1. Создай новый воркспэйс

### Как создать

Создадим рабочее пространство:

```linux

$ mkdir -p ~/catkin_ws/src
$ cd ~/catkin_ws/
$ catkin_make

```

Дополнительную информацию по catkin_make смотри здесь --> http://wiki.ros.org/catkin/Tutorials/create_a_workspace.

### Как засоурсить

Сначала компилируешь пакеты(если необходимо):

```
*In a catkin workspace*

$ catkin_make [make_targets] [-DCMAKE_VARIABLES=...]
```

Приведенная команда скомпилирует все пакеты, лежащие в текущем рабочем пространстве.

Затем:

```
cd ~/your_ws
source devel/setup.bash
```

> Если не хочешь каждый раз соурсить воркспэйс, то добавь в ~/.bashrc в конец следующие строки:
> ```
> source ~/<твой_воркспэйс>/devel/setup.bash
> ```
> На будущее: **Если у тебя несколько воркспэйсов, то проверь их на наличее конфликтов имен файлов кода(\*.cpp, \*.py и т.д.)**. Если конфликты имеются, то либо убирай, либо ты можешь прописать так только один воркспэйс.

## 2. Создай пакет

### Как создать 

**catkin_create_pkg <package_name> [depend1] [dependN]** - создаёт пакет catkin с указанными зависимостями.

1. Вы должны создать рабочее пространство [^1]
2. Запустить указанную команду
3. Затем в вашей рабочей среде запустить команду [[catkin_make]]

#### Первоочередные зависимости

В процессе запуска команды catkin_create_pkg были указаны первоочередные зависимости. Их можно посмотреть при помощи команды rospack:

```linux
$ rospack depends1 beginner_tutorials
```

#### Косвенные зависимости

```
$ rospack depends1 rospy
```

Покажет зависимости пакета rospy. 

#### Отстройка пакета

##### package.xml

Текстовый документ, можно открыть gedit, в котором информация идёт в следующем порядке:
1. Название
2. Версия
3. Описание
4. Разработчик
5. Лицензия[^2]
6. Ссылки 
7. Зависимости[^3]

Пункты с первого по пятый являются необходимым минимумом, чтобы пакет считался завершенным.

##### CMakeLists.txt

### Компиляция пакета catkin

Перед продолжением не забудь засоурсить свой setup файл, если ещё не сделал этого:

```
$ source /opt/ros/noetic/setup.bash
```

Использование:

```
*In a catkin workspace*

$ catkin_make [make_targets] [-DCMAKE_VARIABLES=...]
```

Приведенная команда скомпилирует все пакеты, лежащие в текущем рабочем пространстве.

> Вместо нее рекомендуется использовать **catkin build**.


# В существующем воркспэйсе

## 1. Засоурсь setup.bash рабочего пространства, в котором будешь работать

```
cd ~/your_ws
source devel/setup.bash
```

Или настрой скрипт для ~/.bashrc.

## 2. Подготовь msg и srv файлы

### Настройка пакета для работы с сообщениями

Нам нужно убедиться, что наши msg файлы превращаются в код для C++, питона и других языков:

#### 1.

Откройте package.xml и убедитесь, что там есть две строки и они незакомментированы:

```
  <build_depend>message_generation</build_depend>
  <exec_depend>message_runtime</exec_depend>
```

Заметь, что во время компиляции нам нужен "message_generation", а во время запуска нам нужен только "message_runtime".

#### 2.

Open **CMakeLists.txt** in your favorite text editor ([rosed](http://wiki.ros.org/ROS/Tutorials/UsingRosEd) from the previous tutorial is a good option).

Добавь зависимость **message_generation** в**find_package** call, который уже существует в вашем CMakeLists.txt так, что вы сможете генерировать сообщения. ВЫ можете сделать это просто добавив **message_generation** в **list of COMPONENTS** так, что это будет выглядеть так:

```
# НЕ ВСТАВЛЯТЬ БЕЗДУМНО, ОТРЕДАКТИРОВАТЬ УЖЕ СУЩЕСТВУЮЩЕЕ
find_package(catkin REQUIRED COMPONENTS
   roscpp
   rospy
   std_msgs
   message_generation
)
```

> You may notice that sometimes your project builds fine even if you did not call find_package with all dependencies. This is because catkin combines all your projects into one, so if an earlier project calls find_package, yours is configured with the same values. But forgetting the call means your project can easily break when built in isolation.

#### 3.

Убедитесь, что вы экспортируете message runtime dependency.

```
catkin_package(
  ...
  CATKIN_DEPENDS message_runtime ...
  ...)
```

#### 4.

Науди следующий блок кода:

```
# add_message_files(
#   FILES
#   Message1.msg
#   Message2.msg
# )
```

Раскомментируй и затем замени Message*.msg своими:

```
add_message_files(
  FILES
  Num.msg
)
```

Добавляя .msg файлы вручную, мы убеждаемся, что CMake знает, когда ему нужно перенастроить проект, после того, как вы добавили новые .msg файлы.

#### 5.

Теперь нам нужно убедиться, что generate_messages() вызывается.

_Для ROS Hydro и позже_ вам нужно раскомментировать следующие строки:

```
# generate_messages(
#   DEPENDENCIES
#   std_msgs
# )
```

-   так что это выглядит так:

```
    generate_messages(
      DEPENDENCIES
      std_msgs
    )
```

### Общий шаг для msg и srv

![[Общий шаг для msg и srv]]


### Настройка srv файлов

#### 1.  Если вы еще не сделали этого, откройте файл package.xml и убедитесь, что эти две строки в нем не закомментированы:

```
  <build_depend>message_generation</build_depend>
  <exec_depend>message_runtime</exec_depend>
```

#### 2. Если вы еще не сделали этого для сообщений в предыдущем шаге, добавьте зависимость message_generation для генерации сообщений в CMakeLists.txt:
```
# Не просто добавьте эту строку в ваш CMakeLists.txt, измените существующую строку
find_package(catkin REQUIRED COMPONENTS
  roscpp
  rospy
  std_msgs
  генерация_сообщений
)
```
(Несмотря на свое название, message_generation работает как для msg, так и для srv.)

#### 3. Удалите #, чтобы откомментировать следующие строки:
```
# add_service_files(
# FILES
# Service1.srv
# Service2.srv
# )
```
#### 4. И замените заглушку Service*.srv на ваши служебные файлы:
```
add_service_files(
  FILES
  AddTwoInts.srv
)
```

### Общий шаг для msg и srv

![[Общий шаг для msg и srv]]

## 3. Настройка CMakeLists.txt после создания нодов

### Настройка CMakeLists.txt после создания нодов паблишера и подписчика

В конец файла добавляются следующие строчки: 

```
add_executable(talker src/talker.cpp)
target_link_libraries(talker ${catkin_LIBRARIES})
add_dependencies(talker beginner_tutorials_generate_messages_cpp)

add_executable(listener src/listener.cpp)
target_link_libraries(listener ${catkin_LIBRARIES})
add_dependencies(listener beginner_tutorials_generate_messages_cpp)
```

 **Или в общем виде**:
 
```
add_executable(<<< executableName >>> src/<executableName>.cpp)
target_link_libraries(<<< executableName >>> ${catkin_LIBRARIES})
add_dependencies(<<< executableName >>> <package_name>_generate_messages_cpp)
```

Это создаст два исполняемых файла, **talker** и **listener**, которые по умолчанию будут находиться в каталоге package вашего пространства devel, расположенном по умолчанию по адресу ~/catkin_ws/devel/lib/<имя пакета>.

*Обратите внимание, что вы должны добавить зависимости для исполняемых целей к целям генерации сообщений:*

``` 
add_dependencies(talker beginner_tutorials_generate_messages_cpp)
```
	
Это гарантирует, что заголовки сообщений этого пакета будут сгенерированы перед использованием. **Если вы используете сообщения из других пакетов в рабочем пространстве catkin, вам необходимо добавить зависимости к соответствующим целям генерации, поскольку catkin собирает все проекты параллельно**.

> Примечание: Если вы добавляете, как новый pkg, вам может потребоваться указать catkin на принудительное создание с помощью опции --force-cmake. Смотрите [catkin/Tutorials/using_a_workspace#With_catkin_make](http://wiki.ros.org/catkin/Tutorials/using_a_workspace#With_catkin_make). 

### Настройка CMakeLists.txt для сервера и клиента

> ### В общем виде:

```cpp
add_executable(<<< executive_name >>> src/<executive_name>.cpp)
target_link_libraries(<<< executive_name >>> ${catkin_LIBRARIES})
add_dependencies(<<< executive_name >>> <package_name>_gencpp)
```

Снова отредактируйте файл beginner_tutorials CMakeLists.txt, расположенный по адресу ~/catkin_ws/src/beginner_tutorials/CMakeLists.txt, и добавьте следующее в конце: 

```cpp
add_executable(add_two_ints_server src/add_two_ints_server.cpp)
target_link_libraries(add_two_ints_server ${catkin_LIBRARIES})
add_dependencies(add_two_ints_server beginner_tutorials_gencpp)

add_executable(add_two_ints_client src/add_two_ints_client.cpp)
target_link_libraries(add_two_ints_client ${catkin_LIBRARIES})
add_dependencies(add_two_ints_client beginner_tutorials_gencpp)
```

Это создаст два исполняемых файла, add_two_ints_server и add_two_ints_client, которые по умолчанию будут помещены в каталог package вашего пространства devel, расположенный по умолчанию по адресу ~/catkin_ws/devel/lib/<имя пакета>. Вы можете вызывать исполняемые файлы напрямую или использовать rosrun для их вызова. 


# [[Особые ситуации]]

## Ситуации

### Любое редактирование CMakeLists.txt

После этого необходимо снова скомпилировать пакет, используя [catkin_make](app://obsidian.md/catkin_make) или [catkin build](app://obsidian.md/catkin%20build). Затем сделайте source файла setup.bash в папке devel вашего рабочего пространства.