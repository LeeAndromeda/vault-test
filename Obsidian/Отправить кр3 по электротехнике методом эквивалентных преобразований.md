---
Тип: ДЗ
Дисциплина: Электротехника
Описание: -
aliases: 
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

День: 06 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Thursday 6th October 2022 15:11:31
день_завершения: 26
месяц_завершения: 10
год_завершения: 2022

Выполнено: Да
Результат: 

Tags: ДомашнееЗадание
---

```dataview
TABLE день_завершения as "День истечения", месяц_завершения as "Месяц истечения", Выполнено, Результат
WHERE contains(file.name, "Доделать кр3 по электротехнике")
limit 1
```

# [[Отправить кр3 по электротехнике методом эквивалентных преобразований]]


## Смотрите также:

- [[кр3_электротехника]]
- [[Метод комплексных амплитуд. Укравнение баланса мощностей - Лекция по электротехнике 2022-09-22#Уравнение баланса мощностей]]

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

---
#Электротехника 