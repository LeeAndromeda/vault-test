---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 11 # Не трогать!
Месяц: 03 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-03-11

---
# [[Python]]


## Теория

**Python** - язык со **строгой динамической типизацией[^1]**.

### Основы
---
- [[Типы данных в Python]]
- [[Округление чисел в Python]]
- [[Переменные в python]]
- [[Переменные среды в Python]]
---
- [[Ввод-вывод в Python]]
- [[Строки в Python]]
- [[F-строки в Python]]
---
- [[Математические операции в Python]]
- [[Условные выражения в Python]]
- [[Циклы в Python]]
---
- [[Коллекции (массивы) в Python]]
- [[Slice оператор в Python]]
- [[Множества set в Python]]
- [[Словари dict в Python]]
---
- [[Функции в Python]]
- [[Скобки у функции Python - ставить или нет]]
- [[Функция map() в Python]]
- [[Функция zip() в Python]]
- [[Функция enumerate() в Python]]
- [[Comprehensions в Python]]
---
- [[Операторы распаковки в Python]]
- [[Исключения в Python]]
- [[Лямбда функции в Python]]
- [[Спецификатор with в Python]]
---
- [[Функция id() в Python]]
- [[Функция sys.getsizeof() в Python]]
- [[Функция os.walk() в Python]]
- [[Функция полцчения случайного элемента массива в Python - random.choice()]]
- [[Строковая функция получения родительской папки, в которой находится файл по заданному пути - Python]]
- [[Строковоая функция поиска совпадения для получения путей - Python]]
- [[ООП в Python 3]]
- [Нижние подчеркивания в питоне](https://www.geeksforgeeks.org/underscore-_-python/)
- [[Docstring - документация кода в Python]]
- [[Решение конфликта зависимостей в Python - pipenv]]
---
- [[Модули в Python]]
- [[Пакеты в Python]]
- [[Порядок импортов в Python]]
- [[Циклические импорты]]
---
- [[assert -  проверка утверждения в Python]]
- [[Декораторы в Python]]
- [[functools.lru_cache - python]]
---

## Модули

- [[os - python]]
- [[pathlib - python]]
- [[itertools - python]]
- [[functools - python]]
- [[NumPy]]
- [[Pandas]]
- [[Pygame]]
- [[Shapely]]



## Сборка приложений в Python

### На windows

- https://tproger.ru/articles/prevrashhaem-kod-na-python-v-ispolnjaemyj-exe-fajl/








## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#Python

---

[^def]: термин
[^que]: вопрос
[^1]: не допускает неявных преобразований
