---
Тип: Задача
Описание:
  - 
Исполнители: 
Проект: pytorch ztm
Дисциплина: 
tags:
  - Задача
regex-to-search: 
regex-to-be-found: NoRgxTBFKey
День: 7
Месяц: 9
Год: 2023
дата_создания: 2023-09-07
дата_завершения: 2023-09-07
Выполнено: нет
Результат:
---
> [!caution] Это текущая задача
> Данная задача была помечена текущей. Если она таковой быть перестает, удалите полностью данный спойлер.
> Выберите тип задачи и удалите ненужный:
> 
> #Задача/РаботаИдет
> 
> ### Прогресс:: 0%


# [[Законспектировать третий урок из Pytorch ZTM]]


1. Computer vision libraries in PyTorch
   - Explore built-in computer vision libraries in PyTorch
   - Learn about their functionalities and how to use them

2. Load data
   - Gain familiarity with the FashionMNIST dataset
   - Learn how to load the dataset in PyTorch

3. Prepare data
   - Use a PyTorch DataLoader to efficiently handle the dataset
   - Preprocess the data to prepare it for training

4. Model 0: Building a baseline model
   - Create a multi-class classification model
   - Select a suitable loss function
   - Choose an appropriate optimizer
   - Build a training loop to train the model

5. Making predictions and evaluating model 0
   - Use the baseline model to make predictions on new data
   - Evaluate the performance of the model using appropriate metrics

6. Setup device-agnostic code for future models
   - Write code that can run on different devices (CPU or GPU)
   - Ensure compatibility and flexibility of the code

7. Model 1: Adding non-linearity
   - Enhance the baseline model by introducing non-linear layers
   - Experiment with different activation functions to improve performance

8. Model 2: Convolutional Neural Network (CNN)
   - Dive into computer vision-specific techniques
   - Implement a Convolutional Neural Network architecture
   - Understand the benefits of using CNNs for computer vision

9. Comparing our models
   - Compare the performance of the baseline model, Model 1, and Model 2
   - Analyze the differences in accuracy, training time, and other metrics

10. Evaluating our best model
    - Make predictions on random images using the best performing model
    - Evaluate the model's performance on unseen data

11. Making a confusion matrix
    - Learn how to create a confusion matrix
    - Visualize and interpret the results to gain insights into model performance

12. Saving and loading the best performing model
    - Save the best performing model for future use
    - Verify the correctness of the saved model by loading and using it again


#pytorch 