# tf2

Используется для контроля сеток координат.

## Статьи 

### Теория

- [[static broadcaster - транслятор статического фрейма]]
- [[transform broadcaster - транслятор динамического фрейма с задаваемыми параметрами трансформации]]
- [[transform listener - слушатель трансформаций]]
- [[tf2 и время - как получить трансформации из прошлого]]

#### Типы данных

- [[transformStamped]]

### Конспекты

- [[Как работают первые два кода из туториалов tf2]]
- [[Шаблоны кода для tf2]]

### Задачи

- [[Целевая точка в глобальной системе координат]]

### Интернет

- [Описание geometry_msgs::TransformStamped tf2_ros::BufferClient::lookupTransform, которое содержит ссылки на exceptions для транасформаци tf](http://docs.ros.org/en/indigo/api/tf2_ros/html/c++/classtf2__ros_1_1BufferClient.html#aab9ce4a39b1ab249621964db11ee6a87)

#Робототехника/ROS 