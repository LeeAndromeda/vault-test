---
Тип: конспект
Описание: -
Исполнители: 
Проект: 

Tags: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: 24 # Не трогать!
Месяц: 04 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-04-24
---

# [[Как работать с повторяющимися полями protobuff C++]]

# Содержание 

```cpp
for ( int i = 0; i < demList.size(); ++i ) 
{
    DemandSummary* summary = response.add_solutioninfo();
    summary->set_solutionindex(solutionID);
    summary->set_demandid(demList[i].toUInt());
}
```

Чтобы добавить новый объект в повторяющееся поле, существует типовая функция:

> [!def]+
> `add_repeatedfieldname()` - функция добавления нового элемента в повторяющееся поле protobuff.
> 
> **возвращает:** указатель на объект
 > 
> Возвращаемый указатель используется сразу для изменения полей объекта.



## Смотрите также:

> [!question]- Соурслист
> 1. https://stackoverflow.com/questions/1770707/how-do-you-add-a-repeated-field-using-googles-protocol-buffer-in-c

> [!quote]- Контекст
> ```dataview
> LIST 
> FROM !outgoing([[]])
> WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
> ```

---
#protobuf 
#cpp 

---

[^def]: термин
[^que]: вопрос