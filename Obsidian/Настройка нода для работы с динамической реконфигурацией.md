---
Тип: конспект
Описание: -

День: 25
Месяц: 07
Год: 2022
Дата_последнего_изменения: Monday 25th July 2022 13:52:58
---
# Настройка нода для работы с динамической реконфигурацией

```cpp 
#include <ros/ros.h>
#include <dynamic_reconfigure/server.h>
#include <package_name/ConfigFileNameConfig.h>

void callback(package_name::ConfigFileNameConfig &config, uint32_t level) 
{
  ROS_INFO("Reconfigure Request: %d %f %s %s %d", 
            config.int_param, config.double_param, 
            config.str_param.c_str(), 
            config.bool_param?"True":"False", 
            config.size);
} // Это колбэк. Тут мы делаем с полученными параметрами то, что хотим

int main(int argc, char **argv) 
{
  ros::init(argc, argv, "package_name");
  dynamic_reconfigure::Server<package_name::ConfigFileNameConfig> server; // Объект сервера
	  dynamic_reconfigure::Server<package_name::ConfigFileNameConfig>::CallbackType f; // Объект типа колбэка...
  f = boost::bind(&callback, _1, _2); // ...привязывается к функции колбэка, прописанной выше и... 
  server.setCallback(f); // ...отправлется на СДР
  ROS_INFO("Spinning node");
  ros::spin();
  return 0;
}
```

> [!hint]- Если колбэк является методом класса...
> Используйте форму ````f = boost::bind(&callback, x, _1, _2)````,  где x - объект класса.

> [!seealso]- Пустой шаблон
> ![[Пустой шаблон для нода с динамической реконфигурацией]]


![[Клиент динамической реконфигурации.svg]]

## Настройка CMakeLists.txt

1. Добавь эти строчки перед catkin_package():
	```CMake
	#add dynamic reconfigure api
	generate_dynamic_reconfigure_options(
	  cfg/ConfigFileName.cfg
	  #...
	)
	```
	
2. И эти после компиляции нодов:
	```CMake
	# make sure configure headers are built before any node using them
	add_dependencies(example_node ${PROJECT_NAME}_gencfg)
	```

3. Здесь добавь dynamic_reconfigure в поиск пакетов:
	```CMake
	find_package(catkin REQUIRED COMPONENTS
		...
		dynamic_reconfigure
		...
		)
	```

#ROS #ДинамическаяРеконфигурация 