---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "(.*)[Rr][Oo][Ss](.*)[Зз]апуск(.*)" # "что-то" regex

Тип: конспект
Описание: -

День: 07 # Не трогать!
Месяц: 12 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Wednesday 7th December 2022 12:07:46

---
# Настройка ROS для запуска

## В новом воркспэйсе

### 1. Создай новый воркспэйс

- [[Рабочее пространство catkin]]

### 2. Создай пакет

- [[Пакет catkin]]

## В существующем воркспэйсе

### 1. Засоурсь setup.bash рабочего пространства, в котором будешь работать

- [[Рабочее пространство catkin]]

### 2. Подготовь msg и srv файлы

- [[msg файлы#Настройка пакета для работы с сообщениями]]
- [[srv файлы#Настройка srv файлов]]
- [[Общий шаг для msg и srv]]

### 3. Настройка CMakeLists.txt после создания нодов

- [[Простой паблишер-сабскрайбер (С++)#Настройка CMakeLists txt после создания нодов паблишера и сабскрайбера]]
- [[Простой сервер-клиент (C++)#Настройка CMakeLists txt для сервера и клиента]]

# Ситуации

## Любое редактирование CMakeLists.txt 

После этого необходимо снова скомпилировать пакет, используя [[catkin_make]] или [[catkin build]]. Затем сделайте source файла setup.bash в папке devel вашего рабочего пространства.

> [!question]- Соурслист
>

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

#Робототехника/ROS 