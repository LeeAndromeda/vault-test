


---
Тип: Задача
Описание: -
Исполнители: 
Проект: ВнедрениеЗнаний

Дисциплина: Программирование
Tags: Задача
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

День: 12 # Не трогать!
Месяц: 11 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Saturday 12th November 2022 19:46:37

день_завершения: нет
месяц_завершения: нет
год_завершения: 2022
дата_завершения: 2022-нет-нет

Выполнено: нет
Результат: 
---

```dataview
TABLE день_завершения as "День истечения", месяц_завершения as "Месяц истечения", Проект, Выполнено, Результат, Количество_выходящих_задач as "Количество выходящих задач"
WHERE contains(file.name, "Poco cpp")
limit 1
```
# [[Poco cpp]]

https://pocoproject.org

## Результат:



## Следующая задача:



## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

---
#ВнедрениеЗнаний #cpp #Программирование