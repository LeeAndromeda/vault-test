# static broadcaster - транслятор статического фрейма

## Особенности

В реальной практике рекоммендуется использовать static_transform_publisher из библиотеки tf2_ros. *В [туториале](http://wiki.ros.org/tf2/Tutorials/Writing%20a%20tf2%20static%20broadcaster%20%28C%2B%2B%29) код рассматривается с целью дать понимание работы.*

По происходящим операциям и коду очень похож на [[transform broadcaster - транслятор динамического фрейма с задаваемыми параметрами трансформации]], но отличается тем, что служит для трансляции статических фреймов, таких как **odom**, **map** и т.д.

В передаче трансформаций используется тип данных ROS [[transformStamped]].

## Инструкция

### Использование static transform publisher

#### В терминале

Есть два варианта использования static_transform_publisher:
1. С заданием углов поворота в эйлеровых углах
2. С заданием углов поворота в векторах кватерниона

```bash
rosrun tf2_ros static_transform_publisher <координаты относительно фрейма-отца(x, y, z)> <углы(roll, pitch, yaw) или кватернион(x, y, z, w)> <название фрейма-отца> <название публикуемого фрейма> 
```

##### Эйлеровы углы

```bash
rosrun tf2_ros static_transform_publisher 0 0 0 30 45 60 world new_frame
```

##### Кватернион

```bash
rosrun tf2_ros static_transform_publisher 0 0 0 0 0 0 1 world new_frame
```

#### В лаунч файле

*Так же, как и в терминале, можно использовать два варианта задавания углов поворота.*[^1] 

```xml
<launch>
<node pkg="tf2_ros" type="static_transform_publisher" name="link1_broadcaster" args="1 0 0 0 0 0 1 link1_parent link1" />
</launch>
```

#tf2 #Робототехника/ROS #Робототехника 

[^1]: Предположение

