# Давление в жидкости,покоящейся в ИСО


Если рассматривать столб жидкости, то давление в каждой её точке зависит от высоты жидкости над данной точкой.

**Сообщающиеся сосуды** - соединенные сосуды

![Image](сосуды.jpg)
Если жидкость имеет различные плотности, то следует учитывать давление, которое оказывает каждая жидкость.

В сообщающихся сосудах высота расположения выше у той жидкости, которая имеет меньшую плотность, поскольку более тяжелая жидкость вытесняет её.

Давление в сообщающихся сосудах зависит от силы, действующей со стороны жидкости, а также от площади их поперечного сечения.

$$P={{F_1} \over {S_1}} ={{F_2} \over {S_2}} $$

[[КК Физика ЕГЭ]](КК%20Физика%20ЕГЭ%20.md)

#Физика