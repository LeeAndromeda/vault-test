---

Tags: 
aliases: "ssh ключ"
regex-to-search: "((.*)[Ss]sh(.*)ключ.*)"
regex-to-be-found: "NoRgxTBFKey"

Тип: конспект
Описание: -

День: 03 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Monday 3rd October 2022 22:04:02

---
# [[Как создать ssh ключ для GitHub]]

1.  Open the terminal app on your computer.
2.  Enter the following command, substituting joe@example.com with your email address:
	```shell
	ssh-keygen -t rsa -b 4096 -C "yourmail@gmail.com"
	```
1.  Press **Enter** to accept the default file location.
2.  Enter a [secure passphrase](https://www.inmotionhosting.com/support/server/ssh/how-to-create-a-strong-passphrase-for-ssh/).
3.  Press **Enter**.
4.  Enter this command to display the contents of your public key:
	```shell
	cat .ssh/id_rsa.pub
	```
1.  Copy the contents of your key to your clipboard (we will need it later).

> [!hint]+
> It’s important to remember that the SSH key has two parts: a private key and a public key. As the name suggests, the private key is only for private use

## Как добавить ssh ключ в github аккаунт

1.  [Log into your GitHub account](https://www.inmotionhosting.com/support/website/git/using-your-github-account/).
2.  Click your avatar and choose **Settings**  
    [![GitHub Settings](https://www.inmotionhosting.com/support/images/stories/git/github_ssh_-01.png "Settings Under Avatar")](https://www.inmotionhosting.com/support/images/stories/git/github_ssh_-01.png)
    
3.  Select **SSH and GPG keys**  
    [![SSH and GPG Keys](https://www.inmotionhosting.com/support/images/stories/git/github_ssh_-02.png "SSH and GPG keys")](https://www.inmotionhosting.com/support/images/stories/git/github_ssh_-02.png)
    
4.  Click **New SSH key**  
    [![New SSH Key](https://www.inmotionhosting.com/support/images/stories/git/github_ssh_-03.png "New SSH Key")](https://www.inmotionhosting.com/support/images/stories/git/github_ssh_-03.png)
    
5.  Enter a title in the field  
    [![Enter Title](https://www.inmotionhosting.com/support/images/stories/git/github_ssh_-04.png "Enter Key Title")](https://www.inmotionhosting.com/support/images/stories/git/github_ssh_-04.png)
    
6.  Paste your public key into the _Key_ field  
    [![Paste public key](https://www.inmotionhosting.com/support/images/stories/git/github_ssh_-05.png "Paste public key")](https://www.inmotionhosting.com/support/images/stories/git/github_ssh_-05.png)
    
7.  Click **Add SSH key**  
    [![Add SSH key](https://www.inmotionhosting.com/support/images/stories/git/github_ssh_-06.png "Add SSH key")](https://www.inmotionhosting.com/support/images/stories/git/github_ssh_-06.png)

## Как использовать ssh ключ для работы с репозиторием

Добавь remote для репозитория с ssh-ссылкой.

- https://www.theserverside.com/blog/Coffee-Talk-Java-News-Stories-and-Opinions/GitHub-SSH-Key-Setup-Config-Ubuntu-Linux

## Смотрите также:

```dataview
LIST 
WHERE regexmatch(this.regex-to-search, file.name) and !this
```

- [[Работа с SSH]]

#SSH #git #GitHub 