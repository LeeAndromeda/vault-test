---
Тип: конспект
Описание: "Краткий гайд по плагину Admonition"

День: 11
Месяц: 07
Год: 2022
---
# Как использовать Admonition

![[Screenshot_2022-07-11-00-44-03-288_md.obsidian.jpg]]![[Screenshot_2022-07-11-00-44-45-329_md.obsidian.jpg]]
![[Screenshot_2022-07-11-00-45-58-823_md.obsidian.jpg]]
```ad-note
title: Title
collapse: true

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod nulla.

```

#PKM/obsidian #Плагины #НовоеПоколениеЗнания  #туториал 