# Как установить OpenSSH

Если вы хотите установить сервер/клиент OpenSSH и настроить автоматический запуск сервера OpenSSH, следуйте следующим инструкциям, которые различаются в зависимости от дистрибутива.  
  
## **Debian, Ubuntu или Linux Mint**  

Код:

```
$ sudo apt-get install openssh-server openssh-client
```

В системах основанных на Debian, сразу после установки, OpenSSH будет запускаться автоматически при загрузке. Если по каким либо причинам сервер OpenSSH не запускается автоматически при запуске системы, вы можете выполнить следущую команду для однозначного добавления ssh в загрузку при старте системы.  

Код:

```
$ sudo update-rc.d ssh defaults
```

## **Fedora или CentOS/RHEL 7**  

Код:

```
$ sudo yum -y install openssh-server openssh-clients
$ sudo systemctl start sshd service
$ sudo systemctl enable sshd.service
```

## **CentOS/RHEL 6**  

Код:

```
$ sudo yum -y install openssh-server openssh-clients
$ sudo service sshd start
$ sudo chkconfig sshd on
```

## **Arch Linux**  

Код:

```
$ sudo pacman -Sy openssh
$ sudo systemctl start sshd service
$ sudo systemctl enable sshd.service
```

#SSH #Linux 