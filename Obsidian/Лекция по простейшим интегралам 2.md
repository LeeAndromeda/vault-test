


---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: Лекция
Описание: -

День: 25 # Не трогать!
Месяц: 05 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-05-25

---
# [[Лекция по простейшим интегралам 2]]

[[Лекция по простейшим интегралам 2.png]]

```leaflet
id: leaflet-idlkjhiyu'oasdjgk;lsajd'objsdlbyhpoesnbsdungab[89]
image: Лекция по простейшим интегралам 2.png 
height: 500px
lat: 50
long: 50
minZoom: 1
maxZoom: 10
defaultZoom: 7
unit: meters
scale: 1
darkMode: false
```


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

> [!question]- Соурслист
> 1. 


---

#Лекция  #Математика 

---

[^def]: термин
[^que]: вопрос