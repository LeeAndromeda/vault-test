---

Tags: 
aliases: 
regex-to-search: "((.*)[Uu]buntu(.*))"
regex-to-be-found: "NoRgxTBFKey"

Тип: конспект
Описание: -

День: 12 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Wednesday 12th October 2022 11:06:54

---
# [[Список программ, которые необходимы в моей экосистеме Ubuntu для разработки на ROS и ROS2]]

1. Terminator
2. Vim
3. VSCode
4. Git
5. Obsidian
6. MyPaint
7. Gazebo
8. Nextcloud
9. Blender
10. Drawio
11. Peek
12. BalenaEtcher
13. Mathpix client
14. ArduinoIDE
15. OpenSSH
16. OpenVPN 3

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

---
#Linux 