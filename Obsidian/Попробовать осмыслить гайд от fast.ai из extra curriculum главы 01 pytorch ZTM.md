---
Тип: Задача
Описание: -
Исполнители: 
Проект: pytorch ZTM

Дисциплина: #Входящие 
Tags: Задача
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: 27 # Не трогать!
Месяц: 06 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-06-27

дата_завершения: "2023-нет-нет"

Выполнено: нет
Результат: 
---


 > [!info] Эта задача находится в очереди в текущие
> Данная задача была отправлена в очередь на выполнение как можно скорее. Если вы решите приняться за выполнение этой  задачи, удалите этот спойлер и установите спойлер ткущей задачи.
> 
> Выберите тип задачи и удалите ненужный:
> 
> #Задача/вОчереди


# [[Попробовать осмыслить гайд от fast.ai из extra curriculum главы 01 pytorch ZTM]]



## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```


---
#pytorch 

---