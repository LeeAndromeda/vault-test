---
Тип: Задача
Описание: -
Исполнители: 
Проект: ВнедрениеЗнаний

Дисциплина: #Входящие 
Tags: Задача
regex-to-search: "[Pp]ython(.*)" # "что-то" regex
regex-to-be-found: "[Pp]ython(.*)" # "что-то" regex

День: 25 # Не трогать!
Месяц: 01 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-01-25

дата_завершения: "2023-нет-нет"

Выполнено: да
Результат: 
---

```dataview
TABLE день_завершения as "День истечения", месяц_завершения as "Месяц истечения", Проект, Выполнено, Результат, Количество_выходящих_задач as "Количество выходящих задач"
WHERE Дата_создания = this.Дата_создания
limit 1
```
# [[Быстрый курс по Python]]

- Получение источников и ссылок
	- [ ] Получение одной ссылки
	- [ ] Получение ссылок на разные источники
- Конспектирование
	- [ ] Достаточное для использования конспектирование
	- [ ] Полное конспектирование
- Создание справок
	- [ ] Справки практического применения
	- [ ] Список терминов
	- [ ] Определение терминов из списка терминов
	- [ ] Список связанных тем(если нет внутри текста)
- Внедрение в базу знаний
	- [ ] Создание связей с оригинальной страницей (этой)
	- [ ] Разбиение на подтемы
	- [ ] Атомизация - (по необходимости) разбиение конспекта по подтемам на страницы
- [ ] Внедрение в практику

%%
## Результат:



## Следующая задача:

==#Входящие ==
%%

**Содержание потом вырезается, все содержимое конспекта удаляется и содержание вставляется по шаблону входящего конспекта.**

# Содержание 



#Входящие 


> [!question]- Соурслист
> 1. https://youtu.be/VchuKL44s6E

%%
## Приложения
%%

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

---
#ВнедрениеЗнаний 
#Python 

[^def]: термин
[^que]: вопрос