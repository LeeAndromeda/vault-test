---
Тип: Задача
Описание: -
Исполнители: 
Проект: Демонстратор

Дисциплина: Работа
Tags: Задача 
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

День: 02 # Не трогать!
Месяц: 12 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Friday 2nd December 2022 16:14:41

день_завершения: нет
месяц_завершения: 12
год_завершения: 2022

Выполнено: да
Результат: 
---

```dataview
TABLE день_завершения as "День истечения", месяц_завершения as "Месяц истечения", Проект, Выполнено, Результат, Количество_выходящих_задач as "Количество выходящих задач"
WHERE contains(file.name, "Поменять голос в озвучке демонстратора на narakeet")
limit 1
```
# [[Поменять голос в озвучке демонстратора на narakeet]]

#Входящие 

%%

## Результат: 

%%

## Следующая задача:

#Входящие 

## Смотрите также:

- [[Сайт для озвучки текста с качественным звучанием и бесплатным пробным периодом - narakeet]]

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```


---

#Демонстратор 