---

Tags: 
aliases: [git]
regex-to-search: "((.*)[Gg]it(.*))|((.*)[Гг]ит(.*))" # "что-то" regex
regex-to-be-found: "((.*)[Gg]it(.*))|((.*)[Гг]ит(.*))" # "что-то" regex

Тип: конспект
Описание: -

День: 31 # Не трогать!
Месяц: 01 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-01-31

---
# [[Работа с Git]]

> [! example] Список определений [^def]
>

> [! example] Список вопросов [^que]
> 


# Работа с Git

## Теория

---
#### Основы

- [[Пара строчек для git config]]
- [[Как использовать VSCode в качестве git difftool и mergetool]]
- [[Основные команды git]]
- [Как использовать .gitignore](https://www.youtube.com/watch?v=0DiNdpUx5_Q)

---
#### diff и работа  ветками

- [[Как показать отличие конкретного файла]]
- [Как слить ветки](https://git-scm.com/book/ru/v2/%D0%92%D0%B5%D1%82%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5-%D0%B2-Git-%D0%9E%D1%81%D0%BD%D0%BE%D0%B2%D1%8B-%D0%B2%D0%B5%D1%82%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D1%8F-%D0%B8-%D1%81%D0%BB%D0%B8%D1%8F%D0%BD%D0%B8%D1%8F)
- [[Как удалить ветку git на remote и  local]]
- [[Pull-request - что это и как его сделать]]
- [Как разрешить конфликт слияния в пользу remote или в пользу локальных изменений](https://stackoverflow.com/questions/10697463/resolve-git-merge-conflicts-in-favor-of-their-changes-during-a-pull/33569970#33569970)
- [Как вернуть репозиторий в состояние выбранного коммита](https://stackoverflow.com/questions/4114095/how-do-i-revert-a-git-repository-to-a-previous-commit)

---

- [Как удалить файл из индекса](https://stackoverflow.com/questions/2223308/how-to-remove-a-file-from-the-staging-area-index-cache-in-git)

--- 
#### Субмодули

- [[Как удалить субмодуль]]
- [[Как запушить изменения в субмодуль]]
- [[Как обновить все субмодули репозитория]]
- [[Как создать ssh ключ для GitHub]]
- [[Как удалить remote git]]
- [[Как переписать историю - git commit --amend]]
- [[git blame - история файла]]

## Различные задачи и их решения

### Идеи

- [[Как установить логин и пароль по умолчанию, чтобы не вводить их постоянно]]
- [[Как использовать VSCode в качестве git difftool и mergetool]]

### Проблемы, с которыми я столкнулся

- [[git status показывает цифры вместо кириллицы]]

## Видео

- [Git и GitHub Курс Для Новичков](https://www.youtube.com/watch?v=zZBiln_2FhM&list=PLISCCfwPA-DdkjFdzbTyaA1At6mhd2bKs)
- [Сборник видео по Git](https://m.vk.com/wall-101965347_418571)
- [Подробный курс по гит за ~1,5 часа](https://m.vk.com/wall-101965347_420110)

## Другое 

- [Сайт для сравнения текстов](http://text.num2word.ru/)
- [Сайт для визуального изучения основ гит](https://learngitbranching.js.org/?locale=ru_RU)
- [Сайт для визуализации действий в гит](http://git-school.github.io/visualizing-git/)

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---

#GitHub #Программирование

---

[^def]: термин
[^que]: вопрос