---

Tags: 
aliases: 
regex-to-search: 
regex-to-be-found: "NoRgxTBFKey"

Тип: конспект
Описание: -

День: 21 # Не трогать!
Месяц: 10 # Не трогать!
Год: 2022 # Не трогать!
Дата_создания: Friday 21st October 2022 16:21:47

---
# [[Простой паблишер-сабскрайбер ROS2(С++)]]

## Простой паблишер

```cpp
#include <chrono>
#include <functional>
#include <memory>
#include <string>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

using namespace std::chrono_literals;

/* This example creates a subclass of Node and uses std::bind() to register a
* member function as a callback from the timer. */

class MinimalPublisher : public rclcpp::Node
{
  public:
    MinimalPublisher()
    : Node("minimal_publisher"), count_(0) // инициалзация полей
    {
      publisher_ = this->create_publisher<std_msgs::msg::String>("topic", 10); // создание паблишера
      timer_ = this->create_wall_timer(500ms, std::bind(&MinimalPublisher::timer_callback, this)); // инициализация таймера. Привязка к таймеру нода, а к ноду колбэка(таймер -> нод -> колбэк) 
    }

  private:
    void timer_callback() // Сама функция колбэка
    {
      auto message = std_msgs::msg::String();
      message.data = "Hello, world! " + std::to_string(count_++);
      RCLCPP_INFO(this->get_logger(), "Publishing: '%s'", message.data.c_str());
      publisher_->publish(message);
    }
    rclcpp::TimerBase::SharedPtr timer_; // Инициализация вышеиспоользуемых полей. В классе так можно
    rclcpp::Publisher<std_msgs::msg::String>::SharedPtr publisher_;
    size_t count_;
};

int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<MinimalPublisher>());
  rclcpp::shutdown();
  return 0;
}
```

> [!info]- Непонятные мне вещи и их разъяснения
> - [[chrono lib c++]]
> - [[std functional c++]]
> - [[memory lib c++]]
> - `std::chrono_literals` - буквенные комбинации для отображения времени в разных формах(дни, часы, секунды, наносекунды)
> - Синтаксис наследования в конструкторе - на самом деле не синтаксис наследования. Это инициализаторы конструктора.
> - `std::bind(&MinimalPublisher::timer_callback, this))`
> - `auto message`
> - `std::make_shared<MinimalPublisher>` - создает shared_ptr на новый объект

> [!done]- # Шаблон паблишера
> ```cpp
> #include <chrono>
>#include <functional>
>#include <memory>
>#include <string>
>
>#include "rclcpp/rclcpp.hpp"
>#include "std_msgs/msg/string.hpp"
>
>using namespace std::chrono_literals;
>
>class Publisher_class_name : public rclcpp::Node
>{
>  public:
>    Publisher_class_name() : Node("node_name"), count_(0)
>    {
>      publisher_ = this->create_publisher<message_type>("topic", 10);
>      timer_ = this->create_wall_timer(500ms, std::bind(&Publisher_class_name::timer_callback, this));
>    }
>
>  private:
>    void timer_callback()
>    {
>      message_type message;
>      do_something();
>      publisher_->publish(message);
>    }
>    rclcpp::TimerBase::SharedPtr timer_;
>    rclcpp::Publisher<message_type>::SharedPtr publisher_;
>    size_t count_;
>};
>
>int main(int argc, char * argv[])
>{
>  rclcpp::init(argc, argv);
>  rclcpp::spin(std::make_shared<Publisher_class_name>());
>  rclcpp::shutdown();
>  return 0;
>}
>```
>

## Простой сабскрайбер

```cpp
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
using std::placeholders::_1;

class MinimalSubscriber : public rclcpp::Node
{
  public:
    MinimalSubscriber()
    : Node("minimal_subscriber")
    {
      subscription_ = this->create_subscription<std_msgs::msg::String>(
      "topic", 10, std::bind(&MinimalSubscriber::topic_callback, this, _1));
    }

  private:
    void topic_callback(const std_msgs::msg::String::SharedPtr msg) const // Проблема такого колбэка в том, что const не позволяет модифицировать переменные за пределами функции
    {
      RCLCPP_INFO(this->get_logger(), "I heard: '%s'", msg->data.c_str());
    }
    rclcpp::Subscription<std_msgs::msg::String>::SharedPtr subscription_;
};

int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<MinimalSubscriber>());
  rclcpp::shutdown();
  return 0;
}
```

> [!done]- # Шаблон сабскрайбера
>  ```cpp
> #include <memory>
>
>#include "rclcpp/rclcpp.hpp"
>#include "std_msgs/msg/string.hpp"
>using std::placeholders::_1;
>
>class Subscriber_class_name : public rclcpp::Node
>{
>  public:
>    Subscriber_class_name() : Node("node_name")
>    {
>      subscription_ = this->create_subscription<message_type>(
>      "topic", 10, std::bind(&Subscriber_class_name::topic_callback, this, _1));
>    }
>
>  private:
>    void topic_callback(const message_type::SharedPtr msg) const
>    {
>      RCLCPP_INFO(this->get_logger(), "I heard: '%s'", msg->data.c_str());
>    }
>    rclcpp::Subscription<message_type>::SharedPtr subscription_;
>};
>
>int main(int argc, char * argv[])
>{
>  rclcpp::init(argc, argv);
>  rclcpp::spin(std::make_shared<Subscriber_class_name>());
>  rclcpp::shutdown();
>  return 0;
>}
>```


## Что делать после создания нового нода

### 1. Настройка package.xml

`package.xml` - это файл, который отвечает за метаинформацию о пакете. Для начала вам стоит заполнить поля `<description>`, `<maintainer>` и `<license>`:

```xml
<description>Examples of minimal publisher/subscriber using rclcpp</description>
<maintainer email="you@email.com">Your Name</maintainer>
<license>Apache License 2.0</license>
```

После того как у вашего нода появились новые библиотеки, от которых он зависит, вы должны добавить их в `package.xml` и в `CMakeLists.txt`:

```xml
<!-- Для package.xml -->
<depend>rclcpp</depend>
<depend>std_msgs</depend>
```


### 2. Настройка CMakeLists.txt

```CMake
# Для CMake
find_package(rclcpp REQUIRED)
find_package(std_msgs REQUIRED)
```

Далее необходимо добавить исполняемый файл:

```CMake
add_executable(talker src/publisher_member_function.cpp)
ament_target_dependencies(talker rclcpp std_msgs)
```

И окончательно - добавьте свой нод в install в конце CMakeLists, чтобы `ros2 run` мог найти ваш нод:

```cmake
install(TARGETS
  talker
  DESTINATION lib/${PROJECT_NAME})
```

> [!tip]- Итоговый CMakeLists.txt
>```cmake
> cmake_minimum_required(VERSION 3.5)
project(cpp_pubsub)
>
>Default to C++14
>if(NOT CMAKE_CXX_STANDARD)
  set(CMAKE_CXX_STANDARD 14)
>endif()
>
>if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
  add_compile_options(-Wall -Wextra -Wpedantic)
>endif()
>
>find_package(ament_cmake REQUIRED)
>find_package(rclcpp REQUIRED)
>find_package(std_msgs REQUIRED)
>
>add_executable(talker src/publisher_member_function.cpp)
>ament_target_dependencies(talker rclcpp std_msgs)
>
>install(TARGETS
  talker
  DESTINATION lib/${PROJECT_NAME})
>
>ament_package()
>```

## Что если изменения в ноде изменили его зависимости

### Изменения в package.xml

```xml
<!-- Для package.xml -->
<depend>rclcpp</depend>
<depend>std_msgs</depend>
<!--   .  .  .   -->
<depend>new_dep</depend>
```

### Изменения в CMakeLists.txt

```cmake
add_executable(talker src/publisher_member_function.cpp)
ament_target_dependencies(talker rclcpp std_msgs new_dep)
```

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```

> [!question]- Соурслист
> 1. https://docs.ros.org/en/foxy/Tutorials/Beginner-Client-Libraries/Writing-A-Simple-Cpp-Publisher-And-Subscriber.html


---
#ROS2 