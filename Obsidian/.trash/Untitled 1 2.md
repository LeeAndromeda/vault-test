# Planning the movement of a wheeled robot in an uncertain environment

---

Author: Andrey V. Leshchev-Romanenko - student of South Federal University
Supervisor: Vyacheslav Khasanovich Pshihopov

---

The paper deals with the design of adaptive control system for wheeled robot in *dynamically varying* uncertain environment. The wheeled robot is represented by known mathematical models of kinematics and dynamics:
 $${dot{y} = R(\Theta)x,$$$${dot{x} = B _{u}\delta - F _{\delta},$$ $$\text{later entered}$$
 
%%%Provide models from the book and make a link. furthermore no paragraph%%% Assume that in order to complete the task it has to follow a global path given by the *high level planner*.%%% Consider the case where the global planner leads us to a dead end and a recalculation of the path is needed. Consider the case where the global path recalculation%%% is required to avoid collisions with fixed obstacles and avoid dead ends. For navigation in case of a priori unpredictable changes in the external environment, a modified potential-field method is proposed. The modification consists in optimization of algorithm result calculation process. This combination of planning algorithms aims to make efficient use of the global planning algorithm and increase its flexibility by adaptive obstacle avoidance using the potential fields method.

The paper also proposes a laser rangefinder data processing procedure optimised to use the combination of algorithms.

Simulation results confirming the effectiveness of the proposed solutions are presented in the report.

The results of the experiment with the implementation of planning algorithms based on ROS2%%%%%%% are given. A photo of the robot from the proposed experiment%%% is presented
![[Pasted image 20220614161447.png]]