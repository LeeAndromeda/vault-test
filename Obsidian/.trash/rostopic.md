# rostopic
```
$ rostopic -h

```

```
rostopic bw     display bandwidth used by topic
rostopic echo   print messages to screen
rostopic hz     display publishing rate of topic    
rostopic list   print information about active topics
rostopic pub    publish data to topic
rostopic type <<< topic >>>  print topic type
```

## Разбор комманд

### list -v

```
Published topics:
 * /turtle1/color_sensor [turtlesim/Color] 1 publisher
 * /turtle1/cmd_vel [geometry_msgs/Twist] 1 publisher
 * /rosout [rosgraph_msgs/Log] 2 publishers
 * /rosout_agg [rosgraph_msgs/Log] 1 publisher
 * /turtle1/pose [turtlesim/Pose] 1 publisher


Subscribed topics:
 * /turtle1/cmd_vel [geometry_msgs/Twist] 1 subscriber
 * /rosout [rosgraph_msgs/Log] 1 subscriber
```

### rostopic pub
Позволяет публиковать сообщения в топик:

```
$ rostopic pub [topic] [msg_type] [args]
```

Для ROS Hydro и более поздних:

Эта команда будет публиковать сообщения в указанный топик:

```rostopic pub```

Эта опция заставляет rostopic публиковать только одно сообщение и выходить:

 ```-1 ```
 
Это имя топика, в который идёт публикация:

```/turtle1/cmd_vel```

Это тип сообщений, публикуемый в топиу:

```geometry_msgs/Twist```

This option (double-dash) tells the option parser that none of the following arguments is an option. This is required in cases where your arguments have a leading dash -, like negative numbers. Эта опция говорит парсеру [^1]

```--```

As noted before, a geometry_msgs/Twist msg has two vectors of three floating point elements each: linear and angular. In this case, '[2.0, 0.0, 0.0]' becomes the linear value with x=2.0, y=0.0, and z=0.0, and '[0.0, 0.0, 1.8]' is the angular value with x=0.0, y=0.0, and z=1.8. These arguments are actually in YAML syntax, which is described more in the YAML command line documentation.

'[2.0, 0.0, 0.0]' '[0.0, 0.0, 1.8]' 

#ROS