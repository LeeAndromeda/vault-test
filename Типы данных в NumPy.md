---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 10 # Не трогать!
Месяц: 04 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-04-10

---
# [[Типы данных в NumPy]]

## Data Types in Python

By default Python have these data types:

-   `strings` - used to represent text data, the text is given under quote marks. e.g. "ABCD"
-   `integer` - used to represent integer numbers. e.g. -1, -2, -3
-   `float` - used to represent real numbers. e.g. 1.2, 42.42
-   `boolean` - used to represent True or False.
-   `complex` - used to represent complex numbers. e.g. 1.0 + 2.0j, 1.5 + 2.5j

---

## Data Types in NumPy

NumPy has some extra data types, and refer to data types with one character, like `i` for integers, `u` for unsigned integers etc.

Below is a list of all data types in NumPy and the characters used to represent them.

-   `i` - integer
-   `b` - boolean
-   `u` - unsigned integer
-   `f` - float
-   `c` - complex float
-   `m` - timedelta
-   `M` - datetime
-   `O` - object
-   `S` - string
-   `U` - unicode string
-   `V` - fixed chunk of memory for other type ( void )


![[Pasted image 20230410175057.png]]
![[Pasted image 20230410175110.png]]

## Отличия None и nan в Python и numpy

![[Pasted image 20231120122527.png]]
![[Pasted image 20231120122701.png]]
## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://numpy.org/doc/stable/user/basics.types.html
> 2. https://www.w3schools.com/python/numpy/numpy_data_types.asp


---
#Python/NumPy 

---

[^def]: термин
[^que]: вопрос