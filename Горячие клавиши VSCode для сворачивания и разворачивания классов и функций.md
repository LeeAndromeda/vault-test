---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 09 # Не трогать!
Месяц: 01 # Не трогать!
Год: 2023 # Не трогать!
Дата_создания: Monday 9th January 2023 16:50:04

---
# [[Горячие клавиши VSCode для сворачивания и разворачивания классов и функций]]


-   **Fold** folds the innermost uncollapsed region at the cursor:
    
    -   `Ctrl + Shift + [` on Windows and Linux
    -  ` ⌥ + ⌘ + [` on macOS
-   **Unfold** unfolds the collapsed region at the cursor:
    
    -   `Ctrl + Shift + ]` on Windows and Linux
    -  ` ⌥ + ⌘ + ]` on macOS
-   **Fold All** folds all regions in the editor:
    
    -   `Ctrl + K, Ctrl + 0` (zero) on Windows and Linux
    -   `⌘ + K, ⌘ +0` (zero) on macOS
-   **Unfold All** unfolds all regions in the editor:
    
    -   `Ctrl + K, Ctrl + J` on Windows and Linux
    -   `⌘ + K, ⌘ + J` on macOS 

%%## Приложения%%

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://stackoverflow.com/questions/30067767/how-do-i-collapse-sections-of-code-in-visual-studio-code-for-windows


---
#Программирование/СредыРазработки 

[^def]: термин
[^que]: вопрос