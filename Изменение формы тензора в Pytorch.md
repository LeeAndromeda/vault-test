---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 19 # Не трогать!
Месяц: 06 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-06-19

---
# [[Изменение формы тензора в Pytorch]]

### Reshaping, stacking, squeezing and unsqueezing

Often times you'll want to reshape or change the dimensions of your tensors without actually changing the values inside them.

To do so, some popular methods are:

| Method                                                                                                      | One-line description                                                                                          |
| ----------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------- |
| [`torch.reshape(input, shape)`](https://pytorch.org/docs/stable/generated/torch.reshape.html#torch.reshape) | Reshapes `input` to `shape` (if compatible), can also use `torch.Tensor.reshape()`.                           |
| [`torch.Tensor.view(shape)`](https://pytorch.org/docs/stable/generated/torch.Tensor.view.html)              | Returns a view of the original tensor in a different `shape` but shares the same data as the original tensor. |
| [`torch.stack(tensors, dim=0)`](https://pytorch.org/docs/1.9.1/generated/torch.stack.html)                  |           Concatenates a sequence of `tensors` along a new dimension (`dim`), all `tensors` must be same size.                                                                                                    |
| [`torch.squeeze(input)`](https://pytorch.org/docs/stable/generated/torch.squeeze.html)                      |          Squeezes `input` to remove all the dimenions with value `1`.                                                                                                     |
| [`torch.unsqueeze(input, dim)`](https://pytorch.org/docs/1.9.1/generated/torch.unsqueeze.html)              |     Returns `input` with a dimension value of `1` added at `dim`.                                                                                                          |
| [`torch.permute(input, dims)`](https://pytorch.org/docs/stable/generated/torch.permute.html)                |             Returns a _view_ of the original `input` with its dimensions permuted (rearranged) to `dims`                                                                                                  |

> [!warning]
> **Note**: Because permuting returns a _view_ (shares the same data as the original), the values in the permuted tensor will be the same as the original tensor and if you change the values in the view, it will change the values of the original.


## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://www.learnpytorch.io/00_pytorch_fundamentals/#reshaping-stacking-squeezing-and-unsqueezing


---
#Входящие 

---

[^def]: термин
[^que]: вопрос