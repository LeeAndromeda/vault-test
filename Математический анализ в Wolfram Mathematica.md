---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 23 # Не трогать!
Месяц: 02 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-02-23

---
# [[Математический анализ в Wolfram Mathematica]]


## Лимиты

![[Лимиты-курс.pdf]]


## Смотрите также:
- [[Wolfram Mathematica]]
```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. https://www.wolframcloud.com/obj/online-courses/introduction-to-calculus/the-limit-of-a-function.html


---
#Математика/Матанализ #Mathematica

---

[^def]: термин
[^que]: вопрос