Plot a graph:

In[1]:=

![](https://reference.wolfram.com/language/ref/Files/GraphPlot.en/I_1.png)

`GraphPlot[PetersenGraph[]]`

`https://wolfram.com/xid/0bim84gy-syyt6`

Out[1]=

![](https://reference.wolfram.com/language/ref/Files/GraphPlot.en/O_1.png)

Plot a graph specified by edge rules:

In[1]:=

![](https://reference.wolfram.com/language/ref/Files/GraphPlot.en/I_2.png)

`GraphPlot[{1 -> 2, 3 -> 1, 3 -> 2, 4 -> 1, 4 -> 2}]`

`https://wolfram.com/xid/0bim84gy-hgjvx8`

Out[1]=

![](https://reference.wolfram.com/language/ref/Files/GraphPlot.en/O_2.png)

Plot a graph specified by its adjacency matrix:

Copy to clipboard.

In[1]:=

![](https://reference.wolfram.com/language/ref/Files/GraphPlot.en/I_3.png)

`GraphPlot[{{0, 1, 1}, {1, 0, 1}, {1, 1, 0}}]`

`https://wolfram.com/xid/0bim84gy-eor`

Out[1]=

![](https://reference.wolfram.com/language/ref/Files/GraphPlot.en/O_3.png)