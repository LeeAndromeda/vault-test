# Держи хвост пулеметом

Ступая на этот путь, важно идти до самого конца, не делая преждевременных выводов. Как показывает практика, причем моя тоже, люди склонны делать преждевременные выводы, даже не увидев и дольки результата. Чаще всего именно на этом моменте они и забрасывают своё дело, так как их мозговые сосочки сказали им, что усё, ничего не получится, сворачиваем удочки.

**Поэтому запомните это правило навсегда — делайте выводы только после РЕЗУЛЬТАТА, а лучше нескольких.**

Не получилось в первый раз? Посмотрите на ошибки, которые вы допустили, исправьте их и попробуйте еще раз. Результат стал лучше? Значит вы на верном пути.

**Создаете → ищите ошибки → исправляете их → пробуете снова → повторяете алгоритм.**

И так до бесконечности. Желательно в период исправления своих ошибок смотреть на опыт других людей, которые в этом деле преуспели. Еще Пикассо говорил:

> Хорошие художники копируют, великие — воруют.

Поэтому не стесняйтесь, берите крутые идеи и применяйте у себя.

**И запомните еще одну важную вещь — не пытайтесь делать всё в точности, как кто-то другой.**

Делайте со своей изюминкой. Идея может быть одна и та же, но реализация должна быть уникальной, а не жалкой копией. И да, наверняка у вас не получится написать так, как пишут другие. Это порой не получается даже у меня.

Все люди мыслят по-разному и под некоторых вы попросту не сможете подстроиться, как бы вы не мучили себя. Поэтому сразу настраивайтесь на то, что будете писать в своем стиле, лишь копируя идеи, приемы и фишки тех, кто получил в этом деле результат.

И напоследок.

Ваши первые тексты наверняка будут скомканными, немного неотесанными и довольно пресными, однако это нормально, вы еще учитесь. Постепенно, впитывая информацию из моего курса и других источников, уровень ваших текстов разительно вырастет. Произойдет это не сразу, вашему мозгу понадобится время, чтобы адаптироваться.

---

На предыдущую страницу — [Почему именно буковки?](https://www.notion.so/a2fca2b952414a3fb4e03ac280a12c78) | На следующую страницу — [Личный бренд](https://www.notion.so/0e8518389a5f43f296ece2d4bfb75049)