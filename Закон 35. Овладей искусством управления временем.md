**Закон 35. Овладей искусством управления временем**

Спешка выдает неумение распоряжаться собой и своим временем. Сохраняйте спокойствие, научитесь выбирать правильный момент, улавливать дух времени. Терпение — главное требование в искусстве управления временем. Контролируя свои эмоциональные реакции, можно замедлить время, принимать решения более взвешенно. Нельзя добиться даже малой власти, если пускать все на самотек. Необходимо научиться управлять временем, если вы не хотите стать его жертвой. Оборотной стороны для этого закона нет.

#48ЗаконовВласти