---

Tags: 
aliases: 
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

Тип: конспект
Описание: -

День: 19 # Не трогать!
Месяц: 07 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-07-19

---
# [[Кодировка типа данных изображения - OpenCV]]


Для указания типа данных используются несколько определений, построенных по следующей схеме:

CV_\[<mark style="background: #FF5582A6;">Количество бит на элемент</mark> ]\[<mark style="background: #ADCCFFA6;">Знаковый или беззнаковый(S/U)</mark> ]\[<mark style="background: #BBFABBA6;">Префикс типа</mark> ]C\[<mark style="background: #FFF3A3A6;">Номер канала</mark> ].

Например, **CV_8UC3** означает, что мы используем типы `unsigned char` длиной **8 бит**, и каждый пиксель имеет **три** таких типа для формирования трех каналов.

## Базовые типы данных в OpenCV

`schar` - знаковое 1-байтовое целое число
`uchar` - беззнаковое 1-байтовое целое число
`short` - знаковое 2-байтовое целое число
`ushort` - беззнаковое 2-байтовое целое число
`int` - знаковое 4-байтовое целое число
`uint` - беззнаковое 4-байтовое целое число
`int64` - знаковое 8-байтовое целое число
`uint64` - беззнаковое 8-байтовое целое число

## Смотрите также:

```dataview
LIST 
FROM !outgoing([[]])
WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
```
> [!question]- Соурслист
> 1. 


---
#КомпьютерноеЗрение/opencv 

---

[^def]: термин
[^que]: вопрос