---
Тип: Задача
Описание: -
Исполнители: 
Проект: ВнедрениеЗнаний

Tags: Задача
regex-to-search: # "что-то" regex
regex-to-be-found: "NoRgxTBFKey" # "что-то" regex

День: 11 # Не трогать!
Месяц: 04 # Не трогать!
Год: 2023 # Не трогать!
дата_создания: 2023-04-11

дата_завершения: "2023-нет-нет"

Выполнено: нет
Результат: 
---

# [[JSON Python]]

> [!danger] Обязательный чеклист
> - [x] Получение одной ссылки
> - [x] Расставить теги 


> [!example]+  Чеклист знания
> - Получение источников и ссылок
> 	- [ ] Получение ссылок на разные источники
> - Конспектирование
> 	- [ ] Достаточное для использования конспектирование
> 	- [ ] Полное конспектирование
> - Создание справок
> 	- [ ] Справки практического применения
> 	- [ ] Список терминов
> 	- [ ] Определение терминов из списка терминов
> 	- [ ] Список связанных тем(если нет внутри текста)
> - Внедрение в базу знаний
> 	- [ ] Создание связей с оригинальной страницей (этой)
> 	- [ ] Разбиение на подтемы
> 	- [ ] Атомизация - (по необходимости) разбиение конспекта по подтемам на страницы
> - [ ] Внедрение в практику
> 

<mark style="background: #FF5582A6;">**Содержание потом вырезается, все содержимое конспекта удаляется и содержание вставляется по шаблону входящего конспекта.**</mark> 

# Содержание 

Это дефолтная библиотека питона. Подключается так:

```Python
import json
```

### Сериализация в JSON объект

```Python
mydict = {"title": string, "code": integer, "data": array}

x = json.dumps(mydict) # для сериализации в JSON
# или
with open("file.json", "w") as fh:
	json.dump(x, filehandler) # dump() для записи в файл *.json*
```

### Десериализация из JSON объекта

```python
y = {}
with open("data.json", "r") as fh:
	x = json.load(fh) # load() для загрузки структуры из файла
	y = json.loads(x) # loads() для десериализации в объект Python
```

## Смотрите также:

> [!question]- Соурслист
> 1. https://sky.pro/media/modul-json-v-python/
> 2. https://www.geeksforgeeks.org/python-difference-between-json-dump-and-json-dumps/
> 3. https://www.geeksforgeeks.org/python-difference-between-json-load-and-json-loads/?ref=rp

> [!quote]- Контекст
> ```dataview
> LIST 
> FROM !outgoing([[]])
> WHERE regexmatch(this.regex-to-search,regex-to-be-found) or regexmatch(this.regex-to-search, file.name) or regexmatch(this.ключи,regex-to-be-found) or regexmatch(regex-to-search, this.regex-to-be-found)
> ```

---
#ВнедрениеЗнаний 
#Python 
#JSON 

---

[^def]: термин
[^que]: вопрос